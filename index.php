<?php
	include "lib/handWebEva.php";
	//include "lib/handVarEva.php";
	include "lib/handDatabaseEva.php";
	
	$webserver = nomserverweb();
	
	foreach ($_REQUEST as $param => $valor)
	{
		$_REQUEST[$param] = protegevars($valor);
	}
	
	$fail = true;
	
	if (isset($_REQUEST["username"]) and isset($_REQUEST["password"]))
	{
		$usuario = $_REQUEST["username"];
		$clave = $_REQUEST["password"];
		
		$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $usuario);
		if (mysqli_num_rows($resultqusuarioeva) > 0)
		{
			$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
			if ($clave == $regusuarioeva["password"])
			{
				session_start();
				$_SESSION['username'] = $usuario;
				$_SESSION['password'] = $clave;
				$fail = false;
				$currentuser = $_SESSION["username"];
			}
			else
			{
				mensaje("Contraseña incorrecta");
			}
		}
		else
		{
			mensaje("Usuario inexistente");
		}
		
	}
	else
	{
		define ("user", false);
	}
	
	
?>
<html>
	<head>
		<title>
			Eva - LOGIN
		</title>
		<link href="CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
	</head>
	<body>
		<?php
			if ($fail == true)
			{
				cabezallogin("LOGIN");
				?>
				<form action = "index.php" method="post">
					<hr />
					<table>
						<tr><th align="left">Nombre de usuario</th>	<td>:</td>	<td align="left"><input type="text" name="username" placeholder="Nombre de usuario" /></td></tr>
						<tr><th align="left">Contrase&ntilde;a</th>		<td>:</td>	<td align="left"><input type="password" name="password" placeholder="****************" /></td></tr>
					</table>
					<hr />
					<table>
						<tr><td align="left"><input type="submit" value="Entrar" /></td></tr>
					</table>
				</form>
				<?php
			}
			else
			{
				//cabezal("MENU PRINCIPAL");
				
				//consulta tipo de usuario
				$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
				$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
				$usuarioevareg = $regusuarioeva["nomusuarioeva"];
				$rutpersonareg = $regusuarioeva["rutusuarioeva"];
				$codtipousuarioeva = $regusuarioeva["codtipousuarioeva"];
				$resultqtipousuarioeva = consultatodo("tipousuarioeva", "codtipousuarioeva", $codtipousuarioeva);
				$regtipousuarioeva = mysqli_fetch_assoc($resultqtipousuarioeva);
				$nomtipousuarioeva = $regtipousuarioeva["nomtipousuarioeva"];
				$resultqpersona = consultatodo("persona", "rutpersona", $rutpersonareg);
				$regpersona = mysqli_fetch_assoc($resultqpersona);
				$nompersona = $regpersona["nompersona"];
				$appaterno = $regpersona["appaterno"];
				$apmaterno = $regpersona["apmaterno"];
				
				if ($codtipousuarioeva == 1)
				{
					cabezal("MENU PRINCIPAL");
				}
				
				if ($codtipousuarioeva == 3)
				{
					cabezalevaluador("MENU PRINCIPAL");
				}
				echo "<p><b>MEN&Uacute; PRINCIPAL</b></p>";
				echo "<hr />";
				?>
				<div id="botonup">
					<table>
						<tr>
							<td width='600' align='left' valign='top'>
								<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
							</td>
						</tr>
					</table>
				</div>
				<hr />
				<?php
				echo "<p><b>Usuario actual</b></p>";
				echo "<table>";
				echo "<tr>";
				echo "<td>Username</td><td align='center'>:</td><td>".$usuarioevareg."</td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td>Nombre</td><td align='center'>:</td><td>".$nompersona." ".$appaterno." ".$apmaterno."</td>";
				echo "</tr>";
				echo "<td>Nivel de acceso</td><td align='center'>:</td><td>".$nomtipousuarioeva."</td>";
				echo "</tr>";
				echo "</table>";
				?>
				<hr />
				<div id="botonbottom">
					<table>
						<tr>
							<td width='600' align='right' valign='top'>
								<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='images/logout.jpg' width='30' height='30' title='Salir'></a>
							</td>
						</tr>
					</table>
				</div>
				<hr />
				<?php
			}
			pie();
		?>
	</body>
</html>