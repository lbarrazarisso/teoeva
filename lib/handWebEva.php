<?php

	function nomserverweb()
	{
		$nomserver = $_SERVER["SERVER_NAME"];
		$port = $_SERVER["SERVER_PORT"];
		if ($port != "80")
		{
			$nomserver = $nomserver.":".$port;
		}
		return $nomserver;
	}
	
	//CONSTRUCTOR MENU
	
	function hazmenu()
	{
		$webserver = nomserverweb();
		?>
		<div id="menu">
			<ul class="menu">
				<li>
					<a href="http://<?php echo $webserver;?>/eva/procesos/mantenedores/indexmantenestrucdepa.php">Estructura Departamental</a>
					<ul>
						<li><a href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/unidadestrategica/indexmantenuniestrat.php'>Definici&oacute;n Unidades Estrat&eacute;gicas</a></li>
						<li><a href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/area/indexmantenarea.php'>Definici&oacute;n de &Aacute;reas</a></li>
					</ul>
				</li>
				<li>
					<a href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/indexmantencargoascom.php'>Cargos</a>
					<ul>
						<li>
							<a href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/cargo/indexmantencargo.php'>Administrar Cargos</a>
						</li>
						<li>
							<a href='http://<?php echo $webserver;?>/eva/procesos/asignadores/competenciaxcargo/asdescompetenciaxcargo.php'>Asignar Competencias a un Cargo</a>
						</li>
					</ul>
				</li>

				<li>
					<a href="http://<?php echo $webserver;?>/eva/procesos/mantenedores/indexmantenpersonaobj.php">Personas</a>
					<ul>
						<li><a href="http://<?php echo $webserver;?>/eva/procesos/mantenedores/persona/indexmantenpersona.php">Administrar Personas</a></li>
						<li><a href='http://<?php echo $webserver;?>/eva/procesos/asignadores/objetivoxpersona/asdesobjetivoxpersona.php'>Asignar Objetivos Generales a una Persona</a></li>
						<li><a href='http://<?php echo $webserver;?>/eva/procesos/asignadores/objetivoespecificoxpersona/asdesobjetivoespecificoxpersona.php'>Asignar Objetivos Espec&iacute;ficos a una Persona</a></li>						
					</ul>
				</li>
				<li>
					<a href="http://<?php echo $webserver;?>/eva/procesos/evaluacion/indexevaluacion.php">Evaluaci&oacute;n</a>
					<ul>
						<li><a href="http://<?php echo $webserver;?>/eva/procesos/evaluacion/evdesempeno.php">Evaluar una persona</a></li>
						<li><a href="http://<?php echo $webserver;?>/eva/procesos/mantenedores/evaluador/indexmantenevaluador.php">Administrar Evaluadores</a></li>
						<li>
							<a href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/indexmantenobjetivotipo.php'>Objetivos Generales</a>
							<ul>
								<li>
									<a href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/objetivo/indexmantenobjetivo.php'>Administrar Objetivos Generales</a>
								</li>
								<li>
									<a href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/tipoobjetivo/indexmantentipoobjetivo.php'>Administrar Tipos de Objetivos</a>
								</li>
							</ul>
						</li>
						<li>
							<a href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/indexmantencompettipo.php'>Competencias</a>
							<ul>
								<li>
									<a href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/competencia/indexmantencompetencia.php'>Administrar Competencias</a>
								</li>
								<li>
									<a href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/tipocompetencia/indexmantentipocompetencia.php'>Administrar Tipos de Competencias</a>
								</li>
							</ul>
						</li>
						<li><a href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/escalaevaluacion/indexmantenescalaevaluacion.php'>Administrar Escala de Evaluaci&oacute;n</a></li>
					</ul>
				</li>
				<li>
					<a href='http://<?php echo $webserver;?>/eva/procesos/reportes/indexreportes.php'>Reportes</a>
				</li>
				<li>
					<a href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/indexmantenherramientas.php'>Herramientas</a>
					<ul>
						<li>
							<a href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/usuarioeva/indexmantenusuarioeva.php'>Administrar Usuarios</a>
						</li>
						<li>
							<a href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/tipousuarioeva/indexmantentipousuarioeva.php'>Administrar Tipos de Usuario</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<?php
	}
	
	function hazmenuevaluador()
	{
		$webserver = nomserverweb();
		?>
		<div id="menu">
			<ul class="menu">
				<li>
					<a href="http://<?php echo $webserver;?>/eva/procesos/operacion/indexadminpersona.php">Adm. de Personas</a>
					<ul>
						<!--<li><a href='http://<?php echo $webserver;?>/eva/procesos/asignadores/objetivoxpersona/asdesobjetivoxpersona.php'>Asignar Objetivos Generales a una Persona</a></li>-->
						<li><a href="http://<?php echo $webserver;?>/eva/procesos/evaluacion/evdesempeno.php">Evaluar</a></li>
						<li><a href='http://<?php echo $webserver;?>/eva/procesos/asignadores/objetivoespecificoxpersona/asdesobjetivoespecificoxpersona.php'>Asignar Objetivos Espec&iacute;ficos a una Persona</a></li>
					</ul>
				</li>
				<li>
					<a href='http://<?php echo $webserver;?>/eva/procesos/reportes/indexreportes.php'>Reportes</a>
				</li>
			</ul>
		</div>
		<?php
	}
	
	function cabezalevaluador($titulo)
	{
		$webserver = nomserverweb();
		?>
			<div id="canvas-global">
				<div id="canvas">
					<div id="cabezal">
						<div id="logo"><a href='http://<?php echo $webserver;?>/eva/main.php'><?=$titulo?></a></div>
					</div>
					<table width="100%" border="0">
						<tr>
							<td width="160" valign="top" id="menu-izq">
								<?php hazmenuevaluador(); ?>
							</td>
							<td valign="top" id="content">
		<?php
	}
	
	function cabezal($titulo)
	{
		$webserver = nomserverweb();
		?>
			<div id="canvas-global">
				<div id="canvas">
					<div id="cabezal">
						<div id="logo"><a href='http://<?php echo $webserver;?>/eva/main.php'><?=$titulo?></a></div>
					</div>
					<table width="100%" border="0">
						<tr>
							<td width="160" valign="top" id="menu-izq">
								<?php hazmenu(); ?>
							</td>
							<td valign="top" id="content">
		<?php
	}
	
	function nomenu()
	{
		$webserver = nomserverweb();
		?><div id="menu"></div><?php
	}
	
	function cabezallogin($titulo)
	{
		$webserver = nomserverweb();
		?>
			<div id="canvas-global">
				<div id="canvas">
					<div id="cabezal">
						<div id="logo"><a href='http://<?php echo $webserver;?>/eva/main.php'><?=$titulo?></a></div>
					</div>
					<table width="100%" border="0">
						<tr>
							<td width="160" valign="top" id="menu-izq">
								<?php nomenu(); ?>
							</td>
							<td valign="top" id="content">
		<?php
	}
	
	
	function pie()
	{
		?>
							</td>
						</tr>
					</table>
				</div>
			</div>
		<?php
	}
?>