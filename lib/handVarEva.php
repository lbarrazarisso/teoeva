<?php
	
	//Valida si una persona es evaluable, con antiguedad de 6 meses.
	
	function evaluable($fechaing, $activo)
	{
		if ($activo == 'S')
		{
			//obtiene fecha actual
			$diaact = date("d");
			$mesact = date("m");
			$agnact = date("Y");
			
			//descompone fecha de ingreso en dia, mes y año.
			$arrayfechaing = explode("-", $fechaing);
			$diaing = $arrayfechaing[0];
			$mesing = $arrayfechaing[1];
			$agning = $arrayfechaing[2];
			
			$diffagn = $agnact - $agning;
			if ($diffagn > 1)
			{
				return true;
			}
			else
			{
				if ($diffagn == 1)
				{
					$diffming = 12 - $mesing;
					$summes = $diffming + $mesact;
					if ($summes > 6)
					{
						return true;
					}
					else
					{
						if ($summes == 6)
						{
							if ($diaact <= $diaing)
							{
								return true;
							}
							else
							{
								return false;
							}
						}
						else
						{
							return false;
						}
					}
				}
				else
				{
					if ($diffagn == 0)
					{
						$diffmes = $mesact - $mesing;
						if ($diffmes > 6)
						{
							return true;
						}
						else
						{
							if ($summes == 6)
							{
								if ($diaact >= $diaing)
								{
									return true;
								}
								else
								{
									return false;
								}
							}
							else
							{
								return false;
							}
						}
					}
				}
			}
		}
		else
		{
			return false;
		}
	}
?>