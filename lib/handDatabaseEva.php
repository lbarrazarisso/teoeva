<?php

	function leeconf($comando)
	{
	
		$webRoot = $_SERVER["DOCUMENT_ROOT"];
		$pathConf = "/eva/conf/"; //INGRESAR PATH RELATIVA
		$fileTarget = "eva.conf";
		$confFile = $webRoot.$pathConf.$fileTarget;
		
		//LEER ARCHIVO LINEA POR LINEA
		$noms = fopen($confFile, "r") or exit("Archivo no existe");
		$cadena = "";
		while(!feof($noms))
		{
			$reg = fgets($noms);
			$cadena = $cadena.$reg;
			//echo $cadena."</br>";
		}
		$cadenaFin = trim($cadena, ";");
		$arrayCadena = explode(";", $cadenaFin);
		$long = count($arrayCadena);
		for ($i = 0; $i < $long; $i++)
		{
			$arrayRegistro = explode(":", $arrayCadena[$i]);
			$auxreg = $arrayRegistro[0];
			$palabra = trim($auxreg);
			switch($palabra)
			{
				case "DBSERVER":
					$aux = $arrayRegistro[1];
					$dbserver = trim($aux);
					if ($dbserver == "")
					{
						$dbserver = "localhost";
					}
				break;
				case "DB":
					$aux = $arrayRegistro[1];
					$db = trim($aux);
					if ($db == "")
					{
						$db = "deccar";
					}
				break;				
				case "USERNAME":
					$aux = $arrayRegistro[1];
					$username = trim($aux);
					if ($username == "")
					{
						$username = "root";
					}
				break;
				case "PASS":
					$aux = $arrayRegistro[1];
					$pass = trim($aux);
				break;
				default:
					echo "ARCHIVO NO VALIDO</br>";
			}
		}
		switch ($comando)
		{
			case "dbserver":
				return $dbserver;
			break;
			case "username":
				return $username;
			break;	
			case "pass":
				return $pass;
			break;
			case "db":
				return $db;
			break;
			default:
				echo "ERROR DE ARCHIVO DE CONFIGURACION";
		}
		fclose($noms);
	}
	
	function mensaje($string)
	{
		echo "<script type='text/javascript'>alert('".$string."');</script>";
	}
	
	function nomserverdb()
	{
		$nomserver = leeconf("dbserver");
		return $nomserver;
	}
	
	function nomuserdb()
	{
		$nomuser = leeconf("username");
		return $nomuser;
	}
	
	function passdb()
	{
		$passdb = leeconf("pass");
		return $passdb;
	}
	
	function nomdb()
	{
		$nomdbase = leeconf("db");
		return $nomdbase;
	}
	
	function validadb($server, $user, $pass)
	{
		$link = mysqli_connect($server, $user, $pass);
		if ($link)
		{
			return  $link;
		}
		else
		{
			return FALSE;
		}
	}

	function validatablas($link, $nomtabla, $query)
	{
		if (mysqli_query($link, $query))
		{
			echo "La tabla ".$nomtabla." fue creada exitosamente</br>";
		}
		else
		{
			echo "Error al crear la tabla ".$nomtabla."</br>";
		}
	}
	
	function creadb()
	{
		echo "==========================================================</br>";
		echo "*               Conectando con el servidor               *</br>";
		echo "==========================================================</br>";
		
		// OBTIENE LOS DATOS DEL SERVER A TRAVES DE LAS FUNCIONES DESCRITAS
		$server = nomserverdb();
		$user = nomuserdb();
		$pass = passdb();
		
		// LLAMA A FUNCION PARA CONECTAR A LA BD Y VALIDAR LA CONEXION
		$link = validadb($server, $user, $pass);
		
		if ($link)
		{
			// SI LA CONEXION ES VALIDA, CREA LA BASE DE DATOS
			$nomdbase = nomdb();
						
			// CONSTRUCCION DE LA QUERY
			$query = "CREATE DATABASE ".$nomdbase;
			
			// EJECUCION DE QUERY
			if (mysqli_query($link, $query))
			{
				echo "La base de datos ".$nomdbase." fue creada satisfactoriamente</br>";
				
				echo "==========================================================</br>";
				echo "*              Creando Tablas                            *</br>";
				echo "==========================================================</br>";
				
				// ARMADO DE QUERYS
				
				/*
				===================================================================== 
				SISTEMA EVA

				SQL TRANSACCIONAL, PARA CREACION DE BASE DE DATOS.
				=====================================================================
				*/
				$querytipoobjetivo = "CREATE TABLE tipoobjetivo (codtipoobjetivo mediumint(8) unsigned not null auto_increment, nomtipoobjetivo varchar(50) not null, PRIMARY KEY (codtipoobjetivo));";      
				$queryobjetivo = "CREATE TABLE objetivo (codobjetivo mediumint(8) unsigned not null, descobjetivo varchar(500) not null, agnoobjetivo mediumint(8) unsigned not null, codtipoobjetivo mediumint(8) unsigned not null, PRIMARY KEY (codobjetivo));";
				$querypersona = "CREATE TABLE persona (nompersona varchar(50) not null, appaterno varchar(30) not null, apmaterno varchar(30) not null, rutpersona varchar(12) not null, fechaingreso varchar(10) not null, codcargo mediumint(8) unsigned not null, codarea mediumint(8) unsigned not null, activo varchar(2) not null, PRIMARY KEY (rutpersona));";
				$queryevaluacionxobjetivoxpersona = "CREATE TABLE evaluacionxobjetivoxpersona (codevaluacionxobjetivoxpersona varchar(50) not null, codevaluacion varchar(50) not null, codobjetivoxpersona varchar(50) not null, codevaluacionobtenida mediumint(8) unsigned not null, PRIMARY KEY (codevaluacionxobjetivoxpersona));"; 
				$queryevaluacion = "CREATE TABLE evaluacion (codevaluacion varchar(50) not null, rutevaluador varchar(12) not null, rutpersona varchar(12) not null, agnoevaluacion mediumint(8) unsigned not null, pondfinalobjetivo mediumint(8) unsigned not null, pondfinalcompetencia mediumint(8) unsigned not null, pondfinalagno mediumint(8) unsigned not null, comentevaluado varchar(500) not null, comentevaluador varchar(500) not null, PRIMARY KEY (codevaluacion));";
				$querycursossolic = "CREATE TABLE cursossolic (codcursossolic mediumint(8) unsigned not null auto_increment, nomcursossolic varchar(50), fechacursossolic varchar(10), prioridadcursossolic varchar(50), codevaluacion varchar(50) not null, PRIMARY KEY (codcursossolic));";
				$queryevaluador = "CREATE TABLE evaluador (rutevaluador varchar(12) not null, nomevaluador varchar(50) not null, appaterno varchar(30) not null, apmaterno varchar(30) not null, PRIMARY KEY (rutevaluador));";
				$queryarea = "CREATE TABLE area (codarea mediumint(8) unsigned not null auto_increment, nomarea varchar(50) not null, rutevaluador varchar(12) not null, coduniestrat mediumint(8) unsigned not null, PRIMARY KEY (codarea));";
				$queryuniestrat = "CREATE TABLE uniestrat(coduniestrat mediumint(8) unsigned default null auto_increment, nomuniestrat varchar(50) not null, PRIMARY KEY (coduniestrat));";
				$querycargo = "CREATE TABLE cargo (codcargo mediumint(8) unsigned not null auto_increment, nomcargo varchar(50) not null, desccargo varchar(500) not null, PRIMARY KEY (codcargo));"; 
				$queryevaluacionxcompetenciaxcargo = "CREATE TABLE evaluacionxcompetenciaxcargo (codevaluacionxcompetenciaxcargo varchar(50) not null, codevaluacion varchar(50) not null, codcompetenciaxcargo varchar(50) not null, codevaluacionobtenida mediumint(8) unsigned not null, PRIMARY KEY (codevaluacionxcompetenciaxcargo));";
				$queryobjetivoxpersona = "CREATE TABLE objetivoxpersona (codobjetivoxpersona varchar(50) not null, codobjetivo mediumint(8) unsigned not null, rutpersona varchar(12) not null, codevaluacionesperada mediumint(8) unsigned not null, PRIMARY KEY (codobjetivoxpersona));";
				$querycompetenciaxcargo = "CREATE TABLE competenciaxcargo (codcompetenciaxcargo varchar(50) not null, codcompetencia mediumint(8) unsigned not null, codcargo mediumint(8) unsigned not null, codevaluacionesperada mediumint(8) unsigned not null, PRIMARY KEY (codcompetenciaxcargo));";
				$queryusuarioeva = "CREATE TABLE usuarioeva (rutusuarioeva varchar(12) not null, nomusuarioeva varchar(50) not null, password varchar(30), codtipousuarioeva mediumint(8) unsigned not null, PRIMARY KEY (rutusuarioeva));";
				$querytipousuarioeva = "CREATE TABLE tipousuarioeva (codtipousuarioeva mediumint(8) unsigned not null, nomtipousuarioeva varchar(50) not null, PRIMARY KEY (codtipousuarioeva));";
				$queryescalaevaluacion = "CREATE TABLE escalaevaluacion (codescalaevaluacion mediumint(8) unsigned not null, percentescalaevaluacion mediumint(8) unsigned not null, nomescalaevaluacion varchar(50) not null, PRIMARY KEY (codescalaevaluacion));";
				$querytipocompetencia = "CREATE TABLE tipocompetencia (codtipocompetencia mediumint(8) unsigned not null, nomtipocompetencia varchar(50) not null, PRIMARY KEY (codtipocompetencia));";
				$querycompetencia = "CREATE TABLE competencia (codcompetencia mediumint(8) unsigned not null auto_increment, desccompetencia varchar(500) not null, agnocompetencia mediumint(8) unsigned not null, codtipocompetencia mediumint(8) unsigned not null, PRIMARY KEY (codcompetencia));";
				
				//TABLAS TEMPORALES
				
				$querytempevaluacion = "CREATE TABLE tempevaluacion (codevaluacion varchar(50) not null, rutevaluador varchar(12) not null, rutpersona varchar(12) not null, agnoevaluacion mediumint(8) unsigned not null, pondfinalobjetivo mediumint(8) unsigned not null, pondfinalcompetencia mediumint(8) unsigned not null, pondfinalagno mediumint(8) unsigned not null, comentevaluado varchar(500) not null, comentevaluador varchar(500) not null, PRIMARY KEY (codevaluacion));";
				$querytempevaluacionxobjetivoxpersona = "CREATE TABLE tempevaluacionxobjetivoxpersona (codevaluacionxobjetivoxpersona varchar(50) not null, codevaluacion varchar(50) not null, codobjetivoxpersona varchar(50) not null, codevaluacionobtenida mediumint(8) unsigned not null, PRIMARY KEY (codevaluacionxobjetivoxpersona));"; 
				$querytempevaluacionxcompetenciaxcargo = "CREATE TABLE tempevaluacionxcompetenciaxcargo (codevaluacionxcompetenciaxcargo varchar(50) not null, codevaluacion varchar(50) not null, codcompetenciaxcargo varchar(50) not null, codevaluacionobtenida mediumint(8) unsigned not null, PRIMARY KEY (codevaluacionxcompetenciaxcargo));";
				
				// SELECCIONA BASE DE DATOS
			
				mysqli_select_db($link, $nomdbase);
				
				// CREACION TABLAS
				
				//TIPO DE OBJETIVO
				validatablas($link, "tipoobjetivo", $querytipoobjetivo);
				
				//OBJETIVO
				validatablas($link, "objetivo", $queryobjetivo);
				
				//PERSONA
				validatablas($link, "persona", $querypersona);
				
				// EVALUACION X OBJETIVO X PERSONA
				validatablas($link, "evaluacionxobjetivoxpersona", $queryevaluacionxobjetivoxpersona);
				
				//EVALUACIÓN
				validatablas($link, "evaluacion", $queryevaluacion);
				
				//CURSOS SOLICITADOS
				validatablas($link, "cursossolic", $querycursossolic);

				//EVALUADOR
				validatablas($link, "evaluador", $queryevaluador);
				
				//AREA
				validatablas($link, "area", $queryarea);
				
				//UNIDAD ESTRATEGICA
				validatablas($link, "uniestrat", $queryuniestrat);
				
				//CARGO
				validatablas($link, "cargo", $querycargo);
				
				//EVALUACIONXCOMPETENCIAXCARGO
				validatablas($link, "evaluacionxcompetenciaxcargo", $queryevaluacionxcompetenciaxcargo);
				
				//COMPETENCIAXCARGO
				validatablas($link, "competenciaxcargo", $querycompetenciaxcargo);
				
				//OBJETIVOXPERSONA
				validatablas($link, "objetivoxpersona", $queryobjetivoxpersona);
				
				//USUARIOEVA
				validatablas($link, "usuarioeva", $queryusuarioeva);
				
				//TIPOUSUARIOEVA
				validatablas($link, "tipousuarioeva", $querytipousuarioeva);
				
				//ESCALAEVALUACION
				validatablas($link, "escalaevaluacion", $queryescalaevaluacion);
				
				//TIPO DE COMPETENCIA
				validatablas($link, "tipocompetencia", $querytipocompetencia);
				
				//COMPETENCIA
				validatablas($link, "competencia", $querycompetencia);
				
				//TABLAS TEMPORALES
				// TEMP EVALUACION X OBJETIVO X PERSONA
				validatablas($link, "tempevaluacionxobjetivoxpersona", $querytempevaluacionxobjetivoxpersona);
				
				//TEMP EVALUACIÓN
				validatablas($link, "tempevaluacion", $querytempevaluacion);
				
				//TEMP EVALUACION X COMPETENCIA X CARGO
				validatablas($link, "tempevaluacionxcompetenciaxcargo", $querytempevaluacionxcompetenciaxcargo);
			}
			else
			{
				echo "Error al crear la base de datos: " . mysql_error() . "</br>";
			}
		}	
	}
	
	//FUNCIONES DE MANIPULACION DE LA BASE DE DATOS
	
	//INSERTAR
	
	function insertaregistro($nomtabla, $valores)
	{
	
		// OBTIENE LOS DATOS DEL SERVER A TRAVES DE LAS FUNCIONES DESCRITAS
		$server = nomserverdb();
		$user = nomuserdb();
		$pass = passdb();
		
		// LLAMA A FUNCION PARA CONECTAR A LA BD Y VALIDAR LA CONEXION
		$link = validadb($server, $user, $pass);
		
		if ($link)
		{
			// SI LA CONEXION ES VALIDA, SELECCIONA LA BASE DE DATOS
			$nomdbase = nomdb();
			mysqli_select_db($link, $nomdbase);
			
			// OBTIENE LOS CAMPOS DE LA TABLA $NOMTABLA
			$querycampos = "DESCRIBE ".$nomtabla.";";
			$resultcampos = mysqli_query($link, $querycampos);
			
			// CONSTRUCCION DE LA QUERY
			$cadenaqueryinsert = "INSERT INTO ".$nomtabla." (";
			while ($regcampos = mysqli_fetch_assoc($resultcampos))
			{
				$cadenaqueryinsert = $cadenaqueryinsert.$regcampos["Field"].", ";
			}
			$cadenaqueryinsert = trim($cadenaqueryinsert, ", ");
			$cadenaqueryinsert = $cadenaqueryinsert.") VALUES (".$valores.");";
			
			// VALIDA SI LA QUERY SE EJECUTO Y SI LOS DATOS FUERON INGRESADOS
			if (mysqli_query($link, $cadenaqueryinsert))
			{
				echo "";
				//echo "<script type='text/javascript'>alert('Los datos fueron ingresados correctamente');</script>";
			}
			else
			{
				echo "<script type='text/javascript'>alert('Error al ingresar los datos');</script>";
			}
			
			// CIERRA LA CONEXION
			mysqli_close($link);
		}
		else
		{
			// SI LA CONEXION NO ES VALIDA, ARROJA MENSAJE DE ERROR.
			echo "No se pudo conectar al servidor".$server."</br>";
		}
	}
	
	//MODIFICAR
	
	function modificaregistro($nomtabla, $campomod, $valor, $campobus, $variable)
	{
		// OBTIENE LOS DATOS DEL SERVER A TRAVES DE LAS FUNCIONES DESCRITAS
		$server = nomserverdb();
		$user = nomuserdb();
		$pass = passdb();
		
		// LLAMA A FUNCION PARA CONECTAR A LA BD Y VALIDAR LA CONEXION
		$link = validadb($server, $user, $pass);
		
		if ($link)
		{
			// SI LA CONEXION ES VALIDA, SELECCIONA LA BASE DE DATOS
			$nomdbase = nomdb();
			mysqli_select_db($link, $nomdbase);
			
			// CONSTRUCCION DE LA QUERY
			$cadenaquerymod = "UPDATE ".$nomtabla." SET ".$campomod."='".$valor."' WHERE ".$campobus."='".$variable."';";
			
			// VALIDA SI LA QUERY SE EJECUTO Y SI LOS DATOS FUERON INGRESADOS
			if (mysqli_query($link, $cadenaquerymod))
			{
				$k = "ok";
			}
			else
			{
				echo "<script type='text/javascript'>alert('Error al modificar el campo ".$campomod." de la tabla ".$nomtabla." ');</script>";
				//echo "Error al modificar el campo ".$campomod."de la tabla".$nomtabla."</br>";
			}
			mysqli_close($link);
		}
		else
		{
			// SI LA CONEXION NO ES VALIDA, ARROJA MENSAJE DE ERROR.
			echo "No se pudo conectar al servidor".$server."</br>";	
		}
	}
	
	//ELIMINAR
	
	function eliminaregistro($nomtabla, $campobus, $variable)
	{
		// OBTIENE LOS DATOS DEL SERVER A TRAVES DE LAS FUNCIONES DESCRITAS
		$server = nomserverdb();
		$user = nomuserdb();
		$pass = passdb();
		
		// LLAMA A FUNCION PARA CONECTAR A LA BD Y VALIDAR LA CONEXION
		$link = validadb($server, $user, $pass);
		
		if ($link)
		{
			// SI LA CONEXION ES VALIDA, SELECCIONA LA BASE DE DATOS
			$nomdbase = nomdb();
			mysqli_select_db($link, $nomdbase);
			
			// CONSTRUCCION DE LA QUERY
			$queryelimina = "DELETE FROM ".$nomtabla." WHERE ".$campobus."='".$variable."'";

			// VALIDA SI LA QUERY SE EJECUTO Y SI LOS DATOS FUERON INGRESADOS
			if (mysqli_query($link, $queryelimina))
			{
				//mensaje("El registro fue eliminado correctamente desde la tabla ".$nomtabla);
				//echo "<script type='text/javascript'>alert('El registro fue eliminado correctamente');</script>";
				//echo "El registro fue eliminado correctamente desde la tabla ".$nomtabla."</br>";
			}
			else
			{
				echo "<script type='text/javascript'>alert('Error al eliminar el registro de la tabla".$nomtabla."');</script>";
				//echo "Error al eliminar el registro de la tabla".$nomtabla."</br>";
			}
			mysqli_close($link);
		}
		else
		{
			// SI LA CONEXION NO ES VALIDA, ARROJA MENSAJE DE ERROR.
			echo "No se pudo conectar al servidor".$server."</br>";	
		}
	}
	
	//FUNCIONES DE CONSULTA
	
	//PARA CONSTRUIR SELECT
	
	function llenacombo($nomtabla)
	{
		// OBTIENE LOS DATOS DEL SERVER A TRAVES DE LAS FUNCIONES DESCRITAS
		$server = nomserverdb();
		$user = nomuserdb();
		$pass = passdb();
		
		// LLAMA A FUNCION PARA CONECTAR A LA BD Y VALIDAR LA CONEXION
		$link = validadb($server, $user, $pass);
		
		if ($link)
		{
			// SI LA CONEXION ES VALIDA, SELECCIONA LA BASE DE DATOS
			$nomdbase = nomdb();
			mysqli_select_db($link, $nomdbase);
			
			// CONSTRUCCION DE LA QUERY
			$queryconsultatabla = "SELECT * FROM ".$nomtabla.";";
			
			// EJECUCION DE QUERY
			$resultq = mysqli_query($link, $queryconsultatabla);
			
			// RETORNA RESULTADO PARA FORMATEO DESDE PROGRAMA CON FETCHASSOC
			return $resultq;
		}
		else
		{
			// SI LA CONEXION NO ES VALIDA, ARROJA MENSAJE DE ERROR.
			echo "No se pudo conectar al servidor".$server."</br>";
		}
		mysqli_close($link);
	}

	//BUSCA UN REGISTRO.
	
	function consultatodo($nomtabla, $nomcampo, $variable)
	{
		// OBTIENE LOS DATOS DEL SERVER A TRAVES DE LAS FUNCIONES DESCRITAS
		$server = nomserverdb();
		$user = nomuserdb();
		$pass = passdb();
		
		// LLAMA A FUNCION PARA CONECTAR A LA BD Y VALIDAR LA CONEXION
		$link = validadb($server, $user, $pass);
		
		if ($link)
		{
			// SI LA CONEXION ES VALIDA, SELECCIONA LA BASE DE DATOS
			$nomdbase = nomdb();
			mysqli_select_db($link, $nomdbase);
			
			// CONSTRUCCION DE LA QUERY
			$queryconsultatabla = "SELECT * FROM ".$nomtabla." WHERE ".$nomcampo."='".$variable."'";
			
			// EJECUCION DE QUERY
			$resultq = mysqli_query($link, $queryconsultatabla);
			
			// RETORNA RESULTADO PARA FORMATEO DESDE PROGRAMA CON FETCHASSOC
			return $resultq;
		}
		else
		{
			// SI LA CONEXION NO ES VALIDA, ARROJA MENSAJE DE ERROR.
			echo "No se pudo conectar al servidor".$server."</br>";
		}
		mysqli_close($link);
	}
	
	function cuentadup($nomtabla, $nomcampo)
	{
		// OBTIENE LOS DATOS DEL SERVER A TRAVES DE LAS FUNCIONES DESCRITAS
		$server = nomserverdb();
		$user = nomuserdb();
		$pass = passdb();
		
		// LLAMA A FUNCION PARA CONECTAR A LA BD Y VALIDAR LA CONEXION
		$link = validadb($server, $user, $pass);
		
		if ($link)
		{
			// SI LA CONEXION ES VALIDA, SELECCIONA LA BASE DE DATOS
			$nomdbase = nomdb();
			mysqli_select_db($link, $nomdbase);
			
			// CONSTRUCCION DE LA QUERY
			$queryconsultatabla = "SELECT ".$nomcampo." FROM ".$nomtabla." GROUP BY ".$nomcampo." HAVING COUNT(".$nomcampo.") > 0";
			
			// EJECUCION DE QUERY
			$resultq = mysqli_query($link, $queryconsultatabla);
			
			// RETORNA RESULTADO PARA FORMATEO DESDE PROGRAMA CON FETCHASSOC
			return $resultq;
		}
		else
		{
			// SI LA CONEXION NO ES VALIDA, ARROJA MENSAJE DE ERROR.
			echo "No se pudo conectar al servidor".$server."</br>";
		}
		mysqli_close($link);
	}
	
	/*===================================================================*/
	/*VALIDADORES DE VARIABLES                                           */
	/*===================================================================*/
	
	function protegevars($string)
	{
		$string = addslashes($string);
		$string = htmlspecialchars($string);
		return $string;
	}
	
	
	/*VALIDADOR DE RUT*/
	
	function validarut($rut)
	{
		if (strstr($rut, "-") == false)
		{
			return false;
		}
		else
		{
			$arrayrut = explode("-", $rut);
			$rutsinguion = $arrayrut[0];
			$digver = $arrayrut[1];
			if (strstr($rutsinguion, ".") == true)
			{
				$rutsinpunto = explode(".", $rutsinguion);
				$largorut = count($rutsinpunto);
				$largoconcat = $largorut - 1;
				$contconcat = 0;
				$rutpuro = "";
				while ($contconcat <= $largoconcat)
				{
					$rutpuro = $rutpuro.$rutsinpunto[$contconcat];
					$contconcat = $contconcat + 1;
				}
				$rutsinguion = $rutpuro;
			}
			$largomulti = strlen($rutsinguion);
			$multiplicador = 2;
			$contadormulti = $largomulti - 1;
			$sumarut = 0;
			$multicelda = 0;
			while ($contadormulti >= 0)
			{
				if ($multiplicador > 7)
				{
					$multiplicador = 2;
				}
				$multicelda = $rutsinguion[$contadormulti] * $multiplicador;
				$sumarut = $sumarut + $multicelda;
				$multiplicador = $multiplicador + 1;
				$contadormulti = $contadormulti - 1;
			}
			$resto = $sumarut % 11;
			$digvervalida = 11 - $resto;
			if ($digvervalida == 11)
			{
				$digvervalida = 0;
			}
			if ($digvervalida == 10)
			{
				$digvervalida = "k";
			}
			if (($digver == "k") or ($digver == "K"))
			{
				$digver = "k";
			}
			if ($digvervalida == $digver)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	//VALIDADOR DE FECHA
	
	function validafecha($stringfecha)
	{
		if (strstr($stringfecha, "-") == false)
		{
			mensaje("Ingrese fecha separada por guion; DD-MM-AAAA");
			return false;
		}
		else
		{
			$arrayfecha = explode("-", $stringfecha);
			$diaactual = date("d");
			$mesactual = date("m");
			$agnactual = date("Y");
			$dia = $arrayfecha[0];
			$mes = $arrayfecha[1];
			$agn = $arrayfecha[2];
			if (($agn <= $agnactual) and ($agn >= 1970))
			{
				if (($mes <= 12) and ($mes >= 1))
				{
					if(($dia >= 0) and ($dia <= 31))
					{
						if (checkdate($mes, $dia, $agn) == true)
						{
							return true;
						}
						else
						{
							mensaje("Fecha Incorrecta");
							return false;
						}
					}
					else
					{
						mensaje("Dia Incorrecto");
						return false;
					}
				}
				else
				{
					mensaje("Mes Incorrecto");
					return false;
				}
			}
			else
			{
				mensaje("Agno Incorrecto");
				return false;
			}
		}
	}
	
	/*===============================================*/
	/*FUNCIONES DE DESPLIEGUE                        */
	/*===============================================*/
	

	function despliegapersona($rutpersona)
	{
		// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
		$tablaShow = "persona";
		$campoShow = "rutpersona";

		// LLAMADA A FUNCION DE CONSULTA
		$punteroconsultaShow = consultatodo($tablaShow, $campoShow, $rutpersona);
		
		// FORMATEO DE LOS RESULTADOS
		$regpersonaShow = mysqli_fetch_assoc($punteroconsultaShow);
		
		// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
		$rutpersonaShow = $regpersonaShow["rutpersona"];
		$nompersonaShow = $regpersonaShow["nompersona"];
		$appaternoShow = $regpersonaShow["appaterno"];
		$apmaternoShow = $regpersonaShow["apmaterno"];
		$fechaingresoShow = $regpersonaShow["fechaingreso"];
		
		$codcargoShow = $regpersonaShow["codcargo"];
		$resultqcargoShow = consultatodo("cargo", "codcargo", $codcargoShow);
		$regcargoShow = mysqli_fetch_assoc($resultqcargoShow);
		$nomcargoShow = $regcargoShow["nomcargo"];
		
		$codareaShow = $regpersonaShow["codarea"];
		$resultqareaShow = consultatodo("area", "codarea", $codareaShow);
		$regareaShow = mysqli_fetch_assoc($resultqareaShow);
		$nomareaShow = $regareaShow["nomarea"];
		
		$activoShow = $regpersonaShow["activo"];
		
		?>
		<font size="4"><p><b>Persona</b></p></font>
		<hr />
		<table border = "0">
			<tr><th align="left"><font size="2">Nombres</font></th>				<th><font size="2">:</font></th>	<td><font size="2"><?php echo $nompersonaShow;?></font></td></tr>
			<tr><th align="left"><font size="2">Ap. Paterno</font></th>			<th><font size="2">:</font></th>	<td><font size="2"><?php echo $appaternoShow;?></font></td></tr>
			<tr><th align="left"><font size="2">Ap. Materno</font></th>			<th><font size="2">:</font></th>	<td><font size="2"><?php echo $apmaternoShow;?></font></td></tr>
			<tr><th align="left"><font size="2">RUT</font></th>					<th><font size="2">:</font></th>	<td><font size="2"><?php echo $rutpersonaShow;?></font></td></tr>
			<tr><th align="left"><font size="2">F. de Ingreso</font></th>		<th><font size="2">:</font></th> 	<td><font size="2"><?php echo $fechaingresoShow;?></font></td></tr>
			<tr><th align="left"><font size="2">Cargo</font></th>				<th><font size="2">:</font></th> 	<td><font size="2"><?php echo $nomcargoShow;?></font></td></tr>
			<tr><th align="left"><font size="2">&Aacute;rea</font></th>				<th><font size="2">:</font></th>	<td><font size="2"><?php echo $nomareaShow;?></font></td></tr>
			<tr><th align="left"><font size="2">Activo?</font></th>				<th><font size="2">:</font></th>	<td><font size="2"><?php echo $activoShow;?></font></td></tr>
		</table>
		<?php
	}
	
	function despliegaevaluador($rutevaluador)
	{
		// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
		$tablaShow = "evaluador";
		$campoShow = "rutevaluador";

		// LLAMADA A FUNCION DE CONSULTA
		$punteroconsultaShow = consultatodo($tablaShow, $campoShow, $rutevaluador);
		
		// FORMATEO DE LOS RESULTADOS
		$regevaluadorShow = mysqli_fetch_assoc($punteroconsultaShow);
		
		// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES

		$nomevaluadorShow = $regevaluadorShow["nomevaluador"];
		$appaternoShow = $regevaluadorShow["appaterno"];
		$apmaternoShow = $regevaluadorShow["apmaterno"];
		$rutevaluadorShow = $regevaluadorShow["rutevaluador"];
		
		?>
		<font size="4"><p><b>Evaluador</b></p></font>
		<hr />
		<table border = "0">
			<tr><th align="left"><font size="2">Nombres</font></th>			<th><font size="2">:</font></th>	<td><font size="2"><?php echo $nomevaluadorShow;?></font></td></tr>
			<tr><th align="left"><font size="2">Ap. Paterno</font></th>		<th><font size="2">:</font></th>	<td><font size="2"><?php echo $appaternoShow;?></font></td></tr>
			<tr><th align="left"><font size="2">Ap. Materno</font></th>		<th><font size="2">:</font></th>	<td><font size="2"><?php echo $apmaternoShow;?></font></td></tr>
			<tr><th align="left"><font size="2">RUT</font></th>				<th><font size="2">:</font></th>	<td><font size="2"><?php echo $rutevaluadorShow;?></font></td></tr>
		</table>
		<?php
	}

	function despliegaarea($codarea)
	{
		// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
		$tablaShow = "area";
		$campoShow = "codarea";

		// LLAMADA A FUNCION DE CONSULTA
		$punteroconsultaShow = consultatodo($tablaShow, $campoShow, $codarea);
		
		// FORMATEO DE LOS RESULTADOS
		$regareaShow = mysqli_fetch_assoc($punteroconsultaShow);
		
		// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
		
		$codareaShow = $regareaShow["codarea"];
		$nomareaShow = $regareaShow["nomarea"];
		$rutevaluadorShow = $regareaShow["rutevaluador"];
		
		$resultqevaluadorShow = consultatodo("evaluador", "rutevaluador", $rutevaluadorShow);
		$regevaluadorShow = mysqli_fetch_assoc($resultqevaluadorShow);
		$nomevaluadorShow = $regevaluadorShow["nomevaluador"]." ".$regevaluadorShow["appaterno"]." ".$regevaluadorShow["apmaterno"];
		
		$coduniestratShow = $regareaShow["coduniestrat"];
		$resultquniestratShow = consultatodo("uniestrat", "coduniestrat", $coduniestratShow);
		$reguniestratShow = mysqli_fetch_assoc($resultquniestratShow);
		$nomuniestratShow = $reguniestratShow["nomuniestrat"];
		
		?>
		<font size="4"><p><b>&Aacute;rea</b></p></font>
		<hr />
		<table border = "0">
			<tr><th align="left">C&oacute;digo &Aacute;rea</th>		<th>:</th>	<td><?php echo $codareaShow;?></td></tr>
			<tr><th align="left">Nombre &Aacute;rea</th>		<th>:</th>	<td><?php echo $nomareaShow;?></td></tr>
			<tr><th align="left">Evaluador</th>	<th>:</th>	<td><?php echo $nomevaluadorShow;?></td></tr>
			<tr><th align="left">Unidad Estrat&eacute;gica</th><th>:</th>	<td><?php echo $nomuniestratShow;?></td></tr>
		</table>
		<?php
	}
	
	function despliegacargo($codcargo)
	{
		// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
		$tablaShow = "cargo";
		$campoShow = "codcargo";

		// LLAMADA A FUNCION DE CONSULTA
		$punteroconsultaShow = consultatodo($tablaShow, $campoShow, $codcargo);
		
		// FORMATEO DE LOS RESULTADOS
		$regcargoShow = mysqli_fetch_assoc($punteroconsultaShow);
		
		// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
		
		$codcargoShow = $regcargoShow["codcargo"];
		$nomcargoShow = $regcargoShow["nomcargo"];
		$desccargoShow = $regcargoShow["desccargo"];
				
		?>
		<font size="4"><p><b>Cargo</b></p></font>
		<hr />
		<table border = "0">
			<tr><th align="left" width="130" nowrap><font size = "2">C&oacute;digo de Cargo</font></th>	<th width="10"><font size = "2">:</font></th>	<td><font size = "2"><?php echo $codcargoShow;?></font></td></tr>
			<tr><th align="left" width="130" nowrap><font size = "2">Nombre de Cargo</font></th>	<th width="10"><font size = "2">:</font></th>	<td><font size = "2"><?php echo $nomcargoShow;?></font></td></tr>
			<tr><th align="left" width="130" nowrap><font size = "2">Desc. del Cargo</font></th>	<th width="10"><font size = "2">:</font></th>	<td><font size = "2"><?php echo $desccargoShow;?></font></td></tr>
		</table>
		<?php
	}
	
	function despliegatipocompetencia($codtipocompetencia)
	{
		// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
		$tablaShow = "tipocompetencia";
		$campoShow = "codtipocompetencia";

		// LLAMADA A FUNCION DE CONSULTA
		$punteroconsultaShow = consultatodo($tablaShow, $campoShow, $codtipocompetencia);
		
		// FORMATEO DE LOS RESULTADOS
		$regtipocompetenciaShow = mysqli_fetch_assoc($punteroconsultaShow);
		
		// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
		
		$codtipocompetenciaShow = $regtipocompetenciaShow["codtipocompetencia"];
		$nomtipocompetenciaShow = $regtipocompetenciaShow["nomtipocompetencia"];
		
		?>
		<font size="4"><p><b>Tipo de Competencia</b></p></font>
		<hr />
		<table border = "0">
			<tr><th align="left">C&oacutedigo de Tipo</th>		<th>:</th>	<td><?php echo $codtipocompetenciaShow;?></td></tr>
			<tr><th align="left">Nombre de Tipo</th>		<th>:</th>	<td><?php echo $nomtipocompetenciaShow;?></td></tr>
		</table>
		<?php
	}
	
	function despliegacompetencia($codcompetencia)
	{
		// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
		$tablaShow = "competencia";
		$campoShow = "codcompetencia";

		// LLAMADA A FUNCION DE CONSULTA
		$punteroconsultaShow = consultatodo($tablaShow, $campoShow, $codcompetencia);
		
		// FORMATEO DE LOS RESULTADOS
		$regcompetenciaShow = mysqli_fetch_assoc($punteroconsultaShow);
		
		// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
		
		$codcompetenciaShow = $regcompetenciaShow["codcompetencia"];
		$desccompetenciaShow = $regcompetenciaShow["desccompetencia"];
		$agnocompetenciaShow = $regcompetenciaShow["agnocompetencia"];
		
		$codtipocompetenciaShow = $regcompetenciaShow["codtipocompetencia"];
		$resultqtipocompetenciaShow = consultatodo("tipocompetencia", "codtipocompetencia", $codtipocompetenciaShow);
		$regtipocompetenciaShow = mysqli_fetch_assoc($resultqtipocompetenciaShow);
		$nomtipocompetenciaShow = $regtipocompetenciaShow["nomtipocompetencia"];
		
		?>
		<font size="4"><p><b>Competencia</b></p></font>
		<hr />
		<table border = "1">
			<tr><th width="200" align="left"><font size = "2">C&oacute;digo de Competencia</font></th>		<th><font size = "2">:</font></th>	<td width="300"><font size = "2"><?php echo $codcompetenciaShow;?></font></td></tr>
			<tr><th width="200" align="left"><font size = "2">Descripci&oacute;n</font></th>		<th><font size = "2">:</font></th>	<td width="300"><font size = "2"><?php echo $desccompetenciaShow;?></font></td></tr>
			<tr><th width="200" align="left"><font size = "2">A&ntilde;o</font></th>				<th><font size = "2">:</font></th>	<td width="300" align="justify"><font size = "2"><?php echo $agnocompetenciaShow;?></font></td></tr>
			<tr><th width="200" align="left"><font size = "2">Tipo de Competencia</font></th>		<th><font size = "2">:</font></th>	<td width="300"><font size = "2"><?php echo $nomtipocompetenciaShow;?></font></td></tr>
		</table>
		<?php
	}
	
	function despliegaobjetivo($codobjetivo)
	{
		// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
		$tablaShow = "objetivo";
		$campoShow = "codobjetivo";

		// LLAMADA A FUNCION DE CONSULTA
		$punteroconsultaShow = consultatodo($tablaShow, $campoShow, $codobjetivo);
		
		// FORMATEO DE LOS RESULTADOS
		$regobjetivoShow = mysqli_fetch_assoc($punteroconsultaShow);
		
		// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
		
		$codobjetivoShow = $regobjetivoShow["codobjetivo"];
		$descobjetivoShow = $regobjetivoShow["descobjetivo"];
		$agnoobjetivoShow = $regobjetivoShow["agnoobjetivo"];
		$codtipoobjetivoShow = $regobjetivoShow["codtipoobjetivo"];
		
		$resultqtipoobjetivoShow = consultatodo("tipoobjetivo", "codtipoobjetivo", $codtipoobjetivoShow);
		$regtipoobjetivoShow = mysqli_fetch_assoc($resultqtipoobjetivoShow);
		$nomtipoobjetivoShow = $regtipoobjetivoShow["nomtipoobjetivo"];
		
		?>
		<font size="4"><p><b>Objetivo</b></p></font>
		<hr />
		<table border = "1">
			<tr><th width="200" align="left"><font size = "2">C&oacute;digo de Objetivo</font></th>		<th><font size = "2">:</font></th>	<td width="300"><font size = "2"><?php echo $codobjetivoShow;?></font></td></tr>
			<tr><th width="200" align="left"><font size = "2">Descripci&oacute;n</font></th>				<th><font size = "2">:</font></th>	<td width="300" align="justify"><font size = "2"><?php echo $descobjetivoShow;?></font></td></tr>
			<tr><th width="200" align="left"><font size = "2">A&ntilde;o</font></th>						<th><font size = "2">:</font></th>	<td width="300"><font size = "2"><?php echo $agnoobjetivoShow;?></font></td></tr>
			<tr><th width="200" align="left"><font size = "2">Tipo de Objetivo</font></th>			<th><font size = "2">:</font></th>	<td width="300"><font size = "2"><?php echo $nomtipoobjetivoShow;?></font></td></tr>
		</table>
		<?php
	}
	
	function despliegatipoobjetivo($codtipoobjetivo)
	{
		// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
		$tablaShow = "tipoobjetivo";
		$campoShow = "codtipoobjetivo";

		// LLAMADA A FUNCION DE CONSULTA
		$punteroconsultaShow = consultatodo($tablaShow, $campoShow, $codtipoobjetivo);
		
		// FORMATEO DE LOS RESULTADOS
		$regtipoobjetivoShow = mysqli_fetch_assoc($punteroconsultaShow);
		
		// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
		
		$codtipoobjetivoShow = $regtipoobjetivoShow["codtipoobjetivo"];
		$nomtipoobjetivoShow = $regtipoobjetivoShow["nomtipoobjetivo"];
		
		?>
		<font size="4"><p><b>Tipo de objetivo</b></p></font>
		<hr />
		<table border = "0">
			<tr><th align="left">C&oacute;digo de Tipo</th>		<th>:</th>	<td><?php echo $codtipoobjetivoShow;?></td></tr>
			<tr><th align="left">Nombre de Tipo</th>		<th>:</th>	<td><?php echo $nomtipoobjetivoShow;?></td></tr>
		</table>
		<?php
	}
	function despliegauniestrat($coduniestrat)
	{
		// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
		$tablaShow = "uniestrat";
		$campoShow = "coduniestrat";

		// LLAMADA A FUNCION DE CONSULTA
		$punteroconsultaShow = consultatodo($tablaShow, $campoShow, $coduniestrat);
		
		// FORMATEO DE LOS RESULTADOS
		$reguniestratShow = mysqli_fetch_assoc($punteroconsultaShow);
		
		// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
		
		$coduniestratShow = $reguniestratShow["coduniestrat"];
		$nomuniestratShow = $reguniestratShow["nomuniestrat"];
		
		?>
		<font size="4"><p><b>Unidad Estrat&eacute;gica</b></p></font>
		<hr />
		<table border = "0">
			<tr><th align="left">C&oacute;digo Unidad Estrat&eacute;gica</th>		<th>:</th>	<td><?php echo $coduniestratShow;?></td></tr>
			<tr><th align="left">Nombre Unidad Estrat&eacute;gica</th>		<th>:</th>	<td><?php echo $nomuniestratShow;?></td></tr>
		</table>
		<?php
	}
	
	function despliegausuarioeva($rutusuarioeva)
	{
		// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
		$tablaShow = "usuarioeva";
		$campoShow = "rutusuarioeva";

		// LLAMADA A FUNCION DE CONSULTA
		$punteroconsultaShow = consultatodo($tablaShow, $campoShow, $rutusuarioeva);
		
		// FORMATEO DE LOS RESULTADOS
		$regusuarioevaShow = mysqli_fetch_assoc($punteroconsultaShow);
		
		// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES

		$nomusuarioevaShow = $regusuarioevaShow["nomusuarioeva"];
		$rutusuarioevaShow = $regusuarioevaShow["rutusuarioeva"];
		$codtipousuarioevaShow = $regusuarioevaShow["codtipousuarioeva"];
		
		$resultqtipousuarioeva = consultatodo("tipousuarioeva", "codtipousuarioeva", $codtipousuarioevaShow);
		$regtipousuarioeva = mysqli_fetch_assoc($resultqtipousuarioeva);
		$nomtipousuarioevaShow = $regtipousuarioeva["nomtipousuarioeva"]
		
		?>
		<font size="4"><p><b>Usuario</b></p></font>
		<hr />
		<table border = "0">
			<tr><th align="left">Nombre de Usuario</th>	<th>:</th>	<td><?php echo $nomusuarioevaShow;?></td></tr>
			<tr><th align="left">RUT</th>				<th>:</th>	<td><?php echo $rutusuarioevaShow;?></td></tr>
			<tr><th align="left">Tipo de Usuario</th>	<th>:</th>	<td><?php echo $nomtipousuarioevaShow;?></td></tr>
		</table>
		<?php
	}
	
	function despliegatipousuarioeva($codtipousuarioeva)
	{
		// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
		$tablaShow = "tipousuarioeva";
		$campoShow = "codtipousuarioeva";

		// LLAMADA A FUNCION DE CONSULTA
		$punteroconsultaShow = consultatodo($tablaShow, $campoShow, $codtipousuarioeva);
		
		// FORMATEO DE LOS RESULTADOS
		$regtipousuarioevaShow = mysqli_fetch_assoc($punteroconsultaShow);
		
		// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
		
		$codtipousuarioevaShow = $regtipousuarioevaShow["codtipousuarioeva"];
		$nomtipousuarioevaShow = $regtipousuarioevaShow["nomtipousuarioeva"];
		
		?>
		<font size="4"><p><b>Tipo de Usuario</b></p></font>
		<hr />
		<table border = "0">
			<tr><th align="left">C&oacute;digo de Tipo</th>		<th>:</th>	<td><?php echo $codtipousuarioevaShow;?></td></tr>
			<tr><th align="left">Nombre de Tipo</th>		<th>:</th>	<td><?php echo $nomtipousuarioevaShow;?></td></tr>
		</table>
		<?php
	}
	
	function despliegaescalaevaluacion($codescalaevaluacion)
	{
		// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
		$tablaShow = "escalaevaluacion";
		$campoShow = "codescalaevaluacion";

		// LLAMADA A FUNCION DE CONSULTA
		$punteroconsultaShow = consultatodo($tablaShow, $campoShow, $codescalaevaluacion);
		
		// FORMATEO DE LOS RESULTADOS
		$regescalaevaluacionShow = mysqli_fetch_assoc($punteroconsultaShow);
		
		// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES

		$codescalaevaluacionShow = $regescalaevaluacionShow["codescalaevaluacion"];
		$percentescalaevaluacionShow = $regescalaevaluacionShow["percentescalaevaluacion"];
		$nomescalaevaluacionShow = $regescalaevaluacionShow["nomescalaevaluacion"];

		?>
		<font size="4"><p><b>Escala de Evaluaci&oacute;n</b></p></font>
		<hr />
		<table border = "0">
			<tr><th align="left">C&oacute;digo</th>				<th>:</th>	<td><?php echo $codescalaevaluacionShow;?></td></tr>
			<tr><th align="left">Porcentaje</th>			<th>:</th>	<td><?php echo $percentescalaevaluacionShow;?></td></tr>
			<tr><th align="left">Niv. de Evaluaci&oacute;n</th>	<th>:</th>	<td><?php echo $nomescalaevaluacionShow;?></td></tr>
		</table>
		<?php
	}
	
	function cuentaevaluados($agnoeval, $coduniestrat)
	{
		$contevaluados = 0;
		$resultqcuentaevaluados = consultatodo("evaluacion", "agnoevaluacion", $agnoeval);
		while ($regcuentaevaluados = mysqli_fetch_assoc($resultqcuentaevaluados))
		{
			$codevaluacion = $regcuentaevaluados["codevaluacion"];
			$arrayCodEvaluacion = explode("-", $codevaluacion);
			$rutevaluado = $arrayCodEvaluacion[0]."-".$arrayCodEvaluacion[1];
			if ($resultqpersona = consultatodo("persona", "rutpersona", $rutevaluado))
			{
				$regpersona = mysqli_fetch_assoc($resultqpersona);
				$areaevaluado = $regpersona["codarea"];
				if ($resultqarea = consultatodo("area", "codarea", $areaevaluado))
				{
					$regarea = mysqli_fetch_assoc($resultqarea);
					$coduniestratevaluado = $regarea["coduniestrat"];
					if ($coduniestrat == $coduniestratevaluado)
					{
						$contevaluados = $contevaluados + 1;
					}
				}
			}
		}
		//$contevaluados = mysqli_num_rows($resultqcuentaevaluados);
		return $contevaluados;
	}
	
	//DESPLIEGUE DE EVALUACION DE DESEMPEÑO.
	function despliegaevaluacion($codevaluacion)
	{
		$agnoactual = date("Y");
		$arrayAgnoeva = explode("-", $codevaluacion);
		$agnoeva = $arrayAgnoeva[2];
		//$agnoeva = $agnoactual - 1;
		$nomserver = $_SERVER["SERVER_NAME"];
		$port = $_SERVER["SERVER_PORT"];
		$webserver = $nomserver.":".$port;
		echo "<table><tr><th><font size='4'>Evaluaci&oacute;n de Personal</font></th><th><font size='4'>".$agnoeva."</font></th></tr></table>";
		echo "<hr />";
		
		$resultqevaluacion = consultatodo("evaluacion", "codevaluacion", $codevaluacion);
		$regevaluacion = mysqli_fetch_assoc($resultqevaluacion);
		
		//=========================================================================
		//BUSCA Y DESPLIEGA DATOS DE EVALUADO.
		//=========================================================================
		
		// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
		$rutpersonaAux = $regevaluacion["rutpersona"];

		// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
		$tabla = "persona";
		$campo = "rutpersona";

		// LLAMADA A FUNCION DE CONSULTA
		$punteroconsulta = consultatodo($tabla, $campo, $rutpersonaAux);
		
		// FORMATEO DE LOS RESULTADOS
		$regpersona = mysqli_fetch_assoc($punteroconsulta);
			
		if ($regpersona["rutpersona"] == "")
		{
			echo "Persona Inexistente</br></br>";
			?>
			<table>
				<tr>
					<td><button><a href="http://<?php echo $webserver;?>/eva/main.php">Men&uacute; Principal</a></button></td>
				</tr>
			</table>
			<?php
		}
		else
		{
			// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
			$nompersonaAux2 = $regpersona["nompersona"];
			$appaternoAux2 = $regpersona["appaterno"];
			$apmaternoAux2 = $regpersona["apmaterno"];
			$rutpersonaAux2 = $regpersona["rutpersona"];
			$fechaingresoAux2 = $regpersona["fechaingreso"];
			
			$codcargopersonaAux2 = $regpersona["codcargo"];
			$resultqcargopersona = consultatodo("cargo", "codcargo", $codcargopersonaAux2);
			$regcargopersona = mysqli_fetch_assoc($resultqcargopersona);
			$nomcargopersonaAux2 = $regcargopersona["nomcargo"];
			
			$codareapersonaAux2 = $regpersona["codarea"];
			$resultqareapersona = consultatodo("area", "codarea", $codareapersonaAux2);
			$regareapersona = mysqli_fetch_assoc($resultqareapersona);
			$nomareapersonaAux2 = $regareapersona["nomarea"];
			
			//PRESENTACION DE DATOS DE PERSONA
			echo "<table border='1'><th align='left' bgcolor='#0080FF' width='1000'><font color='white' size = '2'>1.- Datos del evaluado</font></th></table>";
			?>
			<hr />
			<table border = "0">
				<tr><th align="left"><font size="2">Nombres</font></th>				<th><font size="2">:</font></th>	<td><font size="2"><?php echo $nompersonaAux2;?></font></td></tr>
				<tr><th align="left"><font size="2">Ap. Paterno</font></th>			<th><font size="2">:</font></th>	<td><font size="2"><?php echo $appaternoAux2;?></font></td></tr>
				<tr><th align="left"><font size="2">Ap. Materno</font></th>			<th><font size="2">:</font></th>	<td><font size="2"><?php echo $apmaternoAux2;?></font></td></tr>
				<tr><th align="left"><font size="2">RUT</font></th>					<th><font size="2">:</font></th>	<td><font size="2"><?php echo $rutpersonaAux2;?></font></td></tr>
				<tr><th align="left"><font size="2">F. de Ingreso</font></th>		<th><font size="2">:</font></th> 	<td><font size="2"><?php echo $fechaingresoAux2;?></font></td></tr>
				<tr><th align="left"><font size="2">Cargo</font></th>				<th><font size="2">:</font></th> 	<td><font size="2"><?php echo $nomcargopersonaAux2;?></font></td></tr>
				<tr><th align="left"><font size="2">&Aacute;rea</font></th>			<th><font size="2">:</font></th>	<td><font size="2"><?php echo $nomareapersonaAux2;?></font></td></tr>
			</table>
			<hr />
			<?php
		}
		//=========================================================================
		//FIN PROCESO BUSCA Y DESPLIEGA DATOS DE EVALUADO.
		//=========================================================================
		
		//=========================================================================
		//BUSCA Y DESPLIEGA DATOS DE EVALUADOR.
		//=========================================================================
		
		// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
		$rutevaluadorAux = $regevaluacion["rutevaluador"];

		// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
		$tabla2 = "evaluador";
		$campo2 = "rutevaluador";

		// LLAMADA A FUNCION DE CONSULTA
		$punteroconsulta2 = consultatodo($tabla2, $campo2, $rutevaluadorAux);
		
		// FORMATEO DE LOS RESULTADOS
		$regevaluador = mysqli_fetch_assoc($punteroconsulta2);
			
		if ($regevaluador["rutevaluador"] == "")
		{
			echo "Evaluador Inexistente</br></br>";
			?>
			<table>
				<tr>
					<td><button><a href="http://<?php echo $webserver;?>/eva/main.php">Men&uacute; Principal</a></button></td>
				</tr>
			</table>
			<?php
		}
		else
		{
			// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
			$nomevaluadorAux2 = $regevaluador["nomevaluador"];
			$appaternoevaAux2 = $regevaluador["appaterno"];
			$apmaternoevaAux2 = $regevaluador["apmaterno"];
			$rutevaluadorAux2 = $regevaluador["rutevaluador"];
			
			//PRESENTACION DE DATOS DEL CARGO
			echo "<table border='1'><th align='left' bgcolor='#0080FF' width='1000'><font color='white' size = '2'>2.- Datos del evaluador</font></th></table>";
			?>
			<hr />
				<table border = "0">
					<tr><th align="left"><font size="2">Nombres</font></th>			<th><font size="2">:</font></th>	<td><font size="2"><?php echo $nomevaluadorAux2;?></font></td></tr>
					<tr><th align="left"><font size="2">Ap. Paterno</font></th>		<th><font size="2">:</font></th>	<td><font size="2"><?php echo $appaternoevaAux2;?></font></td></tr>
					<tr><th align="left"><font size="2">Ap. Materno</font></th>		<th><font size="2">:</font></th>	<td><font size="2"><?php echo $apmaternoevaAux2;?></font></td></tr>
					<tr><th align="left"><font size="2">RUT</font></th>				<th><font size="2">:</font></th>	<td><font size="2"><?php echo $rutevaluadorAux2;?></font></td></tr>
				</table>
			<hr />
			<?php
		}
		
		//=========================================================================
		//EVALUACION POR OBJETIVOS GENERALES.
		//=========================================================================
		echo "<table border='1'><th align='left' bgcolor='#0080FF' width='1000'><font color='white' size = '2'>3.- Evaluaci&oacute;n por Objetivos</font></th></table>";
		echo "<hr />";
		echo "<table><th><font size = '2'>Objetivos Generales</font></th></table>";
		
		$resultqevaluacionxobjetivoxpersona = consultatodo("evaluacionxobjetivoxpersona", "codevaluacion", $codevaluacion);
		$rows = mysqli_num_rows($resultqevaluacionxobjetivoxpersona);
		if ($rows != "0")
		{
			$cuentaobjetivogen = 0;
			echo "<table border='1'>";
			echo "<tr><th align='left' width = '250'><font size = '2'>Descripci&oacute;n</font></th><th align='left' width = '150'><font size = '2'>Eval. Esperada</font></th><th align='left' width = '150'><font size = '2'>Eval. Final</font></th><th align='left' width = '50'><font size = '2'>Brecha</font></th></tr>";
			while ($regevaluacionxobjetivoxpersona = mysqli_fetch_assoc($resultqevaluacionxobjetivoxpersona))
			{
				$codobjetivoxpersonaAux4 = $regevaluacionxobjetivoxpersona["codobjetivoxpersona"];
				$resultqobjetivoxpersona = consultatodo("objetivoxpersona", "codobjetivoxpersona", $codobjetivoxpersonaAux4);
				$regobjetivoxpersona = mysqli_fetch_assoc($resultqobjetivoxpersona);
				
				//OBTIENE EVALUACION OBTENIDA
				$codevaluacionobtenidaAux4 = $regevaluacionxobjetivoxpersona["codevaluacionobtenida"];
				$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionobtenidaAux4);
				$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
				$percentevaluacionobtenida = $regescalaevaluacion["percentescalaevaluacion"];
				$nomevaluacionobtenida = $regescalaevaluacion["nomescalaevaluacion"];
				
				$codobjetivoAux4 = $regobjetivoxpersona["codobjetivo"];
				if ($resultqobjetivo = consultatodo("objetivo", "codobjetivo", $codobjetivoAux4))
				{
					$regobjetivo = mysqli_fetch_assoc($resultqobjetivo);
					$descobjetivoAux4 = $regobjetivo["descobjetivo"];
					$agnoobjetivoAux4 = $regobjetivo["agnoobjetivo"];
					$codtipoobjetivoAux4 = $regobjetivo["codtipoobjetivo"];
					if ($resultqtipoobjetivo = consultatodo("tipoobjetivo", "codtipoobjetivo", $codtipoobjetivoAux4))
					{
						$regtipoobjetivo = mysqli_fetch_assoc($resultqtipoobjetivo);
						$nomtipoobjetivoAux4 = $regtipoobjetivo["nomtipoobjetivo"];
					}
				}
				//Preparacion para despliegue de Nivel esperado.
				//rescata codigo evaluacion esperada desde tabla objetivoxpersona.
				$codevaluacionesperadaAux4 = $regobjetivoxpersona["codevaluacionesperada"];
				//consulta en tabla escalaevaluacion con el codigo anterior.
				$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionesperadaAux4);
				$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
				//Obtiene codigo y nombre de escala de evaluación.
				$codescalaevaluacionAux4 = $regescalaevaluacion["codescalaevaluacion"];
				$nomescalaevaluacionAux4 = $regescalaevaluacion["nomescalaevaluacion"];
				$percentescalaevaluacionAux4 = $regescalaevaluacion["percentescalaevaluacion"];
				$brecha = $percentescalaevaluacionAux4 - $percentevaluacionobtenida;
				if (($codtipoobjetivoAux4 == 1) and ($agnoobjetivoAux4 == $agnoeva))
				{
					//Despliegue de registro completo en tabla html.
					echo "<tr><td align='left'><font size = '2'>".$descobjetivoAux4."</font></td><td align='right'><font size = '2'>".$percentescalaevaluacionAux4."% - ".$nomescalaevaluacionAux4."</font></td><td align='right'><font size = '2'>".$percentevaluacionobtenida."% - ".$nomevaluacionobtenida."</font></td><td align='left'><font size = '2'>".$brecha."</font></td></tr>";
					$cuentaobjetivogen = $cuentaobjetivogen + 1;
				}
			}
			//echo "</table>";
			//echo "</br>";
			if ($cuentaobjetivogen == 0)
			{
				echo "<tr><td align='left' colspan = '4'><font size = '2'>No se han asociado objetivos generales</font></td></tr>";
				//echo "No se han asociado objetivos generales";
			}
			echo "</table>";
			echo "</br>";
		}
		else
		{
			echo "No se han asociado objetivos";
		}
		echo "<hr />";
		
		//=========================================================================
		//FIN EVALUACION POR OBJETIVOS GENERALES.
		//=========================================================================
		
		//=========================================================================
		//EVALUACION POR OBJETIVOS ESPECIFICOS.
		//=========================================================================
		
		echo "<table><th><font size = '2'>Objetivos Espec&iacute;ficos</font></th></table>";
		
		$resultqevaluacionxobjetivoxpersona = consultatodo("evaluacionxobjetivoxpersona", "codevaluacion", $codevaluacion);
		$rows = mysqli_num_rows($resultqevaluacionxobjetivoxpersona);
		if ($rows != "0")
		{
			$cuentaobjetivoespec = 0;
			echo "<table border='1'>";
			echo "<tr><th align='left' width = '250'><font size = '2'>Descripci&oacute;n</font></th><th align='left' width = '150'><font size = '2'>Eval. Esperada</font></th><th align='left' width = '150'><font size = '2'>Eval. Final</font></th><th align='left' width = '50'><font size = '2'>Brecha</font></th></tr>";
			while ($regevaluacionxobjetivoxpersona = mysqli_fetch_assoc($resultqevaluacionxobjetivoxpersona))
			{
				$codobjetivoxpersonaAux4 = $regevaluacionxobjetivoxpersona["codobjetivoxpersona"];
				$resultqobjetivoxpersona = consultatodo("objetivoxpersona", "codobjetivoxpersona", $codobjetivoxpersonaAux4);
				$regobjetivoxpersona = mysqli_fetch_assoc($resultqobjetivoxpersona);
				
				//OBTIENE EVALUACION OBTENIDA
				$codevaluacionobtenidaAux4 = $regevaluacionxobjetivoxpersona["codevaluacionobtenida"];
				$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionobtenidaAux4);
				$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
				$percentevaluacionobtenida = $regescalaevaluacion["percentescalaevaluacion"];
				$nomevaluacionobtenida = $regescalaevaluacion["nomescalaevaluacion"];
				
				$codobjetivoAux4 = $regobjetivoxpersona["codobjetivo"];
				if ($resultqobjetivo = consultatodo("objetivo", "codobjetivo", $codobjetivoAux4))
				{
					$regobjetivo = mysqli_fetch_assoc($resultqobjetivo);
					$descobjetivoAux4 = $regobjetivo["descobjetivo"];
					$agnoobjetivoAux4 = $regobjetivo["agnoobjetivo"];
					$codtipoobjetivoAux4 = $regobjetivo["codtipoobjetivo"];
					if ($resultqtipoobjetivo = consultatodo("tipoobjetivo", "codtipoobjetivo", $codtipoobjetivoAux4))
					{
						$regtipoobjetivo = mysqli_fetch_assoc($resultqtipoobjetivo);
						$nomtipoobjetivoAux4 = $regtipoobjetivo["nomtipoobjetivo"];
					}
				}
				//Preparacion para despliegue de Nivel esperado.
				//rescata codigo evaluacion esperada desde tabla objetivoxpersona.
				$codevaluacionesperadaAux4 = $regobjetivoxpersona["codevaluacionesperada"];
				//consulta en tabla escalaevaluacion con el codigo anterior.
				$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionesperadaAux4);
				$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
				//Obtiene codigo y nombre de escala de evaluación.
				$codescalaevaluacionAux4 = $regescalaevaluacion["codescalaevaluacion"];
				$nomescalaevaluacionAux4 = $regescalaevaluacion["nomescalaevaluacion"];
				$percentescalaevaluacionAux4 = $regescalaevaluacion["percentescalaevaluacion"];
				$brecha = $percentescalaevaluacionAux4 - $percentevaluacionobtenida;
				if (($codtipoobjetivoAux4 == 2) and ($agnoobjetivoAux4 == $agnoeva))
				{
					//Despliegue de registro completo en tabla html.
					echo "<tr><td align='left'><font size = '2'>".$descobjetivoAux4."</font></td><td align='right'><font size = '2'>".$percentescalaevaluacionAux4."% - ".$nomescalaevaluacionAux4."</font></td><td align='right'><font size = '2'>".$percentevaluacionobtenida."% - ".$nomevaluacionobtenida."</font></td><td align='left'><font size = '2'>".$brecha."</font></td></tr>";
					$cuentaobjetivoespec = $cuentaobjetivoespec + 1;
				}
			}
			//echo "</table>";
			//echo "</br>";
			if ($cuentaobjetivoespec == 0)
			{
				echo "<tr><td align='left' colspan = '4'><font size = '2'>No se han asociado objetivos espec&iacute;ficos</font></td></tr>";
				//echo "No se han asociado objetivos espec&iacute;ficos";
			}
			echo "</table>";
			echo "</br>";
		}
		else
		{
			echo "No se han asociado objetivos";
		}
		echo "<hr />";
		
		//=========================================================================
		//FIN EVALUACION POR OBJETIVOS ESPECIFICOS.
		//=========================================================================
		
		echo "<div class='saltopagina'></div>";
		
		//=========================================================================
		//EVALUACION POR COMPETENCIAS CONDUCTUALES.
		//=========================================================================
		echo "<table border='1'><th align='left' bgcolor='#0080FF' width='1000'><font color='white' size = '2'>4.- Evaluaci&oacute;n por Competencias</font></th></table>";
		echo "<hr />";
		echo "<table><th><font size = '2'>Competencias Conductuales</font></th></table>";
		
		$resultqevaluacionxcompetenciaxcargo = consultatodo("evaluacionxcompetenciaxcargo", "codevaluacion", $codevaluacion);
		$rows = mysqli_num_rows($resultqevaluacionxcompetenciaxcargo);
		if ($rows != "0")
		{
			$cuentacompetenciacon = 0;
			echo "<table border='1'>";
			echo "<tr><th align='left' width = '250'><font size = '2'>Nombre</font></th><th align='left' width = '150'><font size = '2'>Eval. Esperada</font></th><th align='left' width = '150'><font size = '2'>Eval. Final</font></th><th align='left' width = '50'><font size = '2'>Brecha</font></th></tr>";
			while ($regevaluacionxcompetenciaxcargo = mysqli_fetch_assoc($resultqevaluacionxcompetenciaxcargo))
			{
				$codcompetenciaxcargoAux4 = $regevaluacionxcompetenciaxcargo["codcompetenciaxcargo"];
				$resultqcompetenciaxcargo = consultatodo("competenciaxcargo", "codcompetenciaxcargo", $codcompetenciaxcargoAux4);
				$regcompetenciaxcargo = mysqli_fetch_assoc($resultqcompetenciaxcargo);
				
				//OBTIENE EVALUACION OBTENIDA
				$codevaluacionobtenidaAux4 = $regevaluacionxcompetenciaxcargo["codevaluacionobtenida"];
				$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionobtenidaAux4);
				$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
				$percentevaluacionobtenida = $regescalaevaluacion["percentescalaevaluacion"];
				$nomevaluacionobtenida = $regescalaevaluacion["nomescalaevaluacion"];
				
				$codcompetenciaAux4 = $regcompetenciaxcargo["codcompetencia"];
				if ($resultqcompetencia = consultatodo("competencia", "codcompetencia", $codcompetenciaAux4))
				{
					$regcompetencia = mysqli_fetch_assoc($resultqcompetencia);
					$desccompetenciaAux4 = $regcompetencia["desccompetencia"];
					$agnocompetenciaAux4 = $regcompetencia["agnocompetencia"];
					$codtipocompetenciaAux4 = $regcompetencia["codtipocompetencia"];
					if ($resultqtipocompetencia = consultatodo("tipocompetencia", "codtipocompetencia", $codtipocompetenciaAux4))
					{
						$regtipocompetencia = mysqli_fetch_assoc($resultqtipocompetencia);
						$nomtipocompetenciaAux4 = $regtipocompetencia["nomtipocompetencia"];
					}
				}
				//Preparacion para despliegue de Nivel esperado.
				//rescata codigo evaluacion esperada desde tabla objetivoxpersona.
				$codevaluacionesperadaAux4 = $regcompetenciaxcargo["codevaluacionesperada"];
				//consulta en tabla escalaevaluacion con el codigo anterior.
				$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionesperadaAux4);
				$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
				//Obtiene codigo y nombre de escala de evaluación.
				$codescalaevaluacionAux4 = $regescalaevaluacion["codescalaevaluacion"];
				$nomescalaevaluacionAux4 = $regescalaevaluacion["nomescalaevaluacion"];
				$percentescalaevaluacionAux4 = $regescalaevaluacion["percentescalaevaluacion"];
				if ($percentevaluacionobtenida > 0)
				{
					$brecha = $percentescalaevaluacionAux4 - $percentevaluacionobtenida;
					if (($codtipocompetenciaAux4 == 1) and ($agnocompetenciaAux4 == $agnoeva))
					{
						//Despliegue de registro completo en tabla html.
						echo "<tr><td align='left'><font size = '2'>".$desccompetenciaAux4."</font></td><td align='right'><font size = '2'>".$percentescalaevaluacionAux4."% - ".$nomescalaevaluacionAux4."</font></td><td align='right'><font size = '2'>".$percentevaluacionobtenida."% - ".$nomevaluacionobtenida."</font></td><td align='left'><font size = '2'>".$brecha."</font></td></tr>";
						$cuentacompetenciacon = $cuentacompetenciacon + 1;
					}
				}
			}
			//echo "</table>";
			//echo "</br>";
			if ($cuentacompetenciacon == 0)
			{
				echo "<tr><td align='left' colspan = '4'><font size = '2'>No se han asociado competencias conductuales</font></td></tr>";
			}
			echo "</table>";
			echo "</br>";
		}
		else
		{
			echo "No se han asociado competencias";
		}
		echo "<hr />";
		
		//=========================================================================
		//FIN EVALUACION POR COMPETENCIAS CONDUCTUALES.
		//=========================================================================
		
		//=========================================================================
		//EVALUACION POR COMPETENCIAS ESPECIFICAS.
		//=========================================================================

		echo "<table><th><font size = '2'>Competencias Espec&iacute;ficas</font></th></table>";
		
		$resultqevaluacionxcompetenciaxcargo = consultatodo("evaluacionxcompetenciaxcargo", "codevaluacion", $codevaluacion);
		$rows = mysqli_num_rows($resultqevaluacionxcompetenciaxcargo);
		if ($rows != "0")
		{
			$cuentacompetenciatec = 0;
			echo "<table border='1'>";
			echo "<tr><th align='left' width = '250'><font size = '2'>Nombre</font></th><th align='left' width = '150'><font size = '2'>Eval. Esperada</font></th><th align='left' width = '150'><font size = '2'>Eval. Final</font></th><th align='left' width = '50'><font size = '2'>Brecha</font></th></tr>";
			while ($regevaluacionxcompetenciaxcargo = mysqli_fetch_assoc($resultqevaluacionxcompetenciaxcargo))
			{
				$codcompetenciaxcargoAux4 = $regevaluacionxcompetenciaxcargo["codcompetenciaxcargo"];
				$resultqcompetenciaxcargo = consultatodo("competenciaxcargo", "codcompetenciaxcargo", $codcompetenciaxcargoAux4);
				$regcompetenciaxcargo = mysqli_fetch_assoc($resultqcompetenciaxcargo);
				
				//OBTIENE EVALUACION OBTENIDA
				$codevaluacionobtenidaAux4 = $regevaluacionxcompetenciaxcargo["codevaluacionobtenida"];
				$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionobtenidaAux4);
				$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
				$percentevaluacionobtenida = $regescalaevaluacion["percentescalaevaluacion"];
				$nomevaluacionobtenida = $regescalaevaluacion["nomescalaevaluacion"];
				
				$codcompetenciaAux4 = $regcompetenciaxcargo["codcompetencia"];
				if ($resultqcompetencia = consultatodo("competencia", "codcompetencia", $codcompetenciaAux4))
				{
					$regcompetencia = mysqli_fetch_assoc($resultqcompetencia);
					$desccompetenciaAux4 = $regcompetencia["desccompetencia"];
					$agnocompetenciaAux4 = $regcompetencia["agnocompetencia"];
					$codtipocompetenciaAux4 = $regcompetencia["codtipocompetencia"];
					if ($resultqtipocompetencia = consultatodo("tipocompetencia", "codtipocompetencia", $codtipocompetenciaAux4))
					{
						$regtipocompetencia = mysqli_fetch_assoc($resultqtipocompetencia);
						$nomtipocompetenciaAux4 = $regtipocompetencia["nomtipocompetencia"];
					}
				}
				//Preparacion para despliegue de Nivel esperado.
				//rescata codigo evaluacion esperada desde tabla objetivoxpersona.
				$codevaluacionesperadaAux4 = $regcompetenciaxcargo["codevaluacionesperada"];
				//consulta en tabla escalaevaluacion con el codigo anterior.
				$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionesperadaAux4);
				$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
				//Obtiene codigo y nombre de escala de evaluación.
				$codescalaevaluacionAux4 = $regescalaevaluacion["codescalaevaluacion"];
				$nomescalaevaluacionAux4 = $regescalaevaluacion["nomescalaevaluacion"];
				$percentescalaevaluacionAux4 = $regescalaevaluacion["percentescalaevaluacion"];
				if ($percentevaluacionobtenida > 0)
				{
					$brecha = $percentescalaevaluacionAux4 - $percentevaluacionobtenida;
					if (($codtipocompetenciaAux4 == 2) and ($agnocompetenciaAux4 == $agnoeva))
					{
						//Despliegue de registro completo en tabla html.
						echo "<tr><td align='left'><font size = '2'>".$nomcompetenciaAux4."</font></td><td align='right'><font size = '2'>".$percentescalaevaluacionAux4."% - ".$nomescalaevaluacionAux4."</font></td><td align='right'><font size = '2'>".$percentevaluacionobtenida."% - ".$nomevaluacionobtenida."</font></td><td align='left'><font size = '2'>".$brecha."</font></td></tr>";
						$cuentacompetenciatec = $cuentacompetenciatec + 1;
					}
				}
			}
			//echo "</table>";
			//echo "</br>";
			if ($cuentacompetenciatec == 0)
			{
				echo "<tr><td align='left' colspan = '4'><font size = '2'>No se han asociado competencias t&eacute;cnicas</font></td></tr>";
			}
			echo "</table>";
			echo "</br>";
		}
		else
		{
			echo "No se han asociado competencias";
		}
		echo "<hr />";
		
		//=========================================================================
		//FIN EVALUACION POR COMPETENCIAS ESPECIFICAS.
		//=========================================================================
		
		//=========================================================================
		//DESPLIEGUE DE RESULTADOS FINALES
		//=========================================================================
		
		$sumaponderafinalcomobj = 0;
		$cuentaponderafinalcomobj = 0;
		
		if ($resultqevaluacionxobjetivoxpersona1 = consultatodo("evaluacionxobjetivoxpersona", "codevaluacion", $codevaluacion))
		{
			$sumaponderagen = 0;
			$sumaponderaespec = 0;
			$cuentaobjgen = 0;
			$cuentaobjespec = 0;
			while ($regevaluacionxobjetivoxpersona1 = mysqli_fetch_assoc($resultqevaluacionxobjetivoxpersona1))
			{
				$codobjetivoxpersona1 = $regevaluacionxobjetivoxpersona1["codobjetivoxpersona"];
				$codevaluacionobtenidaobj1 = $regevaluacionxobjetivoxpersona1["codevaluacionobtenida"];
				if ($resultqobjetivoxpersona1 = consultatodo("objetivoxpersona", "codobjetivoxpersona", $codobjetivoxpersona1))
				{
					$regobjetivoxpersona1 = mysqli_fetch_assoc($resultqobjetivoxpersona1);
					$codobjetivo1 = $regobjetivoxpersona1["codobjetivo"];
					if ($resultqobjetivo1 = consultatodo("objetivo", "codobjetivo", $codobjetivo1))
					{
						$regobjetivo1 = mysqli_fetch_assoc($resultqobjetivo1);
						$codtipoobjetivo1 = $regobjetivo1["codtipoobjetivo"];
						if ($codtipoobjetivo1 == 1)
						{
							if ($resultqescalaevaluacion1 = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionobtenidaobj1))
							{
								$regescalaevaluacion1 = mysqli_fetch_assoc($resultqescalaevaluacion1);
								$percentescalaevaluacion1 = $regescalaevaluacion1["percentescalaevaluacion"];
							}
							if ($percentescalaevaluacion1 > 0)
							{
								$sumaponderagen = $sumaponderagen + $percentescalaevaluacion1;
								$cuentaobjgen = $cuentaobjgen + 1;
							}
						}
						if ($codtipoobjetivo1 == 2)
						{
							if ($resultqescalaevaluacion1 = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionobtenidaobj1))
							{
								$regescalaevaluacion1 = mysqli_fetch_assoc($resultqescalaevaluacion1);
								$percentescalaevaluacion1 = $regescalaevaluacion1["percentescalaevaluacion"];
							}
							if ($percentescalaevaluacion1 > 0)
							{
								$sumaponderaespec = $sumaponderaespec + $percentescalaevaluacion1;
								$cuentaobjespec = $cuentaobjespec + 1;
							}
						}
					}
				}
			}
		}
		if ($cuentaobjgen > 0)
		{
			$pondfinalobjgen1 = $sumaponderagen / $cuentaobjgen;
			$pondfinalobjgen = round($pondfinalobjgen1, 2);
			$sumaponderafinalcomobj = $sumaponderafinalcomobj + $pondfinalobjgen;
			$cuentaponderafinalcomobj = $cuentaponderafinalcomobj + 1;
		}
		
		if ($cuentaobjespec > 0)
		{
			$pondfinalobjespec1 = $sumaponderaespec / $cuentaobjespec;
			$pondfinalobjespec = round($pondfinalobjespec1, 2);
			$sumaponderafinalcomobj = $sumaponderafinalcomobj + $pondfinalobjespec;
			$cuentaponderafinalcomobj = $cuentaponderafinalcomobj + 1;
		}

		if ($resultqevaluacionxcompetenciaxcargo1 = consultatodo("evaluacionxcompetenciaxcargo", "codevaluacion", $codevaluacion))
		{
			$sumaponderacon = 0;
			$sumaponderatec = 0;
			$cuentacomcon = 0;
			$cuentacomtec = 0;
			while ($regevaluacionxcompetenciaxcargo1 = mysqli_fetch_assoc($resultqevaluacionxcompetenciaxcargo1))
			{
				$codcompetenciaxcargo1 = $regevaluacionxcompetenciaxcargo1["codcompetenciaxcargo"];
				$codevaluacionobtenidacom1 = $regevaluacionxcompetenciaxcargo1["codevaluacionobtenida"];
				if ($resultqcompetenciaxcargo1 = consultatodo("competenciaxcargo", "codcompetenciaxcargo", $codcompetenciaxcargo1))
				{
					$regcompetenciaxcargo1 = mysqli_fetch_assoc($resultqcompetenciaxcargo1);
					$codcompetencia1 = $regcompetenciaxcargo1["codcompetencia"];
					if ($resultqcompetencia1 = consultatodo("competencia", "codcompetencia", $codcompetencia1))
					{
						$regcompetencia1 = mysqli_fetch_assoc($resultqcompetencia1);
						$codtipocompetencia1 = $regcompetencia1["codtipocompetencia"];
						if ($codtipocompetencia1 == 1)
						{
							if ($resultqescalaevaluacion1 = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionobtenidacom1))
							{
								$regescalaevaluacion1 = mysqli_fetch_assoc($resultqescalaevaluacion1);
								$percentescalaevaluacion1 = $regescalaevaluacion1["percentescalaevaluacion"];
							}
							if ($percentescalaevaluacion1 > 0)
							{
								$sumaponderacon = $sumaponderacon + $percentescalaevaluacion1;
								$cuentacomcon = $cuentacomcon + 1;
							}
						}
						if ($codtipocompetencia1 == 2)
						{
							if ($resultqescalaevaluacion1 = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionobtenidaobj1))
							{
								$regescalaevaluacion1 = mysqli_fetch_assoc($resultqescalaevaluacion1);
								$percentescalaevaluacion1 = $regescalaevaluacion1["percentescalaevaluacion"];
							}
							if ($percentescalaevaluacion1 > 0)
							{
								$sumaponderatec = $sumaponderatec + $percentescalaevaluacion1;
								$cuentacomtec = $cuentacomtec + 1;
							}
						}
					}
				}
			}
		}
		if ($cuentacomcon > 0)
		{
			$pondfinalcomcon1 = $sumaponderacon / $cuentacomcon;
			$pondfinalcomcon = round($pondfinalcomcon1, 2);
			$sumaponderafinalcomobj = $sumaponderafinalcomobj + $pondfinalcomcon;
			$cuentaponderafinalcomobj = $cuentaponderafinalcomobj + 1;
		}

		if ($cuentacomtec > 0)
		{
			$pondfinalcomtec1 = $sumaponderatec / $cuentacomtec;
			$pondfinalcomtec = round($pondfinalcomtec1, 2);
			$sumaponderafinalcomobj = $sumaponderafinalcomobj + $pondfinalcomtec;
			$cuentaponderafinalcomobj = $cuentaponderafinalcomobj + 1;
		}

		if ($cuentaponderafinalcomobj > 0)
		{
			$pondfinalagnoAux1 = $sumaponderafinalcomobj / $cuentaponderafinalcomobj;
			$pondfinalagnoAux = round($pondfinalagnoAux1, 2);
		}
		else
		{
			mensaje("ERROR: No hay datos");
		}
		
		echo "<table border='1'><th align='left' bgcolor='#0080FF' width='1000'><font color='white' size = '2'>5.- Resultados de la Evaluaci&oacute;n</font></th></table>";
		$pondfinalobjetivoAux = $regevaluacion["pondfinalobjetivo"];
		$pondfinalcompetenciaAux = $regevaluacion["pondfinalcompetencia"];
		//$pondfinalagnoAux = $regevaluacion["pondfinalagno"];
		?>
		<hr />
		<fieldset>
			<legend>Objetivos</legend>
			<table border = "0">
				<tr><th align="left" width = '310'><font size = '2'><p>Ponderaci&oacute;n por objetivos espec&iacute;ficos</p></font></th>		<th width = '7'><font size = '2'><p>:</p></font></th>	<td width = '280'><font size = '2'><p><?php echo $pondfinalobjespec."%";?></p></font></td></tr>
				<tr><th align="left" width = '310'><font size = '2'><p>Ponderaci&oacute;n por objetivos generales</p></font></th>		<th width = '7'><font size = '2'><p>:</p></font></th>	<td width = '280'><font size = '2'><p><?php echo $pondfinalobjgen."%";?></p></font></td></tr>
				<!--<tr><th align="left" width = '310'><font size = '2'><p>Ponderaci&oacute;n final por objetivos</p></font></th>			<th width = '7'><font size = '2'><p>:</p></font></th>	<td width = '280'><font size = '2'><p><?php //echo $pondfinalobjetivoAux."%";?></p></font></td></tr>-->
			</table>
		</fieldset>
		<fieldset>
			<legend>Competencias</legend>		
			<table border = "0">
				<tr><th align="left" width = '310'><font size = '2'><p>Ponderaci&oacute;n por Competencias Conductuales</p></font></th>		<th width = '7'><font size = '2'><p>:</p></font></th>	<td width = '280'><font size = '2'><p><?php echo $pondfinalcomcon."%";?></p></font></td></tr>
				<tr><th align="left" width = '310'><font size = '2'><p>Ponderaci&oacute;n por Competencias T&eacute;cnicas</p></font></th>		<th width = '7'><font size = '2'><p>:</p></font></th>	<td width = '280'><font size = '2'><p><?php echo $pondfinalcomtec."%";?></p></font></td></tr>
				<!--<tr><th align="left" width = '310'><font size = '2'><p>Ponderaci&oacute;n final por competencias</p></font></th>			<th width = '7'><font size = '2'><p>:</p></font></th>	<td width = '280'><font size = '2'><p><?php //echo $pondfinalcompetenciaAux."%";?></p></font></td></tr>-->
			</table>
		</fieldset>
		<hr />
		<table border = "0">
			<tr><th align="left" width = '310'><font size = '2'><p>Evaluaci&oacute;n final <?php echo $agnoeva?></p></font></th>		<th width = '7'><font size = '2'><p>:</p></font></th>	<td width = '280'><font size = '2'><p><?php echo $pondfinalagnoAux."%";?></p></font></td></tr>
		</table>

		<?php
		
		//=========================================================================
		//DESPLIEGUE DE COMENTARIOS EVALUADOR
		//=========================================================================
		
		echo "<hr />";
		echo "<table border='1'><th align='left' bgcolor='#0080FF' width='600'><font color='white' size = '2'>6.- Comentarios del Evaluador</font></th></table>";
		$comentevaluadorAux = $regevaluacion["comentevaluador"];
		?>
		<hr />
		<table border = "1" width="580">
			<tr><td><?php echo $comentevaluadorAux;?></td></tr>
		</table>
		<?php
		
		//=========================================================================
		//DESPLIEGUE DE COMENTARIOS EVALUADO
		//=========================================================================
		
		echo "<hr />";
		echo "<table border='1'><th align='left' bgcolor='#0080FF' width='600'><font color='white' size = '2'>7.- Comentarios del Evaluado</font></th></table>";
		$comentevaluadoAux = $regevaluacion["comentevaluado"];
		?>
		<hr />
		<table border = "1" width="580">
			<tr><td><?php echo $comentevaluadoAux;?></td></tr>
		</table>
		<hr />
		<!--
		//=========================================================================
		//DESPLIEGUE DE OBJETIVOS ASIGNADOS PROXIMO ANO
		//=========================================================================
		
		<p><b>Objetivos asignados para <?php //echo $agnoactual;?></b></p>-->
		<?php
			echo "<div class='saltopagina'></div>";
			$agnoObj = $agnoeva + 1;
			echo "<table border='1'><th align='left' bgcolor='#0080FF' width='600'><font color='white' size = '2'>8.- Objetivos asignados para ".$agnoObj."</font></th></table>";
			echo "<hr />";
			$resultqobjetivoxpersona = consultatodo("objetivoxpersona", "rutpersona", $rutpersonaAux2);
			$rows = mysqli_num_rows($resultqobjetivoxpersona);
			if ($rows > "0")
			{
				echo "<table border='1'>";
				echo "<tr><th align='left' width='150'><font size = '2'>Tipo de objetivo</font></th><th align='left' width='300'><font size = '2'>Descripci&oacute;n</font></th><th align='left' width='150'><font size = '2'>Eval. Esperada</font></th></th></tr>";
				$cuentaobj = 0;
				while ($regobjetivoxpersona = mysqli_fetch_assoc($resultqobjetivoxpersona))
				{
					$codobjetivoAux4 = $regobjetivoxpersona["codobjetivo"];
					if ($resultqobjetivo = consultatodo("objetivo", "codobjetivo", $codobjetivoAux4))
					{
						$regobjetivo = mysqli_fetch_assoc($resultqobjetivo);
						$descobjetivoAux4 = $regobjetivo["descobjetivo"];
						$agnoobjetivoAux4 = $regobjetivo["agnoobjetivo"];
						$codtipoobjetivoAux4 = $regobjetivo["codtipoobjetivo"];
						if ($resultqtipoobjetivo = consultatodo("tipoobjetivo", "codtipoobjetivo", $codtipoobjetivoAux4))
						{
							$regtipoobjetivo = mysqli_fetch_assoc($resultqtipoobjetivo);
							$nomtipoobjetivoAux4 = $regtipoobjetivo["nomtipoobjetivo"];
						}
					}
					//Preparacion para despliegue de Nivel esperado.
					//rescata codigo evaluacion esperada desde tabla objetivoxpersona.
					$codevaluacionesperadaAux4 = $regobjetivoxpersona["codevaluacionesperada"];
					//consulta en tabla escalaevaluacion con el codigo anterior.
					$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionesperadaAux4);
					$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
					//Obtiene codigo y nombre de escala de evaluación.
					$codescalaevaluacionAux4 = $regescalaevaluacion["codescalaevaluacion"];
					$nomescalaevaluacionAux4 = $regescalaevaluacion["nomescalaevaluacion"];
					$percentescalaevaluacionAux4 = $regescalaevaluacion["percentescalaevaluacion"];
					//if (($codtipoobjetivoAux4 == 1) and ($agnoobjetivoAux4 == $agnoactual))
					if ($agnoobjetivoAux4 == $agnoObj)
					{
						//Despliegue de registro completo en tabla html.
						echo "<tr><td align='left'><font size = '2'>".$nomtipoobjetivoAux4."</font></td><td align='left'><font size = '2'>".$descobjetivoAux4."</font></td><td align='right'><font size = '2'>".$percentescalaevaluacionAux4." - ".$nomescalaevaluacionAux4."</font></td></tr>";
						$cuentaobj = $cuentaobj + 1;
					}
				}
				if ($cuentaobj == 0)
				{
					echo "<tr><td align='left' colspan = '3'><font size = '2'>No se han asociado objetivos para el a&ntildeo ".$agnoObj."</font></td></tr>";
				}
				echo "</table>";
				echo "</br>";
			}
			else
			{
				echo "No se han asociado objetivos para el a&ntildeo ".$agnoObj;
			}
		?>
		<!--DESPLIEGUE PLAN DE CAPACITACION-->
		<hr />
		<table border='1'><th align='left' bgcolor='#0080FF' width='600'><font color='white' size = '2'>9.- Plan de Desarrollo y Capacitaci&oacute;n <?php echo $agnoObj;?></font></th></table>
		<hr />
		<?php
		$resultqcursossolic = consultatodo("cursossolic", "codevaluacion", $codevaluacion);
		$cuentacurso = 0;
		echo "<table border = '1'>";
		echo "<tr><th width = '50'><font size='2'><p>Item</p></font></th><th width = '200'><font size='2'><p>Actividad</p></font></th><th width = '200'><font size='2'><p>Fecha</p></font></th><th width = '150'><font size='2'><p>Prioridad</p></font></th></tr>";
		while ($regcursossolic = mysqli_fetch_assoc($resultqcursossolic))
		{
			$cuentacurso = $cuentacurso + 1;
			$nomcursossolicAux1 = $regcursossolic["nomcursossolic"];
			$fechacursossolicAux1 = $regcursossolic["fechacursossolic"];
			$prioridadcursossolicAux0 = $regcursossolic["prioridadcursossolic"];
			$prioridadcursossolicAux1 = ucwords($prioridadcursossolicAux0);
			echo "<tr><th><font size='2'><p>".$cuentacurso.".-</p></font></th><td><font size='2'>".$nomcursossolicAux1."</font></td><td><font size='2'>".$fechacursossolicAux1."</font></td><td><font size='2'>".$prioridadcursossolicAux1."</font></td></tr>";			
		}
		
		if ($cuentacurso == 0)
		{
			echo "<tr><td colspan = '4'><font size = '2'><p>Esta persona no posee plan de capacitaci&oacute;n</p></font></td></tr>";
		}
		
		echo "</table>";
		?>
		<!--FIN DESPLIEGUE PLAN DE CAPACITACION-->
		</br></br>
		<!-- FIRMAS-->
		<hr />
		<table border="3">
			<tr>
				<td width="200" height="80"></td>
				<td width="200" height="80"></td>
				<td width="200" height="80"></td>
			</tr>
			<tr>
				<th align="center">Firma Evaluado</th>
				<th align="center">Firma Evaluador</th>
				<th align="center">VoBo Gerente</th>
			</tr>
		</table>
		<?php
	}
?>