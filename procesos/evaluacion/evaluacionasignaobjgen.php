<?php
	include "../../lib/handWebEva.php";
	include "../../lib/handVarEva.php";
	include "../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$rutusuarioevareg = $regusuarioeva["rutusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	//if (($codtipousuarioevaAux == 1) or ($codtipousuarioevaAux == 3))
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Evaluaci&oacute;n de personal
				</title>
				<link href="../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				<style type="text/css" media="all">@media all {div.saltopagina{display:none;}}</style>
				<style type="text/css" media="print">@media print {#menu {display:none;} div.saltopagina{display:block; page-break-before:always;}}</style>
			</head>
			<body>
				<?php
					$webserver = nomserverweb();
					
					if ($codtipousuarioevaAux == 1)
					{
						cabezallogin("EVALUACI&Oacute;N DE PERSONAL");
					}
					
					if ($codtipousuarioevaAux == 3)
					{
						cabezallogin("EVALUACI&Oacute;N DE PERSONAL");
					}					
					?>
					<font size='4'><p><b>Asignar Objetivos Generales</b></p></font>
					<hr />
					<?php
					
					$agnoactual = date("Y");
					$agnoeva = $agnoactual - 1;
					
					if (((isset($_REQUEST["rutpersonasave"])) and ($_REQUEST["rutpersonasave"] != "")) and
					    ((isset($_REQUEST["rutevaluadorsave"])) and ($_REQUEST["rutevaluadorsave"] != "")) and
						((isset($_REQUEST["codcargopersonasave"])) and ($_REQUEST["codcargopersonasave"] != "")) and
						((isset($_REQUEST["comentevaluador"])) and ($_REQUEST["comentevaluador"] != "")) and
						((isset($_REQUEST["comentevaluado"])) and ($_REQUEST["comentevaluado"] != "")) and
						((isset($_REQUEST["nomcursossolic1"])) and ($_REQUEST["nomcursossolic1"] != "")) and
						((isset($_REQUEST["nomcursossolic2"])) and ($_REQUEST["nomcursossolic2"] != "")) and
						((isset($_REQUEST["nomcursossolic3"])) and ($_REQUEST["nomcursossolic3"] != "")) and
						((isset($_REQUEST["fechacursossolic1"])) and ($_REQUEST["fechacursossolic1"] != "")) and
						((isset($_REQUEST["fechacursossolic2"])) and ($_REQUEST["fechacursossolic2"] != "")) and
						((isset($_REQUEST["fechacursossolic3"])) and ($_REQUEST["fechacursossolic3"] != "")) and
						((isset($_REQUEST["prioridadcursossolic1"])) and ($_REQUEST["prioridadcursossolic1"] != "")) and
						((isset($_REQUEST["prioridadcursossolic2"])) and ($_REQUEST["prioridadcursossolic2"] != "")) and
						((isset($_REQUEST["prioridadcursossolic3"])) and ($_REQUEST["prioridadcursossolic3"] != "")) and
						((isset($_REQUEST["action"])) and ($_REQUEST["action"] == "evaluarpersona")))
					{
						$rutpersonasaveAux = $_REQUEST["rutpersonasave"];
						$rutevaluadorsaveAux = $_REQUEST["rutevaluadorsave"];
						$codcargopersonasaveAux = $_REQUEST["codcargopersonasave"];
						
						//el codigo de evaluacion es rutpersona + agnoevaluado.
						$codevaluacion = $rutpersonasaveAux."-".$agnoeva;
						
						//obtiene las evaluaciones por objetivo del año correspondiente y las guarda en tabla evaluacionxobjetivoxpersona
						$resultqobjetivoxpersonaevaluado = consultatodo("objetivoxpersona", "rutpersona", $rutpersonasaveAux);
						$cuentaponderaobjetivo = 0;
						$sumaponderaobjetivo = 0;
						while ($regobjetivoxpersonaevaluado = mysqli_fetch_assoc($resultqobjetivoxpersonaevaluado))
						{
							$codobjetivoxpersonaevaluado = $regobjetivoxpersonaevaluado["codobjetivoxpersona"];
							$codobjetivoevaluado = $regobjetivoxpersonaevaluado["codobjetivo"];
							$resultqobjetivoevaluado = consultatodo("objetivo", "codobjetivo", $codobjetivoevaluado);
							$regobjetivoevaluado = mysqli_fetch_assoc($resultqobjetivoevaluado);
							$agnoobjetivoevaluado = $regobjetivoevaluado["agnoobjetivo"];
							$codtipoobjetivoevaluado = $regobjetivoevaluado["codtipoobjetivo"];
							if ($agnoobjetivoevaluado == $agnoeva)
							{
								if ((isset($_REQUEST[$codobjetivoxpersonaevaluado])) and ($_REQUEST[$codobjetivoxpersonaevaluado] != ""))
								{
									$codevaluacionxobjetivoxpersona = $codevaluacion."-".$codobjetivoxpersonaevaluado;
									$codevaluacionobtenidaobjetivo = $_REQUEST[$codobjetivoxpersonaevaluado];
									$stringalmacenatempevaluacionxobjetivo = "'".$codevaluacionxobjetivoxpersona."', '".$codevaluacion."', '".$codobjetivoxpersonaevaluado."', '".$codevaluacionobtenidaobjetivo."'";
									insertaregistro("tempevaluacionxobjetivoxpersona", $stringalmacenatempevaluacionxobjetivo);
									$resultqescalaevaluacionobjetivo = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionobtenidaobjetivo);
									$regescalaevaluacionobjetivo = mysqli_fetch_assoc($resultqescalaevaluacionobjetivo);
									$ponderaobjetivo = $regescalaevaluacionobjetivo["percentescalaevaluacion"];
									$sumaponderaobjetivo = $sumaponderaobjetivo + $ponderaobjetivo;
									$cuentaponderaobjetivo = $cuentaponderaobjetivo + 1;
								}
								else
								{
									mensaje ("Debe ingresar evaluación por objetivos");
								}
							}
						}
						
						$pondfinalobjetivo1 = $sumaponderaobjetivo / $cuentaponderaobjetivo;
						$pondfinalobjetivo = round($pondfinalobjetivo1, 2);
						
						//obtiene las evaluaciones por competencias y las guarda en tabla evaluacionxcompetenciaxcargo
						$resultqcompetenciaxcargoevaluado = consultatodo("competenciaxcargo", "codcargo", $codcargopersonasaveAux);
						$sumaponderacompetencia = 0;
						$cuentaponderacompetencia = 0;
						while ($regcompetenciaxcargoevaluado = mysqli_fetch_assoc($resultqcompetenciaxcargoevaluado))
						{
							$codcompetenciaxcargoevaluado = $regcompetenciaxcargoevaluado["codcompetenciaxcargo"];
							if ((isset($_REQUEST[$codcompetenciaxcargoevaluado])) and ($_REQUEST[$codcompetenciaxcargoevaluado] != ""))
							{
								$codevaluacionxcompetenciaxcargo = $codevaluacion."-".$codcompetenciaxcargoevaluado;
								$codevaluacionobtenidacompetencia = $_REQUEST[$codcompetenciaxcargoevaluado];
								$stringalmacenatempevaluacionxcompetencia = "'".$codevaluacionxcompetenciaxcargo."', '".$codevaluacion."', '".$codcompetenciaxcargoevaluado."', '".$codevaluacionobtenidacompetencia."'";
								insertaregistro("tempevaluacionxcompetenciaxcargo", $stringalmacenatempevaluacionxcompetencia);
								$resultqescalaevaluacioncompetencia = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionobtenidacompetencia);
								$regescalaevaluacioncompetencia = mysqli_fetch_assoc($resultqescalaevaluacioncompetencia);
								$ponderacompetencia = $regescalaevaluacioncompetencia["percentescalaevaluacion"];
								$sumaponderacompetencia = $sumaponderacompetencia + $ponderacompetencia;
								$cuentaponderacompetencia = $cuentaponderacompetencia + 1;
							}
							else
							{
								mensaje ("Debe ingresar evaluaci&oacute;n por competencias");
							}
						}
						
						$pondfinalcompetencia1 = $sumaponderacompetencia / $cuentaponderacompetencia;
						$pondfinalcompetencia = round($pondfinalcompetencia1, 2);
						
						$pondfinalagno1 = ($pondfinalobjetivo + $pondfinalcompetencia) / 2;
						$pondfinalagno = round($pondfinalagno1, 2);
						
						//obtiene comentarios evaluador
						$comentariosevaluador = $_REQUEST["comentevaluador"];
						//obtiene comentarios evaluado
						$comentariosevaluado = $_REQUEST["comentevaluado"];
						
						//Inserta registros en tabla de evaluacion
						$stringtempevaluacion = "'".$codevaluacion."', '".$rutevaluadorsaveAux."', '".$rutpersonasaveAux."', '".$agnoeva."', '".$pondfinalobjetivo."', '".$pondfinalcompetencia."', '".$pondfinalagno."', '".$comentariosevaluado."', '".$comentariosevaluador."'";
						insertaregistro("tempevaluacion", $stringtempevaluacion);
						
						//obtiene datos de plan de capacitación
						$nomcursossolic1Aux = $_REQUEST["nomcursossolic1"];
						$nomcursossolic2Aux = $_REQUEST["nomcursossolic2"];
						$nomcursossolic3Aux = $_REQUEST["nomcursossolic3"];
						$fechacursossolic1Aux = $_REQUEST["fechacursossolic1"];
						$fechacursossolic2Aux = $_REQUEST["fechacursossolic2"];
						$fechacursossolic3Aux = $_REQUEST["fechacursossolic3"];
						$prioridadcursossolic1Aux = $_REQUEST["prioridadcursossolic1"];
						$prioridadcursossolic2Aux = $_REQUEST["prioridadcursossolic2"];
						$prioridadcursossolic3Aux = $_REQUEST["prioridadcursossolic3"];
						
						$swfecha1 = validafecha($fechacursossolic1Aux);
						$swfecha2 = validafecha($fechacursossolic2Aux);
						$swfecha3 = validafecha($fechacursossolic3Aux);
						
						if ($swfecha1 == true)
						{
							//Inserta registros en tabla cursossolic
							$stringcursossolic1 = "'', '".$nomcursossolic1Aux."', '".$fechacursossolic1Aux."', '".$prioridadcursossolic1Aux."', '".$codevaluacion."'";
							insertaregistro("cursossolic", $stringcursossolic1);
						}
						else
						{
							mensaje ("Fecha incorrecta en Plan de capacitación, actividad 1");
						}
						
						if ($swfecha2 == true)
						{
							//Inserta registros en tabla cursossolic
							$stringcursossolic2 = "'', '".$nomcursossolic2Aux."', '".$fechacursossolic2Aux."', '".$prioridadcursossolic2Aux."', '".$codevaluacion."'";
							insertaregistro("cursossolic", $stringcursossolic2);
						}
						else
						{
							mensaje ("Fecha incorrecta en Plan de capacitación, actividad 2");
						}
						
						if ($swfecha3 == true)
						{
							//Inserta registros en tabla cursossolic
							$stringcursossolic3 = "'', '".$nomcursossolic3Aux."', '".$fechacursossolic3Aux."', '".$prioridadcursossolic3Aux."', '".$codevaluacion."'";
							insertaregistro("cursossolic", $stringcursossolic3);
						}
						else
						{
							mensaje ("Fecha incorrecta en Plan de capacitación, actividad 3");
						}
						
						$_REQUEST["rutpersona"] = $rutpersonasaveAux;
						$_REQUEST["action"] = "buscarpersona";
						
					}
					else
					{
						if ($_REQUEST["action"] == "evaluarpersona")
						{
							echo "Debe llenar todos los datos";
						}
					}
					
					// INSERCIÓN DE REGISTRO EN TABLA objetivoxpersona
					if (((isset($_REQUEST["codobjetivo"])) and ($_REQUEST["codobjetivo"] != "")) and
						((isset($_REQUEST["codevaluacionesperada"])) and ($_REQUEST["codevaluacionesperada"] != "")) and
						((isset($_REQUEST["action"])) and ($_REQUEST["action"] == "asignaevaluacionesperada")) and
						((isset($_REQUEST["rutpersona"])) and ($_REQUEST["rutpersona"] != "")))
					{
						$tabla = "objetivoxpersona";
						$cod1 = $_REQUEST["rutpersona"];
						$cod2 = $_REQUEST["codobjetivo"];
						$codobjetivoxpersona = $cod1."-".$cod2;
						$codobjetivoAux3 = $_REQUEST["codobjetivo"];
						$rutpersonaAux3 = $_REQUEST["rutpersona"];
						$codevaluacionesperadaAux = $_REQUEST["codevaluacionesperada"];
						$valores = "'".$codobjetivoxpersona."', '".$codobjetivoAux3."', '".$rutpersonaAux3."', '".$codevaluacionesperadaAux."'";
						insertaregistro($tabla, $valores);
						$_REQUEST["action"] = "buscarpersona";
					}
					
					if (((isset($_REQUEST["codobjetivo"])) and ($_REQUEST["codobjetivo"] != "")) and
						((isset($_REQUEST["action"])) and ($_REQUEST["action"] == "asignaobjetivo")) and
     					((isset($_REQUEST["rutpersona"])) and ($_REQUEST["rutpersona"] != "")))
					{
						//Presenta datos del persona
						
						// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
						$rutpersonaAux3 = $_REQUEST["rutpersona"];
						// LLAMADA A FUNCION DE CONSULTA
						$resultqpersonaAux3 = consultatodo("persona", "rutpersona", $rutpersonaAux3);
						// FORMATEO DE LOS RESULTADOS
						$regpersonaAux3 = mysqli_fetch_assoc($resultqpersonaAux3);
						//Rescate de código de persona
						$rutpersonaAux3 = $regpersonaAux3["rutpersona"];
						//despliega persona
						despliegapersona($rutpersonaAux3);
						
						//Presenta datos de LA objetivo en cuestión.
						
						// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
						$codobjetivoAux3 = $_REQUEST["codobjetivo"];
						// LLAMADA A FUNCION DE CONSULTA
						$resultqobjetivoAux3 = consultatodo("objetivo", "codobjetivo", $codobjetivoAux3);
						// FORMATEO DE LOS RESULTADOS
						$regobjetivoAux3 = mysqli_fetch_assoc($resultqobjetivoAux3);
						//Rescate de código de objetivo
						$codobjetivoAux3 = $regobjetivoAux3["codobjetivo"];
						//Despliega Competencia.
						despliegaobjetivo($codobjetivoAux3);
						
						//Presenta form con select de nivelesperado
						?>
						<form action="evaluacionasignaobjgen.php" method="post">
							<input type="text" name="rutpersona" style="visibility:hidden" value="<?=$rutpersonaAux3?>" readonly> 
							<input type="text" name="codobjetivo" style="visibility:hidden" value="<?=$codobjetivoAux3?>" readonly>
							<input type="text" name="action" style="visibility:hidden" value="asignaevaluacionesperada" readonly>
							<table border = "0">
								<tr><th align="left">Evaluaci&oacute;n Esperada</th>	<th>:</th>
									<td>
										<select name="codevaluacionesperada" width = "10">
											<?php
												$resultqescalaevaluacion = llenacombo("escalaevaluacion");
												echo "<option value=''>";
												echo "- Seleccione";
												echo "</option>";
												while ($regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion))
												{
													$codescalaevaluacionAux3 = $regescalaevaluacion["codescalaevaluacion"];
													$nomescalaevaluacionAux3 = $regescalaevaluacion["nomescalaevaluacion"];
													$percentescalaevaluacionAux3 = $regescalaevaluacion["percentescalaevaluacion"];
													echo "<option value='".$codescalaevaluacionAux3."'>";
													echo $percentescalaevaluacionAux3." - ".$nomescalaevaluacionAux3;
													echo "</option>";
												}
											?>
										</select>
									</td>
									<td><input type="submit" value="Asignar"></td>
								</tr>
							</table>
						</form>
						<hr />
						<?php						
					}
					
					if (((isset($_REQUEST["rutpersona"])) and ($_REQUEST["rutpersona"] != "")) and
						((isset($_REQUEST["action"])) and ($_REQUEST["action"] == "buscarpersona")))
					{
						//DESPLIEGA DATOS DE CARGO Y COMPETENCIA
						
						// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
						$rutpersonaAux = $_REQUEST["rutpersona"];

						// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
						$tabla = "persona";
						$campo = "rutpersona";

						// LLAMADA A FUNCION DE CONSULTA
						$punteroconsulta = consultatodo($tabla, $campo, $rutpersonaAux);
						
						// FORMATEO DE LOS RESULTADOS
						$regpersona = mysqli_fetch_assoc($punteroconsulta);
							
						if ($regpersona["rutpersona"] == "")
						{
							echo "Persona inexistente</br></br>";
							?>
							<hr />
							<?php
						}
						else
						{
							// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
							$rutpersonaAux2 = $regpersona["rutpersona"];
							
							//PRESENTACION DE DATOS DEL CARGO
							
							despliegapersona($rutpersonaAux2);
							?>
							<hr />
							
							<p><b>Objetivos Generales Asignados:</b></p>
							<?php
								$resultqobjetivoxpersona = consultatodo("objetivoxpersona", "rutpersona", $rutpersonaAux2);
								$rows = mysqli_num_rows($resultqobjetivoxpersona);
								if ($rows != "0")
								{
									echo "<table border='1'>";
									echo "<tr><th align='left'><font size = '2'>Tipo de objetivo</font></th><th align='left'><font size = '2'>A&ntilde;o</font></th><th align='left'><font size = '2'>Descripci&oacute;n</font></th><th align='left'><font size = '2'>Eval. Esperada</font></th></th></tr>";
									while ($regobjetivoxpersona = mysqli_fetch_assoc($resultqobjetivoxpersona))
									{
										$codobjetivoAux4 = $regobjetivoxpersona["codobjetivo"];
										if ($resultqobjetivo = consultatodo("objetivo", "codobjetivo", $codobjetivoAux4))
										{
											$regobjetivo = mysqli_fetch_assoc($resultqobjetivo);
											$descobjetivoAux4 = $regobjetivo["descobjetivo"];
											$agnoobjetivoAux4 = $regobjetivo["agnoobjetivo"];
											$codtipoobjetivoAux4 = $regobjetivo["codtipoobjetivo"];
											if ($resultqtipoobjetivo = consultatodo("tipoobjetivo", "codtipoobjetivo", $codtipoobjetivoAux4))
											{
												$regtipoobjetivo = mysqli_fetch_assoc($resultqtipoobjetivo);
												$nomtipoobjetivoAux4 = $regtipoobjetivo["nomtipoobjetivo"];
											}
										}
										//Preparacion para despliegue de Nivel esperado.
										//rescata codigo evaluacion esperada desde tabla objetivoxpersona.
										$codevaluacionesperadaAux4 = $regobjetivoxpersona["codevaluacionesperada"];
										//consulta en tabla escalaevaluacion con el codigo anterior.
										$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionesperadaAux4);
										$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
										//Obtiene codigo y nombre de escala de evaluación.
										$codescalaevaluacionAux4 = $regescalaevaluacion["codescalaevaluacion"];
										$nomescalaevaluacionAux4 = $regescalaevaluacion["nomescalaevaluacion"];
										$percentescalaevaluacionAux4 = $regescalaevaluacion["percentescalaevaluacion"];
										if (($codtipoobjetivoAux4 == 1) and ($agnoobjetivoAux4 == $agnoactual))
										{
											//Despliegue de registro completo en tabla html.
											echo "<tr><td align='left'><font size = '2'>".$nomtipoobjetivoAux4."</font></td><td align='right'><font size = '2'>".$agnoobjetivoAux4."</font></td><td align='left'><font size = '2'>".$descobjetivoAux4."</font></td><td align='right'><font size = '2'>".$percentescalaevaluacionAux4." - ".$nomescalaevaluacionAux4."</font></td></tr>";
										}
									}
									echo "</table>";
									echo "</br>";
								}
								else
								{
									echo "No se han asociado objetivos";
								}
							?>
							<hr />
							
							<p><b>Asignar objetivos</b></p>
							<form action="evaluacionasignaobjgen.php" method="post">
								<input type="text" name="rutpersona" style="visibility:hidden" value="<?=$rutpersonaAux2?>" readonly> 
								<input type="text" name="action" style="visibility:hidden" value="asignaobjetivo" readonly> 
								<table border = "0">
									<tr><th align="left">Objetivo</th>	<th>:</th>
										<td>
											<select name="codobjetivo" style='width:300'>
												<?php
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													$codtipoobjetivogeneral = 1; //OJO PARAMETRO DURO.
													$resultqobjetivogeneral = consultatodo("objetivo", "codtipoobjetivo", $codtipoobjetivogeneral);
													while ($regobjetivogeneral = mysqli_fetch_assoc($resultqobjetivogeneral))
													{
														$codobjetivonoasignado = $regobjetivogeneral["codobjetivo"];
														$codobjetivoxpersonaAux7 = $rutpersonaAux2."-".$codobjetivonoasignado;
														$resultqobjetivoxpersona = consultatodo("objetivoxpersona", "codobjetivoxpersona", $codobjetivoxpersonaAux7);
														$rowsobjetivoxpersona = mysqli_num_rows($resultqobjetivoxpersona);
														if ($rowsobjetivoxpersona == 0)
														{
															$agnoobjetivoasignable = $agnoactual;
															$agnoobjetivocurrent = $regobjetivogeneral["agnoobjetivo"];
															if ($agnoobjetivoasignable == $agnoobjetivocurrent)
															{
																echo "<option value='".$regobjetivogeneral["codobjetivo"]."'>";
																echo $regobjetivogeneral["descobjetivo"];
																echo "</option>";
															}
														}
													}
												?>
											</select>
										</td>
										<td><input type="submit" value="Asignar"></td>
									</tr>
								</table>
							</form>
							</br>
							<hr />
							</br>
							</br>
							<table>
								<tr>
									<td width='280' align='left' valign='top'>
										<form action="evaluacionasignaobjespec.php" method="post">
											<input type="submit" style="width:270; height:70" value="Asignar Objetivos Espec&iacute;ficos">
											<input type="text" name="rutpersona" style="visibility:hidden" size = "5" value="<?=$rutpersonaAux2?>" readonly>
											<input type="text" name="action" style="visibility:hidden" value="buscarpersona" readonly>
										</form>
									</td>
									<td width='280' align='right'>
										<form action="guardaevaluacion.php" method="post">
											<input type="submit" style="width:270; height:70" value="Finalizar">
											<input type="text" name="rutpersona" style="visibility:hidden" size = "5" value="<?=$rutpersonaAux2?>" readonly>
											<input type="text" name="agnoeva" style="visibility:hidden" size = "5" value="<?=$agnoeva?>" readonly>
										</form>
									</td>
								</tr>
							</table>
							<hr />
							<?php
						}
					}
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Asigna - Desasigna Objetivos
					</title>
					<link href="../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("ASIGNAR / DESASIGNAR OBJETIVOS A CARGOS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>										