<?php
	include "../../lib/handWebEva.php";
	include "../../lib/handVarEva.php";
	include "../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$rutusuarioevareg = $regusuarioeva["rutusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if (($codtipousuarioevaAux == 1) or ($codtipousuarioevaAux == 3))
	{
		?>
		<html>
			<head>
				<title>
					Evaluaci&oacute;n de personal
				</title>
				<link href="../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				<style type="text/css" media="all">@media all {div.saltopagina{display:none;}}</style>
				<style type="text/css" media="print">@media print {#menu {display:none;} div.saltopagina{display:block; page-break-before:always;}}</style>
			</head>
			<body>
				<?php
					$webserver = nomserverweb();
					
					if ($codtipousuarioevaAux == 1)
					{
						cabezal("EVALUACI&Oacute;N DE PERSONAL");
					}
					
					if ($codtipousuarioevaAux == 3)
					{
						cabezalevaluador("EVALUACI&Oacute;N DE PERSONAL");
					}
					
					?>
					<hr />
					<div id="botonup">
						<table>
							<tr>
								<td width='600' align='left' valign='top'>
									<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
								</td>
							</tr>
						</table>
					</div>
					<hr />
					<?php
					
					$agnoactual = date("Y");
					$agnoeva = $agnoactual - 1;
					
					if (((isset($_REQUEST["rutpersona"])) and ($_REQUEST["rutpersona"] != "")) and
					    ((isset($_REQUEST["rutevaluador"])) and ($_REQUEST["rutevaluador"] != "")) and
						((isset($_REQUEST["action"])) and ($_REQUEST["action"] == "buscarpersona")))
					{
						echo "<table><tr><th><font size='4'>Evaluaci&oacute;n de Personal</font></th><th><font size='4'>".$agnoeva."</font></th></tr></table>";
						echo "<hr />";
						//=========================================================================
						//BUSCA Y DESPLIEGA DATOS DE EVALUADO.
						//=========================================================================
						
						// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
						$rutpersonaAux = $_REQUEST["rutpersona"];
						
						$codtempevaluacionAux = $rutpersonaAux."-".$agnoeva;

						// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
						$tabla = "persona";
						$campo = "rutpersona";

						// LLAMADA A FUNCION DE CONSULTA
						$punteroconsulta = consultatodo($tabla, $campo, $rutpersonaAux);
						
						// FORMATEO DE LOS RESULTADOS
						$regpersona = mysqli_fetch_assoc($punteroconsulta);
							
						if ($regpersona["rutpersona"] == "")
						{
							echo "Persona Inexistente</br></br>";
							?>
							<table>
								<tr>
									<td><button><a href="http://<?php echo $webserver;?>/eva/main.php">Men&uacute; Principal</a></button></td>
								</tr>
							</table>
							<?php
						}
						else
						{
							// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
							$nompersonaAux2 = $regpersona["nompersona"];
							$appaternoAux2 = $regpersona["appaterno"];
							$apmaternoAux2 = $regpersona["apmaterno"];
							$rutpersonaAux2 = $regpersona["rutpersona"];
							$fechaingresoAux2 = $regpersona["fechaingreso"];
							
							$codcargopersonaAux2 = $regpersona["codcargo"];
							$resultqcargopersona = consultatodo("cargo", "codcargo", $codcargopersonaAux2);
							$regcargopersona = mysqli_fetch_assoc($resultqcargopersona);
							$nomcargopersonaAux2 = $regcargopersona["nomcargo"];
							
							$codareapersonaAux2 = $regpersona["codarea"];
							$resultqareapersona = consultatodo("area", "codarea", $codareapersonaAux2);
							$regareapersona = mysqli_fetch_assoc($resultqareapersona);
							$nomareapersonaAux2 = $regareapersona["nomarea"];
							
							//PRESENTACION DE DATOS DEL CARGO
							echo "<table border='1'><th align='left' bgcolor='#0080FF' width='1000'><font color='white' size = '2'>1.- Datos del evaluado</font></th></table>";
							?>
							<hr />
							<table border = "0">
								<tr><th align="left"><font size="2">Nombres</font></th>				<th><font size="2">:</font></th>	<td><font size="2"><?php echo $nompersonaAux2;?></font></td></tr>
								<tr><th align="left"><font size="2">Ap. Paterno</font></th>			<th><font size="2">:</font></th>	<td><font size="2"><?php echo $appaternoAux2;?></font></td></tr>
								<tr><th align="left"><font size="2">Ap. Materno</font></th>			<th><font size="2">:</font></th>	<td><font size="2"><?php echo $apmaternoAux2;?></font></td></tr>
								<tr><th align="left"><font size="2">RUT</font></th>					<th><font size="2">:</font></th>	<td><font size="2"><?php echo $rutpersonaAux2;?></font></td></tr>
								<tr><th align="left"><font size="2">F. de Ingreso</font></th>		<th><font size="2">:</font></th> 	<td><font size="2"><?php echo $fechaingresoAux2;?></font></td></tr>
								<tr><th align="left"><font size="2">Cargo</font></th>				<th><font size="2">:</font></th> 	<td><font size="2"><?php echo $nomcargopersonaAux2;?></font></td></tr>
								<tr><th align="left"><font size="2">&Aacute;rea</font></th>				<th><font size="2">:</font></th>	<td><font size="2"><?php echo $nomareapersonaAux2;?></font></td></tr>
							</table>
							<hr />
							<?php
						}
						//=========================================================================
						//FIN PROCESO BUSCA Y DESPLIEGA DATOS DE EVALUADO.
						//=========================================================================
						
						//=========================================================================
						//BUSCA Y DESPLIEGA DATOS DE EVALUADOR.
						//=========================================================================
						
						// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
						$rutevaluadorAux = $_REQUEST["rutevaluador"];

						// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
						$tabla2 = "evaluador";
						$campo2 = "rutevaluador";

						// LLAMADA A FUNCION DE CONSULTA
						$punteroconsulta2 = consultatodo($tabla2, $campo2, $rutevaluadorAux);
						
						// FORMATEO DE LOS RESULTADOS
						$regevaluador = mysqli_fetch_assoc($punteroconsulta2);
							
						if ($regevaluador["rutevaluador"] == "")
						{
							echo "Evaluador Inexistente</br></br>";
							?>
							<table>
								<tr>
									<td><button><a href="http://<?php echo $webserver;?>/eva/main.php">Men&uacute; Principal</a></button></td>
								</tr>
							</table>
							<?php
						}
						else
						{
							// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
							$nomevaluadorAux2 = $regevaluador["nomevaluador"];
							$appaternoevaAux2 = $regevaluador["appaterno"];
							$apmaternoevaAux2 = $regevaluador["apmaterno"];
							$rutevaluadorAux2 = $regevaluador["rutevaluador"];
							
							//PRESENTACION DE DATOS DEL CARGO
							echo "<table border='1'><th align='left' bgcolor='#0080FF' width='1000'><font color='white' size = '2'>2.- Datos del evaluador</font></th></table>";
							?>
							<hr />
								<table border = "0">
									<tr><th align="left"><font size="2">Nombres</font></th>			<th><font size="2">:</font></th>	<td><font size="2"><?php echo $nomevaluadorAux2;?></font></td></tr>
									<tr><th align="left"><font size="2">Ap. Paterno</font></th>		<th><font size="2">:</font></th>	<td><font size="2"><?php echo $appaternoevaAux2;?></font></td></tr>
									<tr><th align="left"><font size="2">Ap. Materno</font></th>		<th><font size="2">:</font></th>	<td><font size="2"><?php echo $apmaternoevaAux2;?></font></td></tr>
									<tr><th align="left"><font size="2">RUT</font></th>				<th><font size="2">:</font></th>	<td><font size="2"><?php echo $rutevaluadorAux2;?></font></td></tr>
								</table>
							<hr />
							<?php
						}
						
						//=========================================================================
						//FIN PROCESO BUSCA Y DESPLIEGA DATOS DE EVALUADOR.
						//=========================================================================
						$resultqtempevaluacion = consultatodo("tempevaluacion", "codevaluacion", $codtempevaluacionAux);
						$rowstempevaluacion = mysqli_num_rows($resultqtempevaluacion);
						if($rowstempevaluacion > 0)
						{
							//=============================================================================
							//INICIO DEL FORMULARIO CON DATOS EXISTENTES EN TABLAS TEMPORALES
							//=============================================================================
							
							$regtempevaluacion = mysqli_fetch_assoc($resultqtempevaluacion);
							
							$codtempevaluacion = $regtempevaluacion["codevaluacion"];
							$ruttempevaluador = $regtempevaluacion["rutevaluador"];
							$ruttemppersona = $regtempevaluacion["rutpersona"];
							$agnotempevaluacion = $regtempevaluacion["agnoevaluacion"];
							$temppondfinalobjetivo = $regtempevaluacion["pondfinalobjetivo"];
							$temppondfinalcompetencia = $regtempevaluacion["pondfinalcompetencia"];
							$temppondfinalagno = $regtempevaluacion["pondfinalagno"];
							$tempcomentevaluado = $regtempevaluacion["comentevaluado"];
							$tempcomentevaluador = $regtempevaluacion["comentevaluador"];
							
							
							?>
							<!--<form action="evaluacionasignaobjgen.php" method="get">-->
							<form action="evaluacionasignaobjespec.php" method="get">
							<?php
							
								//=========================================================================
								//EVALUACION POR OBJETIVOS GENERALES.
								//=========================================================================
								echo "<table border='1'><th align='left' bgcolor='#0080FF' width='600'><font color='white' size = '2'>3.- Evaluaci&oacute;n por Objetivos</font></th></table>";
								echo "<hr />";
								echo "<table><th><font size = '2'>Objetivos Generales</font></th></table>";
								
								$resultqtempevaluacionxobjetivoxpersona = consultatodo("tempevaluacionxobjetivoxpersona", "codevaluacion", $codtempevaluacion);
								$rows = mysqli_num_rows($resultqtempevaluacionxobjetivoxpersona);
								if ($rows != "0")
								{
									$cuentaobjetivogen = 0;
									echo "<table border='1'>";
									echo "<tr><th align='left' width='250'><font size = '2'>Descripci&oacute;n</font></th><th align='left' width='150'><font size = '2'>Eval. Esperada</font></th><th align='left' width='200'><font size = '2'>Eval. Final</font></th></tr>";
									while ($regtempevaluacionxobjetivoxpersona = mysqli_fetch_assoc($resultqtempevaluacionxobjetivoxpersona))
									{
										$codtempobjetivoxpersonaAux4 = $regtempevaluacionxobjetivoxpersona["codobjetivoxpersona"];
										$codtempevaluacionobtenidaAux4 = $regtempevaluacionxobjetivoxpersona["codevaluacionobtenida"];
										if ($resultqobjetivoxpersona = consultatodo("objetivoxpersona", "codobjetivoxpersona", $codtempobjetivoxpersonaAux4))
										{
											$regobjetivoxpersona = mysqli_fetch_assoc($resultqobjetivoxpersona);
											$codobjetivoAux4 = $regobjetivoxpersona["codobjetivo"];
											if ($resultqobjetivo = consultatodo("objetivo", "codobjetivo", $codobjetivoAux4))
											{
												$regobjetivo = mysqli_fetch_assoc($resultqobjetivo);
												$descobjetivoAux4 = $regobjetivo["descobjetivo"];
												$agnoobjetivoAux4 = $regobjetivo["agnoobjetivo"];
												$codtipoobjetivoAux4 = $regobjetivo["codtipoobjetivo"];
												if ($resultqtipoobjetivo = consultatodo("tipoobjetivo", "codtipoobjetivo", $codtipoobjetivoAux4))
												{
													$regtipoobjetivo = mysqli_fetch_assoc($resultqtipoobjetivo);
													$nomtipoobjetivoAux4 = $regtipoobjetivo["nomtipoobjetivo"];
												}
											}
											//Preparacion para despliegue de Nivel esperado.
											//rescata codigo evaluacion esperada desde tabla objetivoxpersona.
											$codevaluacionesperadaAux4 = $regobjetivoxpersona["codevaluacionesperada"];
											//consulta en tabla escalaevaluacion con el codigo anterior.
											$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionesperadaAux4);
											$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
											//Obtiene codigo y nombre de escala de evaluación.
											$codescalaevaluacionAux4 = $regescalaevaluacion["codescalaevaluacion"];
											$nomescalaevaluacionAux4 = $regescalaevaluacion["nomescalaevaluacion"];
											$percentescalaevaluacionAux4 = $regescalaevaluacion["percentescalaevaluacion"];
											if (($codtipoobjetivoAux4 == 1) and ($agnoobjetivoAux4 == $agnoeva))
											{
												//Despliegue de registro completo en tabla html.
												echo "<tr><td align='left'><font size = '2'>".$descobjetivoAux4."</font></td><td align='right'><font size = '2'>".$percentescalaevaluacionAux4."% - ".$nomescalaevaluacionAux4."</font></td>";
												echo "<td><font size='2'><select name='".$codtempobjetivoxpersonaAux4."'>";
												echo "<option value=''>";
												echo "- Seleccione";
												echo "</option>";
												$resultqescalaevaluacion2 = llenacombo("escalaevaluacion");
												while ($regescalaevaluacion2 = mysqli_fetch_assoc($resultqescalaevaluacion2))
												{
													$codescalaevaluacion0 = $regescalaevaluacion2["codescalaevaluacion"];
													$percentescalaevaluacion0 = $regescalaevaluacion2["percentescalaevaluacion"];
													$nomescalaevaluacion0 = $regescalaevaluacion2["nomescalaevaluacion"];
													if ($codtempevaluacionobtenidaAux4 == $codescalaevaluacion0)
													{
														echo "<option value='".$codescalaevaluacion0."' selected>";
														echo $percentescalaevaluacion0."% - ".$nomescalaevaluacion0;
														echo "</option>";
													}
													else
													{
														echo "<option value='".$codescalaevaluacion0."'>";
														echo $percentescalaevaluacion0."% - ".$nomescalaevaluacion0;
														echo "</option>";
													}
												}
												echo "</select>";
												echo "</font></td></tr>";
												$cuentaobjetivogen = $cuentaobjetivogen + 1;
											}
										}
										eliminaregistro("tempevaluacionxobjetivoxpersona", "codevaluacionxobjetivoxpersona", $codtempevaluacionxobjetivoxpersona);
									}
									echo "</table>";
									echo "</br>";
									if ($cuentaobjetivogen == 0)
									{
										echo "No se han asociado objetivos generales";
									}
								}
								else
								{
									echo "No se han asociado objetivos";
								}
								echo "<hr />";
								
								//=========================================================================
								//FIN EVALUACION POR OBJETIVOS GENERALES.
								//=========================================================================
								
								//=========================================================================
								//EVALUACION POR OBJETIVOS ESPECIFICOS.
								//=========================================================================
								echo "<table><th><font size = '2'>Objetivos Espec&iacute;ficos</font></th></table>";
								
								$resultqtempevaluacionxobjetivoxpersona = consultatodo("tempevaluacionxobjetivoxpersona", "codevaluacion", $codtempevaluacion);
								$rows = mysqli_num_rows($resultqtempevaluacionxobjetivoxpersona);
								if ($rows != "0")
								{
									$cuentaobjetivoespec = 0;
									echo "<table border='1'>";
									echo "<tr><th align='left' width='250'><font size = '2'>Descripci&oacute;n</font></th><th align='left' width='150'><font size = '2'>Eval. Esperada</font></th><th align='left' width='200'><font size = '2'>Eval. Final</font></th></tr>";
									while ($regtempevaluacionxobjetivoxpersona = mysqli_fetch_assoc($resultqtempevaluacionxobjetivoxpersona))
									{
										$codtempevaluacionxobjetivoxpersona = $regtempevaluacionxobjetivoxpersona["codevaluacionxobjetivoxpersona"];
										$codtempobjetivoxpersonaAux4 = $regtempevaluacionxobjetivoxpersona["codobjetivoxpersona"];
										$codtempevaluacionobtenidaAux4 = $regtempevaluacionxobjetivoxpersona["codevaluacionobtenida"];
										if ($resultqobjetivoxpersona = consultatodo("objetivoxpersona", "codobjetivoxpersona", $codtempobjetivoxpersonaAux4))
										{
											$regobjetivoxpersona = mysqli_fetch_assoc($resultqobjetivoxpersona);
											$codobjetivoAux4 = $regobjetivoxpersona["codobjetivo"];
											if ($resultqobjetivo = consultatodo("objetivo", "codobjetivo", $codobjetivoAux4))
											{
												$regobjetivo = mysqli_fetch_assoc($resultqobjetivo);
												$descobjetivoAux4 = $regobjetivo["descobjetivo"];
												$agnoobjetivoAux4 = $regobjetivo["agnoobjetivo"];
												$codtipoobjetivoAux4 = $regobjetivo["codtipoobjetivo"];
												if ($resultqtipoobjetivo = consultatodo("tipoobjetivo", "codtipoobjetivo", $codtipoobjetivoAux4))
												{
													$regtipoobjetivo = mysqli_fetch_assoc($resultqtipoobjetivo);
													$nomtipoobjetivoAux4 = $regtipoobjetivo["nomtipoobjetivo"];
												}
											}
											//Preparacion para despliegue de Nivel esperado.
											//rescata codigo evaluacion esperada desde tabla objetivoxpersona.
											$codevaluacionesperadaAux4 = $regobjetivoxpersona["codevaluacionesperada"];
											//consulta en tabla escalaevaluacion con el codigo anterior.
											$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionesperadaAux4);
											$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
											//Obtiene codigo y nombre de escala de evaluación.
											$codescalaevaluacionAux4 = $regescalaevaluacion["codescalaevaluacion"];
											$nomescalaevaluacionAux4 = $regescalaevaluacion["nomescalaevaluacion"];
											$percentescalaevaluacionAux4 = $regescalaevaluacion["percentescalaevaluacion"];
											if (($codtipoobjetivoAux4 == 2) and ($agnoobjetivoAux4 == $agnoeva))
											{
												//Despliegue de registro completo en tabla html.
												echo "<tr><td align='left'><font size = '2'>".$descobjetivoAux4."</font></td><td align='right'><font size = '2'>".$percentescalaevaluacionAux4."% - ".$nomescalaevaluacionAux4."</font></td>";
												echo "<td><font size='2'><select name='".$codtempobjetivoxpersonaAux4."'>";
												echo "<option value=''>";
												echo "- Seleccione";
												echo "</option>";
												$resultqescalaevaluacion2 = llenacombo("escalaevaluacion");
												while ($regescalaevaluacion2 = mysqli_fetch_assoc($resultqescalaevaluacion2))
												{
													$codescalaevaluacion0 = $regescalaevaluacion2["codescalaevaluacion"];
													$percentescalaevaluacion0 = $regescalaevaluacion2["percentescalaevaluacion"];
													$nomescalaevaluacion0 = $regescalaevaluacion2["nomescalaevaluacion"];
													if ($codtempevaluacionobtenidaAux4 == $codescalaevaluacion0)
													{
														echo "<option value='".$codescalaevaluacion0."' selected>";
														echo $percentescalaevaluacion0."% - ".$nomescalaevaluacion0;
														echo "</option>";
													}
													else
													{
														echo "<option value='".$codescalaevaluacion0."'>";
														echo $percentescalaevaluacion0."% - ".$nomescalaevaluacion0;
														echo "</option>";
													}
												}
												echo "</select>";
												echo "</font></td></tr>";
												$cuentaobjetivoespec = $cuentaobjetivoespec + 1;
											}
										}
										eliminaregistro("tempevaluacionxobjetivoxpersona", "codevaluacionxobjetivoxpersona", $codtempevaluacionxobjetivoxpersona);
									}
									echo "</table>";
									echo "</br>";
									if ($cuentaobjetivoespec == 0)
									{
										echo "No se han asociado objetivos espec&iacute;ficos";
									}
								}
								else
								{
									echo "No se han asociado objetivos";
								}
								echo "<hr />";
								//=========================================================================
								//FIN EVALUACION POR OBJETIVOS ESPECIFICOS.
								//=========================================================================
								
								//=========================================================================
								//EVALUACION POR COMPETENCIAS CONDUCTUALES.
								//=========================================================================
								echo "<table border='1'><th align='left' bgcolor='#0080FF' width='600'><font color='white' size = '2'>4.- Evaluaci&oacute;n por Competencias</font></th></table>";
								echo "<hr />";
								echo "<table><th><font size = '2'>Competencias Conductuales</font></th></table>";
								
								$resultqtempevaluacionxcompetenciaxcargo = consultatodo("tempevaluacionxcompetenciaxcargo", "codevaluacion", $codtempevaluacion);
								$rows = mysqli_num_rows($resultqtempevaluacionxcompetenciaxcargo);
								if ($rows != "0")
								{
									$cuentacompetenciacon = 0;
									echo "<table border='1'>";
									echo "<tr><th align='left' width = '250'><font size = '2'>Nombre</font></th><th align='left' width = '150'><font size = '2'>Eval. Esperada</font></th><th align='left' width = '200'><font size = '2'>Eval. Final</font></th></tr>";
									while ($regtempevaluacionxcompetenciaxcargo = mysqli_fetch_assoc($resultqtempevaluacionxcompetenciaxcargo))
									{
										$codtempevaluacionxcompetenciaxcargo = $regtempevaluacionxcompetenciaxcargo["codevaluacionxcompetenciaxcargo"];
										$codtempcompetenciaxcargoAux4 = $regtempevaluacionxcompetenciaxcargo["codcompetenciaxcargo"];
										$codtempevaluacionobtenidaAux4 = $regtempevaluacionxcompetenciaxcargo["codevaluacionobtenida"];
										if ($resultqcompetenciaxcargo = consultatodo("competenciaxcargo", "codcompetenciaxcargo", $codtempcompetenciaxcargoAux4))
										{
											$regcompetenciaxcargo = mysqli_fetch_assoc($resultqcompetenciaxcargo);
											$codcompetenciaAux4 = $regcompetenciaxcargo["codcompetencia"];
											if ($resultqcompetencia = consultatodo("competencia", "codcompetencia", $codcompetenciaAux4))
											{
												$regcompetencia = mysqli_fetch_assoc($resultqcompetencia);
												$desccompetenciaAux4 = $regcompetencia["desccompetencia"];
												$agnocompetenciaAux4 = $regcompetencia["agnocompetencia"];
												$codtipocompetenciaAux4 = $regcompetencia["codtipocompetencia"];
												if ($resultqtipocompetencia = consultatodo("tipocompetencia", "codtipocompetencia", $codtipocompetenciaAux4))
												{
													$regtipocompetencia = mysqli_fetch_assoc($resultqtipocompetencia);
													$nomtipocompetenciaAux4 = $regtipocompetencia["nomtipocompetencia"];
												}
											}
											//Preparacion para despliegue de Nivel esperado.
											//rescata codigo evaluacion esperada desde tabla objetivoxpersona.
											$codevaluacionesperadaAux4 = $regcompetenciaxcargo["codevaluacionesperada"];
											//consulta en tabla escalaevaluacion con el codigo anterior.
											$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionesperadaAux4);
											$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
											//Obtiene codigo y nombre de escala de evaluación.
											$codescalaevaluacionAux4 = $regescalaevaluacion["codescalaevaluacion"];
											$nomescalaevaluacionAux4 = $regescalaevaluacion["nomescalaevaluacion"];
											$percentescalaevaluacionAux4 = $regescalaevaluacion["percentescalaevaluacion"];
											if (($codtipocompetenciaAux4 == 1) and ($agnocompetenciaAux4 == $agnoeva))
											{
												//Despliegue de registro completo en tabla html.
												echo "<tr><td align='left'><font size = '2'>".$desccompetenciaAux4."</font></td><td align='right'><font size = '2'>".$percentescalaevaluacionAux4."% - ".$nomescalaevaluacionAux4."</font></td>";
												echo "<td><font size='2'><select name='".$codtempcompetenciaxcargoAux4."'>";
												echo "<option value=''>";
												echo "- Seleccione";
												echo "</option>";
												$resultqescalaevaluacion2 = llenacombo("escalaevaluacion");
												while ($regescalaevaluacion2 = mysqli_fetch_assoc($resultqescalaevaluacion2))
												{
													$codescalaevaluacion0 = $regescalaevaluacion2["codescalaevaluacion"];
													$percentescalaevaluacion0 = $regescalaevaluacion2["percentescalaevaluacion"];
													$nomescalaevaluacion0 = $regescalaevaluacion2["nomescalaevaluacion"];
													
													if ($codtempevaluacionobtenidaAux4 == $codescalaevaluacion0)
													{
														echo "<option value='".$codescalaevaluacion0."' selected>";
														echo $percentescalaevaluacion0."% - ".$nomescalaevaluacion0;
														echo "</option>";
													}
													else
													{
														echo "<option value='".$codescalaevaluacion0."'>";
														echo $percentescalaevaluacion0."% - ".$nomescalaevaluacion0;
														echo "</option>";
													}
												}
												echo "</select>";
												echo "</font></td></tr>";
												$cuentacompetenciacon = $cuentacompetenciacon + 1;
											}
										}
										eliminaregistro("tempevaluacionxcompetenciaxcargo", "codevaluacionxcompetenciaxcargo", $codtempevaluacionxcompetenciaxcargo);
									}
									echo "</table>";
									echo "</br>";
									if ($cuentacompetenciacon == 0)
									{
										echo "No se han asociado competencias conductuales";
									}
								}
								else
								{
									echo "No se han asociado competencias";
								}
								echo "<hr />";
								
								//=========================================================================
								//FIN EVALUACION POR COMPETENCIAS CONDUCTUALES.
								//=========================================================================
								
								//=========================================================================
								//EVALUACION POR COMPETENCIAS TECNICAS.
								//=========================================================================
								
								echo "<table><th><font size = '2'>Competencias T&eacute;cnicas</font></th></table>";
								
								$resultqtempevaluacionxcompetenciaxcargo = consultatodo("tempevaluacionxcompetenciaxcargo", "codevaluacion", $codtempevaluacion);
								$rows = mysqli_num_rows($resultqtempevaluacionxcompetenciaxcargo);
								if ($rows != "0")
								{
									$cuentacompetenciatec = 0;
									echo "<table border='1'>";
									echo "<tr><th align='left' width = '250'><font size = '2'>Nombre</font></th><th align='left' width = '150'><font size = '2'>Eval. Esperada</font></th><th align='left' width = '200'><font size = '2'>Eval. Final</font></th></tr>";
									while ($regtempevaluacionxcompetenciaxcargo = mysqli_fetch_assoc($resultqtempevaluacionxcompetenciaxcargo))
									{
										$codtempevaluacionxcompetenciaxcargo = $regtempevaluacionxcompetenciaxcargo["codevaluacionxcompetenciaxcargo"];
										$codtempcompetenciaxcargoAux4 = $regtempevaluacionxcompetenciaxcargo["codcompetenciaxcargo"];
										$codtempevaluacionobtenidaAux4 = $regtempevaluacionxcompetenciaxcargo["codevaluacionobtenida"];
										if ($resultqcompetenciaxcargo = consultatodo("competenciaxcargo", "codcompetenciaxcargo", $codtempcompetenciaxcargoAux4))
										{
											$regcompetenciaxcargo = mysqli_fetch_assoc($resultqcompetenciaxcargo);
											$codcompetenciaAux4 = $regcompetenciaxcargo["codcompetencia"];
											if ($resultqcompetencia = consultatodo("competencia", "codcompetencia", $codcompetenciaAux4))
											{
												$regcompetencia = mysqli_fetch_assoc($resultqcompetencia);
												$desccompetenciaAux4 = $regcompetencia["desccompetencia"];
												$agnocompetenciaAux4 = $regcompetencia["agnocompetencia"];
												$codtipocompetenciaAux4 = $regcompetencia["codtipocompetencia"];
												if ($resultqtipocompetencia = consultatodo("tipocompetencia", "codtipocompetencia", $codtipocompetenciaAux4))
												{
													$regtipocompetencia = mysqli_fetch_assoc($resultqtipocompetencia);
													$nomtipocompetenciaAux4 = $regtipocompetencia["nomtipocompetencia"];
												}
											}
											//Preparacion para despliegue de Nivel esperado.
											//rescata codigo evaluacion esperada desde tabla objetivoxpersona.
											$codevaluacionesperadaAux4 = $regcompetenciaxcargo["codevaluacionesperada"];
											//consulta en tabla escalaevaluacion con el codigo anterior.
											$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionesperadaAux4);
											$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
											//Obtiene codigo y nombre de escala de evaluación.
											$codescalaevaluacionAux4 = $regescalaevaluacion["codescalaevaluacion"];
											$nomescalaevaluacionAux4 = $regescalaevaluacion["nomescalaevaluacion"];
											$percentescalaevaluacionAux4 = $regescalaevaluacion["percentescalaevaluacion"];
											if (($codtipocompetenciaAux4 == 2) and ($agnocompetenciaAux4 == $agnoeva))
											{
												//Despliegue de registro completo en tabla html.
												echo "<tr><td align='left'><font size = '2'>".$desccompetenciaAux4."</font></td><td align='right'><font size = '2'>".$percentescalaevaluacionAux4."% - ".$nomescalaevaluacionAux4."</font></td>";
												echo "<td><font size='2'><select name='".$codtempcompetenciaxcargoAux4."'>";
												echo "<option value=''>";
												echo "- Seleccione";
												echo "</option>";
												$resultqescalaevaluacion2 = llenacombo("escalaevaluacion");
												while ($regescalaevaluacion2 = mysqli_fetch_assoc($resultqescalaevaluacion2))
												{
													$codescalaevaluacion0 = $regescalaevaluacion2["codescalaevaluacion"];
													$percentescalaevaluacion0 = $regescalaevaluacion2["percentescalaevaluacion"];
													$nomescalaevaluacion0 = $regescalaevaluacion2["nomescalaevaluacion"];
													
													if ($codtempevaluacionobtenidaAux4 == $codescalaevaluacion0)
													{
														echo "<option value='".$codescalaevaluacion0."' selected>";
														echo $percentescalaevaluacion0."% - ".$nomescalaevaluacion0;
														echo "</option>";
													}
													else
													{
														echo "<option value='".$codescalaevaluacion0."'>";
														echo $percentescalaevaluacion0."% - ".$nomescalaevaluacion0;
														echo "</option>";
													}
												}
												echo "</select>";
												echo "</font></td></tr>";
												$cuentacompetenciatec = $cuentacompetenciatec + 1;
											}
										}
										eliminaregistro("tempevaluacionxcompetenciaxcargo", "codevaluacionxcompetenciaxcargo", $codtempevaluacionxcompetenciaxcargo);
									}
									echo "</table>";
									echo "</br>";
									if ($cuentacompetenciatec == 0)
									{
										echo "No se han asociado competencias t&eacute;cnicas";
									}
								}
								else
								{
									echo "No se han asociado competencias";
								}
								
								//=========================================================================
								//FIN EVALUACION POR COMPETENCIAS TECNICAS.
								//=========================================================================
								
								//=========================================================================
								//COMENTARIOS EVALUADOR.
								//=========================================================================
								
								echo "<hr />";
								echo "<table border='1'><th align='left' bgcolor='#0080FF' width='1000'><font color='white' size = '2'>5.- Comentarios del Evaluador</font></th></table>";
								echo "<hr />";
								?>
								<table border = "0">
									<tr><th align="left"><font size='2'>Max. 500 caracteres.</font></th></tr>
									<tr>							
										<td><textarea type="text" name="comentevaluador" cols="75" rows="5"><?=$tempcomentevaluador?></textarea></td>
									</tr>
								</table>
								<?php

								
								//=========================================================================
								//FIN COMENTARIOS EVALUADOR.
								//=========================================================================
								
								//=========================================================================
								//COMENTARIOS EVALUADO.
								//=========================================================================
								
								echo "<hr />";
								echo "<table border='1'><th align='left' bgcolor='#0080FF' width='1000'><font color='white' size = '2'>6.- Comentarios del Evaluado</font></th></table>";
								echo "<hr />";
								?>
								<table border = "0">
									<tr><th align="left"><font size='2'>Max. 500 caracteres.</font></th></tr>
									<tr>							
										<td><textarea type="text" name="comentevaluado" cols="75" rows="5"><?=$tempcomentevaluado?></textarea></td>
									</tr>
								</table>
								<?php
								
								//=========================================================================
								//FIN COMENTARIOS EVALUADO.
								//=========================================================================
								
								//=========================================================================
								//PLAN DE CAPACITACION
								//=========================================================================

								?>
								<hr />
								<table border='1'><th align='left' bgcolor='#0080FF' width='600'><font color='white' size = '2'>7.- Plan de Desarrollo y Capacitaci&oacute;n <?php echo $agnoactual;?></font></th></table>
								<hr />
								<?php
								
								if ($resultqcursossolic = consultatodo("cursossolic", "codevaluacion", $codtempevaluacion))
								{
									$cuentacursossolic = 1;
									echo "<table>";
									echo "<tr><th></th><th><font size='2'><p>Actividad</p></font></th><th><font size='2'><p>Mes</p></font></th><th><font size='2'><p>Prioridad</p></font></th></tr>";
									while ($regcursossolic = mysqli_fetch_assoc($resultqcursossolic))
									{
										$codcursossolicAux = $regcursossolic["codcursossolic"];
										$nomcursossolicAux = $regcursossolic["nomcursossolic"];
										$fechacursossolicAux = $regcursossolic["fechacursossolic"];
										$prioridadcursossolicAux = $regcursossolic["prioridadcursossolic"];
										echo "<tr><th><font size='2'><p>".$cuentacursossolic.".-</p></font></th><td><font size='2'><input type='text' name='nomcursossolic".$cuentacursossolic."' value='".$nomcursossolicAux."'></font></td>";
										echo "<td><font size='2'><select name='fechacursossolic".$cuentacursossolic."'>";
										if ($fechacursossolicAux == 'enero')
										{
											echo "<option value=''>- Seleccione</option>";
											echo "<option value='enero' selected>Enero</option>";
											echo "<option value='febrero'>Febrero</option>";
											echo "<option value='marzo'>Marzo</option>";
											echo "<option value='abril'>Abril</option>";
											echo "<option value='mayo'>Mayo</option>";
											echo "<option value='junio'>Junio</option>";
											echo "<option value='julio'>Julio</option>";	
											echo "<option value='agosto'>Agosto</option>";
											echo "<option value='septiembre'>Septiembre</option>";
											echo "<option value='octubre'>Octubre</option>";	
											echo "<option value='noviembre'>Noviembre</option>";
											echo "<option value='diciembre'>Diciembre</option>";												
										}
										if ($fechacursossolicAux == 'febrero')
										{
											echo "<option value=''>- Seleccione</option>";
											echo "<option value='enero'>Enero</option>";
											echo "<option value='febrero' selected>Febrero</option>";
											echo "<option value='marzo'>Marzo</option>";
											echo "<option value='abril'>Abril</option>";
											echo "<option value='mayo'>Mayo</option>";
											echo "<option value='junio'>Junio</option>";
											echo "<option value='julio'>Julio</option>";	
											echo "<option value='agosto'>Agosto</option>";
											echo "<option value='septiembre'>Septiembre</option>";
											echo "<option value='octubre'>Octubre</option>";	
											echo "<option value='noviembre'>Noviembre</option>";
											echo "<option value='diciembre'>Diciembre</option>";												
										}
										if ($fechacursossolicAux == 'marzo')
										{
											echo "<option value=''>- Seleccione</option>";
											echo "<option value='enero'>Enero</option>";
											echo "<option value='febrero'>Febrero</option>";
											echo "<option value='marzo' selected>Marzo</option>";
											echo "<option value='abril'>Abril</option>";
											echo "<option value='mayo'>Mayo</option>";
											echo "<option value='junio'>Junio</option>";
											echo "<option value='julio'>Julio</option>";	
											echo "<option value='agosto'>Agosto</option>";
											echo "<option value='septiembre'>Septiembre</option>";
											echo "<option value='octubre'>Octubre</option>";	
											echo "<option value='noviembre'>Noviembre</option>";
											echo "<option value='diciembre'>Diciembre</option>";												
										}
										if ($fechacursossolicAux == 'abril')
										{
											echo "<option value=''>- Seleccione</option>";
											echo "<option value='enero'>Enero</option>";
											echo "<option value='febrero'>Febrero</option>";
											echo "<option value='marzo'>Marzo</option>";
											echo "<option value='abril' selected>Abril</option>";
											echo "<option value='mayo'>Mayo</option>";
											echo "<option value='junio'>Junio</option>";
											echo "<option value='julio'>Julio</option>";	
											echo "<option value='agosto'>Agosto</option>";
											echo "<option value='septiembre'>Septiembre</option>";
											echo "<option value='octubre'>Octubre</option>";	
											echo "<option value='noviembre'>Noviembre</option>";
											echo "<option value='diciembre'>Diciembre</option>";												
										}
										if ($fechacursossolicAux == 'mayo')
										{
											echo "<option value=''>- Seleccione</option>";
											echo "<option value='enero'>Enero</option>";
											echo "<option value='febrero'>Febrero</option>";
											echo "<option value='marzo'>Marzo</option>";
											echo "<option value='abril'>Abril</option>";
											echo "<option value='mayo' selected>Mayo</option>";
											echo "<option value='junio'>Junio</option>";
											echo "<option value='julio'>Julio</option>";	
											echo "<option value='agosto'>Agosto</option>";
											echo "<option value='septiembre'>Septiembre</option>";
											echo "<option value='octubre'>Octubre</option>";	
											echo "<option value='noviembre'>Noviembre</option>";
											echo "<option value='diciembre'>Diciembre</option>";												
										}
										if ($fechacursossolicAux == 'junio')
										{
											echo "<option value=''>- Seleccione</option>";
											echo "<option value='enero'>Enero</option>";
											echo "<option value='febrero'>Febrero</option>";
											echo "<option value='marzo'>Marzo</option>";
											echo "<option value='abril'>Abril</option>";
											echo "<option value='mayo'>Mayo</option>";
											echo "<option value='junio' selected>Junio</option>";
											echo "<option value='julio'>Julio</option>";	
											echo "<option value='agosto'>Agosto</option>";
											echo "<option value='septiembre'>Septiembre</option>";
											echo "<option value='octubre'>Octubre</option>";	
											echo "<option value='noviembre'>Noviembre</option>";
											echo "<option value='diciembre'>Diciembre</option>";												
										}
										if ($fechacursossolicAux == 'julio')
										{
											echo "<option value=''>- Seleccione</option>";
											echo "<option value='enero'>Enero</option>";
											echo "<option value='febrero'>Febrero</option>";
											echo "<option value='marzo'>Marzo</option>";
											echo "<option value='abril'>Abril</option>";
											echo "<option value='mayo'>Mayo</option>";
											echo "<option value='junio'>Junio</option>";
											echo "<option value='julio' selected>Julio</option>";	
											echo "<option value='agosto'>Agosto</option>";
											echo "<option value='septiembre'>Septiembre</option>";
											echo "<option value='octubre'>Octubre</option>";	
											echo "<option value='noviembre'>Noviembre</option>";
											echo "<option value='diciembre'>Diciembre</option>";												
										}
										if ($fechacursossolicAux == 'agosto')
										{
											echo "<option value=''>- Seleccione</option>";
											echo "<option value='enero'>Enero</option>";
											echo "<option value='febrero'>Febrero</option>";
											echo "<option value='marzo'>Marzo</option>";
											echo "<option value='abril'>Abril</option>";
											echo "<option value='mayo'>Mayo</option>";
											echo "<option value='junio'>Junio</option>";
											echo "<option value='julio'>Julio</option>";	
											echo "<option value='agosto' selected>Agosto</option>";
											echo "<option value='septiembre'>Septiembre</option>";
											echo "<option value='octubre'>Octubre</option>";	
											echo "<option value='noviembre'>Noviembre</option>";
											echo "<option value='diciembre'>Diciembre</option>";												
										}
										if ($fechacursossolicAux == 'septiembre')
										{
											echo "<option value=''>- Seleccione</option>";
											echo "<option value='enero'>Enero</option>";
											echo "<option value='febrero'>Febrero</option>";
											echo "<option value='marzo'>Marzo</option>";
											echo "<option value='abril'>Abril</option>";
											echo "<option value='mayo'>Mayo</option>";
											echo "<option value='junio'>Junio</option>";
											echo "<option value='julio'>Julio</option>";	
											echo "<option value='agosto'>Agosto</option>";
											echo "<option value='septiembre' selected>Septiembre</option>";
											echo "<option value='octubre'>Octubre</option>";	
											echo "<option value='noviembre'>Noviembre</option>";
											echo "<option value='diciembre'>Diciembre</option>";												
										}
										if ($fechacursossolicAux == 'octubre')
										{
											echo "<option value=''>- Seleccione</option>";
											echo "<option value='enero'>Enero</option>";
											echo "<option value='febrero'>Febrero</option>";
											echo "<option value='marzo'>Marzo</option>";
											echo "<option value='abril'>Abril</option>";
											echo "<option value='mayo'>Mayo</option>";
											echo "<option value='junio'>Junio</option>";
											echo "<option value='julio'>Julio</option>";	
											echo "<option value='agosto'>Agosto</option>";
											echo "<option value='septiembre'>Septiembre</option>";
											echo "<option value='octubre' selected>Octubre</option>";	
											echo "<option value='noviembre'>Noviembre</option>";
											echo "<option value='diciembre'>Diciembre</option>";												
										}
										if ($fechacursossolicAux == 'noviembre')
										{
											echo "<option value=''>- Seleccione</option>";
											echo "<option value='enero'>Enero</option>";
											echo "<option value='febrero'>Febrero</option>";
											echo "<option value='marzo'>Marzo</option>";
											echo "<option value='abril'>Abril</option>";
											echo "<option value='mayo'>Mayo</option>";
											echo "<option value='junio'>Junio</option>";
											echo "<option value='julio'>Julio</option>";	
											echo "<option value='agosto'>Agosto</option>";
											echo "<option value='septiembre'>Septiembre</option>";
											echo "<option value='octubre'>Octubre</option>";	
											echo "<option value='noviembre' selected>Noviembre</option>";
											echo "<option value='diciembre'>Diciembre</option>";												
										}
										if ($fechacursossolicAux == 'diciembre')
										{
											echo "<option value=''>- Seleccione</option>";
											echo "<option value='enero'>Enero</option>";
											echo "<option value='febrero'>Febrero</option>";
											echo "<option value='marzo'>Marzo</option>";
											echo "<option value='abril'>Abril</option>";
											echo "<option value='mayo'>Mayo</option>";
											echo "<option value='junio'>Junio</option>";
											echo "<option value='julio'>Julio</option>";	
											echo "<option value='agosto'>Agosto</option>";
											echo "<option value='septiembre'>Septiembre</option>";
											echo "<option value='octubre'>Octubre</option>";	
											echo "<option value='noviembre'>Noviembre</option>";
											echo "<option value='diciembre' selected>Diciembre</option>";												
										}
										if ($fechacursossolicAux == '')
										{
											echo "<option value='' selected>- Seleccione</option>";
											echo "<option value='enero'>Enero</option>";
											echo "<option value='febrero'>Febrero</option>";
											echo "<option value='marzo'>Marzo</option>";
											echo "<option value='abril'>Abril</option>";
											echo "<option value='mayo'>Mayo</option>";
											echo "<option value='junio'>Junio</option>";
											echo "<option value='julio'>Julio</option>";	
											echo "<option value='agosto'>Agosto</option>";
											echo "<option value='septiembre'>Septiembre</option>";
											echo "<option value='octubre'>Octubre</option>";	
											echo "<option value='noviembre'>Noviembre</option>";
											echo "<option value='diciembre'>Diciembre</option>";												
										}
										echo "</select></font></td>";
										echo "<td><font size='2'><select name='prioridadcursossolic".$cuentacursossolic."'>";
										if ($prioridadcursossolicAux == 'alta')
										{
											echo "<option value=''>- Seleccione</option>";
											echo "<option value='alta' selected>Alta</option>";
											echo "<option value='media'>Media</option>";
											echo "<option value='baja'>Baja</option>";
										}
										if ($prioridadcursossolicAux == 'media')
										{
											echo "<option value=''>- Seleccione</option>";
											echo "<option value='alta'>Alta</option>";
											echo "<option value='media' selected>Media</option>";
											echo "<option value='baja'>Baja</option>";
										}
										if ($prioridadcursossolicAux == 'baja')
										{
											echo "<option value=''>- Seleccione</option>";
											echo "<option value='alta'>Alta</option>";
											echo "<option value='media'>Media</option>";
											echo "<option value='baja' selected>Baja</option>";
										}
										if ($prioridadcursossolicAux == '')
										{
											echo "<option value='' selected>- Seleccione</option>";
											echo "<option value='alta'>Alta</option>";
											echo "<option value='media'>Media</option>";
											echo "<option value='baja'>Baja</option>";
										}
										echo "</select></font></td></tr>";
										$cuentacursossolic = $cuentacursossolic + 1;
										eliminaregistro("cursossolic", "codcursossolic", $codcursossolicAux);
									}
									echo "</table>";
								}
								
								//=========================================================================
								//FIN PLAN CAPACITACION
								//=========================================================================
								
								eliminaregistro("tempevaluacion", "codevaluacion", $codtempevaluacion);
								
								?>
								</br>
								<hr />
								</br>
								<!--<input type="submit" value="               Enviar Evaluaci&oacute;n               ">-->
								<input type="submit" value="               Continuar >>               ">
								<input type="text" name="rutpersonasave" style="visibility:hidden" size = "5" value="<?=$rutpersonaAux2?>" readonly></br>
								<input type="text" name="rutevaluadorsave" style="visibility:hidden" size = "5" value="<?=$rutevaluadorAux2?>" readonly></br>
								<input type="text" name="codcargopersonasave" style="visibility:hidden" size = "5" value="<?=$codcargopersonaAux2?>" readonly></br>
								<input type="text" name="action" style="visibility:hidden" value="evaluarpersona" readonly>
								
							</form>
							<?php
							//==============================================================================
							//FIN DEL FORMULARIO
							//==============================================================================
						}
						else
						{
							//====================================================================
							//INICIO DEL FORMULARIO SIN DATOS EXISTENTES EN TABLAS TEMPORALES
							//====================================================================

							?>
							<!--<form action="evaluacionasignaobjgen.php" method="get">-->
							<form action="evaluacionasignaobjespec.php" method="get">
							<?php
							
								//=========================================================================
								//EVALUACION POR OBJETIVOS GENERALES.
								//=========================================================================
								echo "<table border='1'><th align='left' bgcolor='#0080FF' width='600'><font color='white' size = '2'>3.- Evaluaci&oacute;n por Objetivos</font></th></table>";
								echo "<hr />";
								echo "<table><th><font size = '2'>Objetivos Generales</font></th></table>";
								
								$resultqobjetivoxpersona = consultatodo("objetivoxpersona", "rutpersona", $rutpersonaAux2);
								$rows = mysqli_num_rows($resultqobjetivoxpersona);
								if ($rows != "0")
								{
									$cuentaobjetivogen = 0;
									echo "<table border='1'>";
									echo "<tr><th align='left' width='250'><font size = '2'>Descripci&oacute;n</font></th><th align='left' width='150'><font size = '2'>Eval. Esperada</font></th><th align='left' width='200'><font size = '2'>Eval. Final</font></th></tr>";
									while ($regobjetivoxpersona = mysqli_fetch_assoc($resultqobjetivoxpersona))
									{
										$codobjetivoxpersonaAux4 = $regobjetivoxpersona["codobjetivoxpersona"];
										$codobjetivoAux4 = $regobjetivoxpersona["codobjetivo"];
										if ($resultqobjetivo = consultatodo("objetivo", "codobjetivo", $codobjetivoAux4))
										{
											$regobjetivo = mysqli_fetch_assoc($resultqobjetivo);
											$descobjetivoAux4 = $regobjetivo["descobjetivo"];
											$agnoobjetivoAux4 = $regobjetivo["agnoobjetivo"];
											$codtipoobjetivoAux4 = $regobjetivo["codtipoobjetivo"];
											if ($resultqtipoobjetivo = consultatodo("tipoobjetivo", "codtipoobjetivo", $codtipoobjetivoAux4))
											{
												$regtipoobjetivo = mysqli_fetch_assoc($resultqtipoobjetivo);
												$nomtipoobjetivoAux4 = $regtipoobjetivo["nomtipoobjetivo"];
											}
										}
										//Preparacion para despliegue de Nivel esperado.
										//rescata codigo evaluacion esperada desde tabla objetivoxpersona.
										$codevaluacionesperadaAux4 = $regobjetivoxpersona["codevaluacionesperada"];
										//consulta en tabla escalaevaluacion con el codigo anterior.
										$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionesperadaAux4);
										$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
										//Obtiene codigo y nombre de escala de evaluación.
										$codescalaevaluacionAux4 = $regescalaevaluacion["codescalaevaluacion"];
										$nomescalaevaluacionAux4 = $regescalaevaluacion["nomescalaevaluacion"];
										$percentescalaevaluacionAux4 = $regescalaevaluacion["percentescalaevaluacion"];
										if (($codtipoobjetivoAux4 == 1) and ($agnoobjetivoAux4 == $agnoeva))
										{
											//Despliegue de registro completo en tabla html.
											echo "<tr><td align='left'><font size = '2'>".$descobjetivoAux4."</font></td><td align='right'><font size = '2'>".$percentescalaevaluacionAux4."% - ".$nomescalaevaluacionAux4."</font></td>";
											echo "<td><font size='2'><select name='".$codobjetivoxpersonaAux4."'>";
											echo "<option value=''>";
											echo "- Seleccione";
											echo "</option>";
											$resultqescalaevaluacion2 = llenacombo("escalaevaluacion");
											while ($regescalaevaluacion2 = mysqli_fetch_assoc($resultqescalaevaluacion2))
											{
												$codescalaevaluacion0 = $regescalaevaluacion2["codescalaevaluacion"];
												$percentescalaevaluacion0 = $regescalaevaluacion2["percentescalaevaluacion"];
												$nomescalaevaluacion0 = $regescalaevaluacion2["nomescalaevaluacion"];
												echo "<option value='".$codescalaevaluacion0."'>";
												echo $percentescalaevaluacion0."% - ".$nomescalaevaluacion0;
												echo "</option>";
											}
											echo "</select>";
											echo "</font></td></tr>";
											$cuentaobjetivogen = $cuentaobjetivogen + 1;
										}
									}
									echo "</table>";
									echo "</br>";
									if ($cuentaobjetivogen == 0)
									{
										echo "No se han asociado objetivos generales";
									}
								}
								else
								{
									echo "No se han asociado objetivos";
								}
								echo "<hr />";
								
								//=========================================================================
								//FIN EVALUACION POR OBJETIVOS GENERALES.
								//=========================================================================
								
								//=========================================================================
								//EVALUACION POR OBJETIVOS ESPECIFICOS.
								//=========================================================================
								echo "<table><th><font size = '2'>Objetivos Espec&iacute;ficos</font></th></table>";
								
								$resultqobjetivoxpersona = consultatodo("objetivoxpersona", "rutpersona", $rutpersonaAux2);
								$rows = mysqli_num_rows($resultqobjetivoxpersona);
								if ($rows != "0")
								{
									$cuentaobjetivoespec = 0;
									echo "<table border='1'>";
									echo "<tr><th align='left' width='250'><font size = '2'>Descripci&oacute;n</font></th><th align='left' width='150'><font size = '2'>Eval. Esperada</font></th><th align='left' width='200'><font size = '2'>Eval. Final</font></th></tr>";
									while ($regobjetivoxpersona = mysqli_fetch_assoc($resultqobjetivoxpersona))
									{
										$codobjetivoxpersonaAux4 = $regobjetivoxpersona["codobjetivoxpersona"];
										$codobjetivoAux4 = $regobjetivoxpersona["codobjetivo"];
										if ($resultqobjetivo = consultatodo("objetivo", "codobjetivo", $codobjetivoAux4))
										{
											$regobjetivo = mysqli_fetch_assoc($resultqobjetivo);
											$descobjetivoAux4 = $regobjetivo["descobjetivo"];
											$agnoobjetivoAux4 = $regobjetivo["agnoobjetivo"];
											$codtipoobjetivoAux4 = $regobjetivo["codtipoobjetivo"];
											if ($resultqtipoobjetivo = consultatodo("tipoobjetivo", "codtipoobjetivo", $codtipoobjetivoAux4))
											{
												$regtipoobjetivo = mysqli_fetch_assoc($resultqtipoobjetivo);
												$nomtipoobjetivoAux4 = $regtipoobjetivo["nomtipoobjetivo"];
											}
										}
										//Preparacion para despliegue de Nivel esperado.
										//rescata codigo evaluacion esperada desde tabla objetivoxpersona.
										$codevaluacionesperadaAux4 = $regobjetivoxpersona["codevaluacionesperada"];
										//consulta en tabla escalaevaluacion con el codigo anterior.
										$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionesperadaAux4);
										$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
										//Obtiene codigo y nombre de escala de evaluación.
										$codescalaevaluacionAux4 = $regescalaevaluacion["codescalaevaluacion"];
										$nomescalaevaluacionAux4 = $regescalaevaluacion["nomescalaevaluacion"];
										$percentescalaevaluacionAux4 = $regescalaevaluacion["percentescalaevaluacion"];
										if (($codtipoobjetivoAux4 == 2) and ($agnoobjetivoAux4 == $agnoeva))
										{
											//Despliegue de registro completo en tabla html.
											echo "<tr><td align='left'><font size = '2'>".$descobjetivoAux4."</font></td><td align='right'><font size = '2'>".$percentescalaevaluacionAux4."% - ".$nomescalaevaluacionAux4."</font></td>";
											echo "<td><font size='2'><select name='".$codobjetivoxpersonaAux4."'>";
											echo "<option value=''>";
											echo "- Seleccione";
											echo "</option>";
											$resultqescalaevaluacion2 = llenacombo("escalaevaluacion");
											while ($regescalaevaluacion2 = mysqli_fetch_assoc($resultqescalaevaluacion2))
											{
												$codescalaevaluacion0 = $regescalaevaluacion2["codescalaevaluacion"];
												$percentescalaevaluacion0 = $regescalaevaluacion2["percentescalaevaluacion"];
												$nomescalaevaluacion0 = $regescalaevaluacion2["nomescalaevaluacion"];
												echo "<option value='".$codescalaevaluacion0."'>";
												echo $percentescalaevaluacion0."% - ".$nomescalaevaluacion0;
												echo "</option>";
											}
											echo "</select>";
											echo "</font></td></tr>";
											$cuentaobjetivoespec = $cuentaobjetivoespec + 1;
										}
									}
									echo "</table>";
									echo "</br>";
									if ($cuentaobjetivoespec == 0)
									{
										echo "No se han asociado objetivos espec&iacute;ficos";
									}
								}
								else
								{
									echo "No se han asociado objetivos";
								}
								echo "<hr />";
								//=========================================================================
								//FIN EVALUACION POR OBJETIVOS ESPECIFICOS.
								//=========================================================================
								
								//=========================================================================
								//EVALUACION POR COMPETENCIAS CONDUCTUALES.
								//=========================================================================
								echo "<table border='1'><th align='left' bgcolor='#0080FF' width='600'><font color='white' size = '2'>4.- Evaluaci&oacute;n por Competencias</font></th></table>";
								echo "<hr />";
								echo "<table><th><font size = '2'>Competencias Conductuales</font></th></table>";
								
								$resultqcompetenciaxcargo = consultatodo("competenciaxcargo", "codcargo", $codcargopersonaAux2);
								$rows = mysqli_num_rows($resultqcompetenciaxcargo);
								if ($rows != "0")
								{
									$cuentacompetenciacon = 0;
									echo "<table border='1'>";
									echo "<tr><th align='left' width = '250'><font size = '2'>Nombre</font></th><th align='left' width = '150'><font size = '2'>Eval. Esperada</font></th><th align='left' width = '200'><font size = '2'>Eval. Final</font></th></tr>";
									while ($regcompetenciaxcargo = mysqli_fetch_assoc($resultqcompetenciaxcargo))
									{
										$codcompetenciaxcargoAux4 = $regcompetenciaxcargo["codcompetenciaxcargo"];
										$codcompetenciaAux4 = $regcompetenciaxcargo["codcompetencia"];
										if ($resultqcompetencia = consultatodo("competencia", "codcompetencia", $codcompetenciaAux4))
										{
											$regcompetencia = mysqli_fetch_assoc($resultqcompetencia);
											$nomcompetenciaAux4 = $regcompetencia["nomcompetencia"];
											$desccompetenciaAux4 = $regcompetencia["desccompetencia"];
											$codtipocompetenciaAux4 = $regcompetencia["codtipocompetencia"];
											if ($resultqtipocompetencia = consultatodo("tipocompetencia", "codtipocompetencia", $codtipocompetenciaAux4))
											{
												$regtipocompetencia = mysqli_fetch_assoc($resultqtipocompetencia);
												$nomtipocompetenciaAux4 = $regtipocompetencia["nomtipocompetencia"];
											}
										}
										//Preparacion para despliegue de Nivel esperado.
										//rescata codigo evaluacion esperada desde tabla objetivoxpersona.
										$codevaluacionesperadaAux4 = $regcompetenciaxcargo["codevaluacionesperada"];
										//consulta en tabla escalaevaluacion con el codigo anterior.
										$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionesperadaAux4);
										$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
										//Obtiene codigo y nombre de escala de evaluación.
										$codescalaevaluacionAux4 = $regescalaevaluacion["codescalaevaluacion"];
										$nomescalaevaluacionAux4 = $regescalaevaluacion["nomescalaevaluacion"];
										$percentescalaevaluacionAux4 = $regescalaevaluacion["percentescalaevaluacion"];
										if ($codtipocompetenciaAux4 == 1)
										{
											//Despliegue de registro completo en tabla html.
											echo "<tr><td align='left'><font size = '2'>".$nomcompetenciaAux4."</font></td><td align='right'><font size = '2'>".$percentescalaevaluacionAux4."% - ".$nomescalaevaluacionAux4."</font></td>";
											echo "<td><font size='2'><select name='".$codcompetenciaxcargoAux4."'>";
											echo "<option value=''>";
											echo "- Seleccione";
											echo "</option>";
											$resultqescalaevaluacion2 = llenacombo("escalaevaluacion");
											while ($regescalaevaluacion2 = mysqli_fetch_assoc($resultqescalaevaluacion2))
											{
												$codescalaevaluacion0 = $regescalaevaluacion2["codescalaevaluacion"];
												$percentescalaevaluacion0 = $regescalaevaluacion2["percentescalaevaluacion"];
												$nomescalaevaluacion0 = $regescalaevaluacion2["nomescalaevaluacion"];
												echo "<option value='".$codescalaevaluacion0."'>";
												echo $percentescalaevaluacion0."% - ".$nomescalaevaluacion0;
												echo "</option>";
											}
											echo "</select>";
											echo "</font></td></tr>";
											$cuentacompetenciacon = $cuentacompetenciacon + 1;
										}
									}
									echo "</table>";
									echo "</br>";
									if ($cuentacompetenciacon == 0)
									{
										echo "No se han asociado competencias conductuales";
									}
								}
								else
								{
									echo "No se han asociado competencias";
								}
								echo "<hr />";
								
								//=========================================================================
								//FIN EVALUACION POR COMPETENCIAS CONDUCTUALES.
								//=========================================================================
								
								//=========================================================================
								//EVALUACION POR COMPETENCIAS TECNICAS.
								//=========================================================================
								
								echo "<table><th><font size = '2'>Competencias T&eacute;cnicas</font></th></table>";
								
								$resultqcompetenciaxcargo = consultatodo("competenciaxcargo", "codcargo", $codcargopersonaAux2);
								$rows = mysqli_num_rows($resultqcompetenciaxcargo);
								if ($rows != "0")
								{
									$cuentacompetenciatec = 0;
									echo "<table border='1'>";
									echo "<tr><th align='left' width = '250'><font size = '2'>Nombre</font></th><th align='left' width = '150'><font size = '2'>Eval. Esperada</font></th><th align='left' width = '200'><font size = '2'>Eval. Final</font></th></tr>";
									while ($regcompetenciaxcargo = mysqli_fetch_assoc($resultqcompetenciaxcargo))
									{
										$codcompetenciaxcargoAux4 = $regcompetenciaxcargo["codcompetenciaxcargo"];
										$codcompetenciaAux4 = $regcompetenciaxcargo["codcompetencia"];
										if ($resultqcompetencia = consultatodo("competencia", "codcompetencia", $codcompetenciaAux4))
										{
											$regcompetencia = mysqli_fetch_assoc($resultqcompetencia);
											$nomcompetenciaAux4 = $regcompetencia["nomcompetencia"];
											$desccompetenciaAux4 = $regcompetencia["desccompetencia"];
											$codtipocompetenciaAux4 = $regcompetencia["codtipocompetencia"];
											if ($resultqtipocompetencia = consultatodo("tipocompetencia", "codtipocompetencia", $codtipocompetenciaAux4))
											{
												$regtipocompetencia = mysqli_fetch_assoc($resultqtipocompetencia);
												$nomtipocompetenciaAux4 = $regtipocompetencia["nomtipocompetencia"];
											}
										}
										//Preparacion para despliegue de Nivel esperado.
										//rescata codigo evaluacion esperada desde tabla objetivoxpersona.
										$codevaluacionesperadaAux4 = $regcompetenciaxcargo["codevaluacionesperada"];
										//consulta en tabla escalaevaluacion con el codigo anterior.
										$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionesperadaAux4);
										$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
										//Obtiene codigo y nombre de escala de evaluación.
										$codescalaevaluacionAux4 = $regescalaevaluacion["codescalaevaluacion"];
										$nomescalaevaluacionAux4 = $regescalaevaluacion["nomescalaevaluacion"];
										$percentescalaevaluacionAux4 = $regescalaevaluacion["percentescalaevaluacion"];
										if ($codtipocompetenciaAux4 == 2)
										{
											//Despliegue de registro completo en tabla html.
											echo "<tr><td align='left'><font size = '2'>".$nomcompetenciaAux4."</font></td><td align='left'><font size = '2'>".$desccompetenciaAux4."</font></td><td align='right'><font size = '2'>".$percentescalaevaluacionAux4."% - ".$nomescalaevaluacionAux4."</font></td>";
											echo "<td><font size='2'><select name='".$codcompetenciaxcargoAux4."'>";
											echo "<option value=''>";
											echo "- Seleccione";
											echo "</option>";
											$resultqescalaevaluacion2 = llenacombo("escalaevaluacion");
											while ($regescalaevaluacion2 = mysqli_fetch_assoc($resultqescalaevaluacion2))
											{
												$codescalaevaluacion0 = $regescalaevaluacion2["codescalaevaluacion"];
												$percentescalaevaluacion0 = $regescalaevaluacion2["percentescalaevaluacion"];
												$nomescalaevaluacion0 = $regescalaevaluacion2["nomescalaevaluacion"];
												echo "<option value='".$codescalaevaluacion0."'>";
												echo $percentescalaevaluacion0."% - ".$nomescalaevaluacion0;
												echo "</option>";
											}
											echo "</select>";
											echo "</font></td></tr>";
											$cuentacompetenciatec = $cuentacompetenciatec + 1;
										}
									}
									echo "</table>";
									echo "</br>";
									if ($cuentacompetenciatec == 0)
									{
										echo "No se han asociado competencias t&eacute;cnicas";
									}
								}
								else
								{
									echo "No se han asociado competencias";
								}
								
								//=========================================================================
								//FIN EVALUACION POR COMPETENCIAS TECNICAS.
								//=========================================================================
								
								//=========================================================================
								//COMENTARIOS EVALUADOR.
								//=========================================================================
								
								echo "<hr />";
								echo "<table border='1'><th align='left' bgcolor='#0080FF' width='1000'><font color='white' size = '2'>5.- Comentarios del Evaluador</font></th></table>";
								echo "<hr />";
								?>
								<table border = "0">
									<tr><th align="left"><font size='2'>Max. 500 caracteres.</font></th></tr>
									<tr>							
										<td><textarea type="text" name="comentevaluador" cols="75" rows="5"></textarea></td>
									</tr>
								</table>
								<?php

								
								//=========================================================================
								//FIN COMENTARIOS EVALUADOR.
								//=========================================================================
								
								//=========================================================================
								//COMENTARIOS EVALUADO.
								//=========================================================================
								
								echo "<hr />";
								echo "<table border='1'><th align='left' bgcolor='#0080FF' width='1000'><font color='white' size = '2'>6.- Comentarios del Evaluado</font></th></table>";
								echo "<hr />";
								?>
								<table border = "0">
									<tr><th align="left"><font size='2'>Max. 500 caracteres.</font></th></tr>
									<tr>							
										<td><textarea type="text" name="comentevaluado" cols="75" rows="5"></textarea></td>
									</tr>
								</table>
								<?php
								
								//=========================================================================
								//FIN COMENTARIOS EVALUADO.
								//=========================================================================
								
								//=========================================================================
								//PLAN DE CAPACITACION
								//=========================================================================
								
								?>
								<hr />
								<table border='1'><th align='left' bgcolor='#0080FF' width='600'><font color='white' size = '2'>7.- Plan de Desarrollo y Capacitaci&oacute;n <?php echo $agnoactual;?></font></th></table>
								<hr />
								<table>
								
									<tr><th></th><th><font size='2'><p>Actividad</p></font></th><th><font size='2'><p>Mes</p></font></th><th><font size='2'><p>Prioridad</p></font></th></tr>
									<tr><th><font size='2'><p>1.-</p></font></th><td><font size='2'><input type="text" name="nomcursossolic1"></font></td><td><font size='2'><select name='fechacursossolic1'><option value=''>- Seleccione</option><option value='enero'>Enero</option><option value='febrero'>Febrero</option><option value='marzo'>Marzo</option><option value='abril'>Abril</option><option value='mayo'>Mayo</option><option value='junio'>Junio</option><option value='julio'>Julio</option><option value='agosto'>Agosto</option><option value='septiembre'>Septiembre</option><option value='octubre'>Octubre</option><option value='noviembre'>Noviembre</option><option value='diciembre'>Diciembre</option></select></font></td><td><font size='2'><select name="prioridadcursossolic1"><option value=''>- Seleccione</option><option value='alta'>Alta</option><option value='media'>Media</option><option value='baja'>Baja</option></select></font></td></tr>
									<tr><th><font size='2'><p>2.-</p></font></th><td><font size='2'><input type="text" name="nomcursossolic2"></font></td><td><font size='2'><select name='fechacursossolic2'><option value=''>- Seleccione</option><option value='enero'>Enero</option><option value='febrero'>Febrero</option><option value='marzo'>Marzo</option><option value='abril'>Abril</option><option value='mayo'>Mayo</option><option value='junio'>Junio</option><option value='julio'>Julio</option><option value='agosto'>Agosto</option><option value='septiembre'>Septiembre</option><option value='octubre'>Octubre</option><option value='noviembre'>Noviembre</option><option value='diciembre'>Diciembre</option></select></font></td><td><font size='2'><select name="prioridadcursossolic2"><option value=''>- Seleccione</option><option value='alta'>Alta</option><option value='media'>Media</option><option value='baja'>Baja</option></select></font></td></tr>
									<tr><th><font size='2'><p>3.-</p></font></th><td><font size='2'><input type="text" name="nomcursossolic3"></font></td><td><font size='2'><select name='fechacursossolic3'><option value=''>- Seleccione</option><option value='enero'>Enero</option><option value='febrero'>Febrero</option><option value='marzo'>Marzo</option><option value='abril'>Abril</option><option value='mayo'>Mayo</option><option value='junio'>Junio</option><option value='julio'>Julio</option><option value='agosto'>Agosto</option><option value='septiembre'>Septiembre</option><option value='octubre'>Octubre</option><option value='noviembre'>Noviembre</option><option value='diciembre'>Diciembre</option></select></font></td><td><font size='2'><select name="prioridadcursossolic3"><option value=''>- Seleccione</option><option value='alta'>Alta</option><option value='media'>Media</option><option value='baja'>Baja</option></select></font></td></tr>
								</table>
								<?php
								
								//=========================================================================
								//FIN PLAN CAPACITACION
								//=========================================================================
								
								?>
								</br>
								<hr />
								</br>
								<!--<input type="submit" value="               Enviar Evaluaci&oacute;n               ">-->
								<input type="submit" value="               Continuar >>               ">
								<input type="text" name="rutpersonasave" style="visibility:hidden" size = "5" value="<?=$rutpersonaAux2?>" readonly></br>
								<input type="text" name="rutevaluadorsave" style="visibility:hidden" size = "5" value="<?=$rutevaluadorAux2?>" readonly></br>
								<input type="text" name="codcargopersonasave" style="visibility:hidden" size = "5" value="<?=$codcargopersonaAux2?>" readonly></br>
								<input type="text" name="action" style="visibility:hidden" value="evaluarpersona" readonly>
								
							</form>
							<?php
							//==============================================================================
							//FIN DEL FORMULARIO
							//==============================================================================
						}
					}
					else
					{					
						//INICIO DEL PROCESO, PARA BUSCAR PERSONAS EVALUABLES
						if ($_REQUEST["action"] == "")
						{
							?>
							<form action="evdesempeno.php" method="get">
								<input type="text" name="action" style="visibility:hidden" value="buscarpersona" readonly> 
								<table border="0">
									<tr>
										<th align="left">
											<font size="2">Seleccione</font>
										</th>
										<th>
											<font size="2">:</font>
										</th>
										<td>
											<font size="2">
											<select name="rutpersona">
												<?php
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													$resultqevaluador = consultatodo("evaluador", "rutevaluador", $rutusuarioevareg);
													if ($regevaluador = mysqli_fetch_assoc($resultqevaluador))
													{
														$rutevaluadorvalidado = $regevaluador["rutevaluador"];
														$resultqareaevaluada = consultatodo("area", "rutevaluador", $rutevaluadorvalidado);
														if ($regareaevaluada = mysqli_fetch_assoc($resultqareaevaluada))
														{
															$codareaevaluada = $regareaevaluada["codarea"];
															$resultqpersonaevaluada = consultatodo("persona", "codarea", $codareaevaluada);
															$cuentaevaluable = 0;
															while ($regpersonaevaluada = mysqli_fetch_assoc($resultqpersonaevaluada))
															{
																$fechaingreso = $regpersonaevaluada["fechaingreso"];
																$activo = $regpersonaevaluada["activo"];
																
																if (evaluable($fechaingreso, $activo))
																{
																	$rutpersona = $regpersonaevaluada["rutpersona"];
																	$codevaluaciontest = $rutpersona."-".$agnoeva;
																	$resultqevaluacion = consultatodo("evaluacion", "codevaluacion", $codevaluaciontest);
																	$rowsevaluaciontest = mysqli_num_rows($resultqevaluacion);
																	if ($rowsevaluaciontest == 0)
																	{
																		echo "<option value='".$regpersonaevaluada["rutpersona"]."'>";
																		echo $regpersonaevaluada["nompersona"]." ".$regpersonaevaluada["appaterno"]." ".$regpersonaevaluada["apmaterno"];
																		echo "</option>";
																		$cuentaevaluable = $cuentaevaluable + 1;
																	}
																}
															}
														}
													}
												?>
											</select>
											</font>
										</td>
										<td><input type="submit" value="Buscar"></td>
										<input type="text" name="rutevaluador" style="visibility:hidden" value="<?=$rutevaluadorvalidado?>" readonly> 
									</tr>
								</table>
								<?php
								if ($cuentaevaluable == 0)
								{
									echo "</br>";
									echo "<p><font size='2'>En esta area el proceso de evaluaci&oacute;n ya fue completado</font></p>";
								}
								?>
								</br>
							</form>
							<hr />
							<div id="botonbottom4">
								<table>
									<tr>
										<td width='35' align='center' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
										</td>
										<td width='565' align='right' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../images/logout.jpg' width='30' height='30' title='Salir'></a>
										</td>
									</tr>
								</table>
							</div>
							<hr />
							<?php
						}
						//FIN INICIO DEL PROCESO, PARA BUSCAR PERSONAS EVALUABLES
					}
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Evaluaci&oacute;n de personas
					</title>
					<link href="../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("EVALUAR PERSONAS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>										