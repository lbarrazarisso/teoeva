<?php
	include "../../lib/handWebEva.php";
	include "../../lib/handVarEva.php";
	include "../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	//if ($codtipousuarioevaAux == 1)
	if (($codtipousuarioevaAux == 1) or ($codtipousuarioevaAux == 3))
	{
		?>
			<html>
				<head>
					<title>
						Eva - Evaluaci&oacute;n
					</title>
					<link href="../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
					<style type="text/css" media="all">@media all {div.saltopagina{display:none;}}</style>
					<style type="text/css" media="print">@media print {#menu {display:none;} div.saltopagina{display:block; page-break-before:always;}}</style>
				</head>
				<body>
					<?php
						//cabezal("EVALUACI&OacuteN;");
						$webserver = nomserverweb();
						
						if ($codtipousuarioevaAux == 1)
						{
							cabezal("REPORTES");
						}
						
						if ($codtipousuarioevaAux == 3)
						{
							cabezalevaluador("REPORTES");
						}
						if (((isset($_REQUEST["rutpersona"])) and ($_REQUEST["rutpersona"] <> "")) and
						    ((isset($_REQUEST["agnoeva"])) and ($_REQUEST["agnoeva"] <> "")))
						{
							$rutpersonaAux = $_REQUEST["rutpersona"];
							$agnoevaAux = $_REQUEST["agnoeva"];
							$codevaluacionAux = $rutpersonaAux."-".$agnoevaAux;
							?>
							<script type='text/javascript'>
								function imprimir() 
								{ 
									botonprint.style.visibility = 'hidden';
									botonmenu.style.visibility = 'hidden';
									window.print(); 
								}
							</script>
							<div id="botonprint">
								<table>
									<tr>
										<td width='55' align='right' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../images/menuprincipal.jpg' width='50' height='50' title='Men&uacute; Principal'></a>
										</td>
										<td width='55' align='right' valign='bottom'>
											<form>
												<input type=image src='../../images/imprimir.jpg' width='50' height='50' title='Imprimir' value='Imprimir' onClick='imprimir()'>
											</form>
										</td>
										<td width='435' align='right' valign='bottom'>
											
										</td>
										<td width='55' align='right' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../images/logout.jpg' width='50' height='50' title='Salir'></a>
										</td>
									</tr>
								</table>
							</div>
							<hr />
							<?php
							despliegaevaluacion($codevaluacionAux);
							?>
							<hr />
							<div id="botonmenu">
								<table>
									<tr>
										<td width='55' align='right' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../images/menuprincipal.jpg' width='50' height='50' title='Men&uacute; Principal'></a>
										</td>
										<td width='55' align='right' valign='bottom'>
											<form>
												<input type=image src='../../images/imprimir.jpg' width='50' height='50' title='Imprimir' value='Imprimir' onClick='imprimir()'>
											</form>
										</td>
										<td width='435' align='right' valign='bottom'>
											
										</td>
										<td width='55' align='right' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../images/logout.jpg' width='50' height='50' title='Salir'></a>
										</td>
									</tr>
								</table>
							</div>
							<?php
						}
						else
						{
							mensaje("No registra evaluacion");
						}
						pie();
					?>
				</body>
			</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Evaluaci&oacute;n
					</title>
					<link href="../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("Evaluacion");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>					