<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Asignacion de competencias a un cargo
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
				<?php
				
					cabezal("ASIGNAR COMPETENCIAS A UN CARGO");
					$webserver = nomserverweb();
					?>
					<p><b>ASIGNAR COMPETENCIAS CONDUCTUALES A PERSONAS</b></p>
					<hr />
					<div id="botonup">
						<table>
							<tr>
								<td width='35' align='center' valign='top'>
									<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Menu Principal'></a>
								</td>
								<td width='35' align='center' valign='top'>
									<a href='http://<?php echo $webserver;?>/eva/procesos/asignadores/competenciaxcargo/asdescompetenciaxcargo.php'><img src='../../../images/back.jpg' width='30' height='30' title='volver'></a>
								</td>
								<td width='530' align='right' valign='top'>
									<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
								</td>
							</tr>
						</table>
					</div>
					<hr />
					<?php
					$agnoactual = date("Y");
					
					// INSERCIÓN DE REGISTRO EN TABLA competenciaxcargo
					if (((isset($_REQUEST["codcompetencia"])) and ($_REQUEST["codcompetencia"] != "")) and
						((isset($_REQUEST["codevaluacionesperada"])) and ($_REQUEST["codevaluacionesperada"] != "")) and
						((isset($_REQUEST["action"])) and ($_REQUEST["action"] == "asignaevaluacionesperada")) and
						((isset($_REQUEST["codcargo"])) and ($_REQUEST["codcargo"] != "")))
					{
						$tabla = "competenciaxcargo";
						$cod1 = $_REQUEST["codcargo"];
						$cod2 = $_REQUEST["codcompetencia"];
						$codcompetenciaxcargo = $cod1."-".$cod2;
						$codcompetenciaAux3 = $_REQUEST["codcompetencia"];
						$codcargoAux3 = $_REQUEST["codcargo"];
						$codevaluacionesperadaAux = $_REQUEST["codevaluacionesperada"];
						$valores = "'".$codcompetenciaxcargo."', '".$codcompetenciaAux3."', '".$codcargoAux3."', '".$codevaluacionesperadaAux."'";
						insertaregistro($tabla, $valores);
						//mensaje("Competencia asignada");
						echo "<hr />";
						$_REQUEST["action"] = "buscarcargo";
					}
					
					if (((isset($_REQUEST["codcompetencia"])) and ($_REQUEST["codcompetencia"] != "")) and
						((isset($_REQUEST["action"])) and ($_REQUEST["action"] == "asignacompetencia")) and
     					((isset($_REQUEST["codcargo"])) and ($_REQUEST["codcargo"] != "")))
					{
						//Presenta datos del cargo
						
						// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
						$codcargoAux3 = $_REQUEST["codcargo"];
						// LLAMADA A FUNCION DE CONSULTA
						$resultqcargoAux3 = consultatodo("cargo", "codcargo", $codcargoAux3);
						// FORMATEO DE LOS RESULTADOS
						$regcargoAux3 = mysqli_fetch_assoc($resultqcargoAux3);
						//Rescate de código de cargo
						$codcargoAux3 = $regcargoAux3["codcargo"];
						//despliega cargo
						despliegacargo($codcargoAux3);
						
						//Presenta datos de LA competencia en cuestión.
						
						// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
						$codcompetenciaAux3 = $_REQUEST["codcompetencia"];
						// LLAMADA A FUNCION DE CONSULTA
						$resultqcompetenciaAux3 = consultatodo("competencia", "codcompetencia", $codcompetenciaAux3);
						// FORMATEO DE LOS RESULTADOS
						$regcompetenciaAux3 = mysqli_fetch_assoc($resultqcompetenciaAux3);
						//Rescate de código de competencia
						$codcompetenciaAux3 = $regcompetenciaAux3["codcompetencia"];
						//Despliega Competencia.
						despliegacompetencia($codcompetenciaAux3);
						
						//Presenta form con select de nivelesperado
						?>
						<form action="asignacompetenciaxcargo.php" method="post">
							<input type="text" name="codcargo" style="visibility:hidden" value="<?=$codcargoAux3?>" readonly> 
							<input type="text" name="codcompetencia" style="visibility:hidden" value="<?=$codcompetenciaAux3?>" readonly>
							<input type="text" name="action" style="visibility:hidden" value="asignaevaluacionesperada" readonly>
							<table border = "0">
								<tr><th align="left">Evaluacion Esperada</th>	<th>:</th>
									<td>
										<select name="codevaluacionesperada" width = "10">
											<?php
												$resultqescalaevaluacion = llenacombo("escalaevaluacion");
												echo "<option value=''>";
												echo "- Seleccione";
												echo "</option>";
												while ($regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion))
												{
													$codescalaevaluacionAux3 = $regescalaevaluacion["codescalaevaluacion"];
													$nomescalaevaluacionAux3 = $regescalaevaluacion["nomescalaevaluacion"];
													$percentescalaevaluacionAux3 = $regescalaevaluacion["percentescalaevaluacion"];
													echo "<option value='".$codescalaevaluacionAux3."'>";
													echo $percentescalaevaluacionAux3." - ".$nomescalaevaluacionAux3;
													echo "</option>";
												}
											?>
										</select>
									</td>
									<td><input type="submit" value="Asignar"></td>
								</tr>
							</table>
						</form>
						<hr />
						<div id="botonbottom1">
							<table>
								<tr>
									<td width='35' align='center' valign='top'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Menu Principal'></a>
									</td>
									<td width='35' align='center' valign='top'>
										<a href='http://<?php echo $webserver;?>/eva/procesos/asignadores/competenciaxcargo/asdescompetenciaxcargo.php'><img src='../../../images/back.jpg' width='30' height='30' title='volver'></a>
									</td>
									<td width='530' align='right' valign='top'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php						
					}
					
					if (((isset($_REQUEST["codcargo"])) and ($_REQUEST["codcargo"] != "")) and
						((isset($_REQUEST["action"])) and ($_REQUEST["action"] == "buscarcargo")))
					{
						//DESPLIEGA DATOS DE CARGO Y COMPETENCIA
						
						// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
						$codcargoAux = $_REQUEST["codcargo"];

						// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
						$tabla = "cargo";
						$campo = "codcargo";

						// LLAMADA A FUNCION DE CONSULTA
						$punteroconsulta = consultatodo($tabla, $campo, $codcargoAux);
						
						// FORMATEO DE LOS RESULTADOS
						$regcargo = mysqli_fetch_assoc($punteroconsulta);
							
						if ($regcargo["codcargo"] == "")
						{
							echo "Cargo no definido</br></br>";
							?>
							<hr />
							<div id="botonbottom2">
								<table>
									<tr>
										<td width='35' align='center' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Menu Principal'></a>
										</td>
										<td width='35' align='center' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/procesos/asignadores/competenciaxcargo/asdescompetenciaxcargo.php'><img src='../../../images/back.jpg' width='30' height='30' title='volver'></a>
										</td>
										<td width='530' align='right' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
										</td>
									</tr>
								</table>
							</div>
							<?php
						}
						else
						{
							// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
							$codcargoAux2 = $regcargo["codcargo"];
							
							//PRESENTACION DE DATOS DEL CARGO
							
							despliegacargo($codcargoAux2);
							?>
							<hr />
							
							<p><b>Competencias Asignadas:</b></p>
							<?php
								$resultqcompetenciaxcargo = consultatodo("competenciaxcargo", "codcargo", $codcargoAux2);
								$rows = mysqli_num_rows($resultqcompetenciaxcargo);
								if ($rows != "0")
								{
									echo "<table border='1'>";
									echo "<tr><th align='left'><font size = '2'>Tipo de competencia</font></th><th align='left'><font size = '2'>A&ntilde;o</font></th><th align='left'><font size = '2'>Descripci&oacute;n</font></th><th align='left'><font size = '2'>Eval. Esperada</font></th></th></tr>";
									while ($regcompetenciaxcargo = mysqli_fetch_assoc($resultqcompetenciaxcargo))
									{
										$codcompetenciaAux4 = $regcompetenciaxcargo["codcompetencia"];
										if ($resultqcompetencia = consultatodo("competencia", "codcompetencia", $codcompetenciaAux4))
										{
											$regcompetencia = mysqli_fetch_assoc($resultqcompetencia);
											$desccompetenciaAux4 = $regcompetencia["desccompetencia"];
											$agnocompetenciaAux4 = $regcompetencia["agnocompetencia"];
											$codtipocompetenciaAux4 = $regcompetencia["codtipocompetencia"];
											if ($resultqtipocompetencia = consultatodo("tipocompetencia", "codtipocompetencia", $codtipocompetenciaAux4))
											{
												$regtipocompetencia = mysqli_fetch_assoc($resultqtipocompetencia);
												$nomtipocompetenciaAux4 = $regtipocompetencia["nomtipocompetencia"];
											}
										}
										//Preparacion para despliegue de Nivel esperado.
										//rescata codigo evaluacion esperada desde tabla competenciaxcargo.
										$codevaluacionesperadaAux4 = $regcompetenciaxcargo["codevaluacionesperada"];
										//consulta en tabla escalaevaluacion con el codigo anterior.
										$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionesperadaAux4);
										$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
										//Obtiene codigo y nombre de escala de evaluación.
										$codescalaevaluacionAux4 = $regescalaevaluacion["codescalaevaluacion"];
										$nomescalaevaluacionAux4 = $regescalaevaluacion["nomescalaevaluacion"];
										$percentescalaevaluacionAux4 = $regescalaevaluacion["percentescalaevaluacion"];
										if (($codtipocompetenciaAux4 == 1) and ($agnocompetenciaAux4 == $agnoactual))
										{
											//Despliegue de registro completo en tabla html.
											echo "<tr><td align='left'><font size = '2'>".$nomtipocompetenciaAux4."</font></td><td align='right'><font size = '2'>".$agnocompetenciaAux4."</font></td><td align='left'><font size = '2'>".$desccompetenciaAux4."</font></td><td align='right'><font size = '2'>".$percentescalaevaluacionAux4." - ".$nomescalaevaluacionAux4."</font></td></tr>";
										}
									}
									echo "</table>";
									echo "</br>";
								}
								else
								{
									echo "No se han asociado competencias";
								}
							?>
							<hr />
							</br>
							<b>Asignar competencias</b>
							<form action="asignacompetenciaxcargo.php" method="post">
								<input type="text" name="codcargo" style="visibility:hidden" value="<?=$codcargoAux2?>" readonly> 
								<input type="text" name="action" style="visibility:hidden" value="asignacompetencia" readonly> 
								<table border = "0">
									<tr><th align="left">Competencia</th>	<th>:</th>
										<td>
											<select name="codcompetencia" width = "10">
												<?php
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													$codtipocompetenciaconductual = 1; //OJO PARAMETRO DURO.
													$resultqcompetenciaconductual = consultatodo("competencia", "codtipocompetencia", $codtipocompetenciaconductual);
													while ($regcompetenciaconductual = mysqli_fetch_assoc($resultqcompetenciaconductual))
													{
														$codcompetencianoasignada = $regcompetenciaconductual["codcompetencia"];
														$codcompetenciaxcargoAux7 = $codcargoAux2."-".$codcompetencianoasignada;
														$resultqcompetenciaxcargo = consultatodo("competenciaxcargo", "codcompetenciaxcargo", $codcompetenciaxcargoAux7);
														$rowscompetenciaxcargo = mysqli_num_rows($resultqcompetenciaxcargo);
														if ($rowscompetenciaxcargo == 0)
														{
															$agnocompetenciaasignable = $agnoactual;
															$agnocompetenciacurrent = $regcompetenciaconductual["agnocompetencia"];
															if ($agnocompetenciaasignable == $agnocompetenciacurrent)
															{
																echo "<option value='".$regcompetenciaconductual["codcompetencia"]."'>";
																echo $regcompetenciaconductual["desccompetencia"];
																echo "</option>";
															}
														}
													}
												?>
											</select>
										</td>
										<td><input type="submit" value="Asignar"></td>
									</tr>
								</table>
							</form>
							<hr />
							<table>
								<tr>
									<td>
										<form action="desasignacompetenciaxcargo.php" method="post">
											<input type="submit" value="Quitar Competencia">
											<input type="text" name="codcargo" style="visibility:hidden" size = "3" value="<?=$codcargoAux2?>" readonly>
										</form>
									</td>
								</tr>
							</table>
							<hr />
							<div id="botonbottom3">
								<table>
									<tr>
										<td width='35' align='center' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Menu Principal'></a>
										</td>
										<td width='35' align='center' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/procesos/asignadores/competenciaxcargo/asdescompetenciaxcargo.php'><img src='../../../images/back.jpg' width='30' height='30' title='volver'></a>
										</td>
										<td width='530' align='right' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
										</td>
									</tr>
								</table>
							</div>
							<hr />
							<?php
						}
					}
					else
					{
						//SELECT PARA BUSCAR CARGO
						if ($_REQUEST["action"] == "")
						{
							?>
							<form action="asignacompetenciaxcargo.php" method="get">
								<input type="text" name="action" style="visibility:hidden" value="buscarcargo" readonly> 
								<table border="0">
									<tr>
										<th align="left">
											Seleccione Cargo
										</th>
										<th>
											:
										</th>
										<td>
											<select name="codcargo">
												<?php
													$resultqcargo = llenacombo("cargo");
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													while ($regcargo = mysqli_fetch_assoc($resultqcargo))
													{
														echo "<option value='".$regcargo["codcargo"]."'>";
														echo $regcargo["nomcargo"];
														echo "</option>";
													}
												?>
											</select>
										</td>
										<td><input type="submit" value="Buscar"></td>
									</tr>
								</table>
								</br>
							</form>
							<hr />
							<div id="botonbottom4">
								<table>
									<tr>
										<td width='35' align='center' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Menu Principal'></a>
										</td>
										<td width='35' align='center' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/procesos/asignadores/competenciaxcargo/asdescompetenciaxcargo.php'><img src='../../../images/back.jpg' width='30' height='30' title='volver'></a>
										</td>
										<td width='530' align='right' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
										</td>
									</tr>
								</table>
							</div>
							<hr />
							<?php
						}
					}
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Asigna - Desasigna Competencias
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("ASIGNAR / DESASIGNAR COMPETENCIAS A CARGOS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCION</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Menu Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>					