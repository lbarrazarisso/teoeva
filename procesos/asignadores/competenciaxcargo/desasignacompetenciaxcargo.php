<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Asignacion de competencias a un cargo
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
				<?php
				
					cabezal("ASIGNAR COMPETENCIAS A UN CARGO");
					$webserver = nomserverweb();
					
					if (((isset($_REQUEST["codcompetenciaxcargo"])) and ($_REQUEST["codcompetenciaxcargo"] != "")) and ((isset($_REQUEST["codcargo"])) and ($_REQUEST["codcargo"] != "")))
					{
						$tabla = "competenciaxcargo";
						$codcompetenciaxcargoAux = $_REQUEST["codcompetenciaxcargo"];
						eliminaregistro($tabla, "codcompetenciaxcargo", $codcompetenciaxcargoAux);
					}
					
					if ((isset($_REQUEST["codcargo"])) and ($_REQUEST["codcargo"] != ""))
					{
						//DESPLIEGA DATOS DE CARGO Y COMPETENCIA
						
						// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
						$codcargoAux = $_REQUEST["codcargo"];

						// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
						$tabla = "cargo";
						$campo = "codcargo";

						// LLAMADA A FUNCION DE CONSULTA
						$punteroconsulta = consultatodo($tabla, $campo, $codcargoAux);
						
						// FORMATEO DE LOS RESULTADOS
						$regcargo = mysqli_fetch_assoc($punteroconsulta);
							
						if ($regcargo["codcargo"] == "")
						{
							echo "Cargo no definido</br></br>";
							?>
							<table>
								<tr>
									<td><button><a style="text-decoration: none;" href="http://<?php echo $webserver;?>/eva/procesos/asignadores/competenciaxcargo/asdescompetenciaxcargo.php">Volver</a></button></td>
									<td><button><a href="http://<?php echo $webserver;?>/eva/main.php">Menu Principal</a></button></td>
								</tr>
							</table>
							<?php
						}
						else
						{
							// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
							$codcargoAux2 = $regcargo["codcargo"];
							
							//PRESENTACION DE DATOS DEL CARGO
							
							despliegacargo($codcargoAux2);
							?>
							<hr />
							
							<p><b>Competencias Asignadas:</b></p>
							<?php
								$resultqcompetenciaxcargo = consultatodo("competenciaxcargo", "codcargo", $codcargoAux2);
								$rows = mysqli_num_rows($resultqcompetenciaxcargo);
								if ($rows != "0")
								{
									echo "<table border='1'>";
									echo "<tr><th align='left' cellpadding='0'><font size = '2'>Nombre</font></th><th align='left'><font size = '2'>Codigo</font></th><th align='left'><font size = '2'>Tipo</font></th><th align='left'><font size = '2'>Eval. Esperada</font></th><th align='left'><font size = '2'>Descripcion</font></th></tr>";
									while ($regcompetenciaxcargo = mysqli_fetch_assoc($resultqcompetenciaxcargo))
									{
										$codcompetenciaAux4 = $regcompetenciaxcargo["codcompetencia"];
										if ($resultqcompetencia = consultatodo("competencia", "codcompetencia", $codcompetenciaAux4))
										{
											$regcompetencia = mysqli_fetch_assoc($resultqcompetencia);
											$nomcompetenciaAux4 = $regcompetencia["nomcompetencia"];
											$desccompetenciaAux4 = $regcompetencia["desccompetencia"];
											$codtipocompetenciaAux4 = $regcompetencia["codtipocompetencia"];
											if ($resultqtipocompetencia = consultatodo("tipocompetencia", "codtipocompetencia", $codtipocompetenciaAux4))
											{
												$regtipocompetencia = mysqli_fetch_assoc($resultqtipocompetencia);
												$nomtipocompetenciaAux4 = $regtipocompetencia["nomtipocompetencia"];
											}
										}
										//Preparacion para despliegue de Nivel esperado.
										//rescata codigo evaluacion esperada desde tabla competenciaxcargo.
										$codevaluacionesperadaAux4 = $regcompetenciaxcargo["codevaluacionesperada"];
										//consulta en tabla escalaevaluacion con el codigo anterior.
										$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionesperadaAux4);
										$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
										//Obtiene codigo y nombre de escala de evaluación.
										$codescalaevaluacionAux4 = $regescalaevaluacion["codescalaevaluacion"];
										$nomescalaevaluacionAux4 = $regescalaevaluacion["nomescalaevaluacion"];
										$percentescalaevaluacionAux4 = $regescalaevaluacion["percentescalaevaluacion"];
										//Despliegue de registro completo en tabla html.
										echo "<tr><td align='left'><font size = '2'>".$nomcompetenciaAux4."</font></td><td align='right'><font size = '2'>".$regcompetenciaxcargo["codcompetenciaxcargo"]."</font></td><td align='left'><font size = '2'>".$nomtipocompetenciaAux4."</font></td><td align='right'><font size = '2'>".$percentescalaevaluacionAux4." - ".$nomescalaevaluacionAux4."</font></td><td align='left'><font size = '2'>".$desccompetenciaAux4."</font></td></tr>";
									}
									echo "</table>";
									echo "</br>";
								}
								else
								{
									echo "No se han asociado competencias";
								}
							?>
							</br>
							<hr />
							<p><b>Desasignar competencias</b></p>
							<form action="desasignacompetenciaxcargo.php" method="post">
								<input type="text" name="codcargo" style="visibility:hidden" value="<?=$codcargoAux2?>" readonly> 
								<!--<input type="text" name="action" style="visibility:hidden" value="asignacompetencia" readonly> -->
								<table border = "0">
									<tr><th align="left">Competencia</th>	<th>:</th>
										<td>
											<select name="codcompetenciaxcargo" width = "10">
												<?php
													$resultq = llenacombo("competencia");
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													while ($regcompetencia = mysqli_fetch_assoc($resultq))
													{
														$codcompetencianoasignada = $regcompetencia["codcompetencia"];
														$codcompetenciaxcargoAux7 = $codcargoAux2."-".$codcompetencianoasignada;
														$resultqcompetenciaxcargo = consultatodo("competenciaxcargo", "codcompetenciaxcargo", $codcompetenciaxcargoAux7);
														$rowscompetenciaxcargo = mysqli_num_rows($resultqcompetenciaxcargo);
														if ($rowscompetenciaxcargo > 0)
														{
															echo "<option value='".$codcompetenciaxcargoAux7."'>";
															echo $regcompetencia["nomcompetencia"];
															echo "</option>";
														}
													}
												?>
											</select>
										</td>
										<td><input type="submit" value="Desasignar"></td>
									</tr>
								</table>
							</form>
							<hr />
							<table>
								<tr>
									<td>
										<form action="asignacompetenciaxcargo.php" method="post">
											<input type="submit" value="Asignar Competencia">
											<input type="text" name="codcargo" style="visibility:hidden" size = "3" value="<?=$codcargoAux2?>" readonly>
										</form>
									</td>
									<td valign = "top"><button><a style="text-decoration: none; color:black" href="http://<?php echo $webserver;?>/eva/main.php">Menu Principal</a></button></td>
								</tr>
							</table>
							<?php
						}
					}
					else
					{
						?>
						<form action="desasignacompetenciaxcargo.php" method="get">
							<table border="0">
								<tr>
									<th align="left">
										Seleccione Cargo
									</th>
									<th>
										:
									</th>
									<td>
										<select name="codcargo">
											<?php
												$resultqcargo = llenacombo("cargo");
												echo "<option value=''>";
												echo "- Seleccione";
												echo "</option>";
												while ($regcargo = mysqli_fetch_assoc($resultqcargo))
												{
													echo "<option value='".$regcargo["codcargo"]."'>";
													echo $regcargo["nomcargo"];
													echo "</option>";
												}
											?>
										</select>
									</td>
									<td><input type="submit" value="Buscar"></td>
								</tr>
							</table>
							</br>
						</form>
						<hr />
						<table>
							<tr>
								<td><button><a style="text-decoration: none;" href="http://<?php echo $webserver;?>/eva/procesos/asignadores/competenciaxcargo/asdescompetenciaxcargo.php">Volver</a></button></td>
								<td><button><a style="text-decoration: none;" href="http://<?php echo $webserver;?>/eva/main.php">Menu Principal</a></button></td>
							</tr>
						</table>
						<?php
					}
					
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Asigna - Desasigna Competencias
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("ASIGNAR / DESASIGNAR COMPETENCIAS A CARGOS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCION</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Menu Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>