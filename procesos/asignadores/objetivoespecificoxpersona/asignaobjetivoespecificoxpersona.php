<?php
	include "../../../lib/handWebEva.php";
	include "../../../lib/handVarEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$rutusuarioevareg = $regusuarioeva["rutusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if (($codtipousuarioevaAux == 1) or ($codtipousuarioevaAux == 3))
	{
		?>
		<html>
			<head>
				<title>
					Asignar objetivos Espec&iacute;ficos a una persona
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
				<?php
					$webserver = nomserverweb();
					
					if ($codtipousuarioevaAux == 1)
					{
						cabezal("ASIGNAR OBJETIVOS ESPEC&Iacute;FICOS A PERSONAS");
					}
					
					if ($codtipousuarioevaAux == 3)
					{
						cabezalevaluador("ASIGNAR OBJETIVOS ESPEC&Iacute;FICOS A PERSONAS");
					}					
					?>
					<p><b>ASIGNAR OBJETIVOS ESPEC&Iacute;FICOS A PERSONAS</b></p>
					<hr />
					<div id="botonup">
						<table>
							<tr>
								<td width='35' align='center' valign='top'>
									<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
								</td>
								<td width='35' align='center' valign='top'>
									<a href='http://<?php echo $webserver;?>/eva/procesos/asignadores/objetivoespecificoxpersona/asdesobjetivoespecificoxpersona.php'><img src='../../../images/back.jpg' width='30' height='30' title='Volver'></a>
								</td>
								<td width='530' align='right' valign='top'>
									<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
								</td>
							</tr>
						</table>
					</div>
					<hr />
					<?php
					$agnoactual = date("Y");
					
					// INSERCIÓN DE REGISTRO EN TABLA objetivoxpersona y objetivo.
					if (((isset($_REQUEST["codtipoobjetivo"])) and ($_REQUEST["codtipoobjetivo"] != "")) and
						((isset($_REQUEST["agnoobjetivo"])) and ($_REQUEST["agnoobjetivo"] != "")) and
						((isset($_REQUEST["descobjetivo"])) and ($_REQUEST["descobjetivo"] != "")) and
						((isset($_REQUEST["codevaluacionesperada"])) and ($_REQUEST["codevaluacionesperada"] != "")) and
						((isset($_REQUEST["action"])) and ($_REQUEST["action"] == "asignaevaluacionesperada")) and
						((isset($_REQUEST["rutpersona"])) and ($_REQUEST["rutpersona"] != "")))
					{
						//Guardar en tabla Objetivo
						$descobjetivoespecificosave = $_REQUEST["descobjetivo"];
						$agnoobjetivoespecificosave = $_REQUEST["agnoobjetivo"];
						$codtipoobjetivoespecificosave = $_REQUEST["codtipoobjetivo"];
						$valoresobjetivoespecificosave = "'', '".$descobjetivoespecificosave."', '".$agnoobjetivoespecificosave."', '".$codtipoobjetivoespecificosave."'";
						insertaregistro("objetivo", $valoresobjetivoespecificosave);
						
						$restultqobjetivoespecificosave = consultatodo("objetivo", "descobjetivo", $descobjetivoespecificosave);
						$regobjetivoespecificosave = mysqli_fetch_assoc($restultqobjetivoespecificosave);
						$codobjetivoespecificosave = $regobjetivoespecificosave["codobjetivo"];
						
						$tabla = "objetivoxpersona";
						$cod1 = $_REQUEST["rutpersona"];
						$cod2 = $codobjetivoespecificosave;
						$codobjetivoxpersona = $cod1."-".$cod2;
						$codobjetivoAux3 = $codobjetivoespecificosave;
						$rutpersonaAux3 = $_REQUEST["rutpersona"];
						$codevaluacionesperadaAux = $_REQUEST["codevaluacionesperada"];
						$valores = "'".$codobjetivoxpersona."', '".$codobjetivoAux3."', '".$rutpersonaAux3."', '".$codevaluacionesperadaAux."'";
						insertaregistro($tabla, $valores);
						$_REQUEST["action"] = "buscarpersona";
					}
					
					//SOLICITA LA EVALUACION ESPERADA
					if (((isset($_REQUEST["codtipoobjetivo"])) and ($_REQUEST["codtipoobjetivo"] != "")) and
						((isset($_REQUEST["agnoobjetivo"])) and ($_REQUEST["agnoobjetivo"] != "")) and
						((isset($_REQUEST["descobjetivo"])) and ($_REQUEST["descobjetivo"] != "")) and
						((isset($_REQUEST["action"])) and ($_REQUEST["action"] == "asignaobjetivo")) and
     					((isset($_REQUEST["rutpersona"])) and ($_REQUEST["rutpersona"] != "")))
					{
						//Presenta datos del persona
						
						// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
						$rutpersonaAux3 = $_REQUEST["rutpersona"];
						// LLAMADA A FUNCION DE CONSULTA
						$resultqpersonaAux3 = consultatodo("persona", "rutpersona", $rutpersonaAux3);
						// FORMATEO DE LOS RESULTADOS
						$regpersonaAux3 = mysqli_fetch_assoc($resultqpersonaAux3);
						//Rescate de código de persona
						$rutpersonaAux3 = $regpersonaAux3["rutpersona"];
						//despliega persona
						despliegapersona($rutpersonaAux3);
						
						//Presenta datos del objetivo en cuestión.
						
						// ALMACENAMIENTO DE Valores ingresados EN VARIABLE AUXILIAR
						//$codobjetivoAux3 = $_REQUEST["codobjetivo"];
						$descobjetivoAux3 = $_REQUEST["descobjetivo"];
						$agnoobjetivoAux3 = $_REQUEST["agnoobjetivo"];
						$codtipoobjetivoAux3 = $_REQUEST["codtipoobjetivo"];
						// LLAMADA A FUNCION DE CONSULTA TIPOOBJETIVO
						$resultqtipoobjetivoAux3 = consultatodo("tipoobjetivo", "codtipoobjetivo", $codtipoobjetivoAux3);
						// FORMATEO DE LOS RESULTADOS
						$regtipoobjetivoAux3 = mysqli_fetch_assoc($resultqtipoobjetivoAux3);
						//Rescate de nombre de tipo de objetivo
						$nomtipoobjetivoAux3 = $regtipoobjetivoAux3["nomtipoobjetivo"];
						//Despliega Objetivo.
						?>
						<hr />
						<font size="4"><b>Objetivo</b></font>
						<hr />
						<table border = "1">
							<!--<tr><th width="200" align="left"><font size = "2">Codigo de Objetivo</font></th>		<th><font size = "2">:</font></th>	<td width="300"><font size = "2"><?php //echo $codobjetivoShow;?></font></td></tr>-->
							<tr><th width="200" align="left"><font size = "2">Descripci&oacute;n</font></th>				<th><font size = "2">:</font></th>	<td width="300" align="justify"><font size = "2"><?php echo $descobjetivoAux3;?></font></td></tr>
							<tr><th width="200" align="left"><font size = "2">A&ntilde;o</font></th>						<th><font size = "2">:</font></th>	<td width="300"><font size = "2"><?php echo $agnoobjetivoAux3;?></font></td></tr>
							<tr><th width="200" align="left"><font size = "2">Tipo de Objetivo</font></th>			<th><font size = "2">:</font></th>	<td width="300"><font size = "2"><?php echo $nomtipoobjetivoAux3;?></font></td></tr>
						</table>
						<?php
						//Presenta form con select de nivelesperado
						?>
						<form action="asignaobjetivoespecificoxpersona.php" method="post">
							<input type="text" name="rutpersona" style="visibility:hidden" value="<?=$rutpersonaAux3?>" readonly> 
							<input type="text" name="descobjetivo" style="visibility:hidden" value="<?=$descobjetivoAux3?>" readonly>
							<input type="text" name="agnoobjetivo" style="visibility:hidden" value="<?=$agnoobjetivoAux3?>" readonly>
							<input type="text" name="codtipoobjetivo" style="visibility:hidden" value="<?=$codtipoobjetivoAux3?>" readonly>
							<input type="text" name="action" style="visibility:hidden" value="asignaevaluacionesperada" readonly>
							<table border = "0">
								<tr><th align="left">Evaluaci&oacute;n Esperada</th>	<th>:</th>
									<td>
										<select name="codevaluacionesperada" width = "10">
											<?php
												$resultqescalaevaluacion = llenacombo("escalaevaluacion");
												echo "<option value=''>";
												echo "- Seleccione";
												echo "</option>";
												while ($regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion))
												{
													$codescalaevaluacionAux3 = $regescalaevaluacion["codescalaevaluacion"];
													$nomescalaevaluacionAux3 = $regescalaevaluacion["nomescalaevaluacion"];
													$percentescalaevaluacionAux3 = $regescalaevaluacion["percentescalaevaluacion"];
													echo "<option value='".$codescalaevaluacionAux3."'>";
													echo $percentescalaevaluacionAux3." - ".$nomescalaevaluacionAux3;
													echo "</option>";
												}
											?>
										</select>
									</td>
									<td><input type="submit" value="Asignar"></td>
								</tr>
							</table>
						</form>
						<hr />
						<div id="botonbottom1">
							<table>
								<tr>
									<td width='35' align='center' valign='top'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='35' align='center' valign='top'>
										<a href='http://<?php echo $webserver;?>/eva/procesos/asignadores/objetivoespecificoxpersona/asdesobjetivoespecificoxpersona.php'><img src='../../../images/back.jpg' width='30' height='30' title='Volver'></a>
									</td>
									<td width='530' align='right' valign='top'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php						
					}
					//FIN PROCESO SOLICITA EVALUACION ESPERADA
					
					if (((isset($_REQUEST["rutpersona"])) and ($_REQUEST["rutpersona"] != "")) and
						((isset($_REQUEST["action"])) and ($_REQUEST["action"] == "buscarpersona")))
					{
						//DESPLIEGA DATOS DE PERSONA Y OBJETIVO
						
						// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
						$rutpersonaAux = $_REQUEST["rutpersona"];

						// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
						$tabla = "persona";
						$campo = "rutpersona";

						// LLAMADA A FUNCION DE CONSULTA
						$punteroconsulta = consultatodo($tabla, $campo, $rutpersonaAux);
						
						// FORMATEO DE LOS RESULTADOS
						$regpersona = mysqli_fetch_assoc($punteroconsulta);
							
						if ($regpersona["rutpersona"] == "")
						{
							echo "Persona Inexistente</br></br>";
							?>
							<hr />
							<div id="botonbottom2">
								<table>
									<tr>
										<td width='35' align='center' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
										</td>
										<td width='35' align='center' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/procesos/asignadores/objetivoespecificoxpersona/asdesobjetivoespecificoxpersona.php'><img src='../../../images/back.jpg' width='30' height='30' title='Volver'></a>
										</td>
										<td width='530' align='right' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
										</td>
									</tr>
								</table>
							</div>
							<?php
						}
						else
						{
							// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
							$rutpersonaAux2 = $regpersona["rutpersona"];
							
							//PRESENTACION DE DATOS DEL CARGO
							
							despliegapersona($rutpersonaAux2);
							?>
							<hr />
							
							<p><b>Objetivos Espec&iacute;ficos Asignados:</b></p>
							<?php
								$resultqobjetivoxpersona = consultatodo("objetivoxpersona", "rutpersona", $rutpersonaAux2);
								$rows = mysqli_num_rows($resultqobjetivoxpersona);
								if ($rows != "0")
								{
									echo "<table border='1'>";
									echo "<tr><th align='left'><font size = '2'>Tipo de objetivo</font></th><th align='left'><font size = '2'>A&ntilde;o</font></th><th align='left'><font size = '2'>Descripci&oacute;n</font></th><th align='left'><font size = '2'>Eval. Esperada</font></th></th></tr>";
									while ($regobjetivoxpersona = mysqli_fetch_assoc($resultqobjetivoxpersona))
									{
										$codobjetivoAux4 = $regobjetivoxpersona["codobjetivo"];
										if ($resultqobjetivo = consultatodo("objetivo", "codobjetivo", $codobjetivoAux4))
										{
											$regobjetivo = mysqli_fetch_assoc($resultqobjetivo);
											$descobjetivoAux4 = $regobjetivo["descobjetivo"];
											$agnoobjetivoAux4 = $regobjetivo["agnoobjetivo"];
											$codtipoobjetivoAux4 = $regobjetivo["codtipoobjetivo"];
											if ($resultqtipoobjetivo = consultatodo("tipoobjetivo", "codtipoobjetivo", $codtipoobjetivoAux4))
											{
												$regtipoobjetivo = mysqli_fetch_assoc($resultqtipoobjetivo);
												$nomtipoobjetivoAux4 = $regtipoobjetivo["nomtipoobjetivo"];
											}
										}
										//Preparacion para despliegue de Nivel esperado.
										//rescata codigo evaluacion esperada desde tabla objetivoxpersona.
										$codevaluacionesperadaAux4 = $regobjetivoxpersona["codevaluacionesperada"];
										//consulta en tabla escalaevaluacion con el codigo anterior.
										$resultqescalaevaluacion = consultatodo("escalaevaluacion", "codescalaevaluacion", $codevaluacionesperadaAux4);
										$regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion);
										//Obtiene codigo y nombre de escala de evaluación.
										$codescalaevaluacionAux4 = $regescalaevaluacion["codescalaevaluacion"];
										$nomescalaevaluacionAux4 = $regescalaevaluacion["nomescalaevaluacion"];
										$percentescalaevaluacionAux4 = $regescalaevaluacion["percentescalaevaluacion"];
										if (($codtipoobjetivoAux4 == 2) and ($agnoobjetivoAux4 == $agnoactual))
										{
											//Despliegue de registro completo en tabla html.
											echo "<tr><td align='left'><font size = '2'>".$nomtipoobjetivoAux4."</font></td><td align='right'><font size = '2'>".$agnoobjetivoAux4."</font></td><td align='left'><font size = '2'>".$descobjetivoAux4."</font></td><td align='right'><font size = '2'>".$percentescalaevaluacionAux4." - ".$nomescalaevaluacionAux4."</font></td></tr>";
										}
									}
									echo "</table>";
									echo "</br>";
								}
								else
								{
									echo "No se han asociado objetivos";
								}
							?>
							<hr />
							<!--PROCESO ASIGNAR OBJETIVOS ESPECIFICOS-->
							<p><b>Asignar objetivos espec&iacute;ficos</b></p>
							<form action="asignaobjetivoespecificoxpersona.php" method="post">
								<input type="text" name="rutpersona" style="visibility:hidden" value="<?=$rutpersonaAux2?>" readonly> 
								<input type="text" name="action" style="visibility:hidden" value="asignaobjetivo" readonly>
								<input type="text" name="codtipoobjetivo" style="visibility:hidden" value="2" readonly>
								<table border = "0">
									<tr><th align="left">A&ntilde;o</th>			<th>:</th>	<td><input type="text" name="agnoobjetivo" value="<?=$agnoactual?>" readonly></td></tr>
									<tr><th align="left">Descripci&oacute;n</th>	<th>:</th>	<td><textarea type="text" name="descobjetivo" cols="30" rows="10"></textarea></td></tr>
									<tr><td><input type="submit" value="Asignar"></td></tr>
								</table>
							</form>
							<hr />
							<table>
								<tr>
									<td>
										<form action="../objetivoxpersona/desasignaobjetivoxpersona.php" method="post">
											<input type="submit" value="Quitar Objetivo">
											<input type="text" name="rutpersona" style="visibility:hidden" size = "3" value="<?=$rutpersonaAux2?>" readonly>
										</form>
									</td>
								</tr>
							</table>
							<hr />
							<div id="botonbottom3">
								<table>
									<tr>
										<td width='35' align='center' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
										</td>
										<td width='35' align='center' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/procesos/asignadores/objetivoespecificoxpersona/asdesobjetivoespecificoxpersona.php'><img src='../../../images/back.jpg' width='30' height='30' title='Volver'></a>
										</td>
										<td width='530' align='right' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
										</td>
									</tr>
								</table>
							</div>
							<hr />
							<?php
						}
					}
					else
					{
					
						//INICIO DEL PROCESO, PARA BUSCAR PERSONAS EVALUABLES
						if ($_REQUEST["action"] == "")
						{
							?>
							<form action="asignaobjetivoespecificoxpersona.php" method="get">
								<input type="text" name="action" style="visibility:hidden" value="buscarpersona" readonly> 
								<table border="0">
									<tr>
										<th align="left">
											<font size="2">Seleccione</font>
										</th>
										<th>
											<font size="2">:</font>
										</th>
										<td>
											<font size="2">
											<select name="rutpersona">
												<?php
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													$resultqevaluador = consultatodo("evaluador", "rutevaluador", $rutusuarioevareg);
													if ($regevaluador = mysqli_fetch_assoc($resultqevaluador))
													{
														$rutevaluadorvalidado = $regevaluador["rutevaluador"];
														$resultqareaevaluada = consultatodo("area", "rutevaluador", $rutevaluadorvalidado);
														if ($regareaevaluada = mysqli_fetch_assoc($resultqareaevaluada))
														{
															$codareaevaluada = $regareaevaluada["codarea"];
															$resultqpersonaevaluada = consultatodo("persona", "codarea", $codareaevaluada);
															while ($regpersonaevaluada = mysqli_fetch_assoc($resultqpersonaevaluada))
															{
																$fechaingreso = $regpersonaevaluada["fechaingreso"];
																$activo = $regpersonaevaluada["activo"];
																
																if (evaluable($fechaingreso, $activo))
																{
																	echo "<option value='".$regpersonaevaluada["rutpersona"]."'>";
																	echo $regpersonaevaluada["nompersona"]." ".$regpersonaevaluada["appaterno"]." ".$regpersonaevaluada["apmaterno"];
																	echo "</option>";
																}
															}
														}
													}
												?>
											</select>
											</font>
										</td>
										<td><input type="submit" value="Buscar"></td>
									</tr>
								</table>
								</br>
							</form>
							<hr />
							<div id="botonbottom4">
								<table>
									<tr>
										<td width='35' align='center' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Menu Principal'></a>
										</td>
										<td width='35' align='center' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/procesos/asignadores/objetivoespecificoxpersona/asdesobjetivoespecificoxpersona.php'><img src='../../../images/back.jpg' width='30' height='30' title='volver'></a>
										</td>
										<td width='530' align='right' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
										</td>
									</tr>
								</table>
							</div>
							<hr />
							<?php
						}
						//FIN INICIO DEL PROCESO, PARA BUSCAR PERSONAS EVALUABLES
					}
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Asigna - Desasigna Objetivos Espec&iacute;ficos
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("ASIGNAR / DESASIGNAR OBJETIVOS A PERSONAS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>					