<?php
	include "../../lib/handWebEva.php";
	include "../../lib/handVarEva.php";
	include "../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
			<html>
				<head>
					<title>
						Eva - Reportes
					</title>
					<link href="../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
					<style type="text/css" media="all">@media all {div.saltopagina{display:none;}}</style>
					<style type="text/css" media="print">@media print {#menu {display:none;} #botonup {display:none;} #botonbottom {display:none;} #separabarra {display:none;} div.saltopagina{display:block; page-break-before:always;}}</style>
				</head>
				<body>
					<?php
						cabezal("Reportes");
						$webserver = nomserverweb();
						$agnoactual = date("Y");
						$agnoeva = $agnoactual - 1;
							?>
							<font size="4"><b>Reporte de Status por Evaluadores</b></font>
							<hr />
							<?php
							
							if((isset($_REQUEST["coduniestrat"])) and ($_REQUEST["coduniestrat"] <> ""))
							{
								$coduniestratAux = $_REQUEST["coduniestrat"];
								$resultquniestratRep = consultatodo("uniestrat", "coduniestrat", $coduniestratAux);
								$reguniestratRep = mysqli_fetch_assoc($resultquniestratRep);
								$nomuniestratAux = $reguniestratRep["nomuniestrat"];
								
								?>
								<div id="botonup">
									<table>
										<tr>
											<td width='25' align='center' valign='top'>
												<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
											</td>
											<td width='25' align='center' valign='top'>
												<form><input type=image src='../../images/imprimir.jpg' width='30' height='30' title='Imprimir' value='Imprimir' onClick='window.print()'></form>
											</td>
											<td width='25' align='center' valign='top'>
												<a href='http://<?php echo $webserver;?>/eva/procesos/reportes/indexreportes.php'><img src='../../images/back.jpg' width='30' height='30' title='Volver'></a>
											</td>
											<td width='500' align='center' valign='top'>
												
											</td>
											<td width='25' align='center' valign='top'>
												<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../images/logout.jpg' width='30' height='30' title='Salir'></a>
											</td>
										</tr>
									</table>
								</div>
								<div id='separabarra'><hr /></div>
								<?php
								
								echo "<table border='1' cellpadding='0'>";
								echo "<tr><th nowrap align='left' bgcolor='#0080FF' colspan = '5'><font size='3' color='white'>Unid. Estrat&eacute;gica : ".$nomuniestratAux."</font></th></tr>";
								if ($resultqarea = consultatodo("area", "coduniestrat", $coduniestratAux))
								{
									echo "<tr><th align='center' bgcolor='#6E6E6E' width='150'><font size='2' color='white'>Nombre Evaluador</font></th><th align='center' bgcolor='#6E6E6E' width='150'><font size='2' color='white'>&Aacute;rea</font></th><th align='center' bgcolor='#6E6E6E' width='100'><font size='2' color='white'>Evaluaciones Terminadas</font></th><th align='center' bgcolor='#6E6E6E' width='100'><font size='2' color='white'>Evaluaciones Pendientes</font></th><th align='center' bgcolor='#6E6E6E' width='100'><font size='2' color='white'>% de avance</font></th></tr>";
									while ($regarea = mysqli_fetch_assoc($resultqarea))
									{
										//obtiene nombre de area
										$codareaAux = $regarea["codarea"];
										$nomareaAux = $regarea["nomarea"];
										$rutevaluadorAux = $regarea["rutevaluador"];
										//obtiene datos de evaluador
										$resultqevaluador = consultatodo("evaluador", "rutevaluador", $rutevaluadorAux);
										$regevaluador = mysqli_fetch_assoc($resultqevaluador);
										$nomevaluadorAux = $regevaluador["nomevaluador"];
										$appaternoevalAux = $regevaluador["appaterno"];
										$apmaternoevaAux = $regevaluador["apmaterno"];
										//obtiene datos de persona bajo area
										if ($resultqpersonaCnt = consultatodo("persona", "codarea", $codareaAux))
										{
											$cuentaevaluables = 0;
											$cuentaevaluados = 0;
											while ($regpersonaCnt = mysqli_fetch_assoc($resultqpersonaCnt))
											{
												$rutpersonaCnt = $regpersonaCnt["rutpersona"];
												$fechaingCnt = $regpersonaCnt["fechaingreso"];
												$activoCnt = $regpersonaCnt["activo"];
												$codevaluacion = $rutpersonaCnt."-".$agnoeva;
												if (evaluable($fechaingCnt, $activoCnt))
												{
													$cuentaevaluables = $cuentaevaluables + 1;
													$resultqevaluado = consultatodo("evaluacion", "codevaluacion", $codevaluacion);
													$rowsevaluado = mysqli_num_rows($resultqevaluado);
													if ($rowsevaluado > 0)
													{
														$cuentaevaluados = $cuentaevaluados + 1;
													}
												}
											}
										}
										$totalevaluables = $cuentaevaluables;
										$totalevaluados = $cuentaevaluados;
										$totalpendientes = $totalevaluables - $totalevaluados;
										if ($totalevaluables > 0)
										{
											$percentavanceflot = ($totalevaluados * 100)/$totalevaluables;
											$percentavance = round($percentavanceflot, 2);
											echo "<tr><td><font size='1'>".$nomevaluadorAux." ".$appaternoevalAux." ".$apmaternoevaAux."</font></td><td><font size='1'>".$nomareaAux."</font></td><td><font size='1'>".$totalevaluados."</font></td><td><font size='1'>".$totalpendientes."</font></td><td><font size='1'>".$percentavance."%</font></td></tr>";
										}
										else
										{
											echo "<hr />";
											echo "No existen evaluables en esta &aacute;rea";
											echo "<hr />";
										}
									}
								}
								echo "</table>";
							}
							else
							{
								?>
								<form action="statusevaluadores.php" method="post">
									<table>
										<tr>
											<th align="left">
												Unid. Estrat&eacute;gica
											</th>
											<th>
												:
											</th>
											<td>
												<select name="coduniestrat">
													<?php
														$resultquniestrat = llenacombo("uniestrat");
														echo "<option value=''>";
														echo "- Seleccione";
														echo "</option>";
														while ($reguniestrat = mysqli_fetch_assoc($resultquniestrat))
														{
															echo "<option value='".$reguniestrat["coduniestrat"]."'>";
															echo $reguniestrat["nomuniestrat"];
															echo "</option>";
														}
													?>
												</select>
											</td>
											<td>
												<input type="submit" value="Consultar">
											</td>
										</tr>
									</table>
								</form>
								<?php
							}
							?>
							<hr />
							<div id='botonbottom'>
								<table>
									<tr>
										<td width='32' align='center' valign='center'>
											<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
										</td>
										<td width='32' align='center' valign='center'>
											<a href='http://<?php echo $webserver;?>/eva/procesos/reportes/indexreportes.php'><img src='../../images/back.jpg' width='30' height='30' title='Volver'></a>
										</td>
										<td width='504' align='center' valign='center'>
											
										</td>
										<td width='32' align='center' valign='center'>
											<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../images/logout.jpg' width='30' height='30' title='Salir'></a>
										</td>
									</tr>
								</table>
							</div>
							<?php
						pie();
					?>
				</body>
			</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Reportes
					</title>
					<link href="../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("Reportes");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>