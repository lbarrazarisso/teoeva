<?php
	include "../../lib/handWebEva.php";
	include "../../lib/handVarEva.php";
	include "../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if (($codtipousuarioevaAux == 1) or ($codtipousuarioevaAux == 3))
	{
		?>
			<html>
				<head>
					<title>
						Eva - Reportes
					</title>
					<link href="../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
					<style type="text/css" media="all">@media all {div.saltopagina{display:none;}}</style>
					<style type="text/css" media="print">@media print {#menu {display:none;} #botonup {display:none;} #botonbottom {display:none;} #separabarra {display:none;} div.saltopagina{display:block; page-break-before:always;}}</style>
				</head>
				<body>
					<?php
						$webserver = nomserverweb();
						if ($codtipousuarioevaAux == 1)
						{
							cabezal("");
						}
						
						if ($codtipousuarioevaAux == 3)
						{
							cabezalevaluador("");
						}
						
						$agnoactual = date("Y");
						$agnoeva = $agnoactual - 1;
						
						echo "</br>";
						echo "<font size='4'><b>Resumen de resultados por Competencias Conductuales ".$agnoeva."</b></font>";
						echo "</br>";
						echo "</br>";
						echo "<hr />";

							
						if(((isset($_REQUEST["coduniestrat"])) and ($_REQUEST["coduniestrat"] <> "")) and 
						((isset($_REQUEST["agnoevaluacion"])) and ($_REQUEST["agnoevaluacion"] <> "")))
						{
							$coduniestratAux = $_REQUEST["coduniestrat"];
							$agnoevaluacionAux = $_REQUEST["agnoevaluacion"];
							
							$resultquniestrat = consultatodo("uniestrat", "coduniestrat", $coduniestratAux);
							$reguniestrat = mysqli_fetch_assoc($resultquniestrat);
							$nomuniestratAux = $reguniestrat["nomuniestrat"];
							
							if ((isset($_REQUEST["codarea"])) and ($_REQUEST["codarea"] <> ""))
							{
								$codareaAux = $_REQUEST["codarea"];
								if ($resultqarea = consultatodo("area", "codarea", $codareaAux))
								{
									$regarea = mysqli_fetch_assoc($resultqarea);
									$nomareaAux = $regarea["nomarea"];
								}
								else
								{
									mensaje("Área desconocida");
								}
							
								//proceso de conteo de evaluados del area ingresada.
								// obtener todas las personas que trabajan en el area.
								if ($resultqpersonaarea = consultatodo("persona", "codarea", $codareaAux))
								{
									$cuentaevaluados = 0;
									while ($regpersonaarea = mysqli_fetch_assoc($resultqpersonaarea))
									{
										$rutevaluado = $regpersonaarea["rutpersona"];
										$codevaluacionAux = $rutevaluado."-".$agnoevaluacionAux;
										if ($resultqevaluacion = consultatodo("evaluacion", "codevaluacion", $codevaluacionAux))
										{
											$rowseval = mysqli_num_rows($resultqevaluacion);
											if ($rowseval > 0)
											{
												$cuentaevaluados = $cuentaevaluados + 1;
											}
										}
									}
								}
								else
								{
									echo "No existen personas en esta &aacute;rea";
								}
							
								?>
								<div id="botonup">
									<table>
										<tr>
											<td width='25' align='center' valign='top'>
												<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
											</td>
											<td width='25' align='center' valign='top'>
												<form><input type=image src='../../images/imprimir.jpg' width='30' height='30' title='Imprimir' value='Imprimir' onClick='window.print()'></form>
											</td>
											<td width='25' align='center' valign='top'>
												<a href='http://<?php echo $webserver;?>/eva/procesos/reportes/indexreportes.php'><img src='../../images/back.jpg' width='30' height='30' title='Volver'></a>
											</td>
											<td width='500' align='center' valign='top'>
												
											</td>
											<td width='25' align='center' valign='top'>
												<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../images/logout.jpg' width='30' height='30' title='Salir'></a>
											</td>
										</tr>
									</table>
								</div>
								<div id='separabarra'><hr /></div>
								<?php
								
								echo "<table border='1' cellpadding='0'>";
								echo "<tr><th nowrap align='left' bgcolor='#0080FF' width='580'><font size='3' color='white'>Unid. Estrat&eacute;gica : ".$nomuniestratAux."</font></th></tr>";
								echo "<tr><th nowrap align='left' bgcolor='#0080FF' width='580'><font size='3' color='white'>&Aacute;rea              : ".$nomareaAux."</font></th></tr>";
								echo "</table>";
								echo "<hr />";
								
								if ($cuentaevaluados > 0)
								{
									$codtipocompetenciaAux = 1; //DATO DURO;
									$resultqcompetencia = consultatodo("competencia", "codtipocompetencia", $codtipocompetenciaAux);
									while ($regcompetencia = mysqli_fetch_assoc($resultqcompetencia))
									{
										$codcompetenciaAux1 = $regcompetencia["codcompetencia"];
										$desccompetenciaAux1 = $regcompetencia["desccompetencia"];
										$agnocompetenciaAux1 = $regcompetencia["agnocompetencia"];
										
										if ($agnocompetenciaAux1 == $agnoevaluacionAux)
										{
											echo "<p><font size = '3'><b>".$desccompetenciaAux1." :</b></font></p></br>";
											echo "<table>";
											$resultqescalaevaluacion = llenacombo("escalaevaluacion");
											while ($regescalaevaluacion = mysqli_fetch_assoc($resultqescalaevaluacion))
											{
												$cuentaescala = 0;
												$codescalaevaluacionAux = $regescalaevaluacion["codescalaevaluacion"];
												//if ($codescalaevaluacionAux > 0)
												//{
												$nomescalaevaluacionAux = $regescalaevaluacion["nomescalaevaluacion"];
												$percentescalaevaluacionAux = $regescalaevaluacion["percentescalaevaluacion"];
												echo "<tr><td width='140'>".$nomescalaevaluacionAux."</td><td>:</td>";

												//para el área consultada, obtener todas las personas que trabajan en ella
												$resultqpersona = consultatodo("persona", "codarea", $codareaAux);
												while ($regpersona = mysqli_fetch_assoc($resultqpersona))
												{
													$rutpersonaAux = $regpersona["rutpersona"];
													$codevaluacionAux = $rutpersonaAux."-".$agnoevaluacionAux;
													//para cada persona, consultar si es que esta tiene evaluación
													$resultqevaluacionxcompetenciaxcargo = consultatodo("evaluacionxcompetenciaxcargo", "codevaluacion", $codevaluacionAux);
													while ($regevaluacionxcompetenciaxcargo = mysqli_fetch_assoc($resultqevaluacionxcompetenciaxcargo))
													{
														$codevaluacionobtenidaAux = $regevaluacionxcompetenciaxcargo["codevaluacionobtenida"];
														//if ($codevaluacionobtenidaAux > 0)
														//{
														$codcompetenciaxcargoAux = $regevaluacionxcompetenciaxcargo["codcompetenciaxcargo"];
														$resultqcompetenciaxcargo = consultatodo("competenciaxcargo", "codcompetenciaxcargo", $codcompetenciaxcargoAux);
														$regcompetenciaxcargo = mysqli_fetch_assoc($resultqcompetenciaxcargo);
														$codcompetenciaAux2 = $regcompetenciaxcargo["codcompetencia"];
														if ($codcompetenciaAux2 == $codcompetenciaAux1)
														{
															if ($codevaluacionobtenidaAux == $codescalaevaluacionAux)
															{
																$cuentaescala = $cuentaescala + 1;
															}
														}
														//}
													}
												}

												//tabla para mostrar porcentajes
												$percentescalafloat = ($cuentaescala * 100)/$cuentaevaluados;
												$percentescala = round($percentescalafloat, 1);
												echo "<td width='120' valign='bottom' height='10'><table><tr><td valign='bottom' align='left' width='".$percentescala."' height='9' bgcolor='#2E64FE'></td></tr></table></td><td align='center'>".$percentescala."%</td></tr>";
												//}
											}
											echo "</table></br>";
										}
									}
								}
								else
								{
									echo "No hay evaluados";
								}
							}
							else
							{
								?>
								<form action="resultadosxcompetenciaconarea.php" method="get">
									<table>
										<tr>
											<th align="left">
												Seleccione &Aacute;rea
											</th>
											<th>
												:
											</th>
											<td>
												<select name="codarea">
													<?php
														$resultqarea = consultatodo("area", "coduniestrat", $coduniestratAux);
														echo "<option value=''>";
														echo "- Seleccione";
														echo "</option>";
														while ($regarea = mysqli_fetch_assoc($resultqarea))
														{
															echo "<option value='".$regarea["codarea"]."'>";
															echo $regarea["nomarea"];
															echo "</option>";
														}
													?>
												</select>
											</td>
											<td>
												<input type="hidden" name="coduniestrat" value="<?=$coduniestratAux?>">
												<input type="hidden" name="agnoevaluacion" value="<?=$agnoevaluacionAux?>">
												<input type="submit" value="Emitir">
											</td>
										</tr>
									</table>
								</form>
								<?php
							}
						}
						else
						{
							?>
							<form action="resultadosxcompetenciaconarea.php" method="post">
								<table>
									<tr>
										<th align="left">
											Unid. Estrat&eacute;gica
										</th>
										<th>
											:
										</th>
										<td>
											<select name="coduniestrat">
												<?php
													$resultquniestrat = llenacombo("uniestrat");
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													while ($reguniestrat = mysqli_fetch_assoc($resultquniestrat))
													{
														echo "<option value='".$reguniestrat["coduniestrat"]."'>";
														echo $reguniestrat["nomuniestrat"];
														echo "</option>";
													}
												?>
											</select>
										</td>
										<td>
											<input type="submit" value="Consultar">
										</td>
									</tr>
									<tr>
										<th align="left">
											A&ntilde;o
										</th>
										<th>
											:
										</th>
										<td>
											<select name="agnoevaluacion">
												<?php
													$resultqevaluacion = cuentadup("evaluacion", "agnoevaluacion");
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													while ($regevaluacion = mysqli_fetch_assoc($resultqevaluacion))
													{
														echo "<option value='".$regevaluacion["agnoevaluacion"]."'>";
														echo $regevaluacion["agnoevaluacion"];
														echo "</option>";
													}
												?>
											</select>
										</td>
									</tr>
								</table>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/procesos/reportes/indexreportes.php'><img src='../../images/back.jpg' width='30' height='30' title='Volver'></a>
									</td>
									<td width='504' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<?php
						pie();
					?>
				</body>
			</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Reportes
					</title>
					<link href="../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("Reportes");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>						