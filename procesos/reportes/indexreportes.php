<?php
	include "../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$rutusuarioevareg = $regusuarioeva["rutusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	if ($resultqevaluador = consultatodo("evaluador", "rutevaluador", $rutusuarioevareg))
	{
		if ($regevaluador = mysqli_fetch_assoc($resultqevaluador))
		{
			$rutevaluadorvalidado = $regevaluador["rutevaluador"];
		}
		else
		{
			mensaje ("Advertencia: Este usuario no es evaluador");
		}
	}
	else
	{
		mensaje ("Advertencia: Este usuario no es evaluador");
	}
	
	if (($codtipousuarioevaAux == 1) or ($codtipousuarioevaAux == 3))
	{
		?>
			<html>
				<head>
					<title>
						Eva - Reportes
					</title>
					<link href="../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
				<?php
				$agnoactual = date("Y");
				$agnoeva = $agnoactual - 1;
				
				$webserver = nomserverweb();
				if ($codtipousuarioevaAux == 1)
				{
					cabezal("INDICE DE REPORTES");
					?>
					<p><b>&Iacute;NDICE DE REPORTES</b></p>
					<hr />
					<div id="botonup">
						<table>
							<tr>
								<td width='600' align='left' valign='top'>
									<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../images/menuprincipal.jpg' width='30' height='30' title='Menu Principal'></a>
								</td>
							</tr>
						</table>
					</div>
					<hr />
					<table border = '1'>
						<tr><td align = 'left' width = '304'><font size = '2'><p>Listado de personal vigente</p></font></td><td align = 'center' width = '296'><button style = 'width:295'><a style='text-decoration: none; color:black' href='http://<?php echo $webserver;?>/eva/procesos/reportes/personaxarea.php'>Emitir</a></button></td></tr>
						<tr><td align = 'left' width = '304'><font size = '2'><p>Listado de personal evaluable por area</p></font></td><td align = 'center' width = '296'><button style = 'width:295'><a style='text-decoration: none; color:black' href='http://<?php echo $webserver;?>/eva/procesos/reportes/personaevaxarea.php'>Emitir</a></button></td></tr>
						<tr><td align = 'left' width = '304'><font size = '2'><p>Listado de personal evaluado por area</p></font></td><td align = 'center' width = '296'><button style = 'width:295'><a style='text-decoration: none; color:black' href='http://<?php echo $webserver;?>/eva/procesos/reportes/personaevaluadaxarea.php'>Emitir</a></button></td></tr>
						<tr><td align = 'left' width = '304'><font size = '2'><p>Listado de total de personas evaluadas</p></font></td><td align = 'center' width = '296'><button style = 'width:295'><a style='text-decoration: none; color:black' href='http://<?php echo $webserver;?>/eva/procesos/reportes/personaevaluadatotal.php'>Emitir</a></button></td></tr>
						<tr><td align = 'left' width = '304'><font size = '2'><p>Status por evaluador</p></font></td><td align = 'center' width = '296'><button style = 'width:295'><a style='text-decoration: none; color:black' href='http://<?php echo $webserver;?>/eva/procesos/reportes/statusevaluadores.php'>Emitir</a></button></td></tr>
						<tr><td align = 'left' width = '304'><font size = '2'><p>Evaluadores por Unid. Estrat&eacute;gica</p></font></td><td align = 'center' width = '296'><button style = 'width:295'><a style='text-decoration: none; color:black' href='http://<?php echo $webserver;?>/eva/procesos/reportes/evaluadores.php'>Emitir</a></button></td></tr>
						<tr><td align = 'left' width = '304'><font size = '2'><p>Resultados por Objetivos Generales - &Aacute;rea</p></font></td><td align = 'center' width = '296'><button style = 'width:295'><a style='text-decoration: none; color:black' href='http://<?php echo $webserver;?>/eva/procesos/reportes/resultadosxobjetivogenarea.php'>Emitir</a></button></td></tr>
						<tr><td align = 'left' width = '304'><font size = '2'><p>Resultados por Comp. Conductuales - &Aacute;rea</p></font></td><td align = 'center' width = '296'><button style = 'width:295'><a style='text-decoration: none; color:black' href='http://<?php echo $webserver;?>/eva/procesos/reportes/resultadosxcompetenciaconarea.php'>Emitir</a></button></td></tr>
						<tr><td align = 'left' width = '304'><font size = '2'><p>Resultados por Objetivos Generales</p></font></td><td align = 'center' width = '296'><button style = 'width:295'><a style='text-decoration: none; color:black' href='http://<?php echo $webserver;?>/eva/procesos/reportes/resultadosxobjetivogen.php'>Emitir</a></button></td></tr>
						<tr><td align = 'left' width = '304'><font size = '2'><p>Resultados por Comp. Conductuales</p></font></td><td align = 'center' width = '296'><button style = 'width:295'><a style='text-decoration: none; color:black' href='http://<?php echo $webserver;?>/eva/procesos/reportes/resultadosxcompetenciacon.php'>Emitir</a></button></td></tr>
					</table>	
					<hr />
					<div id="botonbottom">
						<table>
							<tr>
								<td width='600' align='right' valign='top'>
									<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../images/logout.jpg' width='30' height='30' title='Salir'></a>
								</td>
							</tr>
						</table>
					</div>
					<hr />
					<?php
				}
				
				if ($codtipousuarioevaAux == 3)
				{
					cabezalevaluador("INDICE DE REPORTES");
					?>
					<p><b>INDICE DE REPORTES</b></p>
					<hr />
					<div id="botonup">
						<table>
							<tr>
								<td width='600' align='left' valign='top'>
									<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../images/menuprincipal.jpg' width='30' height='30' title='Menu Principal'></a>
								</td>
							</tr>
						</table>
					</div>
					<hr />
					<?php
					if ($resultqareaevaluada = consultatodo("area", "rutevaluador", $rutevaluadorvalidado))
					{
						$cuentaareas = 1;
						echo "<table border = '1'>";
						while ($regareaevaluada = mysqli_fetch_assoc($resultqareaevaluada))
						{
							$codareaevaluadaAux = $regareaevaluada["codarea"];
							$nomareaAux = $regareaevaluada["nomarea"];
							$coduniestratAux = $regareaevaluada["coduniestrat"];
							echo "<tr><th align = 'left' colspan = '2' width = '600'><font size = '2'><p>Area : ".$nomareaAux."</p></font></th></tr>";
							echo "<tr><td align = 'left' width = '304'><font size = '2'><p>Listado de personal vigente ".$agnoactual." </p></font></td><td align = 'center' width = '296'><button style = 'width:295'><a style='text-decoration: none; color:black' href='http://".$webserver."/eva/procesos/reportes/personaxarea.php?codarea=".$codareaevaluadaAux."&coduniestrat=".$coduniestratAux."'>Emitir</a></button></td></tr>";
							echo "<tr><td align = 'left' width = '304'><font size = '2'><p>Listado de personal evaluable ".$agnoeva." </p></font></td><td align = 'center' width = '296'><button style = 'width:295'><a style='text-decoration: none; color:black' href='http://".$webserver."/eva/procesos/reportes/personaevaxarea.php?codarea=".$codareaevaluadaAux."&coduniestrat=".$coduniestratAux."'>Emitir</a></button></td></tr>";
							echo "<tr><td align = 'left' width = '304'><font size = '2'><p>Listado de personal evaluado ".$agnoeva." </p></font></td><td align = 'center' width = '296'><button style = 'width:295'><a style='text-decoration: none; color:black' href='http://".$webserver."/eva/procesos/reportes/personaevaluadaxarea.php?codarea=".$codareaevaluadaAux."&coduniestrat=".$coduniestratAux."&agnoevaluacion=".$agnoeva."'>Emitir</a></button></td></tr>";
							echo "<tr><td align = 'left' width = '304'><font size = '2'><p>Resultados por objetivos generales ".$agnoeva." </p></font></td><td align = 'center' width = '296'><button style = 'width:295'><a style='text-decoration: none; color:black' href='http://".$webserver."/eva/procesos/reportes/resultadosxobjetivogenarea.php?codarea=".$codareaevaluadaAux."&coduniestrat=".$coduniestratAux."&agnoevaluacion=".$agnoeva."'>Emitir</a></button></td><tr>";
							echo "<tr><td align = 'left' width = '304'><font size = '2'><p>Resultados por comp. conductuales ".$agnoeva." </p></font></td><td align = 'center' width = '296'><button style = 'width:295'><a style='text-decoration: none; color:black' href='http://".$webserver."/eva/procesos/reportes/resultadosxcompetenciaconarea.php?codarea=".$codareaevaluadaAux."&coduniestrat=".$coduniestratAux."&agnoevaluacion=".$agnoeva."'>Emitir</a></button></td><tr>";
						}
						echo "</table>";
					}
					?>
					<hr />
					<div id="botonbottom">
						<table>
							<tr>
								<td width='600' align='right' valign='top'>
									<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../images/logout.jpg' width='30' height='30' title='Salir'></a>
								</td>
							</tr>
						</table>
					</div>
					<hr />
					<?php
				}
				pie();
				?>
				</body>
			</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Reportes
					</title>
					<link href="../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("Reportes");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCION</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Menu Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>