<?php
	include "../../lib/handWebEva.php";
	include "../../lib/handVarEva.php";
	include "../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
			<html>
				<head>
					<title>
						Eva - Reportes
					</title>
					<link href="../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
					<style type="text/css" media="all">@media all {div.saltopagina{display:none;}}</style>
					<style type="text/css" media="print">@media print {#menu {display:none;} #botonup {display:none;} #botonbottom {display:none;} #separabarra {display:none;} div.saltopagina{display:block; page-break-before:always;}}</style>
				</head>
				<body>
					<?php
						cabezal("Reportes");
							$webserver = nomserverweb();
							$agnoactual = date("Y");
							$agnoeva = $agnoactual - 1;
							?>
							<font size="4"><b>Reporte de Personas evaluadas por &Aacute;rea</b></font>
							<hr />
							<?php
							
							if(((isset($_REQUEST["coduniestrat"])) and ($_REQUEST["coduniestrat"] <> "")) and 
							((isset($_REQUEST["agnoevaluacion"])) and ($_REQUEST["agnoevaluacion"] <> "")))
							{
								$coduniestratAux = $_REQUEST["coduniestrat"];
								$agnoevaluacionAux = $_REQUEST["agnoevaluacion"];
								
								//Proceso de contador de personas evaluables. obtiene universo de evaluables.
								$resultqareaCnt = consultatodo("area", "coduniestrat", $coduniestratAux);
								$cuentaevaluables = 0;
								$cuentaevaluados = 0;
								while ($regareaCnt = mysqli_fetch_assoc($resultqareaCnt))
								{
									$codareaCnt = $regareaCnt["codarea"];
									if ($resultqpersonaCnt = consultatodo("persona", "codarea", $codareaCnt))
									{
										while ($regpersonaCnt = mysqli_fetch_assoc($resultqpersonaCnt))
										{
											$rutpersonaCnt = $regpersonaCnt["rutpersona"];
											$fechaingCnt = $regpersonaCnt["fechaingreso"];
											$activoCnt = $regpersonaCnt["activo"];
											$codevaluacion = $rutpersonaCnt."-".$agnoevaluacionAux;
											if ($activoCnt == 'S')
											{
												if (evaluable($fechaingCnt, $activoCnt))
												{
													$cuentaevaluables = $cuentaevaluables + 1;
												}
											}
											$resultqevaluado = consultatodo("evaluacion", "codevaluacion", $codevaluacion);
											$rowsevaluado = mysqli_num_rows($resultqevaluado);
											if ($rowsevaluado > 0)
											{
												$cuentaevaluados = $cuentaevaluados + 1;
											}
										}
									}
								}
								
								//Barra de encabezado
								?>
								<div id="botonup">
									<table>
										<tr>
											<td width='25' align='center' valign='top'>
												<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
											</td>
											<td width='25' align='center' valign='top'>
												<form><input type=image src='../../images/imprimir.jpg' width='30' height='30' title='Imprimir' value='Imprimir' onClick='window.print()'></form>
											</td>
											<td width='25' align='center' valign='top'>
												<a href='http://<?php echo $webserver;?>/eva/procesos/reportes/indexreportes.php'><img src='../../images/back.jpg' width='30' height='30' title='Volver'></a>
											</td>
											<td width='500' align='center' valign='top'>
												
											</td>
											<td width='25' align='center' valign='top'>
												<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../images/logout.jpg' width='30' height='30' title='Salir'></a>
											</td>
										</tr>
									</table>
								</div>
								<div id='separabarra'><hr /></div>
								<?php
								
								if ($agnoevaluacionAux == $agnoeva)
								{
									$totalevaluables = $cuentaevaluables;
									$totalevaluados = $cuentaevaluados;
									if ($totalevaluables > 0)
									{
										$percentevaluadosflot = ($totalevaluados * 100)/$totalevaluables;
										$percentevaluados = round($percentevaluadosflot, 2);
										//Despliega resultados
										echo "<table cellpadding='0'>";
										echo "<tr><th nowrap align='left'><font size='3'>Total Personas Evaluables</font></th>    <th><font size='3'>:</font></th><th><font size='3'>".$totalevaluables."</font></th></tr>";
										echo "<tr><th nowrap align='left'><font size='3'>Total Personas Evaluadas</font></th>     <th><font size='3'>:</font></th><th><font size='3'>".$totalevaluados."</font></th></tr>";
										echo "<tr><th nowrap align='left'><font size='3'>Porcent. Personas Evaluadas</font></th>  <th><font size='3'>:</font></th><th><font size='3'>".$percentevaluados."%</font></th></tr>";
										echo "</table>";
										echo "<hr />";
									}
									else
									{
										echo "<hr />";
										echo "No existen evaluables";
										echo "<hr />";
									}
								}
																
								//Proceso de reporte. Despliega todos los que han sido evaluados, por area.
								$resultqareaRep = consultatodo("area", "coduniestrat", $coduniestratAux);
								while ($regareaRep = mysqli_fetch_assoc($resultqareaRep))
								{
									//obtiene datos del area
									$codareaAux = $regareaRep["codarea"];
									$nomareaAux = $regareaRep["nomarea"];
									$rutevaluadorAux = $regareaRep["rutevaluador"];
									//obtiene datos de evaluador
									$resultqevaluador = consultatodo("evaluador", "rutevaluador", $rutevaluadorAux);
									$regevaluador = mysqli_fetch_assoc($resultqevaluador);
									$nomevaluadorAux = $regevaluador["nomevaluador"];
									$appaternoEvalAux = $regevaluador["appaterno"];
									$apmaternoEvalAux = $regevaluador["apmaterno"];
									//escribe encabezado de tabla
									echo "<table border='1' cellpadding='0'>";
									echo "<tr><th nowrap align='left' bgcolor='#0080FF' colspan = '3'><font size='3' color='white'>&Aacute;rea : ".$nomareaAux."</font></th></tr>";
									echo "<tr><th nowrap align='left' bgcolor='#0080FF' colspan = '3'><font size='3' color='white'>Evaluador : ".$nomevaluadorAux." ".$appaternoEvalAux." ".$apmaternoEvalAux."</font></th></tr>";
									//consulta en tabla persona
									if ($resultqpersona = consultatodo("persona", "codarea", $codareaAux))
									{
										//escribe encabezado de registro
										echo "<tr><th align='left' bgcolor='#6E6E6E' width='250'><font size='2' color='white'>Nombre</font></th><th align='left' bgcolor='#6E6E6E' width='250'><font size='2' color='white'>Cargo</font></th><th align='left' bgcolor='#6E6E6E' width='100'><font size='2' color='white'>F. Ingreso</font></th></tr>";
										while ($regpersona = mysqli_fetch_assoc($resultqpersona))
										{
											//obtiene datos de persona
											$rutpersonaAux = $regpersona["rutpersona"];
											//compone codigo de evaluacion para ver si ya está evaluado.
											$codevaluacion = $rutpersonaAux."-".$agnoevaluacionAux;
											//consulta a tabla evaluacion
											$resultqevaluado = consultatodo("evaluacion", "codevaluacion", $codevaluacion);
											$rowsevaluado = mysqli_num_rows($resultqevaluado);
											if ($rowsevaluado > 0)
											{
												//Si la persona ya está evaluada, obtiene sus datos.
												$nompersonaAux = $regpersona["nompersona"];
												$appaternoAux = $regpersona["appaterno"];
												$apmaternoAux = $regpersona["apmaterno"];
												$fechaingAux = $regpersona["fechaingreso"];
												$codcargoAux = $regpersona["codcargo"];
												//consulta datos de cargo
												$resultqcargo = consultatodo("cargo", "codcargo", $codcargoAux);
												$regcargo = mysqli_fetch_assoc($resultqcargo);
												//obtiene nombre de cargo
												$nomcargoAux = $regcargo["nomcargo"];
												//despliega registro
												echo "<tr><td width='250'><font size='1'><a href='http://".$webserver."/eva/procesos/evaluacion/despliegaevaluacion.php?rutpersona=".$rutpersonaAux."&agnoeva=".$agnoevaluacionAux."'>".$nompersonaAux." ".$appaternoAux." ".$apmaternoAux."</a></font></td><td width='250'><font size='1'>".$nomcargoAux."</font></td><td width='100'><font size='1'>".$fechaingAux."</font></td></tr>";
											}
										}
									}
									//fin de tabla
									echo "</table>";
									echo "</br>";
								}
							}
							else
							{
								?>
								<form action="personaevaluadatotal.php" method="post">
									<table>
										<tr>
											<th align="left">
												Unid. Estrat&eacute;gica
											</th>
											<th>
												:
											</th>
											<td>
												<select name="coduniestrat">
													<?php
														$resultquniestrat = llenacombo("uniestrat");
														echo "<option value=''>";
														echo "- Seleccione";
														echo "</option>";
														while ($reguniestrat = mysqli_fetch_assoc($resultquniestrat))
														{
															echo "<option value='".$reguniestrat["coduniestrat"]."'>";
															echo $reguniestrat["nomuniestrat"];
															echo "</option>";
														}
													?>
												</select>
											</td>
											<td>
												<input type="submit" value="Consultar">
											</td>
										</tr>
										<tr>
											<th align="left">
												A&ntilde;o
											</th>
											<th>
												:
											</th>
											<td>
												<select name="agnoevaluacion">
													<?php
														$resultqevaluacion = cuentadup("evaluacion", "agnoevaluacion");
														echo "<option value=''>";
														echo "- Seleccione";
														echo "</option>";
														while ($regevaluacion = mysqli_fetch_assoc($resultqevaluacion))
														{
															echo "<option value='".$regevaluacion["agnoevaluacion"]."'>";
															echo $regevaluacion["agnoevaluacion"];
															echo "</option>";
														}
													?>
												</select>
											</td>
										</tr>
									</table>
								</form>
								<?php
							}
							?>
							<hr />
							<div id='botonbottom'>
								<table>
									<tr>
										<td width='32' align='center' valign='center'>
											<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
										</td>
										<td width='32' align='center' valign='center'>
											<a href='http://<?php echo $webserver;?>/eva/procesos/reportes/indexreportes.php'><img src='../../images/back.jpg' width='30' height='30' title='Volver'></a>
										</td>
										<td width='504' align='center' valign='center'>
											
										</td>
										<td width='32' align='center' valign='center'>
											<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../images/logout.jpg' width='30' height='30' title='Salir'></a>
										</td>
									</tr>
								</table>
							</div>
							<?php
						pie();
					?>
				</body>
			</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Reportes
					</title>
					<link href="../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("Reportes");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>