<?php
	include "../../lib/handWebEva.php";
	include "../../lib/handVarEva.php";
	include "../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if (($codtipousuarioevaAux == 1) or ($codtipousuarioevaAux == 3))
	{
		?>
			<html>
				<head>
					<title>
						Eva - Reportes
					</title>
					<link href="../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
					<style type="text/css" media="all">@media all {div.saltopagina{display:none;}}</style>
					<style type="text/css" media="print">@media print {#menu {display:none;} #botonup {display:none;} #botonbottom {display:none;} #separabarra {display:none;} div.saltopagina{display:block; page-break-before:always;}}</style>
				</head>
				<body>
					<?php
						$webserver = nomserverweb();
						if ($codtipousuarioevaAux == 1)
						{
							cabezal("REPORTES");
						}
						
						if ($codtipousuarioevaAux == 3)
						{
							cabezalevaluador("REPORTES");
						}
						?>
						<font size="4"><b>Reporte de Personas evaluables por &Aacute;rea</b></font>
						<hr />
						<?php
						
						if((isset($_REQUEST["coduniestrat"])) and ($_REQUEST["coduniestrat"] <> ""))
						{
							$coduniestratAux = $_REQUEST["coduniestrat"];
							
							if ((isset($_REQUEST["codarea"])) and ($_REQUEST["codarea"] <> ""))
							{
								$codareaAux = $_REQUEST["codarea"];
								$resultqareaRep = consultatodo("area", "codarea", $codareaAux);
								$regareaRep = mysqli_fetch_assoc($resultqareaRep);
								$nomareaAux = $regareaRep["nomarea"];
								$rutevaluadorAux = $regareaRep["rutevaluador"];
								$resultqevaluador = consultatodo("evaluador", "rutevaluador", $rutevaluadorAux);
								$regevaluador = mysqli_fetch_assoc($resultqevaluador);
								
								$nomevaluadorAux = $regevaluador["nomevaluador"];
								$appaternoEvalAux = $regevaluador["appaterno"];
								$apmaternoEvalAux = $regevaluador["apmaterno"];
								
								?>
								<div id="botonup">
									<table>
										<tr>
											<td width='25' align='center' valign='top'>
												<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
											</td>
											<td width='25' align='center' valign='top'>
												<form><input type=image src='../../images/imprimir.jpg' width='30' height='30' title='Imprimir' value='Imprimir' onClick='window.print()'></form>
											</td>
											<td width='25' align='center' valign='top'>
												<a href='http://<?php echo $webserver;?>/eva/procesos/reportes/indexreportes.php'><img src='../../images/back.jpg' width='30' height='30' title='Volver'></a>
											</td>
											<td width='500' align='center' valign='top'>
												
											</td>
											<td width='25' align='center' valign='top'>
												<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../images/logout.jpg' width='30' height='30' title='Salir'></a>
											</td>
										</tr>
									</table>
								</div>
								<div id='separabarra'><hr /></div>
								<?php
								
								echo "<table border='1' cellpadding='0'>";
								echo "<tr><th nowrap align='left' bgcolor='#0080FF' colspan = '3'><font size='3' color='white'>&Aacute;rea : ".$nomareaAux."</font></th></tr>";
								echo "<tr><th nowrap align='left' bgcolor='#0080FF' colspan = '3'><font size='3' color='white'>Evaluador : ".$nomevaluadorAux." ".$appaternoEvalAux." ".$apmaternoEvalAux."</font></th></tr>";
								if ($resultqpersona = consultatodo("persona", "codarea", $codareaAux))
								{
									echo "<tr><th align='left' bgcolor='#6E6E6E' width='200'><font size='2' color='white'>Nombre</font></th><th align='left' bgcolor='#6E6E6E' width='200'><font size='2' color='white'>Cargo</font></th><th align='left' bgcolor='#6E6E6E' width='100'><font size='2' color='white'>Fecha Ingreso</font></th></tr>";
									while ($regpersona = mysqli_fetch_assoc($resultqpersona))
									{
										$rutpersonaAux = $regpersona["rutpersona"];
										$nompersonaAux = $regpersona["nompersona"];
										$appaternoAux = $regpersona["appaterno"];
										$apmaternoAux = $regpersona["apmaterno"];
										$fechaingAux = $regpersona["fechaingreso"];
										$codcargoAux = $regpersona["codcargo"];
										$activoAux = $regpersona["activo"];
										$resultqcargo = consultatodo("cargo", "codcargo", $codcargoAux);
										$regcargo = mysqli_fetch_assoc($resultqcargo);
										$nomcargoAux = $regcargo["nomcargo"];
										if (evaluable($fechaingAux, $activoAux))
										{
											echo "<tr><td><font size='1'>".$nompersonaAux." ".$appaternoAux." ".$apmaternoAux."</font></td><td><font size='1'>".$nomcargoAux."</font></td><td><font size='1'>".$fechaingAux."</font></td></tr>";
										}
									}
								}
								echo "</table>";
							}
							else
							{
								?>
								<form action="personaevaxarea.php" method="post">
									<table>
										<tr>
											<th align="left">
												Seleccione &Aacute;rea
											</th>
											<th>
												:
											</th>
											<td>
												<select name="codarea">
													<?php
														$resultqarea = consultatodo("area", "coduniestrat", $coduniestratAux);
														echo "<option value=''>";
														echo "- Seleccione";
														echo "</option>";
														while ($regarea = mysqli_fetch_assoc($resultqarea))
														{
															echo "<option value='".$regarea["codarea"]."'>";
															echo $regarea["nomarea"];
															echo "</option>";
														}
													?>
												</select>
											</td>
											<td>
												<input type="hidden" name="coduniestrat" value="<?=$coduniestratAux?>">
												<input type="submit" value="Emitir">
											</td>
										</tr>
									</table>
								</form>
								<?php
							}
						}
						else
						{
							?>
							<form action="personaevaxarea.php" method="post">
								<table>
									<tr>
										<th align="left">
											Unid. Estrat&eacute;gica
										</th>
										<th>
											:
										</th>
										<td>
											<select name="coduniestrat">
												<?php
													$resultquniestrat = llenacombo("uniestrat");
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													while ($reguniestrat = mysqli_fetch_assoc($resultquniestrat))
													{
														echo "<option value='".$reguniestrat["coduniestrat"]."'>";
														echo $reguniestrat["nomuniestrat"];
														echo "</option>";
													}
												?>
											</select>
										</td>
										<td>
											<input type="submit" value="Ir a &Aacute;reas">
										</td>
									</tr>
								</table>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/procesos/reportes/indexreportes.php'><img src='../../images/back.jpg' width='30' height='30' title='Volver'></a>
									</td>
									<td width='504' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<?php
						pie();
					?>
				</body>
			</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Reportes
					</title>
					<link href="../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("Reportes");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>