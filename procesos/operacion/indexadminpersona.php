<?php
	include "../../lib/handWebEva.php";
	include "../../lib/handVarEva.php";
	include "../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if (($codtipousuarioevaAux == 1) or ($codtipousuarioevaAux == 3))
	{
		?>
		<html>
			<head>
				<title>
					Eva - Personas 
				</title>
				<link href="../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
				<?php
						$webserver = nomserverweb();
						if ($codtipousuarioevaAux == 1)
						{
							cabezal("ASIGNAR / DESASIGNAR OBJETIVOS GENERALES A PERSONAS");
						}
						
						if ($codtipousuarioevaAux == 3)
						{
							cabezalevaluador("ASIGNAR / DESASIGNAR OBJETIVOS GENERALES A PERSONAS");
						}
						?>
							<font size="4"><b>Administrar Personas</b></font>
							<hr />
							<div id="botonup">
								<table>
									<tr>
										<td width='25' align='center' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
										</td>
										<td width='575' align='center' valign='top'>
											
										</td>
									</tr>
								</table>
							</div>
							<hr />
							</br>
							<table>
								<tr>
									<!--<td width = '200' align = 'left'><button style = 'width:170; height:70'><a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/procesos/asignadores/objetivoxpersona/asdesobjetivoxpersona.php'>Asignar Objetivos Generales</a></button></td>-->
									<td width = '300' align = 'left'><a style="text-decoration: none; color:black" href="http://<?php echo $webserver;?>/eva/procesos/evaluacion/evdesempeno.php"><button style = 'width:170; height:70'>Evaluar</button></a></td>
									<td width = '300' align = 'right'><a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/procesos/asignadores/objetivoespecificoxpersona/asdesobjetivoespecificoxpersona.php'><button style = 'width:170; height:70'>Asignar Objetivos Espec&iacute;ficos</button></a></td>
								</tr>
							</table>
							</br>
							<hr />
							<div id='botonbottom'>
								<table>
									<tr>
										<td width='568' align='center' valign='center'>
											
										</td>
										<td width='32' align='center' valign='center'>
											<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../images/logout.jpg' width='30' height='30' title='Salir'></a>
										</td>
									</tr>
								</table>
							</div>
							<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Personas
					</title>
					<link href="../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE PERSONAS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>