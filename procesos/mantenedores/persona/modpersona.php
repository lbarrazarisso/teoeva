<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handVarEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Personas
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
					
				<?php
					cabezal("MODIFICAR PERSONAS");
					
						$webserver = nomserverweb();
						
						?>
						<font size="4"><b>Modificar Persona</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					
						$swconsulta = 0;
						
						//PROCESO MODIFICAR
					
						if ((isset($_REQUEST["nompersonamod"])) and
							(isset($_REQUEST["appaternomod"])) and 
							(isset($_REQUEST["apmaternomod"])) and 
							(isset($_REQUEST["rutpersonamod"])) and 
							(isset($_REQUEST["fechaingresomod"])) and 
							(isset($_REQUEST["codcargomod"])) and 
							(isset($_REQUEST["codareamod"])) and 
							(isset($_REQUEST["activomod"])))
						{
							
							$swmod = 0;
							
							if ($_REQUEST["nompersonamod"] == "")
							{
								$swmod = 2;
							}
							
							if ($_REQUEST["appaternomod"] == "")
							{
								$swmod = 3;
							}
							
							if ($_REQUEST["apmaternomod"] == "")
							{
								$swmod = 4;
							}
							
							if ($_REQUEST["rutpersonamod"] == "")
							{
								$swmod = 5;
							}
							
							if (validarut($_REQUEST["rutpersonamod"]) == false)
							{
								mensaje("RUT Ingresado no es válido");
								$swmod = 7;
							}
							
							if ($_REQUEST["fechaingresomod"] == "")
							{
								$swmod = 10;
							}
							
							if (validafecha($_REQUEST["fechaingresomod"]) == false)
							{
								$swing = 5;
							}
							
							if ($_REQUEST["codcargomod"] == "")
							{
								$swmod = 11;
							}
							
							if ($_REQUEST["codareamod"] == "")
							{
								$swmod = 12;
							}
							
							if ($_REQUEST["activomod"] == "")
							{
								$swmod = 12;
							}
							
							if ($swmod == 0)
							{
								
								// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
								$rutpersonaAuxMod2 = $_REQUEST["rutpersonamod"];
								
								// LLAMADO A FUNCION DE CONSULTA
								$resultqMod2 = consultatodo("persona", "rutpersona", $rutpersonaAuxMod2);
								
								// FORMATEO DE LOS RESULTADOS
								$regpersMod2 = mysqli_fetch_assoc($resultqMod2);
								
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								$nompersonaAuxMod2 = $regpersMod2["nompersona"];
								$appaternoAuxMod2 = $regpersMod2["appaterno"];
								$apmaternoAuxMod2 = $regpersMod2["apmaterno"];
								$rutpersonaAuxMod3 = $regpersMod2["rutpersona"];
								$fechaingresoAuxMod2 = $regpersMod2["fechaingreso"];
								
								$codcargoAuxMod2 = $regpersMod2["codcargo"];
								$resultqcargoAuxMod2 = consultatodo("cargo", "codcargo", $codcargoAuxMod2);
								$regcargoAuxMod2 = mysqli_fetch_assoc($resultqcargoAuxMod2);
								$nomcargoAuxMod2 = $regcargoAuxMod2["nomcargo"];
								
								$codareaAuxMod2 = $regpersMod2["codarea"];
								$resultqareaAuxMod2 = consultatodo("area", "codarea", $codareaAuxMod2);
								$regareaAuxMod2 = mysqli_fetch_assoc($resultqareaAuxMod2);
								$nomareaAuxMod2 = $regareaAuxMod2["nomarea"];
								
								$activoAuxMod2 = $regpersMod2["activo"];
								
								$sw = "0";
								
								// PREGUNTA SI CAMPO NOMBRE HA SIDO MODIFICADO
								if ($_REQUEST["nompersonamod"] != $nompersonaAuxMod2)
								{
									modificaregistro("persona", "nompersona", $_REQUEST["nompersonamod"], "rutpersona", $rutpersonaAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI CAMPO APELLIDO PATERNO HA SIDO MODIFICADO
								if ($_REQUEST["appaternomod"] != $appaternoAuxMod2)
								{
									modificaregistro("persona", "appaterno", $_REQUEST["appaternomod"], "rutpersona", $rutpersonaAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI CAMPO APELLIDO MATERNO HA SIDO MODIFICADO
								if ($_REQUEST["apmaternomod"] != $apmaternoAuxMod2)
								{
									modificaregistro("persona", "apmaterno", $_REQUEST["apmaternomod"], "rutpersona", $rutpersonaAuxMod3);
									$sw = "1";
								}

								// PREGUNTA SI CAMPO RUT HA SIDO MODIFICADO
								if ($_REQUEST["rutpersonamod"] != $rutpersonaAuxMod2)
								{
									modificaregistro("persona", "rutpersona", $_REQUEST["rutpersonamod"], "rutpersona", $rutpersonaAuxMod3);
									$sw = "1";
								}					
								
								// PREGUNTA SI CAMPO F. INGRESO HA SIDO MODIFICADO
								if ($_REQUEST["fechaingresomod"] != $fechaingresoAuxMod2)
								{
									modificaregistro("persona", "fechaingreso", $_REQUEST["fechaingresomod"], "rutpersona", $rutpersonaAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI CAMPO CARGO HA SIDO MODIFICADO
								if ($_REQUEST["codcargomod"] != $codcargoAuxMod2)
								{
									modificaregistro("persona", "codcargo", $_REQUEST["codcargomod"], "rutpersona", $rutpersonaAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI CAMPO AREA HA SIDO MODIFICADO
								if ($_REQUEST["codareamod"] != $codareaAuxMod2)
								{
									modificaregistro("persona", "codarea", $_REQUEST["codareamod"], "rutpersona", $rutpersonaAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI CAMPO ACTIVO HA SIDO MODIFICADO
								if ($_REQUEST["activomod"] != $activoAuxMod2)
								{
									modificaregistro("persona", "activo", $_REQUEST["activomod"], "rutpersona", $rutpersonaAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI HUBIERON MODIFICACIONES
								if ($sw == "0")
								{
									mensaje("No se hicieron modificaciones al registro.");											
									?>
									<table border = "0">
										<tr><th align="left">Nombres</th>			<th>:</th>	<td><?php echo $nompersonaAuxMod2;?></td></tr>
										<tr><th align="left">Ap. Paterno</th>		<th>:</th>	<td><?php echo $appaternoAuxMod2;?></td></tr>
										<tr><th align="left">Ap. Materno</th>		<th>:</th>	<td><?php echo $apmaternoAuxMod2;?></td></tr>
										<tr><th align="left">RUT</th>				<th>:</th>	<td><?php echo $rutpersonaAuxMod3;?></td></tr>
										<tr><th align="left">F. de Ingreso</th>		<th>:</th> 	<td><?php echo $fechaingresoAuxMod2;?></td></tr>
										<tr><th align="left">Cargo</th>				<th>:</th> 	<td><?php echo $nomcargoAuxMod2;?></td></tr>
										<tr><th align="left">&Aacute;rea</th>		<th>:</th>	<td><?php echo $nomareaAuxMod2;?></td></tr>
										<tr><th align="left">Activo?</th>			<th>:</th>	<td><?php echo $activoAuxMod2;?></td></tr>
									</table>
									</br>
									<table>
										<tr>
											<td valign="top">
												<form action="modpersona.php" method="get">
													<input type="submit" value="Modificar otro registro">
												</form>
											</td>
											<td>
												<form action="modpersona.php" method="post">
													<input type="hidden" name="accion" value="mod">
													<input type="hidden" name="rutpersonaAction" value="<?=$rutpersonaAuxMod3?>">
													<input type="submit" value="Modificar datos">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
								else
								{
									despliegapersona($rutpersonaAuxMod3);
									?>
									</br>
									<?php
									mensaje("Datos modificados correctamente.");
									?>
									<table>
										<tr>
											<td valign="top" align='left' width='590'>
												<form action="modpersona.php" method="get">
													<input type="submit" value="Consultar otro registro">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
							}
							else
							{
								mensaje("Debe llenar TODOS LOS CAMPOS!!!");
								$_REQUEST["accion"] = "mod";
								$_REQUEST["rutpersonaAction"] = $_REQUEST["rutpersonamod"];
							}
							$swconsulta = 1;
						}
						
						//FIN PROCESO MODIFICAR
						
						//PRESENTACION DE FORMULARIO PARA MODIFICAR
						
						if ((isset($_REQUEST["accion"])) and ($_REQUEST["accion"] == "mod"))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$rutpersonaMod2 = $_REQUEST["rutpersonaAction"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tablaMod = "persona";
							$campoMod = "rutpersona";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultaMod = consultatodo($tablaMod, $campoMod, $rutpersonaMod2);
							
							// FORMATEO DE LOS RESULTADOS
							$regpersMod = mysqli_fetch_assoc($punteroconsultaMod);
							
							// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
							$nompersonaMod = $regpersMod["nompersona"];
							$appaternoMod = $regpersMod["appaterno"];
							$apmaternoMod = $regpersMod["apmaterno"];
							$rutpersonaMod = $regpersMod["rutpersona"];
							$fechaingresoMod = $regpersMod["fechaingreso"];
							
							$codcargoMod = $regpersMod["codcargo"];
							$resultqcargomod = consultatodo("cargo", "codcargo", $codcargomod);
							$regcargomod = mysqli_fetch_assoc($resultqcargomod);
							$nomcargomod = $regcargomod["nomcargo"];
							
							$codareaMod = $regpersMod["codarea"];
							$resultqareamod = consultatodo("area", "codarea", $codareaMod);
							$regareamod = mysqli_fetch_assoc($resultqareamod);
							$nomareamod = $regareamod["nomarea"];
							
							$activoMod = $regpersMod["activo"];
							
							//$codcargoMod = $regpersMod["codcargo"];
							//$codareaMod = $regpersMod["codarea"];
							
							//PRESENTACION DEL FORMULARIO
							?>
							</br>
							<form action="modpersona.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
								<table border = "0">
									<tr><th align="left">Nombres</th>			<th>:</th>	<td><input type="text" name="nompersonamod" value="<?=$nompersonaMod?>"></td></tr>
									<tr><th align="left">Ap. Paterno</th>		<th>:</th>	<td><input type="text" name="appaternomod" value="<?=$appaternoMod?>"></td></tr>
									<tr><th align="left">Ap. Materno</th>		<th>:</th>	<td><input type="text" name="apmaternomod" value="<?=$apmaternoMod?>"></td></tr>
									<tr><th align="left">RUT</th>				<th>:</th>	<td><input type="text" name="rutpersonamod" value="<?=$rutpersonaMod?>" readonly></td></tr>
									<tr><th align="left">F. de Ingreso</th>		<th>:</th> 	<td><input type="text" name="fechaingresomod" value="<?=$fechaingresoMod?>"></td></tr>
									<tr>
										<th align="left">
											Cargo
										</th>
										<th>
											:
										</th> 	
										<td>
											<select name="codcargomod">
												<?php
													$resultqcargo = llenacombo("cargo");
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													while ($regcargo = mysqli_fetch_assoc($resultqcargo))
													{
														if ($regcargo["codcargo"] == $codcargoMod)
														{
															echo "<option value='".$regcargo["codcargo"]."' selected>";
															echo $regcargo["nomcargo"];
															echo "</option>";													
														}
														else
														{
															echo "<option value='".$regcargo["codcargo"]."'>";
															echo $regcargo["nomcargo"];
															echo "</option>";
														}
													}
												?>
											</select>
										</td>
									</tr>
									<tr>
										<th align="left">
											&Aacute;rea
										</th>
										<th>
											:
										</th>
										<td>
											<select name="codareamod">
												<?php
													$resultqarea = llenacombo("area");
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													while ($regarea = mysqli_fetch_assoc($resultqarea))
													{
														if ($regarea["codarea"] == $codareaMod)
														{
															echo "<option value='".$regarea["codarea"]."' selected>";
															echo $regarea["nomarea"];
															echo "</option>";																	
														}
														else
														{
															echo "<option value='".$regarea["codarea"]."'>";
															echo $regarea["nomarea"];
															echo "</option>";
														}
													}
												?>
											</select>
										</td>
									</tr>
									<tr>
										<th align="left">
											Activo?
										</th>
										<th>
											:
										</th>
										<td>
											<select name="activomod">
												<option value=''>
													- Seleccione
												</option>
												<?php
												if ($activoMod == 'S')
												{
													echo "<option value='S' selected>";
													echo "	- Si";
													echo "</option>";
													echo "<option value='N'>";
													echo "  - No";
													echo "</option>";
												}
												if ($activoMod == 'N')
												{
													echo "<option value='S'>";
													echo "	- Si";
													echo "</option>";
													echo "<option value='N' selected>";
													echo "  - No";
													echo "</option>";
												}
												if ($activoMod == '')
												{
													echo "<option value='S'>";
													echo "	- Si";
													echo "</option>";
													echo "<option value='N'>";
													echo "  - No";
													echo "</option>";
												}
												?>
											</select>
										</td>
									</tr>									
								</table>
								</br>
								<table>
									<tr><td width='295' align='left'><input type="submit" value="Modificar"></td><td width='295' align='right'><button><a style='text-decoration:none; color:black'href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/persona/modpersona.php'>Modificar otro registro</a></button></td></tr>
								</table>
							</form>
							<?php
							$swconsulta = 1;
						}
						
						//FIN FORMULARIO PARA MODIFICAR
						
						// RESULTADO CONSULTA
						
						if ((isset($_REQUEST["rutpersonaconsulta"])) and ($_REQUEST["rutpersonaconsulta"] != ""))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$rutpersonaConsultaAux = $_REQUEST["rutpersonaconsulta"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tabla = "persona";
							$campo = "rutpersona";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultacon = consultatodo($tabla, $campo, $rutpersonaConsultaAux);
							
							// FORMATEO DE LOS RESULTADOS
							$regperscon = mysqli_fetch_assoc($punteroconsultacon);
								
							if ($regperscon["rutpersona"] == "")
							{
								mensaje("Persona inexistente");
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="modpersona.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<hr />
								<?php
							}
							else
							{
								$rutpersonaConsultaAux2 = $regperscon["rutpersona"];
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								despliegapersona($rutpersonaConsultaAux2);
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="modpersona.php" method="post">
												<input type="hidden" name="accion" value="mod">
												<input type="hidden" name="rutpersonaAction" value="<?=$rutpersonaConsultaAux2?>">
												<input type="submit" value="Modificar datos">
											</form>
										</td>
										<td>
											<form action="modpersona.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<hr />
								<?php
							}
							$swconsulta = 1;
						}
						
						// FIN RESULTADO CONSULTA

						// FORMULARIO BUSQUEDA REGISTRO
						
						if ($swconsulta == 0)
						{
							?>
							<form action="modpersona.php" method="get">
								</br>
								<table border="0">
									<tr><th>Ingrese RUT de Empleado</th>	<th>:</th>	<td><input type="text" name="rutpersonaconsulta"></td><td><input type="submit" value="Buscar"></td></tr>
								</table>
								</br>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Personas
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE PERSONAS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCION</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Menu Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>