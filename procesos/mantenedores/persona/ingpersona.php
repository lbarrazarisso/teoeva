<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handVarEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Personas
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
				<?php
					cabezal("MANTENEDOR DE PERSONAS");
						$webserver = nomserverweb();
						?>
						<font size="4"><b>Ingresar Persona</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
						// PROCEDIMIENTO PARA INSERTAR REGISTRO
						if ((isset($_REQUEST["nompersonaing"])) and
							(isset($_REQUEST["appaternoing"])) and 
							(isset($_REQUEST["apmaternoing"])) and  
							(isset($_REQUEST["rutpersonaing"])) and 
							(isset($_REQUEST["fechaingresoing"])) and 
							(isset($_REQUEST["codcargoing"])) and 
							(isset($_REQUEST["codareaing"])) and 
							(isset($_REQUEST["activoing"])))
						{
							$nompersonaAgain = $_REQUEST["nompersonaing"];
							$appaternoAgain = $_REQUEST["appaternoing"];
							$apmaternoAgain = $_REQUEST["apmaternoing"];
							$rutpersonaAgain = $_REQUEST["rutpersonaing"];
							$fechaingresoAgain = $_REQUEST["fechaingresoing"];
							$codcargoAgain = $_REQUEST["codcargoing"];
							$codareaAgain = $_REQUEST["codareaing"];
							$activoAgain = $_REQUEST["activoing"];
							
							$swing = 0;
							
							if ($_REQUEST["nompersonaing"] == "")
							{
								$swing = 1;
							}
							
							if ($_REQUEST["appaternoing"] == "")
							{
								$swing = 2;
							}
							
							if ($_REQUEST["apmaternoing"] == "")
							{
								$swing = 3;
							}
							
							if ($_REQUEST["rutpersonaing"] == "")
							{
								$swing = 4;
							}
							
							if (validarut($_REQUEST["rutpersonaing"]) == false)
							{
								mensaje("RUT Ingresado no es válido");
								$swing = 5;
							}
							
							if ($_REQUEST["fechaingresoing"] == "")
							{
								$swing = 9;
							}
							
							if (validafecha($_REQUEST["fechaingresoing"]) == false)
							{
								$swing = 5;
							}
							
							if ($_REQUEST["codcargoing"] == "")
							{
								$swing = 10;
							}
							
							if ($_REQUEST["codareaing"] == "")
							{
								$swing = 11;
							}
							
							if ($_REQUEST["activoing"] == "")
							{
								$swing = 12;
							}
							
							if ($swing == 0)
							{
							
								// ASIGNACION DE DATOS INGRESADOS Y VALIDADOS A VARIABLES AUXILIARES
								$nompersonaAuxing = $_REQUEST["nompersonaing"];
								$appaternoAuxing = $_REQUEST["appaternoing"];
								$apmaternoAuxing = $_REQUEST["apmaternoing"];
								$rutpersonaAuxing = $_REQUEST["rutpersonaing"];
								$fechaingresoAuxing = $_REQUEST["fechaingresoing"];
								$codcargoAuxing = $_REQUEST["codcargoing"];
								$codareaAuxing = $_REQUEST["codareaing"];
								$activoAuxing = $_REQUEST["activoing"];
								
								//LLAMADA A PROCEDIMIENTO PARA INSERTAR REGISTRO
								$tablaing = "persona";
								$valoresing = "'".$nompersonaAuxing."', '".$appaternoAuxing."', '".$apmaternoAuxing."', '".$rutpersonaAuxing."', '".$fechaingresoAuxing."', '".$codcargoAuxing."', '".$codareaAuxing."', '".$activoAuxing."'";
								insertaregistro($tablaing, $valoresing);
								
								//CONSULTA A BASE PARA OBTENER CODIGO DE PERSONA
								$resultqdespliegapersona = consultatodo($tablaing, "rutpersona", $rutpersonaAuxing);
								$regdespliegapersona = mysqli_fetch_assoc($resultqdespliegapersona);
								$rutpersonaAuxing = $regdespliegapersona["rutpersona"];						
								
								//VISUALIZA REGISTRO INSERTADO
								despliegapersona($rutpersonaAuxing);
								?>
								<!-- FIN VISUALIZA REGISTRO -->
								
								</br>
								<table>
									<tr>
										<td valign="top" align='left' width='590'>
											<form action="ingpersona.php" method="get">
												<input type="submit" value="Ingresar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							else
							{
								mensaje("Debe llenar TODOS LOS CAMPOS!!!");
								
								//PRESENTACION DE FORMULARIO PARA INGRESAR CON DATOS PREEXISTENTES
					
								?>
								<form action="ingpersona.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
									</br>
									<table border="0">
										<tr><th align="left">Nombres</th>			<th>:</th>	<td><input type="text" name="nompersonaing" value="<?=$nompersonaAgain?>" style='width:300'></td></tr>
										<tr><th align="left">Ap. Paterno</th>		<th>:</th>	<td><input type="text" name="appaternoing" value="<?=$appaternoAgain?>" style='width:300'></td></tr>
										<tr><th align="left">Ap. Materno</th>		<th>:</th>	<td><input type="text" name="apmaternoing" value="<?=$apmaternoAgain?>" style='width:300'></td></tr>
										<tr><th align="left">RUT</th>				<th>:</th>	<td><input type="text" name="rutpersonaing" value="<?=$rutpersonaAgain?>" style='width:300'></td></tr>
										<tr><th align="left">F. de Ingreso</th>		<th>:</th> 	<td><input type="text" name="fechaingresoing" value="<?=$fechaingresoAgain?>" style='width:300'></td></tr>
										<tr>
											<th align="left">
												Cargo
											</th>
											<th>
												:
											</th> 	
											<td>
												<select name="codcargoing" style='width:300'>
													<?php
														$resultqcargo = llenacombo("cargo");
														echo "<option value=''>";
														echo "- Seleccione";
														echo "</option>";
														while ($regcargo = mysqli_fetch_assoc($resultqcargo))
														{
															if ($regcargo["codcargo"] == $codcargoAgain)
															{
																echo "<option value='".$regcargo["codcargo"]."' selected>";
																echo $regcargo["nomcargo"];
																echo "</option>";													
															}
															else
															{
																echo "<option value='".$regcargo["codcargo"]."'>";
																echo $regcargo["nomcargo"];
																echo "</option>";
															}
														}
													?>
												</select>
											</td>
										</tr>
										<tr>
											<th align="left">
												&Aacute;rea
											</th>
											<th>
												:
											</th>
											<td>
												<select name="codareaing" style='width:300'>
													<?php
														$resultqarea = llenacombo("area");
														echo "<option value=''>";
														echo "- Seleccione";
														echo "</option>";
														while ($regarea = mysqli_fetch_assoc($resultqarea))
														{
															if ($regarea["codarea"] == $codareaAgain)
															{
																echo "<option value='".$regarea["codarea"]."' selected>";
																echo $regarea["nomarea"];
																echo "</option>";																	
															}
															else
															{
																echo "<option value='".$regarea["codarea"]."'>";
																echo $regarea["nomarea"];
																echo "</option>";
															}
														}
													?>
												</select>
											</td>
										</tr>
										<tr>
											<th align="left">
												Activo?
											</th>
											<th>
												:
											</th>
											<td>
												<select name="activoing">
													<option value=''>
														- Seleccione
													</option>
													<?php
													if ($activoAgain == 'S')
													{
														echo "<option value='S' selected>";
														echo "	- Si";
														echo "</option>";
														echo "<option value='N'>";
														echo "  - No";
														echo "</option>";
													}
													if ($activoAgain == 'N')
													{
														echo "<option value='S'>";
														echo "	- Si";
														echo "</option>";
														echo "<option value='N' selected>";
														echo "  - No";
														echo "</option>";
													}
													?>
												</select>
											</td>
										</tr>
									</table>
									</br>
									<table>
										<tr>
											<td valign="top">
												<input type="submit" value="Ingresar datos">
											</td>
										</tr>
									</table>
								</form>
								<?php
							}
						}
						else
						{
						
							//PRESENTACION DE FORMULARIO PARA INGRESAR

							$_REQUEST["nompersonaing"] = "";
							$_REQUEST["appaternoing"] = "";
							$_REQUEST["apmaternoing"] = "";
							$_REQUEST["rutpersonaing"] = "";
							$_REQUEST["fechaingresoing"] = "";
							$_REQUEST["codcargoing"] = "";
							$_REQUEST["codareaing"] = "";
							$_REQUEST["activo"] = "";
							?>
							<form action="ingpersona.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
								</br>
								<table border="0">
									<tr><th align="left">Nombres</th>			<th>:</th>	<td><input type="text" name="nompersonaing" style='width:300'></td></tr>
									<tr><th align="left">Ap. Paterno</th>		<th>:</th>	<td><input type="text" name="appaternoing" style='width:300'></td></tr>
									<tr><th align="left">Ap. Materno</th>		<th>:</th>	<td><input type="text" name="apmaternoing" style='width:300'></td></tr>
									<tr><th align="left">RUT</th>				<th>:</th>	<td><input type="text" name="rutpersonaing" style='width:300'></td></tr>
									<tr><th align="left">F. de Ingreso</th>		<th>:</th> 	<td><input type="text" name="fechaingresoing" style='width:300'></td></tr>
									<tr>
										<th align="left">
											Cargo
										</th>
										<th>
											:
										</th> 	
										<td>
											<select name="codcargoing" style='width:300'>
												<?php
													$resultqcargo = llenacombo("cargo");
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													while ($regcargo = mysqli_fetch_assoc($resultqcargo))
													{
														echo "<option value='".$regcargo["codcargo"]."'>";
														echo $regcargo["nomcargo"];
														echo "</option>";
													}
												?>
											</select>
										</td>
									</tr>
									<tr>
										<th align="left">
											&Aacute;rea
										</th>
										<th>
											:
										</th>
										<td>
											<select name="codareaing" style='width:300'>
												<?php
													$resultqarea = llenacombo("area");
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													while ($regarea = mysqli_fetch_assoc($resultqarea))
													{
														echo "<option value='".$regarea["codarea"]."'>";
														echo $regarea["nomarea"];
														echo "</option>";
													}
												?>
											</select>
										</td>
									</tr>
									<tr>
										<th align="left">
											Activo?
										</th>
										<th>
											:
										</th>
										<td>
											<select name="activoing">
												<option value=''>
													- Seleccione
												</option>
												<option value='S'>
													- Si
												</option>
												<option value='N'>
													- No
												</option>
											</select>
										</td>
									</tr>
								</table>
								</br>
								<table>
									<tr>
										<td valign="top">
											<input type="submit" value="Ingresar datos">
										</td>
									</tr>
								</table>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Personas
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE PERSONAS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCION</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Menu Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>