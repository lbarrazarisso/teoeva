<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - &Aacute;reas
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
				<?php
					cabezal("MANTENEDOR DE AREAS");
						
						$webserver = nomserverweb();
						?>
						<font size="4"><b>Ingresar &Aacute;rea</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php					
						// PROCEDIMIENTO PARA INSERTAR REGISTRO
						if ((isset($_REQUEST["nomareaing"])) and (isset($_REQUEST["coduniestrating"])) and (isset($_REQUEST["rutevaluadoring"])))
						{
							// ASIGNACION DE DATOS INGRESADOS Y VALIDADOS A VARIABLES AUXILIARES
							$nomareaAgain = $_REQUEST["nomareaing"];
							$coduniestratAgain = $_REQUEST["coduniestrating"];
							$coduniestratAgain = $_REQUEST["rutevaluadoring"];
							
							$swing = 0;
							
							if ($_REQUEST["nomareaing"] == "")
							{
								$swing = 1;
							}
							
							if ($_REQUEST["coduniestrating"] == "")
							{
								$swing = 3;
							}
							
							if ($_REQUEST["rutevaluadoring"] == "")
							{
								$swing = 4;
							}
							
							if ($swing == 0)
							{
							
								// ASIGNACION DE DATOS INGRESADOS Y VALIDADOS A VARIABLES AUXILIARES
								
								$nomareaAuxing = $_REQUEST["nomareaing"];
								$coduniestratAuxing = $_REQUEST["coduniestrating"];
								$rutevaluadorAuxing = $_REQUEST["rutevaluadoring"];
								
								//LLAMADA A PROCEDIMIENTO PARA INSERTAR REGISTRO
								$tablaing = "area";
								$valoresing = "'', '".$nomareaAuxing."', '".$rutevaluadorAuxing."', '".$coduniestratAuxing."'";
								insertaregistro($tablaing, $valoresing);
								
								//CONSULTA A BASE PARA OBTENER CODIGO DE AREA
								$resultqdespliegaarea = consultatodo($tablaing, "nomarea", $nomareaAuxing);
								$regdespliegaarea = mysqli_fetch_assoc($resultqdespliegaarea);
								$codareaAuxing = $regdespliegaarea["codarea"];
								
								//VISUALIZA REGISTRO INSERTADO
								despliegaarea($codareaAuxing);
								?>
								<!-- FIN VISUALIZA REGISTRO -->
								
								</br>
								<table>
									<tr>
										<td valign="top" align='left' width='590'>
											<form action="ingarea.php" method="get">
												<input type="submit" value="Ingresar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							else
							{
								mensaje("Debe llenar TODOS LOS CAMPOS!!!");
								
								//PRESENTACION DE FORMULARIO PARA INGRESAR CON DATOS PREEXISTENTES
					
								?>

								<form action="ingarea.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
									</br>
									<table border="0">
										<tr><th align="left">Nombre &Aacute;rea</th>		<th>:</th>	<td><input type="text" name="nomareaing" value="<?=$nomareaAgain?>" style='width:300'></td></tr>
										<tr>
											<th align="left">
												Evaluador
											</th>
											<th>
												:
											</th> 	
											<td>
												<select name="rutevaluadoring" style='width:300'>
													<?php
														$resultqevaluador = llenacombo("evaluador");
														echo "<option value=''>";
														echo "- Seleccione";
														echo "</option>";
														while ($regevaluador = mysqli_fetch_assoc($resultqevaluador))
														{
															if ($regevaluador["rutevaluador"] == $rutevaluadorAgain)
															{
																echo "<option value='".$regevaluador["rutevaluador"]."' selected>";
																echo $regevaluador["nomevaluador"]." ".$regevaluador["appaterno"]." ".$regevaluador["apmaterno"];
																echo "</option>";																	
															}
															else
															{
																echo "<option value='".$regevaluador["rutevaluador"]."'>";
																echo $regevaluador["nomevaluador"]." ".$regevaluador["appaterno"]." ".$regevaluador["apmaterno"];
																echo "</option>";
															}
														}
													?>
												</select>
											</td>
										</tr>
										<tr>
											<th align="left">
												Unidad Estrat&eacute;gica
											</th>
											<th>
												:
											</th> 	
											<td>
												<select name="coduniestrating" style='width:300'>
													<?php
														$resultquniestrat = llenacombo("uniestrat");
														echo "<option value=''>";
														echo "- Seleccione";
														echo "</option>";
														while ($reguniestrat = mysqli_fetch_assoc($resultquniestrat))
														{
															if ($reguniestrat["coduniestrat"] == $coduniestratAgain)
															{
																echo "<option value='".$reguniestrat["coduniestrat"]."' selected>";
																echo $reguniestrat["nomuniestrat"];
																echo "</option>";																	
															}
															else
															{
																echo "<option value='".$reguniestrat["coduniestrat"]."'>";
																echo $reguniestrat["nomuniestrat"];
																echo "</option>";
															}
														}
													?>
												</select>
											</td>
										</tr>
									</table>
									</br>
									<table>
										<tr>
											<td valign="top">
												<input type="submit" value="Ingresar datos">
											</td>
										</tr>
									</table>											
								</form>
								<?php
							}
						}
						else
						{
						
							//PRESENTACION DE FORMULARIO PARA INGRESAR
							
							$_REQUEST["nomareaing"] = "";
							$_REQUEST["rutevaluadoring"] = "";
							$_REQUEST["coduniestrating"] = "";

							?>
							<form action="ingarea.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
								</br>
								<table border="0">
									<tr><th align="left">Nombre &Aacute;rea</th>	<th>:</th>	<td><input type="text" name="nomareaing" style='width:300'></td></tr>
									<tr>
										<th align="left">
											Evaluador
										</th>
										<th>
											:
										</th> 	
										<td>
											<select name="rutevaluadoring" style='width:300'>
												<?php
													$resultqevaluador = llenacombo("evaluador");
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													while ($regevaluador = mysqli_fetch_assoc($resultqevaluador))
													{
														echo "<option value='".$regevaluador["rutevaluador"]."'>";
														echo $regevaluador["nomevaluador"]." ".$regevaluador["appaterno"]." ".$regevaluador["apmaterno"];
														echo "</option>";
													}
												?>
											</select>
										</td>
									</tr>						
									<tr>
										<th align="left">
											Unidad Estrat&eacute;gica
										</th>
										<th>
											:
										</th> 	
										<td>
											<select name="coduniestrating" style='width:300'>
												<?php
													$resultquniestrat = llenacombo("uniestrat");
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													while ($reguniestrat = mysqli_fetch_assoc($resultquniestrat))
													{
														echo "<option value='".$reguniestrat["coduniestrat"]."'>";
														echo $reguniestrat["nomuniestrat"];
														echo "</option>";
													}
												?>
											</select>
										</td>
									</tr>
								</table>
								</br>
								<table>
									<tr>
										<td valign="top">
											<input type="submit" value="Ingresar datos">
										</td>
									</tr>
								</table>		
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - &Aacute;reas
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE AREAS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCION</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>