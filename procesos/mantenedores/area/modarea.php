<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - &Aacute;reas
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
					
				<?php
					cabezal("MODIFICAR AREAS");
					
						$webserver = nomserverweb();
						
						?>
						<font size="4"><b>Modificar &Aacute;rea</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					
						$swconsulta = 0;
						
						//PROCESO MODIFICAR
					
						if ((isset($_REQUEST["codareamod"])) and (isset($_REQUEST["nomareamod"])) and	(isset($_REQUEST["rutevaluadormod"])) and (isset($_REQUEST["coduniestratmod"])) /*and (isset($_REQUEST["codcargosupmod"]))*/)
						{
							
							$swmod = 0;
							
							if ($_REQUEST["codareamod"] == "")
							{
								$swmod = 1;
							}
							
							if ($_REQUEST["nomareamod"] == "")
							{
								$swmod = 2;
							}
							
							if ($_REQUEST["rutevaluadormod"] == "")
							{
								$swmod = 3;
							}
							
							if ($_REQUEST["coduniestratmod"] == "")
							{
								$swmod = 4;
							}
							
							/*if ($_REQUEST["codcargosupmod"] == "")
							{
								$swmod = 5;
							}*/
							
							if ($swmod == 0)
							{
								
								// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
								$codareaAuxMod2 = $_REQUEST["codareamod"];
								
								// LLAMADO A FUNCION DE CONSULTA
								$resultqMod2 = consultatodo("area", "codarea", $codareaAuxMod2);
								
								// FORMATEO DE LOS RESULTADOS
								$regareaMod2 = mysqli_fetch_assoc($resultqMod2);
								
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								
								$codareaAuxMod3 = $regareaMod2["codarea"];
								$nomareaAuxMod2 = $regareaMod2["nomarea"];
								$rutevaluadorAuxMod2 = $regareaMod2["rutevaluador"];
								
								$coduniestratAuxMod2 = $regareaMod2["coduniestrat"];
								$resultquniestratAuxMod2 = consultatodo("uniestrat", "coduniestrat", $coduniestratAuxMod2);
								$reguniestratAuxMod2 = mysqli_fetch_assoc($resultquniestratAuxMod2);
								$nomuniestratAuxMod2 = $reguniestratAuxMod2["nomuniestrat"];
								
								$sw = "0";
								
								// PREGUNTA SI CAMPO NOMBRE DE CARGO HA SIDO MODIFICADO
								if ($_REQUEST["nomareamod"] != $nomareaAuxMod2)
								{
									modificaregistro("area", "nomarea", $_REQUEST["nomareamod"], "codarea", $codareaAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI CAMPO OBJETIVO HA SIDO MODIFICADO
								if ($_REQUEST["rutevaluadormod"] != $rutevaluadorAuxMod2)
								{
									modificaregistro("area", "dirarea", $_REQUEST["rutevaluadormod"], "codarea", $codareaAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI CAMPO FAMILIA DE CARGO HA SIDO MODIFICADO
								if ($_REQUEST["coduniestratmod"] != $coduniestratAuxMod2)
								{
									modificaregistro("area", "coduniestrat", $_REQUEST["coduniestratmod"], "codarea", $codareaAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI HUBIERON MODIFICACIONES
								if ($sw == "0")
								{
									mensaje("No se hicieron modificaciones al registro.");											
									?>
									<table border = "0">
										<tr><th align="left">C&oacute;digo de &Aacute;rea</th>		<th>:</th>	<td><?php echo $codareaAuxMod3;?></td></tr>
										<tr><th align="left">Nombre de &Aacute;rea</th>		<th>:</th>	<td><?php echo $nomareaAuxMod2;?></td></tr>
										<tr><th align="left">Evaluador</th>		<th>:</th>	<td><?php echo $nomevaluadorAuxMod2.$appaternoevaluadorAuxMod2.$apmaternoevaluadorAuxMod2;?></td></tr>-->
										<tr><th align="left">Unidad Estrat&eacute;gica</th>	<th>:</th>	<td><?php echo $nomuniestratAuxMod2;?></td></tr>
									</table>
									</br>
									<table>
										<tr>
											<td valign="top">
												<form action="modarea.php" method="get">
													<input type="submit" value="Modificar otro registro">
												</form>
											</td>
											<td>
												<form action="modarea.php" method="post">
													<input type="hidden" name="accion" value="mod">
													<input type="hidden" name="codareaAction" value="<?=$codareaAuxMod3?>">
													<input type="submit" value="Modificar datos">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
								else
								{
									despliegaarea($codareaAuxMod3);
									?>
									</br>
									<?php
									mensaje("Datos modificados correctamente.");
									?>
									<table>
										<tr>
											<td valign="top" align='left' width='590'>
												<form action="modarea.php" method="get">
													<input type="submit" value="Consultar otro registro">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
							}
							else
							{
								mensaje("Debe llenar TODOS LOS CAMPOS!!!");
								$_REQUEST["accion"] = "mod";
								$_REQUEST["codareaAction"] = $_REQUEST["codareamod"];
							}
							$swconsulta = 1;
						}
						
						//FIN PROCESO MODIFICAR
						
						//PRESENTACION DE FORMULARIO PARA MODIFICAR
						
						if ((isset($_REQUEST["accion"])) and ($_REQUEST["accion"] == "mod"))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$codareaMod2 = $_REQUEST["codareaAction"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tablaMod = "area";
							$campoMod = "codarea";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultaMod = consultatodo($tablaMod, $campoMod, $codareaMod2);
							
							// FORMATEO DE LOS RESULTADOS
							$regareaMod = mysqli_fetch_assoc($punteroconsultaMod);
							
							// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
							
							$codareaMod = $regareaMod["codarea"];
							$nomareaMod = $regareaMod["nomarea"];
							$rutevaluadorMod = $regareaMod["rutevaluador"];
							$coduniestratMod = $regareaMod["coduniestrat"];
						
							//PRESENTACION DEL FORMULARIO
							?>
							</br>
							<form action="modarea.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
								<table border = "0">
									<tr><th align="left">C&oacute;digo de &Aacute;rea</th>	<th>:</th>	<td><input type="text" name="codareamod" value="<?=$codareaMod?>" readonly style='width:300'></td></tr>
									<tr><th align="left">Nombre de &Aacute;rea</th>	<th>:</th>	<td><input type="text" name="nomareamod" value="<?=$nomareaMod?>" style='width:300'></td></tr>
									<tr>
										<th align="left">
											Evaluador
										</th>
										<th>
											:
										</th> 	
										<td>
											<select name="rutevaluadormod" style='width:300'>
												<?php
													$resultqevaluador = llenacombo("evaluador");
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													while ($regevaluador = mysqli_fetch_assoc($resultqevaluador))
													{
														if ($regevaluador["rutevaluador"] == $rutevaluadorMod)
														{
															echo "<option value='".$regevaluador["rutevaluador"]."' selected>";
															echo $regevaluador["nomevaluador"]." ".$regevaluador["appaternoevaluador"]." ".$regevaluador["apmaternoevaluador"];
															echo "</option>";																	
														}
														else
														{
															echo "<option value='".$regevaluador["rutevaluador"]."'>";
															echo $regevaluador["nomevaluador"]." ".$regevaluador["appaternoevaluador"]." ".$regevaluador["apmaternoevaluador"];
															echo "</option>";
														}
													}
												?>
											</select>
										</td>
									</tr>
									<tr>
										<th align="left">
											Unidad Estrat&eacute;gica
										</th>
										<th>
											:
										</th> 	
										<td>
											<select name="coduniestratmod" style='width:300'>
												<?php
													$resultquniestrat = llenacombo("uniestrat");
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													while ($reguniestrat = mysqli_fetch_assoc($resultquniestrat))
													{
														if ($reguniestrat["coduniestrat"] == $coduniestratMod)
														{
															echo "<option value='".$reguniestrat["coduniestrat"]."' selected>";
															echo $reguniestrat["nomuniestrat"];
															echo "</option>";																	
														}
														else
														{
															echo "<option value='".$reguniestrat["coduniestrat"]."'>";
															echo $reguniestrat["nomuniestrat"];
															echo "</option>";
														}
													}
												?>
											</select>
										</td>
									</tr>									
								</table>
								</br>
								<table>
									<tr><td width='295' align='left'><input type="submit" value="Modificar"></td><td width='295' align='right'><button><a style='text-decoration:none; color:black'href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/area/modarea.php'>Modificar otro registro</a></button></td></tr>
								</table>
							</form>
							<?php
							$swconsulta = 1;
						}
						
						//FIN FORMULARIO PARA MODIFICAR
						
						// RESULTADO CONSULTA
						
						if ((isset($_REQUEST["codareaconsulta"])) and ($_REQUEST["codareaconsulta"] != ""))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$codareaConsultaAux = $_REQUEST["codareaconsulta"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tabla = "area";
							$campo = "codarea";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultacon = consultatodo($tabla, $campo, $codareaConsultaAux);
							
							// FORMATEO DE LOS RESULTADOS
							$regareacon = mysqli_fetch_assoc($punteroconsultacon);
								
							if ($regareacon["codarea"] == "")
							{
								mensaje("Área inexistente");
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="modarea.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							else
							{
								$codareaConsultaAux2 = $regareacon["codarea"];
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								despliegaarea($codareaConsultaAux2);
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="modarea.php" method="post">
												<input type="hidden" name="accion" value="mod">
												<input type="hidden" name="codareaAction" value="<?=$codareaConsultaAux2?>">
												<input type="submit" value="Modificar datos">
											</form>
										</td>
										<td>
											<form action="modarea.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							$swconsulta = 1;
						}
						
						// FIN RESULTADO CONSULTA

						// FORMULARIO BUSQUEDA REGISTRO
						
						if ($swconsulta == 0)
						{
							?>
							<form action="modarea.php" method="get">
								</br>
								<table border="0">
									<tr><th>Ingrese C&oacute;digo de &Aacute;rea</th>	<th>:</th>	<td><input type="text" name="codareaconsulta"></td><td><input type="submit" value="Buscar"></td></tr>
								</table>
								</br>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - &Aacute;reas
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE AREAS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>