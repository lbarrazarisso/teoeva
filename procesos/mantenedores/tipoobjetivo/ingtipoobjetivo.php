<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Tipos de Objetivos
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
				<?php
					cabezal("MANTENEDOR DE TIPOS DE OBJETIVOS");
					
						$webserver = nomserverweb();
						?>
						<font size="4"><b>Ingresar Tipo de Objetivo</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
						// PROCEDIMIENTO PARA INSERTAR REGISTRO
						if ((isset($_REQUEST["codtipoobjetivoing"])) and (isset($_REQUEST["nomtipoobjetivoing"])))
						{
							// ASIGNACION DE DATOS INGRESADOS Y VALIDADOS A VARIABLES AUXILIARES
							$codtipoobjetivoAgain = $_REQUEST["codtipoobjetivoing"];
							$nomtipoobjetivoAgain = $_REQUEST["nomtipoobjetivoing"];
							
							$swing = 0;
							
							if ($_REQUEST["codtipoobjetivoing"] == "")
							{
								$swing = 1;
							}
							
							if ($_REQUEST["nomtipoobjetivoing"] == "")
							{
								$swing = 2;
							}
							
							if ($swing == 0)
							{
							
								// ASIGNACION DE DATOS INGRESADOS Y VALIDADOS A VARIABLES AUXILIARES
								
								$codtipoobjetivoAuxing = $_REQUEST["codtipoobjetivoing"];
								$nomtipoobjetivoAuxing = $_REQUEST["nomtipoobjetivoing"];
								
								//LLAMADA A PROCEDIMIENTO PARA INSERTAR REGISTRO
								$tablaing = "tipoobjetivo";
								$valoresing = "'".$codtipoobjetivoAuxing."', '".$nomtipoobjetivoAuxing."'";
								insertaregistro($tablaing, $valoresing);
								
								//VISUALIZA REGISTRO INSERTADO
								despliegatipoobjetivo($codtipoobjetivoAuxing);
								?>
								<!-- FIN VISUALIZA REGISTRO -->
								
								</br>
								<table>
									<tr>
										<td valign="top" align='left' width='590'>
											<form action="ingtipoobjetivo.php" method="get">
												<input type="submit" value="Ingresar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							else
							{
								mensaje("Debe llenar TODOS LOS CAMPOS!!!");
								$_REQUEST["codtipoobjetivoAuxR"] = $_REQUEST["codtipoobjetivoing"];
								
								//PRESENTACION DE FORMULARIO PARA INGRESAR CON DATOS PREEXISTENTES
					
								?>
								<form action="ingtipoobjetivo.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
									</br>
									<table border="0">
										<tr><th align="left">C&oacute;digo de Tipo</th>	<th>:</th>	<td><input type="text" name="codtipoobjetivoing" value="<?=$_REQUEST['codtipoobjetivoAuxR']?>" readonly></td></tr>
										<tr><th align="left">Nombre de Tipo</th>	<th>:</th>	<td><input type="text" name="nomtipoobjetivoing" value="<?=$nomtipoobjetivoAgain?>" ></td></tr>
									</table>
									</br>
									<table>
										<tr>
											<td valign="top">
												<input type="submit" value="Ingresar datos">
											</td>
										</tr>
									</table>
								</form>
								<?php
							}
						}
						else
						{
						
							//PRESENTACION DE FORMULARIO PARA INGRESAR
							
							$_REQUEST["codtipoobjetivoing"] = "";
							$_REQUEST["nomtipoobjetivoing"] = "";

							?>
							<form action="ingtipoobjetivo.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
								</br>
								<table border="0">
									<tr><th align="left">C&oacute;digo de Tipo</th>	<th>:</th>	<td><input type="text" name="codtipoobjetivoing"></td></tr>
									<tr><th align="left">Nombre de Tipo</th>	<th>:</th>	<td><input type="text" name="nomtipoobjetivoing"></td></tr>
								</table>
								</br>
								<table>
									<tr>
										<td valign="top">
											<input type="submit" value="Ingresar datos">
										</td>
									</tr>
								</table>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Objetivo
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE OBJETIVOS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCION</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Menu Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>