<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Tipos de Objetivos
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
					
				<?php
					cabezal("MODIFICAR TIPOS DE OBJETIVOS");
					
						$webserver = nomserverweb();
						
						?>
						<font size="4"><b>Modificar Tipo de Objetivo</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					
						$swconsulta = 0;
						
						//PROCESO MODIFICAR
					
						if ((isset($_REQUEST["codtipoobjetivomod"])) and (isset($_REQUEST["nomtipoobjetivomod"])))
						{
							
							$swmod = 0;
							
							if ($_REQUEST["codtipoobjetivomod"] == "")
							{
								$swmod = 1;
							}
							
							if ($_REQUEST["nomtipoobjetivomod"] == "")
							{
								$swmod = 2;
							}
							
							if ($swmod == 0)
							{
								
								// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
								$codtipoobjetivoAuxMod2 = $_REQUEST["codtipoobjetivomod"];
								
								// LLAMADO A FUNCION DE CONSULTA
								$resultqMod2 = consultatodo("tipoobjetivo", "codtipoobjetivo", $codtipoobjetivoAuxMod2);
								
								// FORMATEO DE LOS RESULTADOS
								$regtipoobjetivoMod2 = mysqli_fetch_assoc($resultqMod2);
								
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								
								$codtipoobjetivoAuxMod3 = $regtipoobjetivoMod2["codtipoobjetivo"];
								$nomtipoobjetivoAuxMod2 = $regtipoobjetivoMod2["nomtipoobjetivo"];
								
								$sw = "0";
								
								// PREGUNTA SI CAMPO NOMBRE DE TIPO REQUISITO HA SIDO MODIFICADO
								if ($_REQUEST["nomtipoobjetivomod"] != $nomtipoobjetivoAuxMod2)
								{
									modificaregistro("tipoobjetivo", "nomtipoobjetivo", $_REQUEST["nomtipoobjetivomod"], "codtipoobjetivo", $codtipoobjetivoAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI HUBIERON MODIFICACIONES
								if ($sw == "0")
								{
									mensaje("No se hicieron modificaciones al registro.");											
									?>

									<table border = "0">
										<tr><th align="left">C&oacute;digo de Tipo</th>	<th>:</th>	<td><?php echo $codtipoobjetivoAuxMod3;?></td></tr>
										<tr><th align="left">Nombre de Tipo</th>	<th>:</th>	<td><?php echo $nomtipoobjetivoAuxMod2;?></td></tr>
									</table>
									</br>
									<hr />
									<table>
										<tr>
											<td valign="top">
												<form action="modtipoobjetivo.php" method="get">
													<input type="submit" value="Modificar otro registro">
												</form>
											</td>
											<td>
												<form action="modtipoobjetivo.php" method="post">
													<input type="hidden" name="accion" value="mod">
													<input type="hidden" name="codtipoobjetivoAction" value="<?=$codtipoobjetivoAuxMod3?>">
													<input type="submit" value="Modificar datos">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
								else
								{
									despliegatipoobjetivo($codtipoobjetivoAuxMod3);
									?>
									</br>
									<?php
									mensaje("Datos modificados correctamente.");
									?>
									<table>
										<tr>
											<td valign="top" align='left' width='590'>
												<form action="modtipoobjetivo.php" method="get">
													<input type="submit" value="Consultar otro registro">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
							}
							else
							{
								mensaje("Debe llenar TODOS LOS CAMPOS!!!");
								$_REQUEST["accion"] = "mod";
								$_REQUEST["codtipoobjetivoAction"] = $_REQUEST["codtipoobjetivomod"];
							}
							$swconsulta = 1;
						}
						
						//FIN PROCESO MODIFICAR
						
						//PRESENTACION DE FORMULARIO PARA MODIFICAR
						
						if ((isset($_REQUEST["accion"])) and ($_REQUEST["accion"] == "mod"))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$codtipoobjetivoMod2 = $_REQUEST["codtipoobjetivoAction"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tablaMod = "tipoobjetivo";
							$campoMod = "codtipoobjetivo";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultaMod = consultatodo($tablaMod, $campoMod, $codtipoobjetivoMod2);
							
							// FORMATEO DE LOS RESULTADOS
							$regtipoobjetivoMod = mysqli_fetch_assoc($punteroconsultaMod);
							
							// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
							
							$codtipoobjetivoMod = $regtipoobjetivoMod["codtipoobjetivo"];
							$nomtipoobjetivoMod = $regtipoobjetivoMod["nomtipoobjetivo"];
						
							//PRESENTACION DEL FORMULARIO
							?>
							</br>
							<form action="modtipoobjetivo.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
								<table border = "0">
									<tr><th align="left">C&oacute;digo de Tipo</th>	<th>:</th>	<td><input type="text" name="codtipoobjetivomod" value="<?=$codtipoobjetivoMod?>" readonly></td></tr>
									<tr><th align="left">Nombre de Tipo</th>	<th>:</th>	<td><input type="text" name="nomtipoobjetivomod" value="<?=$nomtipoobjetivoMod?>"></td></tr>
								</table>
								</br>
								<table>
									<tr><td width='295' align='left'><input type="submit" value="Modificar"></td><td width='295' align='right'><button><a style='text-decoration:none; color:black'href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/tipoobjetivo/modtipoobjetivo.php'>Modificar otro registro</a></button></td></tr>
								</table>
							</form>
							<?php
							$swconsulta = 1;
						}
						
						//FIN FORMULARIO PARA MODIFICAR
						
						// RESULTADO CONSULTA
						
						if ((isset($_REQUEST["codtipoobjetivoconsulta"])) and ($_REQUEST["codtipoobjetivoconsulta"] != ""))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$codtipoobjetivoConsultaAux = $_REQUEST["codtipoobjetivoconsulta"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tabla = "tipoobjetivo";
							$campo = "codtipoobjetivo";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultacon = consultatodo($tabla, $campo, $codtipoobjetivoConsultaAux);
							
							// FORMATEO DE LOS RESULTADOS
							$regtipoobjetivocon = mysqli_fetch_assoc($punteroconsultacon);
								
							if ($regtipoobjetivocon["codtipoobjetivo"] == "")
							{
								mensaje("Tipo de objetivo inexistente");
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="modtipoobjetivo.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							else
							{
								$codtipoobjetivoConsultaAux2 = $regtipoobjetivocon["codtipoobjetivo"];
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								despliegatipoobjetivo($codtipoobjetivoConsultaAux2);
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="modtipoobjetivo.php" method="post">
												<input type="hidden" name="accion" value="mod">
												<input type="hidden" name="codtipoobjetivoAction" value="<?=$codtipoobjetivoConsultaAux2?>">
												<input type="submit" value="Modificar datos">
											</form>
										</td>
										<td>
											<form action="modtipoobjetivo.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							$swconsulta = 1;
						}
						
						// FIN RESULTADO CONSULTA

						// FORMULARIO BUSQUEDA REGISTRO
						
						if ($swconsulta == 0)
						{
							?>
							<form action="modtipoobjetivo.php" method="get">
								</br>
								<table border="0">
									<tr><th>Ingrese C&oacute;digo de Tipo Objetivo</th>	<th>:</th>	<td><input type="text" name="codtipoobjetivoconsulta"></td><td><input type="submit" value="Buscar"></td></tr>
								</table>
								</br>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Objetivo
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE OBJETIVOS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>