<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Eliminar Escala <!-- Cambiar -->
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" /> <!-- Cambiar -->
			</head>
			<body>
				<?php
					cabezal("ELIMINAR ESCALA");
						
						$webserver = nomserverweb();
						
						?>
						<font size="4"><b>Eliminar Escala</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
						
						$swconsulta = 0;
						
						if ((isset($_REQUEST["accion"])) and ($_REQUEST["accion"] == "elim"))
						{
							if ((isset($_REQUEST["codescalaevaluacionAction"])) and ($_REQUEST["codescalaevaluacionAction"] != ""))
							{
								// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
								$codescalaevaluacionActionAux = $_REQUEST["codescalaevaluacionAction"];
								eliminaregistro("escalaevaluacion", "codescalaevaluacion", $codescalaevaluacionActionAux);
							}
						}
						
						if ((isset($_REQUEST["codescalaevaluacionconsulta"])) and ($_REQUEST["codescalaevaluacionconsulta"] != ""))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$codescalaevaluacionConsultaAux = $_REQUEST["codescalaevaluacionconsulta"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tabla = "escalaevaluacion";
							$campo = "codescalaevaluacion";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultacon = consultatodo($tabla, $campo, $codescalaevaluacionConsultaAux);
							
							// FORMATEO DE LOS RESULTADOS
							$regperscon = mysqli_fetch_assoc($punteroconsultacon);
								
							if ($regperscon["codescalaevaluacion"] == "")
							{
								mensaje("Escala inexistente");
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="elimescalaevaluacion.php" method="get">
												<input type="submit" value="Eliminar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							else
							{
								$codescalaevaluacionConsultaAux2 = $regperscon["codescalaevaluacion"];
								despliegaescalaevaluacion($codescalaevaluacionConsultaAux2);
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="elimescalaevaluacion.php" method="post">
												<input type="hidden" name="accion" value="elim">
												<input type="hidden" name="codescalaevaluacionAction" value="<?=$codescalaevaluacionConsultaAux2?>">
												<input type="submit" value="Eliminar registro">
											</form>
										</td>
										<td>
											<form action="elimescalaevaluacion.php" method="get">
												<input type="submit" value="Eliminar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							$swconsulta = 1;
						}
						
						if ($swconsulta == 0)
						{
							?>
							<form action="elimescalaevaluacion.php" method="get">
								</br>
								<table border="0">
									<tr><th>Ingrese Codigo de Escala</th>	<th>:</th>	<td><input type="text" name="codescalaevaluacionconsulta"></td><td><input type="submit" value="Buscar"></td></tr>
								</table>
								</br>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Escala
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE ESCALA");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>