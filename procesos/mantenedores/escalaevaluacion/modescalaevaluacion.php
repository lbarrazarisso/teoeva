<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Escala de Evaluacion
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
					
				<?php
					cabezal("MODIFICAR ESCALA DE EVALUACION");
					
						$webserver = nomserverweb();
						
						?>
						<font size="4"><b>Modificar Escala de Evaluaci&oacute;n</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					
						$swconsulta = 0;
						
						//PROCESO MODIFICAR
					
						if ((isset($_REQUEST["codescalaevaluacionmod"])) and 
							(isset($_REQUEST["percentescalaevaluacionmod"])) and
							(isset($_REQUEST["nomescalaevaluacionmod"])))
						{
							
							$swmod = 0;
							if ($_REQUEST["codescalaevaluacionmod"] == "")
							{
								$swmod = 1;
							}
							
							if ($_REQUEST["percentescalaevaluacionmod"] == "")
							{
								$swmod = 2;
							}
							
							if ($_REQUEST["nomescalaevaluacionmod"] == "")
							{
								$swmod = 3;
							}
							
							if ($swmod == 0)
							{
								
								// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
								$codescalaevaluacionAuxMod2 = $_REQUEST["codescalaevaluacionmod"];
								
								// LLAMADO A FUNCION DE CONSULTA
								$resultqescalaevaluacionMod2 = consultatodo("escalaevaluacion", "codescalaevaluacion", $codescalaevaluacionAuxMod2);
								
								// FORMATEO DE LOS RESULTADOS
								$regescalaevaluacionMod2 = mysqli_fetch_assoc($resultqescalaevaluacionMod2);
								
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								$codescalaevaluacionAuxMod3 = $regescalaevaluacionMod2["codescalaevaluacion"];
								$percentescalaevaluacionAuxMod2 = $regescalaevaluacionMod2["percentescalaevaluacion"];
								$nomescalaevaluacionAuxMod2 = $regescalaevaluacionMod2["nomescalaevaluacion"];
								$codescalaevaluacionAuxMod2 = $regescalaevaluacionMod2["codescalaevaluacion"];
								
								$sw = "0";
								
								// PREGUNTA SI CAMPO NOMBRE HA SIDO MODIFICADO
								if ($_REQUEST["percentescalaevaluacionmod"] != $percentescalaevaluacionAuxMod2)
								{
									modificaregistro("escalaevaluacion", "percentescalaevaluacion", $_REQUEST["percentescalaevaluacionmod"], "codescalaevaluacion", $codescalaevaluacionAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI CAMPO APELLIDO PATERNO HA SIDO MODIFICADO
								if ($_REQUEST["nomescalaevaluacionmod"] != $nomescalaevaluacionAuxMod2)
								{
									modificaregistro("escalaevaluacion", "nomescalaevaluacion", $_REQUEST["nomescalaevaluacionmod"], "codescalaevaluacion", $codescalaevaluacionAuxMod3);
									$sw = "1";
								}

								// PREGUNTA SI CAMPO RUT HA SIDO MODIFICADO
								if ($_REQUEST["codescalaevaluacionmod"] != $codescalaevaluacionAuxMod2)
								{
									modificaregistro("escalaevaluacion", "codescalaevaluacion", $_REQUEST["codescalaevaluacionmod"], "codescalaevaluacion", $codescalaevaluacionAuxMod3);
									$sw = "1";
								}					
								
								// PREGUNTA SI HUBIERON MODIFICACIONES
								if ($sw == "0")
								{
									mensaje("No se hicieron modificaciones al registro.");
									despliegaescalaevaluacion($codescalaevaluacionAuxMod2);
									?>
									</br>
									<table>
										<tr>
											<td valign="top">
												<form action="modescalaevaluacion.php" method="get">
													<input type="submit" value="Modificar otro registro">
												</form>
											</td>
											<td>
												<form action="modescalaevaluacion.php" method="post">
													<input type="hidden" name="accion" value="mod">
													<input type="hidden" name="codescalaevaluacionAction" value="<?=$codescalaevaluacionAuxMod3?>">
													<input type="submit" value="Modificar datos">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
								else
								{
									despliegaescalaevaluacion($codescalaevaluacionAuxMod3);
									?>
									</br>
									<?php
									mensaje("Datos modificados correctamente.");
									?>
									<table>
										<tr>
											<td valign="top" align='left' width='590'>
												<form action="modescalaevaluacion.php" method="get">
													<input type="submit" value="Consultar otro registro">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
							}
							else
							{
								mensaje("Debe llenar TODOS LOS CAMPOS!!!");
								$_REQUEST["accion"] = "mod";
								$_REQUEST["codescalaevaluacionAction"] = $_REQUEST["codescalaevaluacionmod"];
							}
							$swconsulta = 1;
						}
						
						//FIN PROCESO MODIFICAR
						
						//PRESENTACION DE FORMULARIO PARA MODIFICAR
						
						if ((isset($_REQUEST["accion"])) and ($_REQUEST["accion"] == "mod"))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$codescalaevaluacionMod2 = $_REQUEST["codescalaevaluacionAction"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tablaMod = "escalaevaluacion";
							$campoMod = "codescalaevaluacion";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultaMod = consultatodo($tablaMod, $campoMod, $codescalaevaluacionMod2);
							
							// FORMATEO DE LOS RESULTADOS
							$regescalaevaluacionMod = mysqli_fetch_assoc($punteroconsultaMod);
							
							// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
							$codescalaevaluacionMod = $regescalaevaluacionMod["codescalaevaluacion"];
							$percentescalaevaluacionMod = $regescalaevaluacionMod["percentescalaevaluacion"];
							$nomescalaevaluacionMod = $regescalaevaluacionMod["nomescalaevaluacion"];
							
							//PRESENTACION DEL FORMULARIO
							?>
							</br>
							<form action="modescalaevaluacion.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
								<table border = "0">
									<tr><th align="left">C&oacute;digo</th>			<th>:</th>	<td><input type="text" name="codescalaevaluacionmod" value="<?=$codescalaevaluacionMod?>" readonly></td></tr>
									<tr><th align="left">Porcentaje</th>		<th>:</th>	<td><input type="text" name="percentescalaevaluacionmod" value="<?=$percentescalaevaluacionMod?>"></td></tr>
									<tr><th align="left">Niv. de Evaluaci&oacute;n</th><th>:</th>	<td><input type="text" name="nomescalaevaluacionmod" value="<?=$nomescalaevaluacionMod?>"></td></tr>
								</table>
								</br>
								<table>
									<tr><td width='295' align='left'><input type="submit" value="Modificar"></td><td width='295' align='right'><button><a style='text-decoration:none; color:black'href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/escalaevaluacion/modescalaevaluacion.php'>Modificar otro registro</a></button></td></tr>
								</table>
							</form>
							<?php
							$swconsulta = 1;
						}
						
						//FIN FORMULARIO PARA MODIFICAR
						
						// RESULTADO CONSULTA
						
						if ((isset($_REQUEST["codescalaevaluacionconsulta"])) and ($_REQUEST["codescalaevaluacionconsulta"] != ""))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$codescalaevaluacionConsultaAux = $_REQUEST["codescalaevaluacionconsulta"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tabla = "escalaevaluacion";
							$campo = "codescalaevaluacion";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultacon = consultatodo($tabla, $campo, $codescalaevaluacionConsultaAux);
							
							// FORMATEO DE LOS RESULTADOS
							$regescalaevaluacionconsulta = mysqli_fetch_assoc($punteroconsultacon);
								
							if ($regescalaevaluacionconsulta["codescalaevaluacion"] == "")
							{
								mensaje("Escala inexistente");
								?>
								<table>
									<tr>
										<td>
											<form action="modescalaevaluacion.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							else
							{
								$codescalaevaluacionConsultaAux2 = $regescalaevaluacionconsulta["codescalaevaluacion"];
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								despliegaescalaevaluacion($codescalaevaluacionConsultaAux2);
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="modescalaevaluacion.php" method="post">
												<input type="hidden" name="accion" value="mod">
												<input type="hidden" name="codescalaevaluacionAction" value="<?=$codescalaevaluacionConsultaAux2?>">
												<input type="submit" value="Modificar datos">
											</form>
										</td>
										<td>
											<form action="modescalaevaluacion.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							$swconsulta = 1;
						}
						
						// FIN RESULTADO CONSULTA

						// FORMULARIO BUSQUEDA REGISTRO
						
						if ($swconsulta == 0)
						{
							?>
							<form action="modescalaevaluacion.php" method="get">
								</br>
								<table border="0">
									<tr><th>Ingrese C&oacute;digo de Escala</th>	<th>:</th>	<td><input type="text" name="codescalaevaluacionconsulta"></td><td><input type="submit" value="Buscar"></br></td></tr>
								</table>
								</br>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Escala de Evaluacion
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE ESCALA DE EVALUACION");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>