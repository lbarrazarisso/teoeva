<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handVarEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Evaluadores
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
					
				<?php
					cabezal("MODIFICAR EVALUADORES");
					
						$webserver = nomserverweb();
						
						?>
						<font size="4"><b>Modificar Evaluador</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					
						$swconsulta = 0;
						
						//PROCESO MODIFICAR
					
						if ((isset($_REQUEST["rutevaluadormod"])) and 
							(isset($_REQUEST["nomevaluadormod"])) and
							(isset($_REQUEST["appaternomod"])) and 
							(isset($_REQUEST["apmaternomod"])))
						{
							
							$swmod = 0;
							if ($_REQUEST["rutevaluadormod"] == "")
							{
								$swmod = 1;
							}
							
							if ($_REQUEST["nomevaluadormod"] == "")
							{
								$swmod = 2;
							}
							
							if ($_REQUEST["appaternomod"] == "")
							{
								$swmod = 3;
							}
							
							if ($_REQUEST["apmaternomod"] == "")
							{
								$swmod = 4;
							}
							
							if (validarut($_REQUEST["rutevaluadormod"]) == false)
							{
								mensaje("RUT Ingresado no es válido");
								$swmod = 7;
							}
							
							if ($swmod == 0)
							{
								
								// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
								$rutevaluadorAuxMod2 = $_REQUEST["rutevaluadormod"];
								
								// LLAMADO A FUNCION DE CONSULTA
								$resultqevaluadorMod2 = consultatodo("evaluador", "rutevaluador", $rutevaluadorAuxMod2);
								
								// FORMATEO DE LOS RESULTADOS
								$regevaluadorMod2 = mysqli_fetch_assoc($resultqevaluadorMod2);
								
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								$rutevaluadorAuxMod3 = $regevaluadorMod2["rutevaluador"];
								$nomevaluadorAuxMod2 = $regevaluadorMod2["nomevaluador"];
								$appaternoAuxMod2 = $regevaluadorMod2["appaterno"];
								$apmaternoAuxMod2 = $regevaluadorMod2["apmaterno"];
								$rutevaluadorAuxMod2 = $regevaluadorMod2["rutevaluador"];
								
								$sw = "0";
								
								// PREGUNTA SI CAMPO NOMBRE HA SIDO MODIFICADO
								if ($_REQUEST["nomevaluadormod"] != $nomevaluadorAuxMod2)
								{
									modificaregistro("evaluador", "nomevaluador", $_REQUEST["nomevaluadormod"], "rutevaluador", $rutevaluadorAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI CAMPO APELLIDO PATERNO HA SIDO MODIFICADO
								if ($_REQUEST["appaternomod"] != $appaternoAuxMod2)
								{
									modificaregistro("evaluador", "appaterno", $_REQUEST["appaternomod"], "rutevaluador", $rutevaluadorAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI CAMPO APELLIDO MATERNO HA SIDO MODIFICADO
								if ($_REQUEST["apmaternomod"] != $apmaternoAuxMod2)
								{
									modificaregistro("evaluador", "apmaterno", $_REQUEST["apmaternomod"], "rutevaluador", $rutevaluadorAuxMod3);
									$sw = "1";
								}

								// PREGUNTA SI CAMPO RUT HA SIDO MODIFICADO
								if ($_REQUEST["rutevaluadormod"] != $rutevaluadorAuxMod2)
								{
									modificaregistro("evaluador", "rutevaluador", $_REQUEST["rutevaluadormod"], "rutevaluador", $rutevaluadorAuxMod3);
									$sw = "1";
								}					
								
								// PREGUNTA SI HUBIERON MODIFICACIONES
								if ($sw == "0")
								{
									mensaje("No se hicieron modificaciones al registro.");											
									?>
									<table border = "0">
										<tr><th align="left">RUT</th>	<th>:</th>	<td><?php echo $rutevaluadorAuxMod3;?></td></tr>
										<tr><th align="left">Nombres</th>			<th>:</th>	<td><?php echo $nomevaluadorAuxMod2;?></td></tr>
										<tr><th align="left">Ap. Paterno</th>		<th>:</th>	<td><?php echo $appaternoAuxMod2;?></td></tr>
										<tr><th align="left">Ap. Materno</th>		<th>:</th>	<td><?php echo $apmaternoAuxMod2;?></td></tr>
									</table>
									</br>
									<table>
										<tr>
											<td valign="top">
												<form action="modevaluador.php" method="get">
													<input type="submit" value="Modificar otro registro">
												</form>
											</td>
											<td>
												<form action="modevaluador.php" method="post">
													<input type="hidden" name="accion" value="mod">
													<input type="hidden" name="rutevaluadorAction" value="<?=$rutevaluadorAuxMod3?>">
													<input type="submit" value="Modificar datos">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
								else
								{
									despliegaevaluador($rutevaluadorAuxMod3);
									?>
									</br>
									<?php
									mensaje("Datos modificados correctamente.");
									?>
									<table>
										<tr>
											<td valign="top" align='left' width='590'>
												<form action="modevaluador.php" method="get">
													<input type="submit" value="Consultar otro registro">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
							}
							else
							{
								mensaje("Debe llenar TODOS LOS CAMPOS!!!");
								$_REQUEST["accion"] = "mod";
								$_REQUEST["rutevaluadorAction"] = $_REQUEST["rutevaluadormod"];
							}
							$swconsulta = 1;
						}
						
						//FIN PROCESO MODIFICAR
						
						//PRESENTACION DE FORMULARIO PARA MODIFICAR
						
						if ((isset($_REQUEST["accion"])) and ($_REQUEST["accion"] == "mod"))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$rutevaluadorMod2 = $_REQUEST["rutevaluadorAction"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tablaMod = "evaluador";
							$campoMod = "rutevaluador";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultaMod = consultatodo($tablaMod, $campoMod, $rutevaluadorMod2);
							
							// FORMATEO DE LOS RESULTADOS
							$regevaluadorMod = mysqli_fetch_assoc($punteroconsultaMod);
							
							// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
							$rutevaluadorMod = $regevaluadorMod["rutevaluador"];
							$nomevaluadorMod = $regevaluadorMod["nomevaluador"];
							$appaternoMod = $regevaluadorMod["appaterno"];
							$apmaternoMod = $regevaluadorMod["apmaterno"];
							
							//PRESENTACION DEL FORMULARIO
							?>
							</br>
							<form action="modevaluador.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
								<table border = "0">
									<tr><th align="left">RUT</th>	<th>:</th>	<td><input type="text" name="rutevaluadormod" value="<?=$rutevaluadorMod?>" readonly></td></tr>
									<tr><th align="left">Nombres</th>			<th>:</th>	<td><input type="text" name="nomevaluadormod" value="<?=$nomevaluadorMod?>"></td></tr>
									<tr><th align="left">Ap. Paterno</th>		<th>:</th>	<td><input type="text" name="appaternomod" value="<?=$appaternoMod?>"></td></tr>
									<tr><th align="left">Ap. Materno</th>		<th>:</th>	<td><input type="text" name="apmaternomod" value="<?=$apmaternoMod?>"></td></tr>
								</table>
								</br>
								<table>
									<tr><td width='295' align='left'><input type="submit" value="Modificar"></td><td width='295' align='right'><button><a style='text-decoration:none; color:black'href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/area/modarea.php'>Modificar otro registro</a></button></td></tr>
								</table>
							</form>
							<?php
							$swconsulta = 1;
						}
						
						//FIN FORMULARIO PARA MODIFICAR
						
						// RESULTADO CONSULTA
						
						if ((isset($_REQUEST["rutevaluadorconsulta"])) and ($_REQUEST["rutevaluadorconsulta"] != ""))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$rutevaluadorConsultaAux = $_REQUEST["rutevaluadorconsulta"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tabla = "evaluador";
							$campo = "rutevaluador";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultacon = consultatodo($tabla, $campo, $rutevaluadorConsultaAux);
							
							// FORMATEO DE LOS RESULTADOS
							$regevaluadorcon = mysqli_fetch_assoc($punteroconsultacon);
								
							if ($regevaluadorcon["rutevaluador"] == "")
							{
								mensaje("Evaluador inexistente");
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="modevaluador.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<hr />
								<?php
							}
							else
							{
								$rutevaluadorConsultaAux2 = $regevaluadorcon["rutevaluador"];
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								despliegaevaluador($rutevaluadorConsultaAux2);
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="modevaluador.php" method="post">
												<input type="hidden" name="accion" value="mod">
												<input type="hidden" name="rutevaluadorAction" value="<?=$rutevaluadorConsultaAux2?>">
												<input type="submit" value="Modificar datos">
											</form>
										</td>
										<td>
											<form action="modevaluador.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							$swconsulta = 1;
						}
						
						// FIN RESULTADO CONSULTA

						// FORMULARIO BUSQUEDA REGISTRO
						
						if ($swconsulta == 0)
						{
							?>
							<form action="modevaluador.php" method="get">
								</br>
								<table border="0">
									<tr><th>Ingrese Rut de Evaluador</th>	<th>:</th>	<td><input type="text" name="rutevaluadorconsulta"></td><td><input type="submit" value="Buscar"></td></tr>
								</table>
								</br>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Evaluadores
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE EVALUADORES");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>