<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handVarEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Evaluadores
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
				<?php
					cabezal("MANTENEDOR DE EVALUADORES");
						$webserver = nomserverweb();
						?>
						<font size="4"><b>Ingresar Evaluador</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php		
						// PROCEDIMIENTO PARA INSERTAR REGISTRO
						if ((isset($_REQUEST["nomevaluadoring"])) and
							(isset($_REQUEST["appaternoing"])) and 
							(isset($_REQUEST["apmaternoing"])) and  
							(isset($_REQUEST["rutevaluadoring"])))
						{
							$nomevaluadorAgain = $_REQUEST["nomevaluadoring"];
							$appaternoAgain = $_REQUEST["appaternoing"];
							$apmaternoAgain = $_REQUEST["apmaternoing"];
							$rutevaluadorAgain = $_REQUEST["rutevaluadoring"];
							
							$swing = 0;
							
							if ($_REQUEST["nomevaluadoring"] == "")
							{
								$swing = 1;
							}
							
							if ($_REQUEST["appaternoing"] == "")
							{
								$swing = 2;
							}
							
							if ($_REQUEST["apmaternoing"] == "")
							{
								$swing = 3;
							}
							
							if ($_REQUEST["rutevaluadoring"] == "")
							{
								$swing = 4;
							}
							
							if (validarut($_REQUEST["rutevaluadoring"]) == false)
							{
								mensaje("RUT Ingresado no es válido");
								$swing = 5;
							}
							
							if ($swing == 0)
							{
							
								// ASIGNACION DE DATOS INGRESADOS Y VALIDADOS A VARIABLES AUXILIARES
								$nomevaluadorAuxing = $_REQUEST["nomevaluadoring"];
								$appaternoAuxing = $_REQUEST["appaternoing"];
								$apmaternoAuxing = $_REQUEST["apmaternoing"];
								$rutevaluadorAuxing = $_REQUEST["rutevaluadoring"];
								
								//LLAMADA A PROCEDIMIENTO PARA INSERTAR REGISTRO
								$tablaing = "evaluador";
								$valoresing = "'".$rutevaluadorAuxing."', '".$nomevaluadorAuxing."', '".$appaternoAuxing."', '".$apmaternoAuxing."'";
								insertaregistro($tablaing, $valoresing);
								
								//CONSULTA A BASE PARA OBTENER CODIGO DE evaluador
								$resultqdespliegaevaluador = consultatodo($tablaing, "rutevaluador", $rutevaluadorAuxing);
								$regdespliegaevaluador = mysqli_fetch_assoc($resultqdespliegaevaluador);
								$rutevaluadorAuxing = $regdespliegaevaluador["rutevaluador"];						
								
								//VISUALIZA REGISTRO INSERTADO
								despliegaevaluador($rutevaluadorAuxing);
								?>
								<!-- FIN VISUALIZA REGISTRO -->
								
								</br>
								<table>
									<tr>
										<td valign="top" align='left' width='590'>
											<form action="ingevaluador.php" method="get">
												<input type="submit" value="Ingresar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							else
							{
								mensaje("Debe llenar TODOS LOS CAMPOS!!!");
								
								//PRESENTACION DE FORMULARIO PARA INGRESAR CON DATOS PREEXISTENTES
					
								?>
								<form action="ingevaluador.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
									</br>
									<table border="0">
										<tr><th align="left">Nombres</th>			<th>:</th>	<td><input type="text" name="nomevaluadoring" value="<?=$nomevaluadorAgain?>" ></td></tr>
										<tr><th align="left">Ap. Paterno</th>		<th>:</th>	<td><input type="text" name="appaternoing" value="<?=$appaternoAgain?>" ></td></tr>
										<tr><th align="left">Ap. Materno</th>		<th>:</th>	<td><input type="text" name="apmaternoing" value="<?=$apmaternoAgain?>" ></td></tr>
										<tr><th align="left">RUT</th>				<th>:</th>	<td><input type="text" name="rutevaluadoring" value="<?=$rutevaluadorAgain?>"></td></tr>
									</table>
									</br>
									<table>
										<tr>
											<td valign="top">
												<input type="submit" value="Ingresar datos">
											</td>
										</tr>
									</table>
								</form>
								<?php
							}
						}
						else
						{
						
							//PRESENTACION DE FORMULARIO PARA INGRESAR

							$_REQUEST["nomevaluadoring"] = "";
							$_REQUEST["appaternoing"] = "";
							$_REQUEST["apmaternoing"] = "";
							$_REQUEST["rutevaluadoring"] = "";
							?>
							<form action="ingevaluador.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
								</br>
								<table border="0">
									<tr><th align="left">Nombres</th>			<th>:</th>	<td><input type="text" name="nomevaluadoring"></td></tr>
									<tr><th align="left">Ap. Paterno</th>		<th>:</th>	<td><input type="text" name="appaternoing"></td></tr>
									<tr><th align="left">Ap. Materno</th>		<th>:</th>	<td><input type="text" name="apmaternoing"></td></tr>
									<tr><th align="left">RUT</th>				<th>:</th>	<td><input type="text" name="rutevaluadoring"></td></tr>
								</table>
								</br>
								<table>
									<tr>
										<td valign="top">
											<input type="submit" value="Ingresar datos">
										</td>
									</tr>
								</table>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Evaluador
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE EVALUADOR");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCION</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Menu Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>