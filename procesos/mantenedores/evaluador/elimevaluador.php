<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Eliminar Evaluador <!-- Cambiar -->
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" /> <!-- Cambiar -->
			</head>
			<body>
				<?php
					cabezal("ELIMINAR EVALUADORES");
						
						$webserver = nomserverweb();
						
						?>
						<font size="4"><b>Eliminar Evaluador</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
						
						$swconsulta = 0;
						
						if ((isset($_REQUEST["accion"])) and ($_REQUEST["accion"] == "elim"))
						{
							if ((isset($_REQUEST["rutevaluadorAction"])) and ($_REQUEST["rutevaluadorAction"] != ""))
							{
								// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
								$rutevaluadorActionAux = $_REQUEST["rutevaluadorAction"];
								eliminaregistro("evaluador", "rutevaluador", $rutevaluadorActionAux);
							}
						}
						
						if ((isset($_REQUEST["rutevaluadorconsulta"])) and ($_REQUEST["rutevaluadorconsulta"] != ""))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$rutevaluadorConsultaAux = $_REQUEST["rutevaluadorconsulta"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tabla = "evaluador";
							$campo = "rutevaluador";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultacon = consultatodo($tabla, $campo, $rutevaluadorConsultaAux);
							
							// FORMATEO DE LOS RESULTADOS
							$regperscon = mysqli_fetch_assoc($punteroconsultacon);
								
							if ($regperscon["rutevaluador"] == "")
							{
								mensaje("evaluador inexistente");
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="elimevaluador.php" method="get">
												<input type="submit" value="Eliminar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							else
							{
								$rutevaluadorConsultaAux2 = $regperscon["rutevaluador"];
								despliegaevaluador($rutevaluadorConsultaAux2);
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="elimevaluador.php" method="post">
												<input type="hidden" name="accion" value="elim">
												<input type="hidden" name="rutevaluadorAction" value="<?=$rutevaluadorConsultaAux2?>">
												<input type="submit" value="Eliminar registro">
											</form>
										</td>
										<td>
											<form action="elimevaluador.php" method="get">
												<input type="submit" value="Eliminar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							$swconsulta = 1;
						}
						
						if ($swconsulta == 0)
						{
							?>
							<form action="elimevaluador.php" method="get">
								</br>
								<table border="0">
									<tr><th>Ingrese RUT de Evaluador</th>	<th>:</th>	<td><input type="text" name="rutevaluadorconsulta"></td><td><input type="submit" value="Buscar"></td></tr>
								</table>
								</br>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Evaluadores
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE EVALUADORES");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCION</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Menu Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>