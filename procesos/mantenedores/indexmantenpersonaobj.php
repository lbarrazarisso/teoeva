<?php
	include "../../lib/handWebEva.php";
	//include "../../lib/handDisplayEva.php";
	include "../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Personas
				</title>
				<link href="../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
				<?php
					cabezal("MANTENEDOR DE PERSONAS");
						$webserver = nomserverweb();
						?>
							<font size="4"><b>Personas</b></font>
							<hr />
							<div id="botonup">
								<table>
									<tr>
										<td width='600' align='left' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
										</td>
									</tr>
								</table>
							</div>
							<hr />
							<font size='3'><p><b>Administrar personas</b></p></font>
							<hr />
							</br>
							<table>
								<tr>
									<td width = '200'><button style = 'width:165; height:70'><a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/persona/ingpersona.php'>Ingresar Personas</a></button></td>
									<td width = '200'><button style = 'width:165; height:70'><a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/persona/modpersona.php'>Modificar Personas</a></button></td>
									<td width = '200'><button style = 'width:165; height:70'><a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/persona/elimpersona.php'>Eliminar Personas</a></button></td>
								</tr>
							</table>
							</br>
							<hr />
							<font size='3'><p><b>Asignar objetivos generales a una persona</b></p></font>
							<hr />
							</br>
							<table>
								<tr>
									<td width = '300' align = 'left'><button style = 'width:250; height:70'><a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/procesos/asignadores/objetivoxpersona/asignaobjetivoxpersona.php'>Asignar Objetivos Generales</a></button></td>
									<td width = '300' align = 'right'><button style = 'width:250; height:70'><a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/procesos/asignadores/objetivoxpersona/desasignaobjetivoxpersona.php'>Desasignar Objetivos</a></button></td>
								</tr>
							</table>
							</br>
							<hr />
							<font size='3'><p><b>Asignar objetivos espec&iacute;ficos a una persona</b></p></font>
							<hr />
							</br>
							<table>
								<tr>
									<td width = '300' align = 'left'><button style = 'width:250; height:70'><a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/procesos/asignadores/objetivoespecificoxpersona/asignaobjetivoespecificoxpersona.php'>Asignar Objetivos Espec&iacute;ficos</a></button></td>
									<td width = '300' align = 'right'><button style = 'width:250; height:70'><a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/procesos/asignadores/objetivoxpersona/desasignaobjetivoxpersona.php'>Desasignar Objetivos</a></button></td>
								</tr>
							</table>
							</br>
							<hr />
							<div id="botonbottom">
								<table>
									<tr>
										<td width='600' align='right' valign='top'>
											<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../images/logout.jpg' width='30' height='30' title='Salir'></a>
										</td>
									</tr>
								</table>
							</div>
							<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Personas
					</title>
					<link href="../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE PERSONAS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCION</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Menu Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>