<?php 
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Tipos de Competencias
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
					
				<?php
					cabezal("MODIFICAR TIPOS DE COMPETENCIAS");
					
						$webserver = nomserverweb();
						
						?>
						<font size="4"><b>Modificar Tipo de Competencia</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					
						$swconsulta = 0;
						
						//PROCESO MODIFICAR
					
						if ((isset($_REQUEST["codtipocompetenciamod"])) and (isset($_REQUEST["nomtipocompetenciamod"])))
						{
							
							$swmod = 0;
							
							if ($_REQUEST["codtipocompetenciamod"] == "")
							{
								$swmod = 1;
							}
							
							if ($_REQUEST["nomtipocompetenciamod"] == "")
							{
								$swmod = 2;
							}
							
							if ($swmod == 0)
							{
								
								// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
								$codtipocompetenciaAuxMod2 = $_REQUEST["codtipocompetenciamod"];
								
								// LLAMADO A FUNCION DE CONSULTA
								$resultqMod2 = consultatodo("tipocompetencia", "codtipocompetencia", $codtipocompetenciaAuxMod2);
								
								// FORMATEO DE LOS RESULTADOS
								$regtipocompetenciaMod2 = mysqli_fetch_assoc($resultqMod2);
								
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								
								$codtipocompetenciaAuxMod3 = $regtipocompetenciaMod2["codtipocompetencia"];
								$nomtipocompetenciaAuxMod2 = $regtipocompetenciaMod2["nomtipocompetencia"];
								
								$sw = "0";
								
								// PREGUNTA SI CAMPO NOMBRE DE TIPO REQUISITO HA SIDO MODIFICADO
								if ($_REQUEST["nomtipocompetenciamod"] != $nomtipocompetenciaAuxMod2)
								{
									modificaregistro("tipocompetencia", "nomtipocompetencia", $_REQUEST["nomtipocompetenciamod"], "codtipocompetencia", $codtipocompetenciaAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI HUBIERON MODIFICACIONES
								if ($sw == "0")
								{
									mensaje("No se hicieron modificaciones al registro.");											
									?>
									<table border = "0">
										<tr><th align="left">C&oacute;digo de Tipo</th>	<th>:</th>	<td><?php echo $codtipocompetenciaAuxMod3;?></td></tr>
										<tr><th align="left">Nombre de Tipo</th>	<th>:</th>	<td><?php echo $nomtipocompetenciaAuxMod2;?></td></tr>
									</table>
									</br>
									<hr />
									<table>
										<tr>
											<td valign="top">
												<form action="modtipocompetencia.php" method="get">
													<input type="submit" value="Modificar otro registro">
												</form>
											</td>
											<td>
												<form action="modtipocompetencia.php" method="post">
													<input type="hidden" name="accion" value="mod">
													<input type="hidden" name="codtipocompetenciaAction" value="<?=$codtipocompetenciaAuxMod3?>">
													<input type="submit" value="Modificar datos">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
								else
								{
									despliegatipocompetencia($codtipocompetenciaAuxMod3);
									?>
									</br>
									<?php
									mensaje("Datos modificados correctamente.");
									?>
									<table>
										<tr>
											<td valign="top" align='left' width='590'>
												<form action="modtipocompetencia.php" method="get">
													<input type="submit" value="Consultar otro registro">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
							}
							else
							{
								mensaje("Debe llenar TODOS LOS CAMPOS!!!");
								$_REQUEST["accion"] = "mod";
								$_REQUEST["codtipocompetenciaAction"] = $_REQUEST["codtipocompetenciamod"];
							}
							$swconsulta = 1;
						}
						
						//FIN PROCESO MODIFICAR
						
						//PRESENTACION DE FORMULARIO PARA MODIFICAR
						
						if ((isset($_REQUEST["accion"])) and ($_REQUEST["accion"] == "mod"))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$codtipocompetenciaMod2 = $_REQUEST["codtipocompetenciaAction"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tablaMod = "tipocompetencia";
							$campoMod = "codtipocompetencia";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultaMod = consultatodo($tablaMod, $campoMod, $codtipocompetenciaMod2);
							
							// FORMATEO DE LOS RESULTADOS
							$regtipocompetenciaMod = mysqli_fetch_assoc($punteroconsultaMod);
							
							// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
							
							$codtipocompetenciaMod = $regtipocompetenciaMod["codtipocompetencia"];
							$nomtipocompetenciaMod = $regtipocompetenciaMod["nomtipocompetencia"];
						
							//PRESENTACION DEL FORMULARIO
							?>
							</br>
							<form action="modtipocompetencia.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
								<table border = "0">
									<tr><th align="left">C&oacute;digo de Tipo</th>	<th>:</th>	<td><input type="text" name="codtipocompetenciamod" value="<?=$codtipocompetenciaMod?>" readonly></td></tr>
									<tr><th align="left">Nombre de Tipo</th>	<th>:</th>	<td><input type="text" name="nomtipocompetenciamod" value="<?=$nomtipocompetenciaMod?>"></td></tr>
								</table>
								</br>
								<table>
									<tr><td width='295' align='left'><input type="submit" value="Modificar"></td><td width='295' align='right'><button><a style='text-decoration:none; color:black'href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/tipocompetencia/modtipocompetencia.php'>Modificar otro registro</a></button></td></tr>
								</table>
							</form>
							<?php
							$swconsulta = 1;
						}
						
						//FIN FORMULARIO PARA MODIFICAR
						
						// RESULTADO CONSULTA
						
						if ((isset($_REQUEST["codtipocompetenciaconsulta"])) and ($_REQUEST["codtipocompetenciaconsulta"] != ""))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$codtipocompetenciaConsultaAux = $_REQUEST["codtipocompetenciaconsulta"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tabla = "tipocompetencia";
							$campo = "codtipocompetencia";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultacon = consultatodo($tabla, $campo, $codtipocompetenciaConsultaAux);
							
							// FORMATEO DE LOS RESULTADOS
							$regtipocompetenciacon = mysqli_fetch_assoc($punteroconsultacon);
								
							if ($regtipocompetenciacon["codtipocompetencia"] == "")
							{
								mensaje("Tipo de Competencia inexistente");
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="modtipocompetencia.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							else
							{
								$codtipocompetenciaConsultaAux2 = $regtipocompetenciacon["codtipocompetencia"];
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								despliegatipocompetencia($codtipocompetenciaConsultaAux2);
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="modtipocompetencia.php" method="post">
												<input type="hidden" name="accion" value="mod">
												<input type="hidden" name="codtipocompetenciaAction" value="<?=$codtipocompetenciaConsultaAux2?>">
												<input type="submit" value="Modificar datos">
											</form>
										</td>
										<td>
											<form action="modtipocompetencia.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							$swconsulta = 1;
						}
						
						// FIN RESULTADO CONSULTA

						// FORMULARIO BUSQUEDA REGISTRO
						
						if ($swconsulta == 0)
						{
							?>
							<form action="modtipocompetencia.php" method="get">
								</br>
								<table border="0">
									<tr><th>Ingrese C&oacute;digo de Tipo Competencia</th>	<th>:</th>	<td><input type="text" name="codtipocompetenciaconsulta"></td><td><input type="submit" value="Buscar"></td></tr>
								</table>
								</br>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Competencia
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE COMPETENCIAS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>