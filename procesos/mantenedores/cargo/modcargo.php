<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Cargos
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
					
				<?php
					cabezal("MODIFICAR CARGOS");
					
						$webserver = nomserverweb();
						
						?>
						<font size="4"><b>Modificar Cargo</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					
						$swconsulta = 0;
						
						//PROCESO MODIFICAR
					
						if ((isset($_REQUEST["codcargomod"])) and (isset($_REQUEST["nomcargomod"])) and	(isset($_REQUEST["desccargomod"])))
						{
							
							$swmod = 0;
							
							if ($_REQUEST["codcargomod"] == "")
							{
								$swmod = 1;
							}
							
							if ($_REQUEST["nomcargomod"] == "")
							{
								$swmod = 2;
							}
							
							if ($_REQUEST["desccargomod"] == "")
							{
								$swmod = 3;
							}
							
							if ($swmod == 0)
							{
								
								// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
								$codcargoAuxMod2 = $_REQUEST["codcargomod"];
								
								// LLAMADO A FUNCION DE CONSULTA
								$resultqMod2 = consultatodo("cargo", "codcargo", $codcargoAuxMod2);
								
								// FORMATEO DE LOS RESULTADOS
								$regcargoMod2 = mysqli_fetch_assoc($resultqMod2);
								
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								
								$codcargoAuxMod3 = $regcargoMod2["codcargo"];
								$nomcargoAuxMod2 = $regcargoMod2["nomcargo"];
								$desccargoAuxMod2 = $regcargoMod2["desccargo"];
								
								$sw = "0";
								
								// PREGUNTA SI CAMPO NOMBRE DE CARGO HA SIDO MODIFICADO
								if ($_REQUEST["nomcargomod"] != $nomcargoAuxMod2)
								{
									modificaregistro("cargo", "nomcargo", $_REQUEST["nomcargomod"], "codcargo", $codcargoAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI CAMPO descETIVO HA SIDO MODIFICADO
								if ($_REQUEST["desccargomod"] != $desccargoAuxMod2)
								{
									modificaregistro("cargo", "desccargo", $_REQUEST["desccargomod"], "codcargo", $codcargoAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI HUBIERON MODIFICACIONES
								if ($sw == "0")
								{
									mensaje("No se hicieron modificaciones al registro.");											
									?>
									<table border = "0">
										<tr><th align="left">Codigo de Cargo</th>	<th>:</th>	<td><?php echo $codcargoAuxMod3;?></td></tr>
										<tr><th align="left">Nombre de Cargo</th>	<th>:</th>	<td><?php echo $nomcargoAuxMod2;?></td></tr>
										<tr><th align="left">Desc. del Cargo</th>	<th>:</th>	<td><?php echo $desccargoAuxMod2;?></td></tr>
									</table>
									</br>
									<table>
										<tr>
											<td valign="top">
												<form action="modcargo.php" method="get">
													<input type="submit" value="Modificar otro registro">
												</form>
											</td>
											<td>
												<form action="modcargo.php" method="post">
													<input type="hidden" name="accion" value="mod">
													<input type="hidden" name="codcargoAction" value="<?=$codcargoAuxMod3?>">
													<input type="submit" value="Modificar datos">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
								else
								{
									despliegacargo($codcargoAuxMod3);
									?>
									</br>
									<?php
									mensaje("Datos modificados correctamente.");
									?>
									<table>
										<tr>
											<td valign="top" align='left' width='590'>
												<form action="modcargo.php" method="get">
													<input type="submit" value="Consultar otro registro">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
							}
							else
							{
								mensaje("Debe llenar TODOS LOS CAMPOS!!!");
								$_REQUEST["accion"] = "mod";
								$_REQUEST["codcargoAction"] = $_REQUEST["codcargomod"];
							}
							$swconsulta = 1;
						}
						
						//FIN PROCESO MODIFICAR
						
						//PRESENTACION DE FORMULARIO PARA MODIFICAR
						
						if ((isset($_REQUEST["accion"])) and ($_REQUEST["accion"] == "mod"))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$codcargoMod2 = $_REQUEST["codcargoAction"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tablaMod = "cargo";
							$campoMod = "codcargo";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultaMod = consultatodo($tablaMod, $campoMod, $codcargoMod2);
							
							// FORMATEO DE LOS RESULTADOS
							$regcargoMod = mysqli_fetch_assoc($punteroconsultaMod);
							
							// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
							
							$codcargoMod = $regcargoMod["codcargo"];
							$nomcargoMod = $regcargoMod["nomcargo"];
							$desccargoMod = $regcargoMod["desccargo"];
						
							//PRESENTACION DEL FORMULARIO
							?>
							</br>
							<form action="modcargo.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
								<table border = "0">
									<tr><th align="left">Codigo de Cargo</th>	<th>:</th>	<td><input type="text" name="codcargomod" value="<?=$codcargoMod?>" readonly></td></tr>
									<tr><th align="left">Nombre de Cargo</th>	<th>:</th>	<td><input type="text" name="nomcargomod" value="<?=$nomcargoMod?>"></td></tr>
									<tr><th align="left">Desc. del Cargo</th>	<th>:</th>	<td><textarea type="text" name="desccargomod" cols="30" rows="10"><?=$desccargoMod?></textarea></td></tr>
				
								</table>
								</br>
								<table>
									<tr><td width='295' align='left'><input type="submit" value="Modificar"></td><td width='295' align='right'><button><a style='text-decoration:none; color:black'href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/cargo/modcargo.php'>Modificar otro registro</a></button></td></tr>
								</table>
							</form>
							<?php
							$swconsulta = 1;
						}
						
						//FIN FORMULARIO PARA MODIFICAR
						
						// RESULTADO CONSULTA
						
						if ((isset($_REQUEST["codcargoconsulta"])) and ($_REQUEST["codcargoconsulta"] != ""))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$codcargoConsultaAux = $_REQUEST["codcargoconsulta"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tabla = "cargo";
							$campo = "codcargo";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultacon = consultatodo($tabla, $campo, $codcargoConsultaAux);
							
							// FORMATEO DE LOS RESULTADOS
							$regcargocon = mysqli_fetch_assoc($punteroconsultacon);
								
							if ($regcargocon["codcargo"] == "")
							{
								mensaje("Cargo inexistente");
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="modcargo.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<hr />
								<?php
							}
							else
							{
								$codcargoConsultaAux2 = $regcargocon["codcargo"];
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								despliegacargo($codcargoConsultaAux2);
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="modcargo.php" method="post">
												<input type="hidden" name="accion" value="mod">
												<input type="hidden" name="codcargoAction" value="<?=$codcargoConsultaAux2?>">
												<input type="submit" value="Modificar datos">
											</form>
										</td>
										<td>
											<form action="modcargo.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							$swconsulta = 1;
						}
						
						// FIN RESULTADO CONSULTA

						// FORMULARIO BUSQUEDA REGISTRO
						
						if ($swconsulta == 0)
						{
							?>
							<form action="modcargo.php" method="get">
								</br>
								<table border="0">
									<tr><th>Ingrese C&oacute;digo de Cargo</th>	<th>:</th>	<td><input type="text" name="codcargoconsulta"></td><td><input type="submit" value="Buscar"></td></tr>
								</table>
								</br>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Cargos
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE CARGOS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>