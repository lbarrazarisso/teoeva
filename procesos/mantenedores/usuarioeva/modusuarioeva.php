<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handVarEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Usuarios
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
					
				<?php
					cabezal("MODIFICAR UUARIOS");
					
						$webserver = nomserverweb();
						
						?>
						<font size="4"><b>Modificar Usuario</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					
						$swconsulta = 0;
						
						//PROCESO MODIFICAR
					
						if ((isset($_REQUEST["rutusuarioevamod"])) and 
							(isset($_REQUEST["nomusuarioevamod"])) and
							(isset($_REQUEST["codtipousuarioevamod"])))
						{
							
							$swmod = 0;
							if ($_REQUEST["rutusuarioevamod"] == "")
							{
								$swmod = 1;
							}
							
							if ($_REQUEST["nomusuarioevamod"] == "")
							{
								$swmod = 2;
							}
							
							if ($_REQUEST["codtipousuarioevamod"] == "")
							{
								$swmod = 3;
							}
							
							if (validarut($_REQUEST["rutusuarioevamod"]) == false)
							{
								mensaje("RUT Ingresado no es válido");
								$swmod = 7;
							}
							
							if ($swmod == 0)
							{
								
								// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
								$rutusuarioevaAuxMod2 = $_REQUEST["rutusuarioevamod"];
								
								// LLAMADO A FUNCION DE CONSULTA
								$resultqusuarioevaMod2 = consultatodo("usuarioeva", "rutusuarioeva", $rutusuarioevaAuxMod2);
								
								// FORMATEO DE LOS RESULTADOS
								$regusuarioevaMod2 = mysqli_fetch_assoc($resultqusuarioevaMod2);
								
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								$rutusuarioevaAuxMod3 = $regusuarioevaMod2["rutusuarioeva"];
								$nomusuarioevaAuxMod2 = $regusuarioevaMod2["nomusuarioeva"];
								$codtipousuarioevaAuxMod2 = $regusuarioevaMod2["codtipousuarioeva"];
								$rutusuarioevaAuxMod2 = $regusuarioevaMod2["rutusuarioeva"];
								
								$sw = "0";
								
								// PREGUNTA SI CAMPO NOMBRE HA SIDO MODIFICADO
								if ($_REQUEST["nomusuarioevamod"] != $nomusuarioevaAuxMod2)
								{
									modificaregistro("usuarioeva", "nomusuarioeva", $_REQUEST["nomusuarioevamod"], "rutusuarioeva", $rutusuarioevaAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI CAMPO APELLIDO PATERNO HA SIDO MODIFICADO
								if ($_REQUEST["codtipousuarioevamod"] != $codtipousuarioevaAuxMod2)
								{
									modificaregistro("usuarioeva", "codtipousuarioeva", $_REQUEST["codtipousuarioevamod"], "rutusuarioeva", $rutusuarioevaAuxMod3);
									$sw = "1";
								}

								// PREGUNTA SI CAMPO RUT HA SIDO MODIFICADO
								if ($_REQUEST["rutusuarioevamod"] != $rutusuarioevaAuxMod2)
								{
									modificaregistro("usuarioeva", "rutusuarioeva", $_REQUEST["rutusuarioevamod"], "rutusuarioeva", $rutusuarioevaAuxMod3);
									$sw = "1";
								}					
								
								// PREGUNTA SI HUBIERON MODIFICACIONES
								if ($sw == "0")
								{
									mensaje("No se hicieron modificaciones al registro.");
									despliegausuarioeva($rutusuarioevaAuxMod2);
									?>
									</br>
									<table>
										<tr>
											<td valign="top">
												<form action="modusuarioeva.php" method="get">
													<input type="submit" value="Modificar otro registro">
												</form>
											</td>
											<td>
												<form action="modusuarioeva.php" method="post">
													<input type="hidden" name="accion" value="mod">
													<input type="hidden" name="rutusuarioevaAction" value="<?=$rutusuarioevaAuxMod3?>">
													<input type="submit" value="Modificar datos">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
								else
								{
									despliegausuarioeva($rutusuarioevaAuxMod3);
									?>
									</br>
									<?php
									mensaje("Datos modificados correctamente.");
									?>
									<table>
										<tr>
											<td valign="top">
												<form action="modusuarioeva.php" method="get">
													<input type="submit" value="Consultar otro registro">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
							}
							else
							{
								mensaje("Debe llenar TODOS LOS CAMPOS!!!");
								$_REQUEST["accion"] = "mod";
								$_REQUEST["rutusuarioevaAction"] = $_REQUEST["rutusuarioevamod"];
							}
							$swconsulta = 1;
						}
						
						//FIN PROCESO MODIFICAR
						
						//PRESENTACION DE FORMULARIO PARA MODIFICAR
						
						if ((isset($_REQUEST["accion"])) and ($_REQUEST["accion"] == "mod"))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$rutusuarioevaMod2 = $_REQUEST["rutusuarioevaAction"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tablaMod = "usuarioeva";
							$campoMod = "rutusuarioeva";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultaMod = consultatodo($tablaMod, $campoMod, $rutusuarioevaMod2);
							
							// FORMATEO DE LOS RESULTADOS
							$regusuarioevaMod = mysqli_fetch_assoc($punteroconsultaMod);
							
							// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
							$rutusuarioevaMod = $regusuarioevaMod["rutusuarioeva"];
							$nomusuarioevaMod = $regusuarioevaMod["nomusuarioeva"];
							$codtipousuarioevaMod = $regusuarioevaMod["codtipousuarioeva"];
							
							//PRESENTACION DEL FORMULARIO
							?>
							</br>
							<form action="modusuarioeva.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
								<table border = "0">
									<tr><th align="left">Nombre de usuario</th>	<th>:</th>	<td><input type="text" name="nomusuarioevamod" value="<?=$nomusuarioevaMod?>"></td></tr>
									<tr><th align="left">RUT</th>				<th>:</th>	<td><input type="text" name="rutusuarioevamod" value="<?=$rutusuarioevaMod?>" readonly></td></tr>
									<tr>
										<th align="left">
											Tipo de Usuario
										</th>
										<th>
											:
										</th>
										<td>
											<select name="codtipousuarioevamod">
												<?php
													$resultqtipousuarioeva = llenacombo("tipousuarioeva");
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													while ($regtipousuarioeva = mysqli_fetch_assoc($resultqtipousuarioeva))
													{
														if ($regtipousuarioeva["codtipousuarioeva"] == $codtipousuarioevaAgain)
														{
															echo "<option value='".$regtipousuarioeva["codtipousuarioeva"]."' selected>";
															echo $regtipousuarioeva["nomtipousuarioeva"];
															echo "</option>";																	
														}
														else
														{
															echo "<option value='".$regtipousuarioeva["codtipousuarioeva"]."'>";
															echo $regtipousuarioeva["nomtipousuarioeva"];
															echo "</option>";
														}
													}
												?>
											</select>
										</td>
									</tr>
								</table>
								</br>
								<table>
									<tr><td width='295' align='left'><input type="submit" value="Modificar"></td><td width='295' align='right'><button><a style='text-decoration:none; color:black'href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/usuarioeva/modusuarioeva.php'>Modificar otro registro</a></button></td></tr>
								</table>
							</form>
							<?php
							$swconsulta = 1;
						}
						
						//FIN FORMULARIO PARA MODIFICAR
						
						// RESULTADO CONSULTA
						
						if ((isset($_REQUEST["rutusuarioevaconsulta"])) and ($_REQUEST["rutusuarioevaconsulta"] != ""))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$rutusuarioevaConsultaAux = $_REQUEST["rutusuarioevaconsulta"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tabla = "usuarioeva";
							$campo = "rutusuarioeva";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultacon = consultatodo($tabla, $campo, $rutusuarioevaConsultaAux);
							
							// FORMATEO DE LOS RESULTADOS
							$regusuarioevaconsulta = mysqli_fetch_assoc($punteroconsultacon);
								
							if ($regusuarioevaconsulta["rutusuarioeva"] == "")
							{
								mensaje("Usuario inexistente");
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="modusuarioeva.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							else
							{
								$rutusuarioevaConsultaAux2 = $regusuarioevaconsulta["rutusuarioeva"];
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								despliegausuarioeva($rutusuarioevaConsultaAux2);
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="modusuarioeva.php" method="post">
												<input type="hidden" name="accion" value="mod">
												<input type="hidden" name="rutusuarioevaAction" value="<?=$rutusuarioevaConsultaAux2?>">
												<input type="submit" value="Modificar datos">
											</form>
										</td>
										<td>
											<form action="modusuarioeva.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							$swconsulta = 1;
						}
						
						// FIN RESULTADO CONSULTA

						// FORMULARIO BUSQUEDA REGISTRO
						
						if ($swconsulta == 0)
						{
							?>
							<form action="modusuarioeva.php" method="get">
								</br>
								<table border="0">
									<tr><th>Ingrese Rut de Usuario</th>	<th>:</th>	<td><input type="text" name="rutusuarioevaconsulta"></td><td><input type="submit" value="Buscar"></td></tr>
								</table>
								</br>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Usuarios
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE USUARIOS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCION</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Menu Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>