<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Tipos de Usuario
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
				<?php
					cabezal("MANTENEDOR DE TIPOS DE USUARIO");
					
						$webserver = nomserverweb();
						?>
						<font size="4"><b>Ingresar Tipo de Usuario</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					
						// PROCEDIMIENTO PARA INSERTAR REGISTRO
						if ((isset($_REQUEST["codtipousuarioevaing"])) and (isset($_REQUEST["nomtipousuarioevaing"])))
						{
							// ASIGNACION DE DATOS INGRESADOS Y VALIDADOS A VARIABLES AUXILIARES
							$codtipousuarioevaAgain = $_REQUEST["codtipousuarioevaing"];
							$nomtipousuarioevaAgain = $_REQUEST["nomtipousuarioevaing"];
							
							$swing = 0;
							
							if ($_REQUEST["codtipousuarioevaing"] == "")
							{
								$swing = 1;
							}
							
							if ($_REQUEST["nomtipousuarioevaing"] == "")
							{
								$swing = 2;
							}
							
							if ($swing == 0)
							{
							
								// ASIGNACION DE DATOS INGRESADOS Y VALIDADOS A VARIABLES AUXILIARES
								
								$codtipousuarioevaAuxing = $_REQUEST["codtipousuarioevaing"];
								$nomtipousuarioevaAuxing = $_REQUEST["nomtipousuarioevaing"];
								
								//LLAMADA A PROCEDIMIENTO PARA INSERTAR REGISTRO
								$tablaing = "tipousuarioeva";
								$valoresing = "'".$codtipousuarioevaAuxing."', '".$nomtipousuarioevaAuxing."'";
								insertaregistro($tablaing, $valoresing);
								
								//VISUALIZA REGISTRO INSERTADO
								despliegatipousuarioeva($codtipousuarioevaAuxing);
								?>
								<!-- FIN VISUALIZA REGISTRO -->
								
								</br>
								<table>
									<tr>
										<td valign="top" align='left' width='590'>
											<form action="ingtipousuarioeva.php" method="get">
												<input type="submit" value="Ingresar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							else
							{
								mensaje("Debe llenar TODOS LOS CAMPOS!!!");
								$_REQUEST["codtipousuarioevaAuxR"] = $_REQUEST["codtipousuarioevaing"];
								
								//PRESENTACION DE FORMULARIO PARA INGRESAR CON DATOS PREEXISTENTES
					
								?>
								<form action="ingtipousuarioeva.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
									</br>
									<table border="0">
										<tr><th align="left">C&oacute;digo de Tipo</th>	<th>:</th>	<td><input type="text" name="codtipousuarioevaing" value="<?=$_REQUEST['codtipousuarioevaAuxR']?>" readonly></td></tr>
										<tr><th align="left">Nombre de Tipo</th>	<th>:</th>	<td><input type="text" name="nomtipousuarioevaing" value="<?=$nomtipousuarioevaAgain?>" ></td></tr>
									</table>
									</br>
									<table>
										<tr>
											<td valign="top">
												<input type="submit" value="Ingresar datos">
											</td>
										</tr>
									</table>
								</form>
								<?php
							}
						}
						else
						{
						
							//PRESENTACION DE FORMULARIO PARA INGRESAR
							
							$_REQUEST["codtipousuarioevaing"] = "";
							$_REQUEST["nomtipousuarioevaing"] = "";

							?>
							<form action="ingtipousuarioeva.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
								</br>
								<table border="0">
									<tr><th align="left">C&oacute;digo de Tipo</th>	<th>:</th>	<td><input type="text" name="codtipousuarioevaing"></td></tr>
									<tr><th align="left">Nombre de Tipo</th>	<th>:</th>	<td><input type="text" name="nomtipousuarioevaing"></td></tr>
								</table>
								</br>
								<table>
									<tr>
										<td valign="top">
											<input type="submit" value="Ingresar datos">
										</td>
									</tr>
								</table>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Usuarios
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE USUARIOS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>