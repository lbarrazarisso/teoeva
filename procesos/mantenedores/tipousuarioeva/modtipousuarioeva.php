<?php 
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Tipos de Usuario
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
					
				<?php
					cabezal("MODIFICAR TIPOS DE USUARIO");
					
						$webserver = nomserverweb();
						
						?>
						<font size="4"><b>Modificar Usuario</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					
						$swconsulta = 0;
						
						//PROCESO MODIFICAR
					
						if ((isset($_REQUEST["codtipousuarioevamod"])) and (isset($_REQUEST["nomtipousuarioevamod"])))
						{
							
							$swmod = 0;
							
							if ($_REQUEST["codtipousuarioevamod"] == "")
							{
								$swmod = 1;
							}
							
							if ($_REQUEST["nomtipousuarioevamod"] == "")
							{
								$swmod = 2;
							}
							
							if ($swmod == 0)
							{
								
								// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
								$codtipousuarioevaAuxMod2 = $_REQUEST["codtipousuarioevamod"];
								
								// LLAMADO A FUNCION DE CONSULTA
								$resultqMod2 = consultatodo("tipousuarioeva", "codtipousuarioeva", $codtipousuarioevaAuxMod2);
								
								// FORMATEO DE LOS RESULTADOS
								$regtipousuarioevaMod2 = mysqli_fetch_assoc($resultqMod2);
								
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								
								$codtipousuarioevaAuxMod3 = $regtipousuarioevaMod2["codtipousuarioeva"];
								$nomtipousuarioevaAuxMod2 = $regtipousuarioevaMod2["nomtipousuarioeva"];
								
								$sw = "0";
								
								// PREGUNTA SI CAMPO NOMBRE DE TIPO REQUISITO HA SIDO MODIFICADO
								if ($_REQUEST["nomtipousuarioevamod"] != $nomtipousuarioevaAuxMod2)
								{
									modificaregistro("tipousuarioeva", "nomtipousuarioeva", $_REQUEST["nomtipousuarioevamod"], "codtipousuarioeva", $codtipousuarioevaAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI HUBIERON MODIFICACIONES
								if ($sw == "0")
								{
									mensaje("No se hicieron modificaciones al registro.");											
									?>
									</br>
									<table border = "0">
										<tr><th align="left">C&oacute;digo de Tipo</th>	<th>:</th>	<td><?php echo $codtipousuarioevaAuxMod3;?></td></tr>
										<tr><th align="left">Nombre de Tipo</th>	<th>:</th>	<td><?php echo $nomtipousuarioevaAuxMod2;?></td></tr>
									</table>
									</br>
									<table>
										<tr>
											<td valign="top">
												<form action="modtipousuarioeva.php" method="get">
													<input type="submit" value="Modificar otro registro">
												</form>
											</td>
											<td>
												<form action="modtipousuarioeva.php" method="post">
													<input type="hidden" name="accion" value="mod">
													<input type="hidden" name="codtipousuarioevaAction" value="<?=$codtipousuarioevaAuxMod3?>">
													<input type="submit" value="Modificar datos">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
								else
								{
									despliegatipousuarioeva($codtipousuarioevaAuxMod3);
									?>
									</br>
									<?php
									mensaje("Datos modificados correctamente.");
									?>
									<table>
										<tr>
											<td valign="top">
												<form action="modtipousuarioeva.php" method="get">
													<input type="submit" value="Consultar otro registro">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
							}
							else
							{
								mensaje("Debe llenar TODOS LOS CAMPOS!!!");
								$_REQUEST["accion"] = "mod";
								$_REQUEST["codtipousuarioevaAction"] = $_REQUEST["codtipousuarioevamod"];
							}
							$swconsulta = 1;
						}
						
						//FIN PROCESO MODIFICAR
						
						//PRESENTACION DE FORMULARIO PARA MODIFICAR
						
						if ((isset($_REQUEST["accion"])) and ($_REQUEST["accion"] == "mod"))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$codtipousuarioevaMod2 = $_REQUEST["codtipousuarioevaAction"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tablaMod = "tipousuarioeva";
							$campoMod = "codtipousuarioeva";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultaMod = consultatodo($tablaMod, $campoMod, $codtipousuarioevaMod2);
							
							// FORMATEO DE LOS RESULTADOS
							$regtipousuarioevaMod = mysqli_fetch_assoc($punteroconsultaMod);
							
							// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
							
							$codtipousuarioevaMod = $regtipousuarioevaMod["codtipousuarioeva"];
							$nomtipousuarioevaMod = $regtipousuarioevaMod["nomtipousuarioeva"];
						
							//PRESENTACION DEL FORMULARIO
							?>
							</br>
							<form action="modtipousuarioeva.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
								<table border = "0">
									<tr><th align="left">C&oacute;digo de Tipo</th>	<th>:</th>	<td><input type="text" name="codtipousuarioevamod" value="<?=$codtipousuarioevaMod?>" readonly></td></tr>
									<tr><th align="left">Nombre de Tipo</th>	<th>:</th>	<td><input type="text" name="nomtipousuarioevamod" value="<?=$nomtipousuarioevaMod?>"></td></tr>
								</table>
								</br>
								<table>
									<tr><td width='295' align='left'><input type="submit" value="Modificar"></td><td width='295' align='right'><button><a style='text-decoration:none; color:black'href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/tipousuarioeva/modtipousuarioeva.php'>Modificar otro registro</a></button></td></tr>
								</table>
							</form>
							<?php
							$swconsulta = 1;
						}
						
						//FIN FORMULARIO PARA MODIFICAR
						
						// RESULTADO CONSULTA
						
						if ((isset($_REQUEST["codtipousuarioevaconsulta"])) and ($_REQUEST["codtipousuarioevaconsulta"] != ""))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$codtipousuarioevaConsultaAux = $_REQUEST["codtipousuarioevaconsulta"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tabla = "tipousuarioeva";
							$campo = "codtipousuarioeva";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultacon = consultatodo($tabla, $campo, $codtipousuarioevaConsultaAux);
							
							// FORMATEO DE LOS RESULTADOS
							$regtipousuarioevacon = mysqli_fetch_assoc($punteroconsultacon);
								
							if ($regtipousuarioevacon["codtipousuarioeva"] == "")
							{
								mensaje("Tipo de usuarioeva inexistente");
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="modtipousuarioeva.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							else
							{
								$codtipousuarioevaConsultaAux2 = $regtipousuarioevacon["codtipousuarioeva"];
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								despliegatipousuarioeva($codtipousuarioevaConsultaAux2);
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="modtipousuarioeva.php" method="post">
												<input type="hidden" name="accion" value="mod">
												<input type="hidden" name="codtipousuarioevaAction" value="<?=$codtipousuarioevaConsultaAux2?>">
												<input type="submit" value="Modificar datos">
											</form>
										</td>
										<td>
											<form action="modtipousuarioeva.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							$swconsulta = 1;
						}
						
						// FIN RESULTADO CONSULTA

						// FORMULARIO BUSQUEDA REGISTRO
						
						if ($swconsulta == 0)
						{
							?>
							<form action="modtipousuarioeva.php" method="get">
								</br>
								<table border="0">
									<tr><th>Ingrese Codigo de Tipo de Usuario</th>	<th>:</th>	<td><input type="text" name="codtipousuarioevaconsulta"></td><td><input type="submit" value="Buscar"></td></tr>
								</table>
								</br>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Usuarios
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE USUARIOS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>