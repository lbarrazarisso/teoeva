<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Competencias
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
					
				<?php
					cabezal("MODIFICAR COMPETENCIA");
					
						$webserver = nomserverweb();
						
						?>
						<font size="4"><b>Modificar Competencia</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					
						$swconsulta = 0;
						
						//PROCESO MODIFICAR
					
						if ((isset($_REQUEST["codcompetenciamod"])) and (isset($_REQUEST["desccompetenciamod"])) and (isset($_REQUEST["codtipocompetenciamod"])))
						{
							
							$swmod = 0;
							
							if ($_REQUEST["codcompetenciamod"] == "")
							{
								$swmod = 1;
							}
							
							//if ($_REQUEST["nomcompetenciamod"] == "")
							//{
							//	$swmod = 2;
							//}
							
							if ($_REQUEST["desccompetenciamod"] == "")
							{
								$swmod = 3;
							}
							
							if ($_REQUEST["codtipocompetenciamod"] == "")
							{
								$swmod = 4;
							}
							
							if ($swmod == 0)
							{
								
								// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
								$codcompetenciaAuxMod2 = $_REQUEST["codcompetenciamod"];
								
								// LLAMADO A FUNCION DE CONSULTA
								$resultqMod2 = consultatodo("competencia", "codcompetencia", $codcompetenciaAuxMod2);
								
								// FORMATEO DE LOS RESULTADOS
								$regcompetenciaMod2 = mysqli_fetch_assoc($resultqMod2);
								
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								
								$codcompetenciaAuxMod3 = $regcompetenciaMod2["codcompetencia"];
								//$nomcompetenciaAuxMod2 = $regcompetenciaMod2["nomcompetencia"];
								$desccompetenciaAuxMod2 = $regcompetenciaMod2["desccompetencia"];
								
								$codtipocompetenciaAuxMod2 = $regcompetenciaMod2["codtipocompetencia"];
								$resultqtipocompetenciaAuxMod2 = consultatodo("tipocompetencia", "codtipocompetencia", $codtipocompetenciaAuxMod2);
								$regtipocompetenciaAuxMod2 = mysqli_fetch_assoc($resultqtipocompetenciaAuxMod2);
								$nomtipocompetenciaAuxMod2 = $regtipocompetenciaAuxMod2["nomtipocompetencia"];
								
								$sw = "0";
								
								// PREGUNTA SI CAMPO NOMBRE DE COMPETENCIA HA SIDO MODIFICADO
								//if ($_REQUEST["nomcompetenciamod"] != $nomcompetenciaAuxMod2)
								//{
								//	modificaregistro("competencia", "nomcompetencia", $_REQUEST["nomcompetenciamod"], "codcompetencia", $codcompetenciaAuxMod3);
								//	$sw = "1";
								//}
								
								// PREGUNTA SI CAMPO DESCRIPCION HA SIDO MODIFICADO
								if ($_REQUEST["desccompetenciamod"] != $desccompetenciaAuxMod2)
								{
									modificaregistro("competencia", "desccompetencia", $_REQUEST["desccompetenciamod"], "codcompetencia", $codcompetenciaAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI CAMPO TIPO COMPETENCIA HA SIDO MODIFICADO
								if ($_REQUEST["codtipocompetenciamod"] != $codtipocompetenciaAuxMod2)
								{
									modificaregistro("competencia", "codtipocompetencia", $_REQUEST["codtipocompetenciamod"], "codcompetencia", $codcompetenciaAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI HUBIERON MODIFICACIONES
								if ($sw == "0")
								{
									mensaje("No se hicieron modificaciones al registro.");											
									?>
									<table border = "0">
										<tr><th align="left">C&oacute;digo de Competencia</th>	<th>:</th>	<td><?php echo $codcompetenciaAuxMod3;?></td></tr>
										<!--<tr><th align="left">Nombre de Competencia</th>	<th>:</th>	<td><?php echo $nomcompetenciaAuxMod2;?></td></tr>-->
										<tr><th align="left">Descripci&oacute;n</th>			<th>:</th>	<td><?php echo $desccompetenciaAuxMod2;?></td></tr>
										<tr><th align="left">Tipo de Competencia</th>		<th>:</th>	<td><?php echo $nomtipocompetenciaAuxMod2;?></td></tr>
									</table>
									</br>
									<table>
										<tr>
											<td valign="top">
												<form action="modcompetencia.php" method="get">
													<input type="submit" value="Modificar otro registro">
												</form>
											</td>
											<td>
												<form action="modcompetencia.php" method="post">
													<input type="hidden" name="accion" value="mod">
													<input type="hidden" name="codcompetenciaAction" value="<?=$codcompetenciaAuxMod3?>">
													<input type="submit" value="Modificar datos">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
								else
								{
									echo "</br>";
									despliegacompetencia($codcompetenciaAuxMod3);
									?>
									</br>
									<?php
									mensaje("Datos modificados correctamente.");
									?>
									<table>
										<tr>
											<td valign="top" align='left' width='590'>
												<form action="modcompetencia.php" method="get">
													<input type="submit" value="Consultar otro registro">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
							}
							else
							{
								mensaje("Debe llenar TODOS LOS CAMPOS!!!");
								$_REQUEST["accion"] = "mod";
								$_REQUEST["codcompetenciaAction"] = $_REQUEST["codcompetenciamod"];
							}
							$swconsulta = 1;
						}
						
						//FIN PROCESO MODIFICAR
						
						//PRESENTACION DE FORMULARIO PARA MODIFICAR
						
						if ((isset($_REQUEST["accion"])) and ($_REQUEST["accion"] == "mod"))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$codcompetenciaMod2 = $_REQUEST["codcompetenciaAction"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tablaMod = "competencia";
							$campoMod = "codcompetencia";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultaMod = consultatodo($tablaMod, $campoMod, $codcompetenciaMod2);
							
							// FORMATEO DE LOS RESULTADOS
							$regcompetenciaMod = mysqli_fetch_assoc($punteroconsultaMod);
							
							// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
							
							$codcompetenciaMod = $regcompetenciaMod["codcompetencia"];
							//$nomcompetenciaMod = $regcompetenciaMod["nomcompetencia"];
							$desccompetenciaMod = $regcompetenciaMod["desccompetencia"];
							$codtipocompetenciaMod = $regcompetenciaMod["codtipocompetencia"];
						
							//PRESENTACION DEL FORMULARIO
							?>
							</br>
							<form action="modcompetencia.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
								<table border = "0">
									<tr><th align="left">C&oacute;digo de Competencia</th>	<th>:</th>	<td><input type="text" name="codcompetenciamod" value="<?=$codcompetenciaMod?>" readonly></td></tr>
									<!--<tr><th align="left">Nombre de Competencia</th>	<th>:</th>	<td><input type="text" name="nomcompetenciamod" value="<?=$nomcompetenciaMod?>"></td></tr>-->
									<tr><th align="left">Descripci&oacute;n</th>			<th>:</th>	<td><textarea type="text" name="desccompetenciamod" cols="30" rows="10"><?=$desccompetenciaMod?></textarea></td></tr>
									<tr>
										<th align="left">
											Tipo de Competencia
										</th>
										<th>
											:
										</th> 	
										<td>
											<select name="codtipocompetenciamod">
												<?php
													$resultqtipocompetencia = llenacombo("tipocompetencia");
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													while ($regtipocompetencia = mysqli_fetch_assoc($resultqtipocompetencia))
													{
														if ($regtipocompetencia["codtipocompetencia"] == $codtipocompetenciaMod)
														{
															echo "<option value='".$regtipocompetencia["codtipocompetencia"]."' selected>";
															echo $regtipocompetencia["nomtipocompetencia"];
															echo "</option>";																	
														}
														else
														{
															echo "<option value='".$regtipocompetencia["codtipocompetencia"]."'>";
															echo $regtipocompetencia["nomtipocompetencia"];
															echo "</option>";
														}
													}
												?>
											</select>
										</td>
									</tr>									
								</table>
								</br>
								<table>
									<tr><td width='295' align='left'><input type="submit" value="Modificar"></td><td width='295' align='right'><button><a style='text-decoration:none; color:black'href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/competencia/modcompetencia.php'>Modificar otro registro</a></button></td></tr>
								</table>
							</form>
							<?php
							$swconsulta = 1;
						}
						
						//FIN FORMULARIO PARA MODIFICAR
						
						// RESULTADO CONSULTA
						
						if ((isset($_REQUEST["codcompetenciaconsulta"])) and ($_REQUEST["codcompetenciaconsulta"] != ""))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$codcompetenciaConsultaAux = $_REQUEST["codcompetenciaconsulta"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tabla = "competencia";
							$campo = "codcompetencia";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultacon = consultatodo($tabla, $campo, $codcompetenciaConsultaAux);
							
							// FORMATEO DE LOS RESULTADOS
							$regcompetenciacon = mysqli_fetch_assoc($punteroconsultacon);
								
							if ($regcompetenciacon["codcompetencia"] == "")
							{
								mensaje("Competencia inexistente");
								?>
								<table>
									<tr>
										<td>
											<form action="modcompetencia.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							else
							{
								$codcompetenciaConsultaAux2 = $regcompetenciacon["codcompetencia"];
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								despliegacompetencia($codcompetenciaConsultaAux2);
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="modcompetencia.php" method="post">
												<input type="hidden" name="accion" value="mod">
												<input type="hidden" name="codcompetenciaAction" value="<?=$codcompetenciaConsultaAux2?>">
												<input type="submit" value="Modificar datos">
											</form>
										</td>
										<td>
											<form action="modcompetencia.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<hr />
								<?php
							}
							$swconsulta = 1;
						}
						
						// FIN RESULTADO CONSULTA

						// FORMULARIO BUSQUEDA REGISTRO
						
						if ($swconsulta == 0)
						{
							?>
							<form action="modcompetencia.php" method="get">
								</br>
								<table border="0">
									<tr><th>Ingrese C&oacute;digo de Competencia</th>	<th>:</th>	<td><input type="text" name="codcompetenciaconsulta"></td><td><input type="submit" value="Buscar"></td></tr>
								</table>
								</br>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Competencias
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE COMPETENCIAS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>