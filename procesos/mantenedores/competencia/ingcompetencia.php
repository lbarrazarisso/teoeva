<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					eva - Competencias
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
				<?php
					cabezal("MANTENEDOR DE COMPETENCIAS");
					
						$webserver = nomserverweb();
						$agnoactual = date("Y");
						echo "<font size='4'><b>Ingresar Competencia ".$agnoactual."</b></font>";
						?>
						<!-- <font size="4"><b>Ingresar Competencia</b></font> -->
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					
						// PROCEDIMIENTO PARA INSERTAR REGISTRO
						if ((isset($_REQUEST["desccompetenciaing"])) and (isset($_REQUEST["codtipocompetenciaing"])))
						{
							// ASIGNACION DE DATOS INGRESADOS Y VALIDADOS A VARIABLES AUXILIARES
							// $nomcompetenciaAgain = $_REQUEST["nomcompetenciaing"];
							$desccompetenciaAgain = $_REQUEST["desccompetenciaing"];
							$codtipocompetenciaAgain = $_REQUEST["codtipocompetenciaing"];
							
							$swing = 0;
							
							//if ($_REQUEST["nomcompetenciaing"] == "")
							//{
							//	$swing = 1;
							//}
							
							if ($_REQUEST["desccompetenciaing"] == "")
							{
								$swing = 2;
							}
							
							if ($_REQUEST["codtipocompetenciaing"] == "")
							{
								$swing = 3;
							}
							
							if ($swing == 0)
							{
							
								// ASIGNACION DE DATOS INGRESADOS Y VALIDADOS A VARIABLES AUXILIARES
								
								//$nomcompetenciaAuxing = $_REQUEST["nomcompetenciaing"];
								$desccompetenciaAuxing = $_REQUEST["desccompetenciaing"];
								$agnocompetenciaAuxing = $agnoactual;
								$codtipocompetenciaAuxing = $_REQUEST["codtipocompetenciaing"];
								
								//LLAMADA A PROCEDIMIENTO PARA INSERTAR REGISTRO
								$tablaing = "competencia";
								$valoresing = "'', '".$desccompetenciaAuxing."', '".$agnocompetenciaAuxing."', '".$codtipocompetenciaAuxing."'";
								insertaregistro($tablaing, $valoresing);
								
								//CONSULTA A BASE PARA OBTENER CODIGO DE COMPETENCIA
								$resultqdespliegacompetencia = consultatodo($tablaing, "nomcompetencia", $nomcompetenciaAuxing);
								$regdespliegacompetencia = mysqli_fetch_assoc($resultqdespliegacompetencia);
								$codcompetenciaAuxing = $regdespliegacompetencia["codcompetencia"];
								
								//VISUALIZA REGISTRO INSERTADO
								despliegacompetencia($codcompetenciaAuxing);
								?>
								<!-- FIN VISUALIZA REGISTRO -->
								
								</br>
								<table>
									<tr>
										<td valign="top" align='left' width='590'>
											<form action="ingcompetencia.php" method="get">
												<input type="submit" value="Ingresar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							else
							{
								echo "<script type='text/javascript'>alert('Debe llenar TODOS LOS CAMPOS!!!');</script>";
								
								//PRESENTACION DE FORMULARIO PARA INGRESAR CON DATOS PREEXISTENTES
					
								?>
								<form action="ingcompetencia.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
									</br>
									<table border="0">
										<!--<tr><th align="left">Nombre de Competencia</th>	<th>:</th>	<td><input type="text" name="nomcompetenciaing" value="<?=$nomcompetenciaAgain?>" style='width:300'></td></tr>-->
										<tr><th align="left">Descripci&oacute;n</th>					<th>:</th>	<td><textarea type="text" name="desccompetenciaing" cols="30" rows="10" style='width:300'><?=$desccompetenciaAgain?></textarea></td></tr>
										<tr>
											<th align="left">
												Tipo de Competencia
											</th>
											<th>
												:
											</th> 	
											<td>
												<select name="codtipocompetenciaing" style='width:300'>
													<?php
														$resultqtipocompetencia = llenacombo("tipocompetencia");
														echo "<option value=''>";
														echo "- Seleccione";
														echo "</option>";
														while ($regtipocompetencia = mysqli_fetch_assoc($resultqtipocompetencia))
														{
															if ($regtipocompetencia["codtipocompetencia"] == $codtipocompetenciaAgain)
															{
																echo "<option value='".$regtipocompetencia["codtipocompetencia"]."' selected>";
																echo $regtipocompetencia["nomtipocompetencia"];
																echo "</option>";																	
															}
															else
															{
																echo "<option value='".$regtipocompetencia["codtipocompetencia"]."'>";
																echo $regtipocompetencia["nomtipocompetencia"];
																echo "</option>";
															}
														}
													?>
												</select>
											</td>
										</tr>
									</table>
									</br>
									<table>
										<tr>
											<td valign="top">
												<input type="submit" value="Ingresar datos">
											</td>
										</tr>
									</table>
								</form>
								<?php
							}
						}
						else
						{
						
							//PRESENTACION DE FORMULARIO PARA INGRESAR
							
							//$_REQUEST["nomcompetenciaing"] = "";
							$_REQUEST["desccompetenciaing"] = "";
							$_REQUEST["codtipocompetenciaing"] = "";

							?>
							<form action="ingcompetencia.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
								</br>
								<table border="0">
									<!--<tr><th align="left">Nombre de Competencia</th>	<th>:</th>	<td><input type="text" name="nomcompetenciaing" style='width:300'></td></tr>-->
									<tr><th align="left">Descripci&oacute;n</th>				<th>:</th>	<td><textarea type="text" name="desccompetenciaing" cols="30" rows="10" style='width:300'></textarea></td></tr>
									<tr>
										<th align="left">
											Tipo de Competencia
										</th>
										<th>
											:
										</th> 	
										<td>
											<select name="codtipocompetenciaing" style='width:300'>
												<?php
													$resultqtipocompetencia = llenacombo("tipocompetencia");
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													while ($regtipocompetencia = mysqli_fetch_assoc($resultqtipocompetencia))
													{
														echo "<option value='".$regtipocompetencia["codtipocompetencia"]."'>";
														echo $regtipocompetencia["nomtipocompetencia"];
														echo "</option>";
													}
												?>
											</select>
										</td>
									</tr>
								</table>
								</br>
								<table>
									<tr>
										<td valign="top">
											<input type="submit" value="Ingresar datos">
										</td>
									</tr>
								</table>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Competencias
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE COMPETENCIAS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>