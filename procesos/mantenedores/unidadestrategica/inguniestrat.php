<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Unidades Estrat&eacute;gicas
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
				<?php
					cabezal("");
						
						$webserver = nomserverweb();
						?>
						<font size="4"><b>Ingresar Unidad Estrat&eacute;gica</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
						// PROCEDIMIENTO PARA INSERTAR REGISTRO
						if (isset($_REQUEST["nomuniestrating"]))
						{
							// ASIGNACION DE DATOS INGRESADOS Y VALIDADOS A VARIABLES AUXILIARES
							$nomuniestratAgain = $_REQUEST["nomuniestrating"];
							
							$swing = 0;
							
							if ($_REQUEST["nomuniestrating"] == "")
							{
								$swing = 1;
							}
							
							if ($swing == 0)
							{
							
								// ASIGNACION DE DATOS INGRESADOS Y VALIDADOS A VARIABLES AUXILIARES
								
								$nomuniestratAuxing = $_REQUEST["nomuniestrating"];
								
								//LLAMADA A PROCEDIMIENTO PARA INSERTAR REGISTRO
								$tablaing = "uniestrat";
								$valoresing = "'', '".$nomuniestratAuxing."'";
								insertaregistro($tablaing, $valoresing);
								
								//CONSULTA A BASE PARA OBTENER CODIGO DE FAMILIA DE CARGO
								$resultqdespliegauniestrat = consultatodo($tablaing, "nomuniestrat", $nomuniestratAuxing);
								$regdespliegauniestrat = mysqli_fetch_assoc($resultqdespliegauniestrat);
								$coduniestratAuxing = $regdespliegauniestrat["coduniestrat"];
								
								//VISUALIZA REGISTRO INSERTADO
								despliegauniestrat($coduniestratAuxing);
								?>
								<!-- FIN VISUALIZA REGISTRO -->
								</br>
								<table>
									<tr>
										<td valign="top" align='left' width='590'>
											<form action="inguniestrat.php" method="get">
												<input type="submit" value="Ingresar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							else
							{
								mensaje("Debe llenar TODOS LOS CAMPOS!!!");
								
								//PRESENTACION DE FORMULARIO PARA INGRESAR CON DATOS PREEXISTENTES
					
								?>
								<form action="inguniestrat.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
									</br>
									<table border="0">
										<tr><th align="left">Nombre Unidad Estrat&eacute;gica</th>		<th>:</th>	<td><input type="text" name="nomuniestrating" value="<?=$nomuniestratAgain?>" ></td><td valign="top"><input type="submit" value="Ingresar datos"></td></tr>
									</table>
									</br>
								</form>
								<?php
							}
						}
						else
						{
						
							//PRESENTACION DE FORMULARIO PARA INGRESAR
							
							$_REQUEST["nomuniestrating"] = "";

							?>
							<form action="inguniestrat.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
								</br>
								<table border="0">
									<tr><th align="left">Nombre Unidad Estrat&eacute;gica</th>	<th>:</th>	<td><input type="text" name="nomuniestrating"></td><td valign="top"><input type="submit" value="Ingresar datos"></td></tr>
								</table>
								</br>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						EVA - Unidades Estrat&eacute;gicas
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE UNIDADES ESTRATEGICAS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>