<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Unidades Estrat&eacute;gicas
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
					
				<?php
					cabezal("MODIFICAR UNIDADES ESTRATEGICAS");
					
						$webserver = nomserverweb();
						
						?>
						<font size="4"><b>Modificar Unidad Estrat&eacute;gica</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					
						$swconsulta = 0;
						
						//PROCESO MODIFICAR
					
						if ((isset($_REQUEST["coduniestratmod"])) and (isset($_REQUEST["nomuniestratmod"])))
						{
							
							$swmod = 0;
							
							if ($_REQUEST["coduniestratmod"] == "")
							{
								$swmod = 1;
							}
							
							if ($_REQUEST["nomuniestratmod"] == "")
							{
								$swmod = 2;
							}
							
							if ($swmod == 0)
							{
								
								// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
								$coduniestratAuxMod2 = $_REQUEST["coduniestratmod"];
								
								// LLAMADO A FUNCI&Oacute;N DE CONSULTA
								$resultqMod2 = consultatodo("uniestrat", "coduniestrat", $coduniestratAuxMod2);
								
								// FORMATEO DE LOS RESULTADOS
								$reguniestratMod2 = mysqli_fetch_assoc($resultqMod2);
								
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								
								$coduniestratAuxMod3 = $reguniestratMod2["coduniestrat"];
								$nomuniestratAuxMod2 = $reguniestratMod2["nomuniestrat"];
								
								$sw = "0";
								
								// PREGUNTA SI CAMPO NOMBRE DE FAMILIA HA SIDO MODIFICADO
								if ($_REQUEST["nomuniestratmod"] != $nomuniestratAuxMod2)
								{
									modificaregistro("uniestrat", "nomuniestrat", $_REQUEST["nomuniestratmod"], "coduniestrat", $coduniestratAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI HUBIERON MODIFICACIONES
								if ($sw == "0")
								{
									mensaje("No se hicieron modificaciones al registro.");											
									?>
									
									<table border = "0">
										<tr><th align="left">C&oacute;digo Unidad Estrat&eacute;gica</th>	<th>:</th>	<td><?php echo $coduniestratAuxMod3;?></td></tr>
										<tr><th align="left">Nombre Unidad Estrat&eacute;gica</th>	<th>:</th>	<td><?php echo $nomuniestratAuxMod2;?></td></tr>
									</table>
									</br>
									<table>
										<tr>
											<td valign="top">
												<form action="moduniestrat.php" method="get">
													<input type="submit" value="Modificar otro registro">
												</form>
											</td>
											<td>
												<form action="moduniestrat.php" method="post">
													<input type="hidden" name="accion" value="mod">
													<input type="hidden" name="coduniestratAction" value="<?=$coduniestratAuxMod3?>">
													<input type="submit" value="Modificar datos">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
								else
								{
									despliegauniestrat($coduniestratAuxMod3);
									?>
									</br>
									<?php
									mensaje("Datos modificados correctamente.");
									?>
									<table>
										<tr>
											<td valign="top" align='left' width='590'>
												<form action="moduniestrat.php" method="get">
													<input type="submit" value="Consultar otro registro">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
							}
							else
							{
								mensaje("Debe llenar TODOS LOS CAMPOS!!!");
								$_REQUEST["accion"] = "mod";
								$_REQUEST["coduniestratAction"] = $_REQUEST["coduniestratmod"];
							}
							$swconsulta = 1;
						}
						
						//FIN PROCESO MODIFICAR
						
						//PRESENTACION DE FORMULARIO PARA MODIFICAR
						
						if ((isset($_REQUEST["accion"])) and ($_REQUEST["accion"] == "mod"))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$coduniestratMod2 = $_REQUEST["coduniestratAction"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCI&Oacute;N
							$tablaMod = "uniestrat";
							$campoMod = "coduniestrat";

							// LLAMADA A FUNCI&Oacute;N DE CONSULTA
							$punteroconsultaMod = consultatodo($tablaMod, $campoMod, $coduniestratMod2);
							
							// FORMATEO DE LOS RESULTADOS
							$reguniestratMod = mysqli_fetch_assoc($punteroconsultaMod);
							
							// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
							
							$coduniestratMod = $reguniestratMod["coduniestrat"];
							$nomuniestratMod = $reguniestratMod["nomuniestrat"];
						
							//PRESENTACION DEL FORMULARIO
							?>
							</br>
							<form action="moduniestrat.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
								<table border = "0">
									<tr><th align="left">C&oacute;digo Unidad Estrat&eacute;gica</th>	<th>:</th>	<td><input type="text" name="coduniestratmod" value="<?=$coduniestratMod?>" readonly></td></tr>
									<tr><th align="left">Nombre Unidad Estrat&eacute;gica</th>	<th>:</th>	<td><input type="text" name="nomuniestratmod" value="<?=$nomuniestratMod?>"></td></tr>
								</table>
								</br>
								<table>
									<tr><td width='295' align='left'><input type="submit" value="Modificar"></td><td width='295' align='right'><button><a style='text-decoration:none; color:black'href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/unidadestrategica/moduniestrat.php'>Modificar otro registro</a></button></td></tr>
								</table>
							</form>
							<?php
							$swconsulta = 1;
						}
						
						//FIN FORMULARIO PARA MODIFICAR
						
						// RESULTADO CONSULTA
						
						if ((isset($_REQUEST["coduniestratconsulta"])) and ($_REQUEST["coduniestratconsulta"] != ""))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$coduniestratConsultaAux = $_REQUEST["coduniestratconsulta"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCI&Oacute;N
							$tabla = "uniestrat";
							$campo = "coduniestrat";

							// LLAMADA A FUNCI&Oacute;N DE CONSULTA
							$punteroconsultacon = consultatodo($tabla, $campo, $coduniestratConsultaAux);
							
							// FORMATEO DE LOS RESULTADOS
							$reguniestratcon = mysqli_fetch_assoc($punteroconsultacon);
								
							if ($reguniestratcon["coduniestrat"] == "")
							{
								mensaje("Unidad Estratégica inexistente");
								?>
								</br>
								<table>
									<tr>
										<td valign="top" align='left' width='590'>
											<form action="moduniestrat.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							else
							{
								$coduniestratConsultaAux2 = $reguniestratcon["coduniestrat"];
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								despliegauniestrat($coduniestratConsultaAux2);
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="moduniestrat.php" method="post">
												<input type="hidden" name="accion" value="mod">
												<input type="hidden" name="coduniestratAction" value="<?=$coduniestratConsultaAux2?>">
												<input type="submit" value="Modificar datos">
											</form>
										</td>
										<td>
											<form action="moduniestrat.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							$swconsulta = 1;
						}
						
						// FIN RESULTADO CONSULTA

						// FORMULARIO BUSQUEDA REGISTRO
						
						if ($swconsulta == 0)
						{
							?>
							<form action="moduniestrat.php" method="get">
								</br>
								<table border="0">
									<tr><th>Ingrese C&oacute;digo Unidad Estrat&eacute;gica</th>	<th>:</th>	<td><input type="text" name="coduniestratconsulta"></td><td><input type="submit" value="Buscar"></td></tr>
								</table>
								</br>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
			<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						EVA - Unidades Estrat&eacute;gicas
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE UNIDADES ESTRATEGICAS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>