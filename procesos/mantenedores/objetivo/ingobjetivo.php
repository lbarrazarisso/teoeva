<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Objetivos
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
				<?php
					cabezal("MANTENEDOR DE OBJETIVOS");
					
						$webserver = nomserverweb();
						
						?>
						<font size="4"><b>Ingresar Objetivo</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					
						// PROCEDIMIENTO PARA INSERTAR REGISTRO
						if ((isset($_REQUEST["agnoobjetivoing"])) and	(isset($_REQUEST["descobjetivoing"])) and (isset($_REQUEST["codtipoobjetivoing"])))
						{
							// ASIGNACION DE DATOS INGRESADOS Y VALIDADOS A VARIABLES AUXILIARES
							$agnoobjetivoAgain = $_REQUEST["agnoobjetivoing"];
							$descobjetivoAgain = $_REQUEST["descobjetivoing"];
							$codtipoobjetivoAgain = $_REQUEST["codtipoobjetivoing"];
							
							$swing = 0;
							
							if ($_REQUEST["agnoobjetivoing"] == "")
							{
								$swing = 1;
							}
							
							if ($_REQUEST["descobjetivoing"] == "")
							{
								$swing = 2;
							}
							
							if ($_REQUEST["codtipoobjetivoing"] == "")
							{
								$swing = 3;
							}
							
							if ($swing == 0)
							{
							
								// ASIGNACION DE DATOS INGRESADOS Y VALIDADOS A VARIABLES AUXILIARES
								
								$agnoobjetivoAuxing = $_REQUEST["agnoobjetivoing"];
								$descobjetivoAuxing = $_REQUEST["descobjetivoing"];
								$codtipoobjetivoAuxing = $_REQUEST["codtipoobjetivoing"];
								
								//LLAMADA A PROCEDIMIENTO PARA INSERTAR REGISTRO
								$tablaing = "objetivo";
								$valoresing = "'', '".$descobjetivoAuxing."', '".$agnoobjetivoAuxing."', '".$codtipoobjetivoAuxing."'";
								insertaregistro($tablaing, $valoresing);
								
								//CONSULTA A BASE PARA OBTENER CODIGO DE COMPETENCIA
								$resultqdespliegaobjetivo = consultatodo($tablaing, "descobjetivo", $descobjetivoAuxing);
								$regdespliegaobjetivo = mysqli_fetch_assoc($resultqdespliegaobjetivo);
								$codobjetivoAuxing = $regdespliegaobjetivo["codobjetivo"];
								
								//VISUALIZA REGISTRO INSERTADO
								despliegaobjetivo($codobjetivoAuxing);
								?>
								<!-- FIN VISUALIZA REGISTRO -->
								
								<br />
								<table>
									<tr>
										<td valign="top" align='left' width='590'>
											<form action="ingobjetivo.php" method="get">
												<input type="submit" value="Ingresar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							else
							{
								echo "<script type='text/javascript'>alert('Debe llenar TODOS LOS CAMPOS!!!');</script>";
								
								//PRESENTACION DE FORMULARIO PARA INGRESAR CON DATOS PREEXISTENTES
					
								?>
								<form action="ingobjetivo.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
									</br>
									<table border="0">
										<tr><th align="left">A&ntilde;o</th>	<th>:</th>	<td><input type="text" name="agnoobjetivoing" value="<?=$agnoobjetivoAgain?>" ></td></tr>
										<tr><th align="left">Descripci&oacute;n</th>					<th>:</th>	<td><textarea type="text" name="descobjetivoing" cols="30" rows="10"><?=$descobjetivoAgain?></textarea></td></tr>
										<tr>
											<th align="left">
												Tipo de Objetivo
											</th>
											<th>
												:
											</th> 	
											<td>
												<select name="codtipoobjetivoing">
													<?php
														$resultqtipoobjetivo = llenacombo("tipoobjetivo");
														echo "<option value=''>";
														echo "- Seleccione";
														echo "</option>";
														while ($regtipoobjetivo = mysqli_fetch_assoc($resultqtipoobjetivo))
														{
															if ($regtipoobjetivo["codtipoobjetivo"] == $codtipoobjetivoAgain)
															{
																echo "<option value='".$regtipoobjetivo["codtipoobjetivo"]."' selected>";
																echo $regtipoobjetivo["nomtipoobjetivo"];
																echo "</option>";																	
															}
															else
															{
																echo "<option value='".$regtipoobjetivo["codtipoobjetivo"]."'>";
																echo $regtipoobjetivo["nomtipoobjetivo"];
																echo "</option>";
															}
														}
													?>
												</select>
											</td>
										</tr>
									</table>
									</br>
									<table>
										<tr>
											<td valign="top">
												<input type="submit" value="Ingresar datos">
											</td>
										</tr>
									</table>
								</form>
								<?php
							}
						}
						else
						{
						
							//PRESENTACION DE FORMULARIO PARA INGRESAR
							
							$_REQUEST["agnoobjetivoing"] = "";
							$_REQUEST["descobjetivoing"] = "";
							$_REQUEST["codtipoobjetivoing"] = "";

							?>
							<form action="ingobjetivo.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
								</br>
								<table border="0">
									<tr><th align="left">A&ntilde;o</th>						<th>:</th>	<td><input type="text" name="agnoobjetivoing" value="<?=date("Y")?>" style='width:300'></td></tr>
									<tr><th align="left">Descripci&oacute;n</th>				<th>:</th>	<td><textarea type="text" name="descobjetivoing" cols="30" rows="10" style='width:300'></textarea></td></tr>
									<tr>
										<th align="left">
											Tipo de Objetivo
										</th>
										<th>
											:
										</th> 	
										<td>
											<select name="codtipoobjetivoing" style='width:300'>
												<?php
													$resultqtipoobjetivo = llenacombo("tipoobjetivo");
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													while ($regtipoobjetivo = mysqli_fetch_assoc($resultqtipoobjetivo))
													{
														echo "<option value='".$regtipoobjetivo["codtipoobjetivo"]."'>";
														echo $regtipoobjetivo["nomtipoobjetivo"];
														echo "</option>";
													}
												?>
											</select>
										</td>
									</tr>
								</table>
								</br>
								<table>
									<tr>
										<td valign="top">
											<input type="submit" value="Ingresar datos">
										</td>
									</tr>
								</table>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Objetivos
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE OBJETIVOS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCION</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Menu Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>