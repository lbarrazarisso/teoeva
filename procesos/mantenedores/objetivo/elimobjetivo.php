<?php 
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Eliminar Objetivo
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
				<?php
					cabezal("ELIMINAR OBJETIVO");
					
						$webserver = nomserverweb();
						$agnoactual = date("Y");
						
						?>
						<font size="4"><b>Eliminar Objetivo</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php

						$swconsulta = 0;
						
						if ((isset($_REQUEST["accion"])) and ($_REQUEST["accion"] == "elim"))
						{
							if ((isset($_REQUEST["codobjetivoAction"])) and ($_REQUEST["codobjetivoAction"] != ""))
							{
								// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
								$codobjetivoActionAux = $_REQUEST["codobjetivoAction"];
								eliminaregistro("objetivo", "codobjetivo", $codobjetivoActionAux);
							}
						}
						
						if ((isset($_REQUEST["codobjetivoconsulta"])) and ($_REQUEST["codobjetivoconsulta"] != ""))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$codobjetivoConsultaAux = $_REQUEST["codobjetivoconsulta"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tabla = "objetivo";
							$campo = "codobjetivo";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultacon = consultatodo($tabla, $campo, $codobjetivoConsultaAux);
							
							// FORMATEO DE LOS RESULTADOS
							$regobjetivocon = mysqli_fetch_assoc($punteroconsultacon);
								
							if ($regobjetivocon["codobjetivo"] == "")
							{
								mensaje("Objetivo inexistente");
								?>
								<table>
									<tr>
										<td>
											<form action="elimobjetivo.php" method="get">
												<input type="submit" value="Eliminar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							else
							{
								$codobjetivoConsultaAux2 = $regobjetivocon["codobjetivo"];
								despliegaobjetivo($codobjetivoConsultaAux2);
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="elimobjetivo.php" method="post">
												<input type="hidden" name="accion" value="elim">
												<input type="hidden" name="codobjetivoAction" value="<?=$codobjetivoConsultaAux2?>">
												<input type="submit" value="Eliminar registro">
											</form>
										</td>
										<td>
											<form action="elimobjetivo.php" method="get">
												<input type="submit" value="Eliminar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							$swconsulta = 1;
						}
						
						if ($swconsulta == 0)
						{
							?>
							<form action="elimobjetivo.php" method="get">
								</br>
								<table border="0">
									<tr>
										<th align="left">
											Objetivo General
										</th>
										<th>
											:
										</th>
										<td>
											<select name="codobjetivoconsulta">
												<?php
													$codtipoobjetivoconsulta = 1; //OJO PARAMETRO DURO
													$resultqobjetivoconsulta = consultatodo("objetivo", "codtipoobjetivo", $codtipoobjetivoconsulta);
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													while ($regobjetivoconsulta = mysqli_fetch_assoc($resultqobjetivoconsulta))
													{
														if ($regobjetivoconsulta["agnoobjetivo"] == $agnoactual)
														{
															echo "<option value='".$regobjetivoconsulta["codobjetivo"]."'>";
															echo $regobjetivoconsulta["descobjetivo"];
															echo "</option>";
														}
													}
												?>
											</select>
										</td>
										<td>
											<input type="submit" value="Buscar">
										</td>
									</tr>
								</table>
							</form>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Objetivos
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE OBJETIVOS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCI&Oacute;N</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Men&uacute; Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>