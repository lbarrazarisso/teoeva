<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Objetivos
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
					
				<?php
					cabezal("MODIFICAR OBJETIVO");
					
						$webserver = nomserverweb();
						
						?>
						<font size="4"><b>Modificar Objetivo General, A&ntilde;o en curso</b></font>
						<hr />
						<div id="botonup">
							<table>
								<tr>
									<td width='25' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='../../../images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
									</td>
									<td width='575' align='center' valign='top'>
										
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
						$agnoactual = date("Y");
						$swconsulta = 0;
						
						//PROCESO MODIFICAR
					
						if ((isset($_REQUEST["codobjetivomod"])) and (isset($_REQUEST["agnoobjetivomod"])) and	(isset($_REQUEST["descobjetivomod"])) and (isset($_REQUEST["codtipoobjetivomod"])))
						{
							
							$swmod = 0;
							
							if ($_REQUEST["codobjetivomod"] == "")
							{
								$swmod = 1;
							}
							
							if ($_REQUEST["agnoobjetivomod"] == "")
							{
								$swmod = 2;
							}
							
							if ($_REQUEST["descobjetivomod"] == "")
							{
								$swmod = 3;
							}
							
							if ($_REQUEST["codtipoobjetivomod"] == "")
							{
								$swmod = 4;
							}
							
							if ($swmod == 0)
							{
								
								// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
								$codobjetivoAuxMod2 = $_REQUEST["codobjetivomod"];
								
								// LLAMADO A FUNCION DE CONSULTA
								$resultqMod2 = consultatodo("objetivo", "codobjetivo", $codobjetivoAuxMod2);
								
								// FORMATEO DE LOS RESULTADOS
								$regobjetivoMod2 = mysqli_fetch_assoc($resultqMod2);
								
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								
								$codobjetivoAuxMod3 = $regobjetivoMod2["codobjetivo"];
								$agnoobjetivoAuxMod2 = $regobjetivoMod2["agnoobjetivo"];
								$descobjetivoAuxMod2 = $regobjetivoMod2["descobjetivo"];
								
								$codtipoobjetivoAuxMod2 = $regobjetivoMod2["codtipoobjetivo"];
								$resultqtipoobjetivoAuxMod2 = consultatodo("tipoobjetivo", "codtipoobjetivo", $codtipoobjetivoAuxMod2);
								$regtipoobjetivoAuxMod2 = mysqli_fetch_assoc($resultqtipoobjetivoAuxMod2);
								$nomtipoobjetivoAuxMod2 = $regtipoobjetivoAuxMod2["nomtipoobjetivo"];
								
								$sw = "0";
								
								// PREGUNTA SI CAMPO NOMBRE DE REQUISITO HA SIDO MODIFICADO
								if ($_REQUEST["agnoobjetivomod"] != $agnoobjetivoAuxMod2)
								{
									modificaregistro("objetivo", "agnoobjetivo", $_REQUEST["agnoobjetivomod"], "codobjetivo", $codobjetivoAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI CAMPO DESCRIPCION HA SIDO MODIFICADO
								if ($_REQUEST["descobjetivomod"] != $descobjetivoAuxMod2)
								{
									modificaregistro("objetivo", "descobjetivo", $_REQUEST["descobjetivomod"], "codobjetivo", $codobjetivoAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI CAMPO TIPO REQUISITO HA SIDO MODIFICADO
								if ($_REQUEST["codtipoobjetivomod"] != $codtipoobjetivoAuxMod2)
								{
									modificaregistro("objetivo", "codtipoobjetivo", $_REQUEST["codtipoobjetivomod"], "codobjetivo", $codobjetivoAuxMod3);
									$sw = "1";
								}
								
								// PREGUNTA SI HUBIERON MODIFICACIONES
								if ($sw == "0")
								{
									mensaje("No se hicieron modificaciones al registro.");											
									?>
									<table border = "0">
										<tr><th align="left">C&oacute;digo de Objetivo</th>	<th>:</th>	<td><?php echo $codobjetivoAuxMod3;?></td></tr>
										<tr><th align="left">A&ntilde;o</th>	<th>:</th>	<td><?php echo $agnoobjetivoAuxMod2;?></td></tr>
										<tr><th align="left">Descripci&oacute;n</th>			<th>:</th>	<td><?php echo $descobjetivoAuxMod2;?></td></tr>
										<tr><th align="left">Tipo de Objetivo</th>		<th>:</th>	<td><?php echo $nomtipoobjetivoAuxMod2;?></td></tr>
									</table>
									</br>
									<table>
										<tr>
											<td valign="top">
												<form action="modobjetivo.php" method="get">
													<input type="submit" value="Modificar otro registro">
												</form>
											</td>
											<td>
												<form action="modobjetivo.php" method="post">
													<input type="hidden" name="accion" value="mod">
													<input type="hidden" name="codobjetivoAction" value="<?=$codobjetivoAuxMod3?>">
													<input type="submit" value="Modificar datos">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
								else
								{
									despliegaobjetivo($codobjetivoAuxMod3);
									?>
									</br>
									<?php
									mensaje("Datos modificados correctamente.");
									?>
									<table>
										<tr>
											<td valign="top">
												<form action="modobjetivo.php" method="get">
													<input type="submit" value="Consultar otro registro">
												</form>
											</td>
										</tr>
									</table>
									<?php
								}
							}
							else
							{
								mensaje("Debe llenar TODOS LOS CAMPOS!!!");
								$_REQUEST["accion"] = "mod";
								$_REQUEST["codobjetivoAction"] = $_REQUEST["codobjetivomod"];
							}
							$swconsulta = 1;
						}
						
						//FIN PROCESO MODIFICAR
						
						//PRESENTACION DE FORMULARIO PARA MODIFICAR
						
						if ((isset($_REQUEST["accion"])) and ($_REQUEST["accion"] == "mod"))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$codobjetivoMod2 = $_REQUEST["codobjetivoAction"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tablaMod = "objetivo";
							$campoMod = "codobjetivo";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultaMod = consultatodo($tablaMod, $campoMod, $codobjetivoMod2);
							
							// FORMATEO DE LOS RESULTADOS
							$regobjetivoMod = mysqli_fetch_assoc($punteroconsultaMod);
							
							// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
							
							$codobjetivoMod = $regobjetivoMod["codobjetivo"];
							$agnoobjetivoMod = $regobjetivoMod["agnoobjetivo"];
							$descobjetivoMod = $regobjetivoMod["descobjetivo"];
							$codtipoobjetivoMod = $regobjetivoMod["codtipoobjetivo"];
						
							//PRESENTACION DEL FORMULARIO
							?>
							</br>
							<form action="modobjetivo.php" method="post"> <!--CAMBIAR NOMBRE DE ARCHIVO EN ACTION; CAMBIAR METODO get/post EN METHOD-->
								<table border = "0">
									<tr><th align="left">C&oacute;digo de Objetivo</th>	<th>:</th>	<td><input type="text" name="codobjetivomod" value="<?=$codobjetivoMod?>" readonly></td></tr>
									<tr><th align="left">A&ntilde;o</th>	<th>:</th>	<td><input type="text" name="agnoobjetivomod" value="<?=$agnoobjetivoMod?>"></td></tr>
									<tr><th align="left">Descripci&oacute;n</th>			<th>:</th>	<td><textarea type="text" name="descobjetivomod" cols="30" rows="10"><?=$descobjetivoMod?></textarea></td></tr>
									<tr>
										<th align="left">
											Tipo de Objetivo
										</th>
										<th>
											:
										</th> 	
										<td>
											<select name="codtipoobjetivomod">
												<?php
													$resultqtipoobjetivo = llenacombo("tipoobjetivo");
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													while ($regtipoobjetivo = mysqli_fetch_assoc($resultqtipoobjetivo))
													{
														if ($regtipoobjetivo["codtipoobjetivo"] == $codtipoobjetivoMod)
														{
															echo "<option value='".$regtipoobjetivo["codtipoobjetivo"]."' selected>";
															echo $regtipoobjetivo["nomtipoobjetivo"];
															echo "</option>";																	
														}
														else
														{
															echo "<option value='".$regtipoobjetivo["codtipoobjetivo"]."'>";
															echo $regtipoobjetivo["nomtipoobjetivo"];
															echo "</option>";
														}
													}
												?>
											</select>
										</td>
									</tr>									
								</table>
								</br>
								<table>
									<tr><td width='295' align='left'><input type="submit" value="Modificar"></td><td width='295' align='right'><button><a style='text-decoration:none; color:black'href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/objetivo/modobjetivo.php'>Modificar otro registro</a></button></td></tr>
								</table>
							</form>
							<?php
							$swconsulta = 1;
						}
						
						//FIN FORMULARIO PARA MODIFICAR
						
						// RESULTADO CONSULTA
						
						if ((isset($_REQUEST["codobjetivoconsulta"])) and ($_REQUEST["codobjetivoconsulta"] != ""))
						{
							// ALMACENAMIENTO DE PATRON DE BUSQUEDA EN VARIABLE AUXILIAR
							$codobjetivoConsultaAux = $_REQUEST["codobjetivoconsulta"];

							// DEFINICION DE VARIABLES PARA LLAMADA A FUNCION
							$tabla = "objetivo";
							$campo = "codobjetivo";

							// LLAMADA A FUNCION DE CONSULTA
							$punteroconsultacon = consultatodo($tabla, $campo, $codobjetivoConsultaAux);
							
							// FORMATEO DE LOS RESULTADOS
							$regobjetivocon = mysqli_fetch_assoc($punteroconsultacon);
								
							if ($regobjetivocon["codobjetivo"] == "")
							{
								mensaje("Objetivo inexistente");
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="modobjetivo.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							else
							{
								$codobjetivoConsultaAux2 = $regobjetivocon["codobjetivo"];
								// ALMACENAMIENTO DE DATOS EN VARIABLES AUXILIARES
								despliegaobjetivo($codobjetivoConsultaAux2);
								?>
								</br>
								<table>
									<tr>
										<td>
											<form action="modobjetivo.php" method="post">
												<input type="hidden" name="accion" value="mod">
												<input type="hidden" name="codobjetivoAction" value="<?=$codobjetivoConsultaAux2?>">
												<input type="submit" value="Modificar datos">
											</form>
										</td>
										<td>
											<form action="modobjetivo.php" method="get">
												<input type="submit" value="Consultar otro registro">
											</form>
										</td>
									</tr>
								</table>
								<?php
							}
							$swconsulta = 1;
						}
						
						// FIN RESULTADO CONSULTA

						// FORMULARIO BUSQUEDA REGISTRO
						
						if ($swconsulta == 0)
						{
							?>
							<form action="modobjetivo.php" method="get">
								</br>
								<table border="0">
									<tr>
										<th align="left">
											Objetivo General
										</th>
										<th>
											:
										</th>
										<td>
											<select name="codobjetivoconsulta">
												<?php
													$codtipoobjetivoconsulta = 1; //OJO PARAMETRO DURO
													$resultqobjetivoconsulta = consultatodo("objetivo", "codtipoobjetivo", $codtipoobjetivoconsulta);
													echo "<option value=''>";
													echo "- Seleccione";
													echo "</option>";
													while ($regobjetivoconsulta = mysqli_fetch_assoc($resultqobjetivoconsulta))
													{
														if ($regobjetivoconsulta["agnoobjetivo"] == $agnoactual)
														{
															echo "<option value='".$regobjetivoconsulta["codobjetivo"]."'>";
															echo $regobjetivoconsulta["descobjetivo"];
															echo "</option>";
														}
													}
												?>
											</select>
										</td>
										<td>
											<input type="submit" value="Buscar"></br>
										</td>
									</tr>
								</table>
								</br>
							<?php
						}
						?>
						<hr />
						<div id='botonbottom'>
							<table>
								<tr>
									<td width='568' align='center' valign='center'>
										
									</td>
									<td width='32' align='center' valign='center'>
										<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='../../../images/logout.jpg' width='30' height='30' title='Salir'></a>
									</td>
								</tr>
							</table>
						</div>
						<hr />
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Objetivos
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE OBJETIVOS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCION</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Menu Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>