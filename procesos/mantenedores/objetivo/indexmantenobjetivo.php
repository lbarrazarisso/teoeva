<?php
	include "../../../lib/handWebEva.php";
	//include "../../../lib/handDisplayEva.php";
	include "../../../lib/handDatabaseEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$codtipousuarioevaAux = $regusuarioeva["codtipousuarioeva"];
	
	if ($codtipousuarioevaAux == 1)
	{
		?>
		<html>
			<head>
				<title>
					Eva - Objetivos
				</title>
				<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
				<?php
					cabezal("MANTENEDOR DE OBJETIVOS");
						
						$webserver = nomserverweb();
						
						?>
							<font size="4"><b>Mantenedor de Objetivos</b></font>
							<hr />
							</br>
							<table>
								<tr>
									<td><button><a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/objetivo/ingobjetivo.php'>Ingresar Objetivo</a></button></td>
									<td><button><a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/objetivo/modobjetivo.php'>Modificar Objetivo</a></button></td>
									<td><button><a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/procesos/mantenedores/objetivo/elimobjetivo.php'>Eliminar Objetivo</a></button></td>
								</tr>
							</table>
							</br>
							<hr />
							</br>
							<table>
								<tr>
									<td><button><a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>Menu Principal</a></button></td>
									<td width="400" align="right"><button><a style="text-decoration: none; color: black" href='http://<?php echo $webserver;?>/eva/logout.php'>Salir</a></button></td>
								</tr>
							</table>
						<?php
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		//TAL CUAL
		if ($codtipousuarioevaAux == 2)
		{
			?>
			<html>
				<head>
					<title>
						Eva - Objetivos
					</title>
					<link href="../../../CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
				</head>
				<body>
					<?php
						cabezal("MANTENEDOR DE OBJETIVOS");
							$webserver = nomserverweb();
							?>
								<p><b>UD. NO POSEE ACCESO A ESTA FUNCION</b></p>
								<hr />
								<table>
									<tr>
										<td valign="top">
											<button>
												<a style="text-decoration: none; color:black" href='http://<?php echo $webserver;?>/eva/main.php'>
													Menu Principal
												</a>
											</button>
										</td>
										<td width="400" align="right">
											<form action = "../../../logout.php" method="post">
												<input type="submit" value="Salir" />
											</form>
										</td>
									</tr>
								</table>
							<?php
						pie();
					?>
				</body>
			</html>
			<?php
		}
		else
		{
			mensaje("ERROR: Acceso denegado");
		}
	}
?>