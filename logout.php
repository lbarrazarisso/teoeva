<?php
	include "lib/handWebEva.php";
	include "lib/handDatabaseEva.php";
	
	session_start();
	
	foreach ($_REQUEST as $param => $valor)
	{
		$_REQUEST[$param] = protegevars($valor);
	}
	
	if (isset($_REQUEST["action"]) and ($_REQUEST["action"] == "exit"))
	{
		session_destroy();
		
		?>
		<html>
			<head>
				<title>
					Eva - Logout
				</title>
				<link href="CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
				<?php
					$webserver = nomserverweb();
					cabezallogin("LOGOUT");
					echo "Hasta Luego!";
					pie();
				?>
			</body>
		</html>
		<?php
	}
	else
	{
		?>
		<html>
			<head>
				<title>
					Eva - Logout
				</title>
				<link href="CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
			</head>
			<body>
				<?php
					$webserver = nomserverweb();
					cabezallogin("LOGOUT");
					echo $_SESSION["username"];
					echo "<fieldset style='width:170px'>";
					echo "<p>Seguro que desea salir?</p>";
					echo "<hr />";
					?>
					<p>
					<table>
						<tr>
							<td width="20">
							</td>
							<td align="right">
								<button>
									<a style="text-decoration: none; color:black" href="?action=exit">
										Si
									</a>
								</button>
							</td>
							<td align="left">
								<button>
									<a style="text-decoration: none; color:black" href="http://<?php echo $webserver;?>/eva/main.php">
										No
									</a>
								</button>
							</td>
						</tr>
					</table>
					</p>
					<?php	
					echo "</fieldset>";
					pie();
				?>
			</body>
		</html>
		<?php
	}
?>