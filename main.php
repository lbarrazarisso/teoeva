<?php
	include "lib/handDatabaseEva.php";
	include "lib/handWebEva.php";
	
	//recupera sesión
	session_start();
	$currentuser = $_SESSION["username"];
	
	//consulta tipo de usuario
	$resultqusuarioeva = consultatodo("usuarioeva", "nomusuarioeva", $currentuser);
	$regusuarioeva = mysqli_fetch_assoc($resultqusuarioeva);
	$usuarioevareg = $regusuarioeva["nomusuarioeva"];
	$rutpersonareg = $regusuarioeva["rutusuarioeva"];
	$codtipousuarioeva = $regusuarioeva["codtipousuarioeva"];
	$resultqtipousuarioeva = consultatodo("tipousuarioeva", "codtipousuarioeva", $codtipousuarioeva);
	$regtipousuarioeva = mysqli_fetch_assoc($resultqtipousuarioeva);
	$nomtipousuarioeva = $regtipousuarioeva["nomtipousuarioeva"];
	$resultqpersona = consultatodo("persona", "rutpersona", $rutpersonareg);
	$regpersona = mysqli_fetch_assoc($resultqpersona);
	$nompersona = $regpersona["nompersona"];
	$appaterno = $regpersona["appaterno"];
	$apmaterno = $regpersona["apmaterno"];
?>
<html>
	<head>
		<title>
			Eva - Men&uacute; Principal
		</title>
		<link href="CSS/style.css" media="screen" rel="StyleSheet" type="text/css" />
	</head>
	<body>
		<?php
			$webserver = nomserverweb();
			if ($codtipousuarioeva == 1)
			{
				cabezal("MEN&Uacute; PRINCIPAL");
			}
			
			if ($codtipousuarioeva == 3)
			{
				cabezalevaluador("MEN&Uacute; PRINCIPAL");
			}
			echo "<p><b>MEN&Uacute; PRINCIPAL</b></p>";
			echo "<hr />";
			?>
			<div id="botonup">
				<table>
					<tr>
						<td width='600' align='left' valign='top'>
							<a href='http://<?php echo $webserver;?>/eva/main.php'><img src='images/menuprincipal.jpg' width='30' height='30' title='Men&uacute; Principal'></a>
						</td>
					</tr>
				</table>
			</div>
			<hr />
			<?php
			echo "<p><b>Usuario actual</b></p>";
			echo "<table>";
			echo "<tr>";
			echo "<td>Username</td><td align='center'>:</td><td>".$usuarioevareg."</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>Nombre</td><td align='center'>:</td><td>".$nompersona." ".$appaterno." ".$apmaterno."</td>";
			echo "</tr>";
			echo "<td>Nivel de acceso</td><td align='center'>:</td><td>".$nomtipousuarioeva."</td>";
			echo "</tr>";
			echo "</table>";
			?>
			<hr />
			<div id="botonbottom">
				<table>
					<tr>
						<td width='600' align='right' valign='top'>
							<a href='http://<?php echo $webserver;?>/eva/logout.php'><img src='images/logout.jpg' width='30' height='30' title='Salir'></a>
						</td>
					</tr>
				</table>
			</div>
			<hr />
			<?php
			pie();
		?>
	</body>
</html>